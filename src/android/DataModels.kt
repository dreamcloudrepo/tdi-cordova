/**
 * DataModels.kt
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

package cordova.plugin.tdi

import com.thalesgroup.tdi.Input
import java.io.Serializable

data class PCapture(
    val captures: MutableList<MutableMap<String,String>>
)

data class PProfileCapture(
    val name: String,
    val isSkipAllowed: Boolean,
    val returnRisk: Boolean,
    val idpIdentifier: String?,
    val response: MutableList<String?>?,
    val steps: MutableList<PCapture>
)

data class PField (
    val format: String,
    val isDisplayed: Boolean,
    val mandatory: Boolean,
    val pattern: String,
    val editable: MutableList<String?>?
)

data class PScenario (
    val idx: String,
    val name: String,
    val captures: MutableList<PProfileCapture>,
    val responses: MutableList<PResponsesCategory>?,
    val infoData: MutableMap<String, String?>?
)

data class PResponsesCategory (
    val name: String,
    val fields: MutableList<PResponsesCategoryField>
)

data class PResponsesCategoryField (
    val name: String,
    val field: PField
)

data class PProfileCaptor (
    val pages: MutableList<String>?,
    val metadata: MutableMap<String, MutableMap<String, String?>?>?,
    val isUploadAllowed: Boolean,
    val category: String?
)

data class PProfileConfigs (
    val k1: String?,
    val k1m: String?,
    val k1e: String?,
    val httpTimeoutSecond: String?,
    val httpMaxRetryNum: String?,
    val httpRetryDelaySecond: String?,
    val pushNotifTimeoutSecond: String?,
    val pullMaxRetryNum: String?,
    val lang: String?,
    val sizeLimit: String?,
    val enableRisk: String
)

data class PTdiProfileInfo (
    val version: String,
    val scenarios: MutableList<PScenario>,
    val captors: MutableMap<String, PProfileCaptor>,
    val configs: PProfileConfigs,
    val theme: MutableMap<String, kotlinx.serialization.json.JsonElement>?,
    val localization: MutableMap<String, String?>
)

data class InputImpl(override val pages: MutableMap<String, String>) : Input, Serializable
