/**
 * PLog.kt
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

package cordova.plugin.tdi

import android.util.Log
import applicationId.BuildConfig

internal object PLog {
    private val isDebug: Boolean = BuildConfig.DEBUG

    // Debug
    internal fun d(_tag: String, _msg: String) {
        if (isDebug) {
            Log.d(_tag, _msg)
        }
    }

    // Error
    internal fun e(_tag: String, _msg: String) {
        Log.e(_tag, _msg)
    }

    // Verbose
    internal fun v(_tag: String, _msg: String) {
        if (isDebug) {
            Log.v(_tag, _msg)
        }
    }

    internal fun fullLog(tag: String, content: String) {
        if (content.length > 4000) {
            d(tag, content.substring(0, 4000))
            fullLog(tag, content.substring(4000))
        } else {
            d(tag, content)
        }
    }
}
