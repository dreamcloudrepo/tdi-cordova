/**
 * TdiSdkAgent.kt
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

package cordova.plugin.tdi

import android.content.Context
import com.thalesgroup.tdi.*
import com.google.gson.Gson
import org.apache.cordova.CallbackContext
import org.apache.cordova.PluginResult
import org.json.JSONArray
import org.json.JSONObject

class TdiSdkAgent : (TdiProfileInfo?, Status, MutableList<Task>?, Result?) -> Unit {
    companion object {
        private val LOG_TAG = TdiSdkAgent::class.java.name
        private var instance: TdiSdkAgent? = null

        fun getInstance(): TdiSdkAgent? {
            instance ?: synchronized(this) {
                if (instance == null) {
                    PLog.d(LOG_TAG, "getInstance")
                    instance = TdiSdkAgent()
                }
            }
            return instance
        }
    }

    private var mSession: Session? = null
    var mCallbackCtx: CallbackContext? = null
    private var tdiMobileSdk: TdiMobileSdk? = null
    private var mIsNotifyCaptor: Boolean = false
    private var mCaptureType: String = ""
    private var mIsLicenseApiUsed: Boolean = false
    private var mProfileApiCall: Boolean = false

    fun tdiSdkInit(_context: Context, _idv_license: String) {
        PLog.d(LOG_TAG, "TDI init")
        if (tdiMobileSdk == null) {
            TdiMobileSdk.initialize(_context)
            tdiMobileSdk = TdiMobileSdk.getInstance()
            tdiMobileSdk?.taskStateListener = this
        }
        if (tdiMobileSdk != null) {
            if (_idv_license.isNotEmpty()) {
                tdiMobileSdk?.setLicenses(mutableMapOf("EXTRA_IDV_LICENSE" to _idv_license))
                mIsLicenseApiUsed = true
            }
            PLog.d(LOG_TAG, "TDI init OK")
            mCallbackCtx?.success("TDI init OK")
        } else {
            PLog.d(LOG_TAG, "TDI init failed")
            mCallbackCtx?.error("TDI init failed")
        }
    }

    fun getProfile(
        tdi_server_url: String,
        jwt_token: String,
        tenant_id: String
    ) {
        PLog.d(LOG_TAG, "TDI get profile: url= $tdi_server_url tenant= $tenant_id")
        try {
            if (tdiMobileSdk != null) {
                tdiMobileSdk?.getProfile(tdi_server_url, jwt_token, tenant_id)
                mProfileApiCall = true
            } else {
                mCallbackCtx?.error("get profile: TDI not initialized!")
            }
        } catch (e: Exception) {
            PLog.e(LOG_TAG,"get profile: ${e.message}")
            mCallbackCtx?.error("get profile: ${e.message}")
        }
    }

    fun newSession(
        tdi_server_url: String,
        scenario_name: String,
        jwt_token: String,
        tenant_id: String
    ) {
        PLog.d(LOG_TAG, "TDI new session: url= $tdi_server_url scenario= $scenario_name tenant= $tenant_id")
        try {
            if (tdiMobileSdk != null) {
                mSession = tdiMobileSdk?.newSession(tdi_server_url, scenario_name, jwt_token, tenant_id)
            }

            if (mSession != null) {
                PLog.d(LOG_TAG,"TDI new session OK")
                mCallbackCtx?.success("New session OK")
            } else {
                PLog.d(LOG_TAG,"TDI new session: Invalid session")
                mCallbackCtx?.error("new session: Invalid session")
            }
        } catch (e: Exception) {
            PLog.e(LOG_TAG,"new session: ${e.message}")
            mCallbackCtx?.error("new session: ${e.message}")
        }
    }

    fun sessionStart() {
        PLog.d(LOG_TAG,"TDI session start")
        try {
            if (mSession != null) {
                mSession?.start()
            } else {
                PLog.d(LOG_TAG,"TDI session start: Invalid session")
                mCallbackCtx?.error("Session start: Invalid session")
            }
        } catch (e: Exception) {
            PLog.e(LOG_TAG,"session start: ${e.message}")
            mCallbackCtx?.error("session start: ${e.message}")
        }
    }

    fun sessionResume(_taskId: String) {
        PLog.d(LOG_TAG,"TDI session resume: task id= $_taskId")
        try {
            if (mSession != null) {
                mSession?.resume(_taskId)
            } else {
                PLog.d(LOG_TAG,"TDI session resume: Invalid session")
                mCallbackCtx?.error("Session resume: Invalid session")
            }
        } catch (e: Exception) {
            PLog.e(LOG_TAG,"session resume: ${e.message}")
            mCallbackCtx?.error("session resume: ${e.message}")
        }
    }

    fun sessionResult() {
        PLog.d(LOG_TAG,"TDI session result")
        try {
            if (mSession != null) {
                mSession?.result()
            } else {
                PLog.d(LOG_TAG,"TDI session result: Invalid session")
                mCallbackCtx?.error("Session result: Invalid session")
            }
        } catch (e: Exception) {
            PLog.e(LOG_TAG,"session result: ${e.message}")
            mCallbackCtx?.error("session result: ${e.message}")
        }
    }

    fun sessionUpdate(
        _taskId: String,
        _pages: MutableMap<String,String>
    ) {
        PLog.d(LOG_TAG,"TDI session update: task id= $_taskId  has pages= ${_pages.isNotEmpty()}")
        try {
            val pageData = mutableMapOf<String,String>()

            if (_pages.count() > 0) {
                _pages.forEach { (idx, metadata) ->
                    pageData[idx] = metadata
                }
            }

            if (mSession != null) {
                mSession?.update(_taskId, InputImpl(pageData))
            } else {
                PLog.d(LOG_TAG,"TDI session update: Invalid session")
                mCallbackCtx?.error("Session update: Invalid session")
            }
        } catch (e: Exception) {
            PLog.e(LOG_TAG,"session update: ${e.message}")
            mCallbackCtx?.error("session update: ${e.message}")
        }
    }

    fun sessionStop() {
        PLog.d(LOG_TAG,"TDI session stop")
        try {
            if (mSession != null) {
                mSession?.stop()
                mSession = null
                PLog.d(LOG_TAG,"TDI stop session OK")
                mCallbackCtx?.success("Stop session OK")
            } else {
                PLog.d(LOG_TAG,"TDI session stop: Invalid session")
                mCallbackCtx?.error("Session stop: Invalid session")
            }
        } catch (e: Exception) {
            PLog.e(LOG_TAG,"session stop: ${e.message}")
            mCallbackCtx?.error("session stop: ${e.message}")
        }
    }

    fun getTdiCaptor(_config: MutableMap<String,Any>) {
        if (tdiMobileSdk != null) {
            if (_config.containsKey("CAPTURE_TYPE")) {
                mCaptureType = _config["CAPTURE_TYPE"] as String
                mIsNotifyCaptor = if (_config.containsKey("EXTRA_CAPTOR_CALLBACK")) {
                    _config["EXTRA_CAPTOR_CALLBACK"] as Boolean
                } else {
                    false
                }

                val captorConfig = mutableMapOf<String, Any>()

                if (_config.count() > 0) {
                    _config.forEach { (idx, data) ->
                        if (idx != "EXTRA_CAPTOR_CALLBACK" && idx != "CAPTURE_TYPE") {
                            if (idx == "EXTRA_IDV_LICENSE" && mIsLicenseApiUsed) {
                                return@forEach // continue
                            } else {
                                captorConfig[idx] = data
                            }
                        }
                    }
                }

                if (mIsNotifyCaptor) {
                    captorConfig["EXTRA_CAPTOR_CALLBACK"] to captorCallback
                }

                tdiMobileSdk!!.captorProvider[mCaptureType]?.config = captorConfig
                val captor = tdiMobileSdk!!.captorProvider[mCaptureType]

                if (mIsNotifyCaptor) {
                    captor?.execute(captorCallback)
                }
            }
        }
    }

    fun getSdkVersion(): String {
        return tdiMobileSdk?.getVersion() ?: ""
    }

    private val captorCallback = object : Captor.Callback {
        override fun onDataCaptured(input: Input) {
            PLog.d(LOG_TAG, "$mCaptureType Captor OK")
            val jsonObject = JSONObject()
            input.let {
                val tempJsonObj = JSONObject()
                it.pages.forEach { (idx, data) ->
                    tempJsonObj.put(idx, data)
                }
                jsonObject.accumulate("pages", tempJsonObj)
            }

            mCallbackCtx?.success(jsonObject)
        }
        override fun onFailure(failure: Failure) {
            val failMsg = "$mCaptureType Captor failure. code=${failure.errorCode} msg=${failure.errorMessage}"
            PLog.d(LOG_TAG, failMsg)
            mCallbackCtx?.error(failMsg)
        }
    }

    override fun invoke(profile: TdiProfileInfo?, status: Status, tasks: MutableList<Task>?, result: Result?) {
        val mainJsonObj = JSONObject()

        when (status) {
            Status.CREATED -> {
                PLog.d(LOG_TAG, "Session created")
            }
            Status.ACTION -> {
                PLog.d(LOG_TAG, "Session in action")
            }
            Status.PENDING -> {
                PLog.d(LOG_TAG, "Session pending")
            }
            Status.COMPLETED -> {
                PLog.d(LOG_TAG, "Session completed")
            }
        }
        mainJsonObj.put("status", status.name)

        if (mProfileApiCall) profile?.let { profInfo ->
            PLog.fullLog(LOG_TAG, "Received profile: \n $profile")

            // Scenario
            val newScenario = mutableListOf<PScenario>()
            profInfo.scenarios.let { scenarios ->
                scenarios.forEach { scenario ->

                    // Captures
                    val newCaptures = mutableListOf<PProfileCapture>()
                    scenario.captures.forEach { capture ->
                        val newSteps = mutableListOf<PCapture>()
                        capture.steps.forEach { step ->
                            val tempStep = PCapture(
                                step.captures
                            )
                            newSteps.add(tempStep)
                        }

                        val tempCapture = PProfileCapture(
                            capture.name,
                            capture.isSkipAllowed,
                            capture.returnRisk,
                            capture.idpIdentifier,
                            capture.response,
                            newSteps
                        )
                        newCaptures.add(tempCapture)
                    }

                    // Responses
                    val newResponses = mutableListOf<PResponsesCategory>()
                    scenario.responses?.forEach { respCat ->
                        val newRespCatField = mutableListOf<PResponsesCategoryField>()
                        respCat.fields.forEach { respCatField ->
                            val tempField = PField(
                                respCatField.field.format,
                                respCatField.field.isDisplayed,
                                respCatField.field.mandatory,
                                respCatField.field.pattern,
                                respCatField.field.editable
                            )
                            val tempRespCatField = PResponsesCategoryField (
                                respCatField.name,
                                tempField
                            )
                            newRespCatField.add(tempRespCatField)
                        }

                        val tempRespCategory = PResponsesCategory(
                            respCat.name,
                            newRespCatField
                        )
                        newResponses.add(tempRespCategory)
                    }

                    val tempScenario = PScenario(
                        scenario.idx,
                        scenario.name,
                        newCaptures,
                        newResponses,
                        scenario.infoData
                    )
                    newScenario.add(tempScenario)
                }
            }

            // ProfileCaptor
            val newProfileCaptors = mutableMapOf<String, PProfileCaptor>()
            profInfo.captors.let { captors ->
                captors.forEach { (idx, pCaptor) ->
                    val tempProfileCaptor = PProfileCaptor(
                        pCaptor.pages,
                        pCaptor.metadata,
                        pCaptor.isUploadAllowed,
                        pCaptor.category
                    )
                    newProfileCaptors[idx] = tempProfileCaptor
                }
            }

            // ProfileConfigs
            val newProfileConfigs = PProfileConfigs(
                profInfo.configs.k1,
                profInfo.configs.k1m,
                profInfo.configs.k1e,
                profInfo.configs.httpTimeoutSecond,
                profInfo.configs.httpMaxRetryNum,
                profInfo.configs.httpRetryDelaySecond,
                profInfo.configs.pushNotifTimeoutSecond,
                profInfo.configs.pullMaxRetryNum,
                profInfo.configs.lang,
                profInfo.configs.sizeLimit,
                profInfo.configs.enableRisk
            )

            val newProfile = PTdiProfileInfo(
                profInfo.version,
                newScenario,
                newProfileCaptors,
                newProfileConfigs,
                profInfo.theme,
                profInfo.localization
            )

            val profileInJson: String = Gson().toJson(newProfile)

            PLog.fullLog(LOG_TAG, "Profile json: \n $profileInJson")
            mainJsonObj.put("profile", profileInJson)

            mProfileApiCall = false
        }

        tasks?.let { taskList ->
            PLog.d(LOG_TAG, "Received tasks: \n $taskList")
            val tempTaskList = extractTasks(taskList)
            mainJsonObj.put("tasks", tempTaskList)
        }

        var callbkStatus = PluginResult.Status.OK

        result?.let { resultData ->
            PLog.d(LOG_TAG, "Received result: \n $resultData")

            try {
                val tempJsonObj = JSONObject()
                tempJsonObj.put("code", resultData.code)
                tempJsonObj.put("message", resultData.message)

                val tempDesc = JSONObject()
                resultData.description?.forEach { (mKey, mVal) ->
                    tempDesc.put(mKey, mVal)
                }
                tempJsonObj.put("description", tempDesc)

                mainJsonObj.put("result", tempJsonObj)

                /* TODO:
                resultData.code?.let { error ->
                    if (error.toInt() != 0) {
                        callbkStatus = PluginResult.Status.ERROR
                    }
                }*/
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val pluginResult = PluginResult(callbkStatus, mainJsonObj)
        mCallbackCtx?.sendPluginResult(pluginResult)
    }

    private fun extractTasks(_taskList: MutableList<Task>): JSONArray {
        val tempTaskList = JSONArray()
        _taskList.forEach { task ->
            val tempJsonObj = JSONObject()
            tempJsonObj.put("id", task.id)
            tempJsonObj.put("name", task.name)
            tempJsonObj.put("status", task.status)
            tempJsonObj.put("category", task.category)
            tempTaskList.put(tempJsonObj)
        }
        return tempTaskList
    }

}
