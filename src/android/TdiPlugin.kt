/**
 * TdiPlugin.kt
 * Cordova plugin wrapper for TDI SDK.
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

package cordova.plugin.tdi

import android.content.Intent
import com.thalesgroup.idv.sdk.doc.api.Configuration
import org.apache.cordova.CallbackContext
import org.apache.cordova.CordovaPlugin
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class TdiPlugin : CordovaPlugin() {
    companion object {
        private val LOG_TAG = TdiPlugin::class.java.name
    }

    private var tdiSdkAgent: TdiSdkAgent? = null
    private var customCaptor: CustomCaptor? = null

    override fun pluginInitialize() {
        super.pluginInitialize()
        tdiSdkAgent = TdiSdkAgent.getInstance()
        customCaptor = CustomCaptor()
    }

    @Throws(JSONException::class)
    override fun execute(
        action: String,
        args: JSONArray,
        callbackContext: CallbackContext
    ): Boolean {

        tdiSdkAgent?.mCallbackCtx = callbackContext
        customCaptor?.setPluginCallback(callbackContext)

        when (action) {
            "tdiSdkInit" -> {
                PLog.d(LOG_TAG, "tdiSdkInit")
                tdiSdkAgent?.tdiSdkInit(
                    cordova.activity,
                    args.getString(0)
                )
                return true
            }
            "getProfile" -> {
                PLog.d(LOG_TAG, "getProfile")
                tdiSdkAgent?.getProfile(
                    args.getString(0),
                    args.getString(1),
                    args.getString(2)
                )
                return true
            }
            "newSession" -> {
                pluginVersion()
                PLog.d(LOG_TAG, "newSession")
                tdiSdkAgent?.newSession(
                    args.getString(0),
                    args.getString(1),
                    args.getString(2),
                    args.getString(3)
                )
                return true
            }
            "sessionStart" -> {
                PLog.d(LOG_TAG, "sessionStart")
                tdiSdkAgent?.sessionStart()
                return true
            }
            "sessionResume" -> {
                PLog.d(LOG_TAG, "sessionResume")
                tdiSdkAgent?.sessionResume(
                    args.getString(0)
                )
                return true
            }
            "sessionResult" -> {
                PLog.d(LOG_TAG, "sessionResult")
                tdiSdkAgent?.sessionResult()
                return true
            }
            "sessionUpdate" -> {
                PLog.d(LOG_TAG, "sessionUpdate")

                val taskId = args.getString(0)
                val jsonObject = args.getJSONObject(1)
                val inputPages = mutableMapOf<String, String>()

                jsonObject.let { jsonData ->
                    val jsonKey = jsonData.keys()

                    jsonKey.forEach {
                        inputPages[it] = jsonData[it] as String
                    }
                }

                tdiSdkAgent?.sessionUpdate(
                    taskId,
                    inputPages
                )
                return true
            }
            "sessionStop" -> {
                PLog.d(LOG_TAG, "sessionStop")
                tdiSdkAgent?.sessionStop()
                return true
            }
            "getCaptor" -> {
                PLog.d(LOG_TAG, "getCaptor")
                val jsonObject = args.getJSONObject(0)

                jsonObject.let { config ->
                    val captureType = config["CAPTURE_TYPE"]
                    val jsonKey = config.keys()
                    val capConfig = mutableMapOf<String, Any>()

                    jsonKey.forEach {
                        capConfig[it] = config[it]
                    }

                    if (captureType == "Custom") {
                        getCustomCaptor(capConfig)
                    } else {
                        getTdiCaptor(capConfig)
                    }
                }

                return true
            }
        }
        return false
    }

    private fun getTdiCaptor(_config: MutableMap<String, Any>) {
        PLog.d(LOG_TAG, "Tdi captor")
        tdiSdkAgent?.getTdiCaptor(_config)
    }

    private fun getCustomCaptor(_config: MutableMap<String, Any>) {
        PLog.d(LOG_TAG, "Custom captor: \n $_config")
        val context = cordova.activity
        val intent = Intent(context, customCaptor!!::class.java)

        if (_config.containsKey(CustomCaptor.EXTRA_CAPTURE_NB_SIDES)) {
            intent.putExtra(CustomCaptor.EXTRA_CAPTURE_NB_SIDES, _config[CustomCaptor.EXTRA_CAPTURE_NB_SIDES] as? Int)
        }

        if (_config.containsKey(CustomCaptor.EXTRA_CAPTURE_OVERLAY)) {
            intent.putExtra(CustomCaptor.EXTRA_CAPTURE_OVERLAY, _config[CustomCaptor.EXTRA_CAPTURE_OVERLAY] as? Boolean)
        }

        if (_config.containsKey(CustomCaptor.EXTRA_IDV_LICENSE)) {
            intent.putExtra(CustomCaptor.EXTRA_IDV_LICENSE, _config[CustomCaptor.EXTRA_IDV_LICENSE] as? String)
        }

        if (_config.containsKey(CustomCaptor.EXTRA_CAPTURE_DETECTION_MODE)) {
            val detection = when (_config[CustomCaptor.EXTRA_CAPTURE_DETECTION_MODE] as? Int) {
                Configuration.ImageProcessing -> Configuration.ImageProcessing
                Configuration.MachineLearning -> Configuration.MachineLearning
                else -> Configuration.Disabled
            }
            intent.putExtra(CustomCaptor.EXTRA_CAPTURE_DETECTION_MODE, detection as? Int)
        }

        if (_config.containsKey(CustomCaptor.EXTRA_CAPTURE_QUALITY_CHECK_BLUR)) {
            val blur = when (_config[CustomCaptor.EXTRA_CAPTURE_QUALITY_CHECK_BLUR] as? Int) {
                Configuration.Relaxed -> Configuration.Relaxed
                Configuration.Strict -> Configuration.Strict
                Configuration.TextBlock -> Configuration.TextBlock
                else -> Configuration.Disabled
            }
            intent.putExtra(CustomCaptor.EXTRA_CAPTURE_QUALITY_CHECK_BLUR, blur as? Int)
        }

        if (_config.containsKey(CustomCaptor.EXTRA_CAPTURE_QUALITY_CHECK_GLARE)) {
            val glare = when (_config[CustomCaptor.EXTRA_CAPTURE_QUALITY_CHECK_GLARE] as? Int) {
                Configuration.White -> Configuration.White
                Configuration.Color -> Configuration.Color
                else -> Configuration.Disabled
            }
            intent.putExtra(CustomCaptor.EXTRA_CAPTURE_QUALITY_CHECK_GLARE, glare as? Int)
        }

        if (_config.containsKey(CustomCaptor.EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS)) {
            val darkness = when (_config[CustomCaptor.EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS] as? Int) {
                Configuration.Relaxed -> Configuration.Relaxed
                else -> Configuration.Disabled
            }
            intent.putExtra(CustomCaptor.EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS, darkness as? Int)
        }

        if (_config.containsKey(CustomCaptor.EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY)) {
            val photocopy = when (_config[CustomCaptor.EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY] as? Int) {
                Configuration.BlackAndWhite -> Configuration.BlackAndWhite
                else -> Configuration.Disabled
            }
            intent.putExtra(CustomCaptor.EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY, photocopy as? Int)
        }

        if (_config.containsKey(CustomCaptor.EXTRA_DOC_SIZE)) {
            val jsonObject = _config[CustomCaptor.EXTRA_DOC_SIZE] as JSONObject

            jsonObject.let { config ->
                val jsonKey = config.keys()
                val docConfig = mutableMapOf<String, Any>()

                jsonKey.forEach {
                    docConfig[it] = config[it]
                }

                val docSize = FloatArray(2)
                val width = docConfig["Width"] as Number
                docSize[0] = width.toFloat()
                val height = docConfig["Height"] as Number
                docSize[1] = height.toFloat()
                intent.putExtra(CustomCaptor.EXTRA_DOC_SIZE, docSize)
            }
        }

        if (_config.containsKey(CustomCaptor.EXTRA_CAPTURE_JPEG_COMPRESSION)) {
          intent.putExtra(CustomCaptor.EXTRA_CAPTURE_JPEG_COMPRESSION, _config[CustomCaptor.EXTRA_CAPTURE_JPEG_COMPRESSION] as? Int)
        }

        if (_config.containsKey(CustomCaptor.EXTRA_CAPTURE_SHUTTER_BUTTON_DELAY)) {
            intent.putExtra(CustomCaptor.EXTRA_CAPTURE_SHUTTER_BUTTON_DELAY, _config[CustomCaptor.EXTRA_CAPTURE_SHUTTER_BUTTON_DELAY] as? Int)
        }

        context.startActivity(intent)
    }

    private fun pluginVersion() {
        val verStr = plugin_version
        val sdkVer = tdiSdkAgent?.getSdkVersion()
        PLog.d(LOG_TAG, "TDI-$verStr-$sdkVer")
    }
}
