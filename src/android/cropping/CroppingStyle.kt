/**
 * CroppingStyle.kt
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

package cordova.plugin.tdi.cropping

internal class CroppingStyle {
    var cropping: Cropping = Cropping()

    companion object {
        private var instance: CroppingStyle? = null
        fun sharedInstance(): CroppingStyle? {
            if (instance == null) {
                instance = CroppingStyle()
            }
            return instance
        }
    }
}