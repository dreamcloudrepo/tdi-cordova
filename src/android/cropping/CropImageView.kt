/**
 * CropImageView.kt
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

package cordova.plugin.tdi.cropping

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.thalesgroup.tdi.R

class CropImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) :
    TouchImageView(context, attrs, defStyle) {
    private var imageWidth = 0
    private var imageHeight = 0
    lateinit var image: ByteArray
        private set

    protected enum class ECropCorner {
        none, topLeft, topRight, bottomRight, bottomLeft
    }

    inner class MovableEdge {
        var side_point: PointF = PointF()
        var move_corner_1: PointF = PointF()
        var move_corner_2: PointF = PointF()
        var anchor_corner_1: PointF = PointF()
        var anchor_corner_2: PointF = PointF()
    }

    private inner class Line2d {
        var m = 0.0
        var n = 0.0
    }

    protected var m_borderPainter = Paint()
    protected var m_selectedPainter = Paint()
    protected var m_dotPainter = Paint()
    protected var m_topLeftCorner: PointF = PointF()
    protected var m_topRightCorner: PointF = PointF()
    protected var m_bottomRightCorner: PointF = PointF()
    protected var m_bottomLeftCorner: PointF = PointF()
    protected var m_topEdge: MovableEdge = MovableEdge()
    protected var m_rightEdge: MovableEdge = MovableEdge()
    protected var m_bottomEdge: MovableEdge = MovableEdge()
    protected var m_leftEdge: MovableEdge = MovableEdge()
    private var m_topLeftCornerInitial: PointF = PointF()
    private var m_topRightCornerInitial: PointF = PointF()
    private var m_bottomRightCornerInitial: PointF = PointF()
    private var m_bottomLeftCornerInitial: PointF = PointF()
    protected var m_editedCorner = ECropCorner.none
    protected var m_editedEdge: MovableEdge? = null
    protected fun setup() {
        val style: CroppingStyle? = CroppingStyle.sharedInstance()
        style?.cropping?.line?.color = resources.getColor(R.color.idv_doc_cropping_line)
        style?.cropping?.line?.width = resources.getDimension(R.dimen.idv_doc_cropping_line).toInt()
        m_borderPainter.style = Paint.Style.FILL
        style?.cropping?.circle?.color = resources.getColor(R.color.idv_doc_cropping_circle)
        style?.cropping?.circle?.radio = resources.getDimension(R.dimen.idv_doc_cropping_circle).toInt()
        style?.cropping?.circle?.selected_color =
            resources.getColor(R.color.idv_doc_cropping_selected_circle)
        style?.cropping?.circle?.selected_radio = resources.getDimension(R.dimen.idv_doc_cropping_selected_circle).toInt()
        m_dotPainter.style = Paint.Style.FILL_AND_STROKE
        m_selectedPainter.style = Paint.Style.FILL
        setOnTouchListener(CropTouchListener())
    }

    private fun updateCustomization() {
        m_borderPainter.color = CroppingStyle.sharedInstance()?.cropping?.line?.color!!
        m_borderPainter.strokeWidth = CroppingStyle.sharedInstance()?.cropping?.line?.width?.toFloat()!!
        m_dotPainter.color = CroppingStyle.sharedInstance()?.cropping?.circle?.color!!
        m_selectedPainter.color = CroppingStyle.sharedInstance()?.cropping?.circle?.selected_color!!
    }

    ////////////////////////////////////
    // setters and getters
    ////////////////////////////////////
    fun setImageAndCorners(
        p_image: ByteArray,
        p_topLeft: PointF,
        p_topRight: PointF,
        p_bottomRight: PointF,
        p_bottomLeft: PointF
    ) {
        image = p_image
        val bitmap = BitmapFactory.decodeByteArray(image, 0, image.size)
        setImageBitmap(bitmap)
        setEdgeCorners(
            p_topLeft,
            p_topRight,
            p_bottomRight,
            p_bottomLeft,
            bitmap.width,
            bitmap.height
        )
    }

    private fun setEdgeCorners(
        p_topLeft: PointF,
        p_topRight: PointF,
        p_bottomRight: PointF,
        p_bottomLeft: PointF,
        imageWidth: Int,
        imageHeight: Int
    ) {
        //Update style before draw the edges and corners
        updateCustomization()
        this.imageHeight = imageHeight
        this.imageWidth = imageWidth
        val n_topLeft = PointF(p_topLeft.x * imageWidth, p_topLeft.y * imageHeight)
        val n_topRight = PointF(p_topRight.x * imageWidth, p_topRight.y * imageHeight)
        val n_bottomRight = PointF(p_bottomRight.x * imageWidth, p_bottomRight.y * imageHeight)
        val n_bottomLeft = PointF(p_bottomLeft.x * imageWidth, p_bottomLeft.y * imageHeight)


        //set the four corners anchors
        m_topLeftCorner = n_topLeft
        m_topRightCorner = n_topRight
        m_bottomRightCorner = n_bottomRight
        m_bottomLeftCorner = n_bottomLeft
        m_topLeftCornerInitial = n_topLeft
        m_topRightCornerInitial = n_topRight
        m_bottomRightCornerInitial = n_bottomRight
        m_bottomLeftCornerInitial = n_bottomLeft

        //compute position for edge anchor
        m_topEdge = setEdge(
            MovableEdge(),
            m_topLeftCorner,
            m_topRightCorner,
            m_bottomLeftCorner,
            m_bottomRightCorner
        )
        m_rightEdge = setEdge(
            MovableEdge(),
            m_topRightCorner,
            m_bottomRightCorner,
            m_topLeftCorner,
            m_bottomLeftCorner
        )
        m_bottomEdge = setEdge(
            MovableEdge(),
            m_bottomLeftCorner,
            m_bottomRightCorner,
            m_topLeftCorner,
            m_topRightCorner
        )
        m_leftEdge = setEdge(
            MovableEdge(),
            m_topLeftCorner,
            m_bottomLeftCorner,
            m_topRightCorner,
            m_bottomRightCorner
        )
    }

    private fun setEdge(
        p_edge: MovableEdge,
        p_movePoint1: PointF,
        p_movePoint2: PointF,
        p_anchorPoint1: PointF,
        p_anchorPoint2: PointF
    ): MovableEdge {
        p_edge.side_point =
            PointF((p_movePoint1.x + p_movePoint2.x) / 2, (p_movePoint1.y + p_movePoint2.y) / 2)
        p_edge.move_corner_1 = p_movePoint1
        p_edge.move_corner_2 = p_movePoint2
        p_edge.anchor_corner_1 = p_anchorPoint1
        p_edge.anchor_corner_2 = p_anchorPoint2
        return p_edge
    }

    val allCropPoints: Array<PointF?>?
        get() = try {
            arrayOf(
                m_topLeftCorner,
                m_topEdge.side_point,
                m_topRightCorner,
                m_bottomLeftCorner,
                m_bottomEdge.side_point,
                m_topRightCorner,
                m_leftEdge.side_point,
                m_rightEdge.side_point
            )
        } catch (e: Exception) {
            null
        }
    val topLeft: PointF
        get() = (m_topLeftCorner)
    val topRight: PointF
        get() = (m_topRightCorner)
    val bottomRight: PointF
        get() = (m_bottomRightCorner)
    val bottomLeft: PointF
        get() = (m_bottomLeftCorner)
    val topLeftPoint: PointF
        get() = PointF(topLeft.x / imageWidth, topLeft.y / imageHeight)
    val topRightPoint: PointF
        get() = PointF(topRight.x / imageWidth, topRight.y / imageHeight)
    val bottomRightPoint: PointF
        get() = PointF(bottomRight.x / imageWidth, bottomRight.y / imageHeight)
    val bottomLeftPoint: PointF
        get() = PointF(bottomLeft.x / imageWidth, bottomLeft.y / imageHeight)

    fun hasBeenModdified(): Boolean {
        return !(m_topLeftCornerInitial == m_topLeftCorner && m_topRightCornerInitial == m_topRightCorner && m_bottomLeftCornerInitial == m_bottomLeftCorner && m_bottomRightCornerInitial == m_bottomRightCorner)
    }

    ////////////////////////////////////
    // Override for the onDrawMethod : display edges and anchors the call super method
    ////////////////////////////////////
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (m_topLeftCorner != null && m_topRightCorner != null && m_bottomLeftCorner != null && m_bottomRightCorner != null) {
            val saveCount = canvas?.saveCount
            val mtxValues = FloatArray(9)
            val mtx: Matrix = getImageMatrix()
            mtx.getValues(mtxValues)
            canvas?.save()
            canvas?.concat(mtx)
            canvas?.drawLine(
                m_topLeftCorner.x,
                m_topLeftCorner.y, m_topRightCorner.x, m_topRightCorner.y,
                m_borderPainter
            )
            canvas?.drawLine(
                m_topRightCorner.x,
                m_topRightCorner.y, m_bottomRightCorner.x, m_bottomRightCorner.y,
                m_borderPainter
            )
            canvas?.drawLine(
                m_bottomRightCorner.x,
                m_bottomRightCorner.y, m_bottomLeftCorner.x, m_bottomLeftCorner.y,
                m_borderPainter
            )
            canvas?.drawLine(
                m_bottomLeftCorner.x,
                m_bottomLeftCorner.y, m_topLeftCorner.x, m_topLeftCorner.y,
                m_borderPainter
            )
            val circle: Cropping.Circle? = CroppingStyle.sharedInstance()?.cropping?.circle
            (if (m_editedCorner == ECropCorner.topLeft) circle?.selected_radio else circle?.radio)?.div(
                mtxValues[Matrix.MSCALE_X]
            )?.let {
                canvas?.drawCircle(
                    m_topLeftCorner.x,
                    m_topLeftCorner.y,
                    it,
                    if (m_editedCorner == ECropCorner.topLeft) m_selectedPainter else m_dotPainter
                )
            }
            (if (m_editedCorner == ECropCorner.topRight) circle?.selected_radio else circle?.radio)?.div(
                mtxValues[Matrix.MSCALE_X]
            )?.let {
                canvas?.drawCircle(
                    m_topRightCorner.x,
                    m_topRightCorner.y,
                    it,
                    if (m_editedCorner == ECropCorner.topRight) m_selectedPainter else m_dotPainter
                )
            }
            (if (m_editedCorner == ECropCorner.bottomRight) circle?.selected_radio else circle?.radio)?.div(
                mtxValues[Matrix.MSCALE_X]
            )?.let {
                canvas?.drawCircle(
                    m_bottomRightCorner.x,
                    m_bottomRightCorner.y,
                    it,
                    if (m_editedCorner == ECropCorner.bottomRight) m_selectedPainter else m_dotPainter
                )
            }
            (if (m_editedCorner == ECropCorner.bottomLeft) circle?.selected_radio else circle?.radio)?.div(
                mtxValues[Matrix.MSCALE_X]
            )?.let {
                canvas?.drawCircle(
                    m_bottomLeftCorner.x,
                    m_bottomLeftCorner.y,
                    it,
                    if (m_editedCorner == ECropCorner.bottomLeft) m_selectedPainter else m_dotPainter
                )
            }


            // Side circles
            m_topEdge = setEdge(
                m_topEdge,
                m_topLeftCorner,
                m_topRightCorner,
                m_bottomLeftCorner,
                m_bottomRightCorner
            )
            m_rightEdge = setEdge(
                m_rightEdge,
                m_topRightCorner,
                m_bottomRightCorner,
                m_topLeftCorner,
                m_bottomLeftCorner
            )
            m_bottomEdge = setEdge(
                m_bottomEdge,
                m_bottomLeftCorner,
                m_bottomRightCorner,
                m_topLeftCorner,
                m_topRightCorner
            )
            m_leftEdge = setEdge(
                m_leftEdge,
                m_topLeftCorner,
                m_bottomLeftCorner,
                m_topRightCorner,
                m_bottomRightCorner
            )
            (if (m_editedEdge === m_topEdge) circle?.selected_radio else circle?.radio)?.div(
                mtxValues[Matrix.MSCALE_X]
            )?.let {
                /*canvas?.drawCircle(
                    m_topEdge.side_point.x,
                    m_topEdge.side_point.y,
                    it,
                    if (m_editedEdge === m_topEdge) m_selectedPainter else m_dotPainter
                )*/
            }
            (if (m_editedEdge === m_rightEdge) circle?.selected_radio else circle?.radio)?.div(
                mtxValues[Matrix.MSCALE_X]
            )?.let {
                /*canvas?.drawCircle(
                    m_rightEdge.side_point.x,
                    m_rightEdge.side_point.y,
                    it,
                    if (m_editedEdge === m_rightEdge) m_selectedPainter else m_dotPainter
                )*/
            }
            (if (m_editedEdge === m_bottomEdge) circle?.selected_radio else circle?.radio)?.div(
                mtxValues[Matrix.MSCALE_X]
            )?.let {
                /*canvas?.drawCircle(
                    m_bottomEdge!!.side_point!!.x,
                    m_bottomEdge!!.side_point!!.y,
                    it,
                    if (m_editedEdge === m_bottomEdge) m_selectedPainter else m_dotPainter
                )*/
            }
            (if (m_editedEdge === m_leftEdge) circle?.selected_radio else circle?.radio)?.div(
                mtxValues[Matrix.MSCALE_X]
            )?.let {
                /*canvas?.drawCircle(
                    m_leftEdge!!.side_point!!.x,
                    m_leftEdge!!.side_point!!.y,
                    it,
                    if (m_editedEdge === m_leftEdge) m_selectedPainter else m_dotPainter
                )*/
            }
            saveCount?.let { canvas.restoreToCount(it) }
        }
    }

    protected inner class CropTouchListener : OnTouchListener {
        override fun onTouch(v: View?, event: MotionEvent): Boolean {
            val mtx: Matrix = getImageMatrix()
            val iMtx = Matrix()
            val eventPos = floatArrayOf(event.x, event.y)
            val mtxValues = FloatArray(9)
            val scale: Float
            mtx.getValues(mtxValues)
            scale = mtxValues[Matrix.MSCALE_X]
            mtx.invert(iMtx)
            iMtx.mapPoints(eventPos)
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    m_editedCorner = getCornerUnderPos(eventPos[0], eventPos[1], scale)
                    m_editedEdge = getEdgeUnderPos(eventPos[0], eventPos[1], scale)
                    return m_editedCorner != ECropCorner.none && m_editedEdge != null
                }
                MotionEvent.ACTION_UP -> {
                    m_editedCorner = ECropCorner.none
                    m_editedEdge = null
                    return false
                }
                MotionEvent.ACTION_MOVE -> {
                    when (m_editedCorner) {
                        ECropCorner.topLeft -> {
                            m_topLeftCorner!!.x = eventPos[0]
                            m_topLeftCorner!!.y = eventPos[1]
                            invalidate()
                            return true
                        }
                        ECropCorner.topRight -> {
                            m_topRightCorner!!.x = eventPos[0]
                            m_topRightCorner!!.y = eventPos[1]
                            invalidate()
                            return true
                        }
                        ECropCorner.bottomRight -> {
                            m_bottomRightCorner!!.x = eventPos[0]
                            m_bottomRightCorner!!.y = eventPos[1]
                            invalidate()
                            return true
                        }
                        ECropCorner.bottomLeft -> {
                            m_bottomLeftCorner!!.x = eventPos[0]
                            m_bottomLeftCorner!!.y = eventPos[1]
                            invalidate()
                            return true
                        }
                    }
                    if (m_editedEdge != null) {
                        val originSide = getLineFromPoints(
                            m_editedEdge!!.move_corner_1, m_editedEdge!!.move_corner_2
                        )
                        val movedSide: Line2d = Line2d()
                        movedSide.m = originSide.m
                        movedSide.n = eventPos[1] - movedSide.m * eventPos[0]
                        val prolongSide1 = getLineFromPoints(
                            m_editedEdge!!.move_corner_1, m_editedEdge!!.anchor_corner_1
                        )
                        m_editedEdge!!.move_corner_1!!.x =
                            ((prolongSide1.n - movedSide.n) / (movedSide.m - prolongSide1.m)).toFloat()
                        m_editedEdge!!.move_corner_1!!.y =
                            (prolongSide1.m * m_editedEdge!!.move_corner_1!!.x + prolongSide1.n).toFloat()
                        val prolongSide2 = getLineFromPoints(
                            m_editedEdge!!.move_corner_2, m_editedEdge!!.anchor_corner_2
                        )
                        m_editedEdge!!.move_corner_2!!.x =
                            ((prolongSide2.n - movedSide.n) / (movedSide.m - prolongSide2.m)).toFloat()
                        m_editedEdge!!.move_corner_2!!.y =
                            (prolongSide2.m * m_editedEdge!!.move_corner_2!!.x + prolongSide2.n).toFloat()
                        invalidate()
                        return true
                    }
                }
            }
            return false
        }
    }

    private fun getLineFromPoints(p1: PointF?, p2: PointF?): Line2d {
        val ret: Line2d = Line2d()
        val epsilon = Math.ulp(1f)
        val infinity = 99999999f
        ret.m =
            if (Math.abs(p1!!.x - p2!!.x) < epsilon) infinity.toDouble() else (p1.y - p2.y) / (p1.x - p2.x).toDouble()
        ret.n = p1.y - ret.m * p1.x
        return ret
    }

    protected fun getCornerUnderPos(p_posX: Float, p_posY: Float, p_scale: Float): ECropCorner {
        var dist = p_scale * Math.sqrt(
            Math.pow(
                (p_posX - m_topLeftCorner.x).toDouble(),
                2.0
            ) + Math.pow(
                (p_posY - m_topLeftCorner.y).toDouble(),
                2.0
            )
        )
            .toFloat()
        if (dist < 100.0) return ECropCorner.topLeft
        dist = p_scale * Math.sqrt(
            Math.pow(
                (p_posX - m_topRightCorner!!.x).toDouble(),
                2.0
            ) + Math.pow((p_posY - m_topRightCorner!!.y).toDouble(), 2.0)
        )
            .toFloat()
        if (dist < 100.0) return ECropCorner.topRight
        dist = p_scale * Math.sqrt(
            Math.pow(
                (p_posX - m_bottomRightCorner!!.x).toDouble(),
                2.0
            ) + Math.pow((p_posY - m_bottomRightCorner!!.y).toDouble(), 2.0)
        )
            .toFloat()
        if (dist < 100.0) return ECropCorner.bottomRight
        dist = p_scale * Math.sqrt(
            Math.pow(
                (p_posX - m_bottomLeftCorner!!.x).toDouble(),
                2.0
            ) + Math.pow((p_posY - m_bottomLeftCorner!!.y).toDouble(), 2.0)
        )
            .toFloat()
        return if (dist < 100.0) (ECropCorner.bottomLeft) else (ECropCorner.none)
    }

    protected fun getEdgeUnderPos(p_posX: Float, p_posY: Float, p_scale: Float): MovableEdge? {
        val touchDist: Float = CroppingStyle.sharedInstance()?.cropping?.circle?.radio?.toFloat()!!
        var dist: Float
        dist = p_scale * Math.sqrt(
            Math.pow(
                (p_posX - m_topEdge!!.side_point!!.x).toDouble(),
                2.0
            ) + Math.pow((p_posY - m_topEdge!!.side_point!!.y).toDouble(), 2.0)
        )
            .toFloat()
        if (dist < touchDist) return m_topEdge
        dist = p_scale * Math.sqrt(
            Math.pow(
                (p_posX - m_rightEdge!!.side_point!!.x).toDouble(),
                2.0
            ) + Math.pow((p_posY - m_rightEdge!!.side_point!!.y).toDouble(), 2.0)
        )
            .toFloat()
        if (dist < touchDist) return m_rightEdge
        dist = p_scale * Math.sqrt(
            Math.pow(
                (p_posX - m_bottomEdge!!.side_point!!.x).toDouble(),
                2.0
            ) + Math.pow((p_posY - m_bottomEdge!!.side_point!!.y).toDouble(), 2.0)
        )
            .toFloat()
        if (dist < touchDist) return m_bottomEdge
        dist = p_scale * Math.sqrt(
            Math.pow(
                (p_posX - m_leftEdge!!.side_point!!.x).toDouble(),
                2.0
            ) + Math.pow((p_posY - m_leftEdge!!.side_point!!.y).toDouble(), 2.0)
        )
            .toFloat()
        return if (dist < touchDist) m_leftEdge else null
    }

    ////////////////////////////////////
    // Constructors
    ////////////////////////////////////
    init {
        setup()
    }
}