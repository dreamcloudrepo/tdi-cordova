/**
 * Cropping.kt
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

package cordova.plugin.tdi.cropping

internal class Cropping {
    var line: Line = Line()
    var circle = Circle()

    internal inner class Line {
        var width = 2
        var color = -0xffff01
    }

    internal inner class Circle {
        var radio = 25
        var color = -0x55ffff01
        var selected_radio = 25
        var selected_color = -0x55000001
    }
}