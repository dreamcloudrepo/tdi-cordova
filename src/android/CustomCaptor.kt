/**
 * CustomCaptor.kt
 * Custom captor using IDV SDK.
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

package cordova.plugin.tdi

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.PointF
import android.graphics.Rect
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.Base64
import android.view.TextureView
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.thalesgroup.tdi.Failure
import com.thalesgroup.idv.sdk.doc.api.*
import cordova.plugin.tdi.cropping.CropImageView
import org.apache.cordova.CallbackContext
import org.apache.cordova.PluginResult
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.util.concurrent.atomic.AtomicBoolean
import applicationId.R

class CustomCaptor: AppCompatActivity() {
    companion object {
        private val TAG = CustomCaptor::class.java.name
        const val EXTRA_CAPTURE_NB_SIDES = "EXTRA_CAPTURE_NB_SIDES"
        const val EXTRA_CAPTURE_OVERLAY = "EXTRA_CAPTURE_OVERLAY"
        const val EXTRA_IDV_LICENSE = "EXTRA_IDV_LICENSE"
        const val EXTRA_CAPTURE_DETECTION_MODE = "EXTRA_CAPTURE_DETECTION_MODE"
        const val EXTRA_CAPTURE_QUALITY_CHECK_BLUR = "EXTRA_CAPTURE_QUALITY_CHECK_BLUR"
        const val EXTRA_CAPTURE_QUALITY_CHECK_GLARE = "EXTRA_CAPTURE_QUALITY_CHECK_GLARE"
        const val EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS = "EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS"
        const val EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY = "EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY"
        const val EXTRA_DOC_SIZE = "EXTRA_DOC_SIZE"
        const val EXTRA_CAPTURE_JPEG_COMPRESSION = "EXTRA_CAPTURE_JPEG_COMPRESSION"
        const val EXTRA_CAPTURE_SHUTTER_BUTTON_DELAY = "EXTRA_CAPTURE_SHUTTER_BUTTON_DELAY"

        var mCallbackCtx: CallbackContext? = null
        private var mIdvSdkLicense = ""
    }

    private var mSDK: CaptureSDK? = null
    private lateinit var captureMode: MutableList<Document>
    private val mConfig = Configuration()
    private var edgeDetectionMode = Configuration.MachineLearning
    private var blurDetectionMode = Configuration.TextBlock
    private var glareDetectionMode = Configuration.White
    private var darknessDetectionMode = Configuration.Relaxed
    private var photocopyDetectionMode = Configuration.Disabled

    private var idvError = Failure.REASON_DOCUMENT_CAPTURE_ABORT
    private var numberOfSides = 1
    private var currentSide = 1
    private var input = mutableMapOf<String,String>()
    private var isOverlay: Boolean = true
    private var isFeedbackOn: Boolean = false
    private var jpegCompression: Int = 80 // Percent

    private var countDownTimer: CountDownTimer? = null
    private var shutterTimer: Int = 10 // Seconds
    private var imageNotTaken = true
    private var isCropReset = false
    private var cropError = false
    private var bitmap: Bitmap? = null
    private var byteArrayCropFrame: ByteArray = byteArrayOf()
    private val shutterPressed = AtomicBoolean(false)
    private var lastCropPoints: CaptureResult.Quadrangle? = null

    private val cameraView by lazy { findViewById<TextureView>(R.id.camera_view)}
    private val overlayView by lazy {findViewById<DrawOverlayView>(R.id.overlay_view)}
    private val warningText by lazy {findViewById<TextView>(R.id.warningText)}
    private val warningLayout by lazy {findViewById<LinearLayout>(R.id.warningLayout)}
    private val cropResultImage by lazy {findViewById<ImageView>(R.id.cropResultImage)}
    private val buttonLayout by lazy {findViewById<LinearLayout>(R.id.buttonLayout)}
    private val turnDocLayout by lazy {findViewById<LinearLayout>(R.id.turnDocLayout)}
    private val cancelButton by lazy {findViewById<Button>(R.id.cancelButton)}
    private val okButton by lazy {findViewById<Button>(R.id.okButton)}
    private val txtValidateCapture by lazy { findViewById<TextView>(R.id.txtValidateCapture) }
    private val nextSideImage by lazy { findViewById<ImageView>(R.id.nextSideImage) }
    private val nextSideText by lazy { findViewById<TextView>(R.id.nextSideText) }
    private val shutterButton by lazy { findViewById<Button>(R.id.shutterButton) }
    private val resetCrop by lazy { findViewById<TextView>(R.id.resetCrop) }
    private val cancelCrop by lazy { findViewById<TextView>(R.id.cancelCrop) }
    private val editButton by lazy { findViewById<Button>(R.id.editButton) }
    private val editDoneButton by lazy { findViewById<Button>(R.id.editDoneButton) }
    private val cropImageView by lazy { findViewById<CropImageView>(R.id.crop_view) }

    fun setPluginCallback(_callbackContext: CallbackContext) {
        mCallbackCtx = _callbackContext
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.custom_captor)

        buttonLayout.visibility = View.GONE
        turnDocLayout.visibility = View.GONE
        warningLayout.visibility = View.GONE
        cameraView.visibility = View.VISIBLE
        editButton.visibility = View.GONE
        shutterButton.visibility = View.GONE

        mSDK = CaptureSDK()

        this.captureMode = when {
            intent.getFloatArrayExtra(EXTRA_DOC_SIZE) != null -> {
                // Custom document
                val docSize: FloatArray = intent.getFloatArrayExtra(EXTRA_DOC_SIZE) as FloatArray
                PLog.d(TAG,"Custom doc: w:${docSize[0]} h:${docSize[1]}")
                mutableListOf(
                    Document(docSize[0], docSize[1]),
                    Document.DocumentTD1,
                    Document.DocumentTD2,
                    Document.DocumentTD3
                )
            }
            else -> Document.DocumentModeICAO
        }

        this.isOverlay = intent.getBooleanExtra(EXTRA_CAPTURE_OVERLAY, isOverlay)
        this.numberOfSides = intent.getIntExtra(EXTRA_CAPTURE_NB_SIDES, numberOfSides)
        this.edgeDetectionMode = intent.getIntExtra(EXTRA_CAPTURE_DETECTION_MODE, Configuration.MachineLearning)
        this.blurDetectionMode = intent.getIntExtra(EXTRA_CAPTURE_QUALITY_CHECK_BLUR, Configuration.TextBlock)
        this.glareDetectionMode = intent.getIntExtra(EXTRA_CAPTURE_QUALITY_CHECK_GLARE, Configuration.White)
        this.darknessDetectionMode = intent.getIntExtra(EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS, Configuration.Relaxed)
        this.photocopyDetectionMode = intent.getIntExtra(EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY, Configuration.Disabled)
        this.jpegCompression = intent.getIntExtra(EXTRA_CAPTURE_JPEG_COMPRESSION, jpegCompression)
        this.shutterTimer = intent.getIntExtra(EXTRA_CAPTURE_SHUTTER_BUTTON_DELAY, shutterTimer)

        PLog.d(TAG,"IDV DOC SDK:" + CaptureSDK.version + " sides:" + numberOfSides
                + " jpegQ:" + jpegCompression+ " edge:" + edgeDetectionMode + " blur:" + blurDetectionMode
                + " glare:" + glareDetectionMode + " dark:" + darknessDetectionMode + " photo:" + photocopyDetectionMode)
    }

    override fun onBackPressed() {
        PLog.d(TAG,"onBackPressed: Cancelling current capture")
        val pluginResult = PluginResult(PluginResult.Status.ERROR,
            "Failure: code=${Failure.REASON_DOCUMENT_CAPTURE_ABORT} msg=${getError(Failure.REASON_DOCUMENT_CAPTURE_ABORT)}")
        mCallbackCtx?.sendPluginResult(pluginResult)
        super.onBackPressed()
    }

    private fun processError() {
        PLog.e(TAG,"processError: " + getError(this.idvError) + ": " + Integer.toHexString(this.idvError))
        mCallbackCtx?.error("Error: code=${Integer.toHexString(this.idvError)} " +
                    "msg=${getError(this.idvError)}")
        finish()
    }

    override fun onDestroy() {
        if (mSDK != null) {
            mSDK = null
        }
        super.onDestroy()
    }

    override fun onPause() {
        if (mSDK != null) {
            mSDK?.finish()
        }
        super.onPause()
    }

    override fun onResume() {
        PLog.d(TAG, "Init SDK")
        super.onResume()
        val license = if (mIdvSdkLicense.isNotEmpty()) mIdvSdkLicense else intent.getStringExtra(EXTRA_IDV_LICENSE)

        try {
            mSDK?.init(license, cameraView, { isCompleted, errorCode ->
                if (isCompleted) {
                    overlayView.drawDocumentPlaceholder(isOverlay)
                    runOnUiThread {
                        overlayView.requestLayout()
                        overlayView.layoutParams = cameraView.layoutParams
                        overlayView.invalidate()
                    }
                    startSDK()
                } else {
                    PLog.e(TAG, "Error on init: 0x" + errorCode.toString(16) + " : " + getError(errorCode))
                    this.idvError = errorCode
                    processError()
                }
            })
        } catch (e: Exception) {
            PLog.e(TAG, "onResume Exception: $e")
            this.idvError = Failure.REASON_CAPTOR_JNA_JNI_ISSUE
            processError()
        }
    }

    private fun startSDK() {
        mConfig.captureDocuments = this.captureMode
        mConfig.detectionMode = this.edgeDetectionMode
        mConfig.qualityChecks.blurDetectionMode = this.blurDetectionMode
        mConfig.qualityChecks.glareDetectionMode = this.glareDetectionMode
        mConfig.qualityChecks.darknessDetectionMode = this.darknessDetectionMode
        mConfig.qualityChecks.photocopyDetectionMode = this.photocopyDetectionMode

        // Clear cache
        if (lastCropPoints != null) {
            lastCropPoints = null
        }

        runOnUiThread {
            shutterSettings()
        }

        PLog.d(TAG, "Start SDK")

        mSDK?.start(mConfig, object : CaptureCallback.StartCallback {
            override fun onProcessedFrame(partial: CaptureResult) {
                if (!isFeedbackOn) {
                    var qualityText = when {
                        partial.deviceStatusInfo.adjustingFocus -> {
                            resources.getString(R.string.idv_doc_autofocus)
                        }
                        partial.deviceStatusInfo.adjustingExposure -> {
                            resources.getString(R.string.idv_doc_exposure)
                        }
                        partial.deviceStatusInfo.adjustingWhiteBalance -> {
                            resources.getString(R.string.idv_doc_wb)
                        }
                        else -> ""
                    }

                    if (!partial.qualityCheckResults.all && qualityText.isEmpty()) {
                        qualityText = when {
                            partial.qualityCheckResults.glare -> {
                                resources.getString(R.string.idv_doc_glare)
                            }
                            partial.qualityCheckResults.blur -> {
                                resources.getString(R.string.idv_doc_blur)
                            }
                            partial.qualityCheckResults.darkness -> {
                                resources.getString(R.string.idv_doc_shadows)
                            }
                            partial.qualityCheckResults.photocopy -> {
                                resources.getString(R.string.idv_doc_photocopy)
                            }
                            partial.qualityCheckResults.contrast -> {
                                resources.getString(R.string.idv_doc_contrast)
                            }
                            else -> ""
                        }
                    }
                    runOnUiThread {
                        if (qualityText != "") {
                            warningLayout.visibility = View.VISIBLE
                            warningText.text = qualityText
                            isFeedbackOn = true
                        } else {
                            warningLayout.visibility = View.GONE
                        }

                        Handler().postDelayed( { // Avoid Feedback to be all time on screen
                            warningLayout.visibility = View.GONE
                            isFeedbackOn = false
                        }, 1500)
                    }
                } else {
                    runOnUiThread {
                        warningLayout.visibility = View.GONE
                    }
                }
            }

            override fun onSuccess(result: CaptureResult) {
                // TODO
                /*if (!shutterPressed.get() && shutterButton.isShown) { // Auto capture
//                    PLog.d(TAG, "Auto capture skipped. Starting again the capture process...")
                    mSDK?.start(mConfig, this)
                    return
                }*/

                PLog.d(TAG,"Success")

                if (shutterPressed.get()) {
                    shutterPressed.set(false)
                    runOnUiThread {
                        editButton.visibility = View.VISIBLE
                    }
                }

                imageNotTaken = false

                try {
                    if (result.cropFrame == null && result.fullFrame != null){
                        // TODO
//                        if(isOverlay){
//                            result.cropFrame = overlayCropping(result.fullFrame)?.cropFrame
//                        }else{
                            result.cropFrame = result.fullFrame
//                        }
                    }

                    if (result.cropFrame != null) {
                        resizeAndDisplay(result)

                        editButton.setOnClickListener {
                            PLog.e(TAG, result.errorCode.toString())
                            runOnUiThread {
                                // TODO
                                if (result.cropFrame.equals(result.fullFrame)) {
                                    manualCropping(result.fullFrame)
                                } else {
                                    autoCropView(result)
                                }
                            }
                        }

                        editDoneButton.setOnClickListener {
                            resetCrop.visibility = View.GONE
                            cancelCrop.visibility = View.GONE
                            cropImageView.visibility = View.GONE
                            editDoneButton.visibility = View.GONE

                            val points = CaptureResult.Quadrangle()
                            points.bottomLeft =  cropImageView.bottomLeftPoint
                            points.bottomRight = cropImageView.bottomRightPoint
                            points.topLeft = cropImageView.topLeftPoint //if(mCropImageView.topLeftPoint.x < 0F || mCropImageView.topLeftPoint.y < 0F){ PointF(0F,0F) }else{ mCropImageView.topLeftPoint }
                            points.topRight = cropImageView.topRightPoint
                            lastCropPoints = points
                            val res: CaptureResult? = mSDK?.cropFrame(cropImageView.image, points, mConfig)

                            // TODO
                            /*if (res?.cropFrame == null && res?.fullFrame != null){
                                res.cropFrame = overlayCropping(res.fullFrame)?.cropFrame
                                editButton.visibility = View.VISIBLE
                            }*/

                            if (res?.fullFrame != null) {
                                resizeAndDisplay(res)
                            } else {
                                cropError = true
                                isCropReset = true
                                resizeAndDisplay(result)
                            }
                        }

                        cancelButton.setOnClickListener {
                            runOnUiThread {
                                PLog.d(TAG, "User reject the capture: $currentSide")
                                editButton.visibility = View.GONE
                                cropResultImage.visibility = View.GONE
                                buttonLayout.visibility = View.GONE
                                turnDocLayout.visibility = View.GONE
                                overlayView.visibility = View.VISIBLE
                                cameraView.visibility = View.VISIBLE
                                isCropReset = true
                                imageNotTaken = true
                                bitmap?.recycle()
                            }
                            startSDK()
                        }

                        okButton.setOnClickListener {
                            PLog.d(TAG, "User accept the capture: $currentSide")
                            isCropReset = true
                            input["page$currentSide"] = Base64.encodeToString(byteArrayCropFrame, Base64.NO_WRAP)

                            if (currentSide++ >= numberOfSides) { // Finish
                                PLog.d(TAG, "End of process for ${currentSide-1} sides")
                                byteArrayCropFrame = byteArrayOf()
                                bitmap?.recycle()
//                                mSDK?.finish()

                                val jsonObject = JSONObject()
                                input.let { mapData ->
                                    val tempJsonObj = JSONObject()
                                    mapData.forEach { eachInput ->
                                        tempJsonObj.put(eachInput.key, eachInput.value)
                                    }
                                    jsonObject.accumulate("pages", tempJsonObj)
                                }

                                // Delay callback to give enough time for mSDK?.finish()
                                Handler().postDelayed( {
                                    mCallbackCtx?.success(jsonObject)
                                }, 300)

                                runOnUiThread {
                                    finish()
                                }
                            } else { //Next side
                                runOnUiThread {
                                    editButton.visibility = View.GONE
                                    turnDocLayout.visibility = View.VISIBLE
                                    nextSideImage.visibility = View.VISIBLE
                                    nextSideText.visibility = View.VISIBLE
                                    nextSideText.text = getString(R.string.idv_doc_next_side)
                                    cropResultImage.visibility = View.GONE
                                    buttonLayout.visibility = View.GONE
                                    byteArrayCropFrame = byteArrayOf()
                                    bitmap?.recycle()
                                    imageNotTaken = true
                                }

                                PLog.d(TAG, "End of process for side:  ${currentSide-1}")

                                turnDocLayout.setOnClickListener {
                                    runOnUiThread {
                                        overlayView.visibility = View.VISIBLE
                                        buttonLayout.visibility = View.GONE
                                        turnDocLayout.visibility = View.GONE
                                        turnDocLayout.isClickable = false
                                        cameraView.visibility = View.VISIBLE
                                    }
                                    PLog.d(TAG, "User Tap on screen for side:  $currentSide")
                                    startSDK()
                                }
                            }
                        }
                    } else {
                        PLog.e(TAG, "onSuccess No Crop image")
                    }
                } catch (e: Exception) {
                    PLog.e(TAG, "onSuccess Exception: $e")
                }
            }
        })
    }

    private fun getError(errorNum: Int): String {
        when (errorNum) {
            0x0100, 0x0200, 0x0300 -> return getString(R.string.idv_doc_no_error)
            0x0002 -> return getString(R.string.idv_doc_error_lib_not_init)
            0x0003 -> return getString(R.string.idv_doc_error_lib_already_init)
            0x0004 -> return getString(R.string.idv_doc_error_init_progress)
            0x0005 -> return getString(R.string.idv_doc_error_lib_stopping)
            0x0006 -> return getString(R.string.idv_doc_error_mrz_not_found)
            0x0007 -> return getString(R.string.idv_doc_error_lib_finish)
            0x0008 -> return getString(R.string.idv_doc_error_lib_iqa)
            0x0009 -> return getString(R.string.idv_doc_error_init_engine)
            0x0010 -> return getString(R.string.idv_doc_error_init_ocr)
            0x0101 -> return getString(R.string.idv_doc_error_architecture_error)
            0x0102 -> return getString(R.string.idv_doc_error_os_error)
            0x0103 -> return getString(R.string.idv_doc_error_permission_denied)
            0x0201 -> return getString(R.string.idv_doc_error_license_empty)
            0x0202 -> return getString(R.string.idv_doc_error_license_expired)
            0x0203 -> return getString(R.string.idv_doc_error_feature)
            0x0204 -> return getString(R.string.idv_doc_error_license_format)
            0x0205 -> return getString(R.string.idv_doc_error_license_hash)
            0x0206 -> return getString(R.string.idv_doc_error_license_version)
            0x0207 -> return getString(R.string.idv_doc_error_license_invalid_feature_list)
            0x0208 -> return getString(R.string.idv_doc_error_license_invalid_expiration)
            0x0209 -> return getString(R.string.idv_doc_error_license_invalid_timestamp)
            0x0210 -> return getString(R.string.idv_doc_error_license_invalid_cache)
            Failure.REASON_DOCUMENT_CAPTURE_ABORT -> return getString(R.string.idv_warning_user_cancellation)
        }
        return getString(R.string.idv_doc_error_unknow_error)
    }

    private fun resizeAndDisplay(result: CaptureResult){
        try {
            byteArrayCropFrame = result.cropFrame
            bitmap = BitmapFactory.decodeByteArray(byteArrayCropFrame, 0, result.cropFrame.size)

            PLog.d(TAG,"Before compression = ${result.cropFrame.size / 1024} KB")

            try {
                val stream = ByteArrayOutputStream()
                bitmap?.compress(Bitmap.CompressFormat.JPEG, jpegCompression, stream)
                byteArrayCropFrame = stream.toByteArray()
                stream.close()
                bitmap = BitmapFactory.decodeByteArray(byteArrayCropFrame, 0, byteArrayCropFrame.size)
                PLog.d(TAG,"After compression = ${byteArrayCropFrame.size / 1024} KB")
            } catch (e: Exception) {
                PLog.e(TAG, "Compress Exception: $e")
            }

            runOnUiThread {
                cropResultImage.setImageBitmap(bitmap)
                cropResultImage.visibility = View.VISIBLE
                buttonLayout.visibility = View.VISIBLE
                if (cropError) {
                    txtValidateCapture.text = getString(R.string.tdi_doc_cropping_error)
                }else{
                    txtValidateCapture.text = if (result.qualityCheckResults.blur) {
                        getString(R.string.tdi_doc_ensure_blur_texts)
                    } else {
                        getString(R.string.tdi_doc_ensure_all_texts)
                    }
                }
                cameraView.visibility = View.GONE
                turnDocLayout.visibility = View.GONE
                warningLayout.visibility = View.GONE
                shutterButton.visibility = View.GONE
                cropError = false
            }
        } catch (e: Exception) {
            PLog.e(TAG, "resizeAndDisplay Exception: $e")
        }
    }

    private fun shutterSettings() {
        if (countDownTimer != null) {
            (countDownTimer as CountDownTimer).cancel()
            countDownTimer = null
        }

        PLog.d(TAG, "Shutter delay: $shutterTimer sec")

        if (shutterTimer >= 0) {
            val delay = shutterTimer * 1000
            countDownTimer = object: CountDownTimer(delay.toLong(), (delay.toLong() / 2)) {
                override fun onTick(millisUntilFinished: Long) {}
                override fun onFinish() {
                    if (imageNotTaken) {
                        shutterButton.visibility = View.VISIBLE
                    }
                }
            }.start()
        } else {
            shutterButton.visibility = View.GONE
        }

        shutterButton.setOnClickListener {
            shutterPressed.set(true)
            mSDK?.triggerCapture()
        }
    }

    // TODO
    private fun autoCropView(result: CaptureResult) {
        runOnUiThread {
            resetCrop.visibility = View.VISIBLE
            cancelCrop.visibility = View.VISIBLE
            editDoneButton.visibility = View.VISIBLE
            cropImageView.visibility = View.VISIBLE
            cropResultImage.visibility = View.GONE
            overlayView.visibility = View.GONE
            cameraView.visibility = View.GONE
            turnDocLayout.visibility = View.GONE
            warningLayout.visibility = View.GONE
            shutterButton.visibility = View.GONE
            buttonLayout.visibility = View.GONE
        }

        var topLeft = result.quadrangle.topLeft
        var topRight = result.quadrangle.topRight
        var bottomRight = result.quadrangle.bottomRight
        var bottomLeft = result.quadrangle.bottomLeft

        lastCropPoints?.let { lastPoints ->
            topLeft = lastPoints.topLeft
            topRight = lastPoints.topRight
            bottomRight = lastPoints.bottomRight
            bottomLeft = lastPoints.bottomLeft
        }

        cropImageView.setImageAndCorners(result.fullFrame, topLeft, topRight, bottomRight, bottomLeft)

        resetCrop.setOnClickListener {
            cropImageView.setImageAndCorners(result.fullFrame, result.quadrangle.topLeft, result.quadrangle.topRight, result.quadrangle.bottomRight, result.quadrangle.bottomLeft)
        }

        cancelCrop.setOnClickListener {
            resetCrop.visibility = View.GONE
            cancelCrop.visibility = View.GONE
            cropImageView.visibility = View.GONE
            editDoneButton.visibility = View.GONE
            cropResultImage.visibility = View.VISIBLE
            buttonLayout.visibility = View.VISIBLE
        }
    }

    private fun manualCropping(fullFrame: ByteArray){
        runOnUiThread {
            resetCrop.visibility = View.VISIBLE
            cancelCrop.visibility = View.VISIBLE
            editDoneButton.visibility = View.VISIBLE
            cropImageView.visibility = View.VISIBLE
            cropResultImage.visibility = View.GONE
            overlayView.visibility = View.GONE
            cameraView.visibility = View.GONE
            turnDocLayout.visibility = View.GONE
            warningLayout.visibility = View.GONE
            shutterButton.visibility = View.GONE
            buttonLayout.visibility = View.GONE
        }

//        val bitmap = BitmapFactory.decodeByteArray(fullFrame, 0, fullFrame.size)
//        val width = bitmap.width
//        val height = bitmap.height
//        bitmap.recycle()

        val rect: Rect = overlayView.getQuadrangle()
        val width = rect.width()
        val height = rect.height()
        val overlayCenter = PointF(width / 2.0F, height / 2.0F)
        val left: Float = overlayCenter.x - width/3.0F
        val top: Float = overlayCenter.y - height/3.0F
        val right: Float = overlayCenter.x + width/3.0F
        val bottom: Float = overlayCenter.y + height/3.0F

        val quadrangle = CaptureResult.Quadrangle()
        if(cropImageView.bottomLeftPoint == PointF(Float.NaN,Float.NaN) || isCropReset){ // Guidance frame points
            quadrangle.topLeft = PointF(left / width,top / height)
            quadrangle.topRight = PointF(right / width, top / height)
            quadrangle.bottomLeft = PointF(left / width, bottom / height)
            quadrangle.bottomRight = PointF(right / width,bottom / height)
            isCropReset = false
        }else{ // Previous cropping points
            quadrangle.bottomLeft = cropImageView.bottomLeftPoint
            quadrangle.bottomRight = cropImageView.bottomRightPoint
            quadrangle.topLeft = cropImageView.topLeftPoint
            quadrangle.topRight = cropImageView.topRightPoint
        }

        var topLeft = quadrangle.topLeft
        var topRight = quadrangle.topRight
        var bottomRight = quadrangle.bottomRight
        var bottomLeft = quadrangle.bottomLeft

        lastCropPoints?.let { lastPoints ->
            topLeft = lastPoints.topLeft
            topRight = lastPoints.topRight
            bottomRight = lastPoints.bottomRight
            bottomLeft = lastPoints.bottomLeft
        }

        cropImageView.setImageAndCorners(fullFrame, topLeft, topRight, bottomRight, bottomLeft)

        resetCrop.setOnClickListener {
            quadrangle.topLeft = PointF(left / width,top / height)
            quadrangle.topRight = PointF(right / width, top / height)
            quadrangle.bottomLeft = PointF(left / width, bottom / height)
            quadrangle.bottomRight = PointF(right / width,bottom / height)

            cropImageView.setImageAndCorners(
                fullFrame,
                quadrangle.topLeft,
                quadrangle.topRight,
                quadrangle.bottomRight,
                quadrangle.bottomLeft
            )
        }

        cancelCrop.setOnClickListener {
            resetCrop.visibility = View.GONE
            cancelCrop.visibility = View.GONE
            cropImageView.visibility = View.GONE
            editDoneButton.visibility = View.GONE
            cropResultImage.visibility = View.VISIBLE
            buttonLayout.visibility = View.VISIBLE
        }
    }
}
