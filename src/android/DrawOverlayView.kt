/**
 * DrawOverlayView.kt
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

package cordova.plugin.tdi

import android.content.Context
import android.graphics.Canvas
import android.graphics.CornerPathEffect
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PointF
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View

import java.util.Objects
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

class DrawOverlayView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0) : View(context, attrs, defStyleAttr, defStyleRes) {
    private val mPath: Path

    private val mDocumentPaint: Paint
    private val mBorderPaint: Paint
    private val mFillPaint: Paint

    private val mNeedToDrawDocument: AtomicBoolean
    private val mNeedToDrawPoints: AtomicBoolean

    private val mWidth: AtomicInteger
    private val mHeight: AtomicInteger
    private val mQuadrangle: Rect

    private var mDrawDocumentPlaceholder = false

    // To customize
    // Document
    private val mDocumentColor = -0x1
    // Corners
    // -- common
    private val mStroke = 10.0f
    private val mPathRadius = 10
    // -- outer
    private val mBorderColor = -0xffff01
    private val mPadding = 20
    // -- inner
    private val mFillColor = -0x6c623101
    private val mAlpha = 100

    init {

        mPath = Path()

        mDocumentPaint = Paint()
        mDocumentPaint.style = Paint.Style.STROKE
        mDocumentPaint.strokeWidth = mStroke
        mDocumentPaint.strokeJoin = Paint.Join.ROUND
        mDocumentPaint.strokeCap = Paint.Cap.ROUND
        mDocumentPaint.pathEffect = CornerPathEffect(mPathRadius.toFloat())
        mDocumentPaint.color = mDocumentColor
        mDocumentPaint.isAntiAlias = true
        mDocumentPaint.isDither = true

        mBorderPaint = Paint()
        mBorderPaint.style = Paint.Style.STROKE
        mBorderPaint.strokeWidth = mStroke
        mBorderPaint.strokeJoin = Paint.Join.ROUND
        mBorderPaint.strokeCap = Paint.Cap.ROUND
        mBorderPaint.pathEffect = CornerPathEffect(mPathRadius.toFloat())
        mBorderPaint.color = mBorderColor
        mBorderPaint.isAntiAlias = true
        mBorderPaint.isDither = true

        mFillPaint = Paint()
        mFillPaint.style = Paint.Style.FILL
        mFillPaint.color = mFillColor
        mFillPaint.alpha = mAlpha
        mFillPaint.isAntiAlias = true
        mFillPaint.isDither = true

        mNeedToDrawDocument = AtomicBoolean(false)
        mNeedToDrawPoints = AtomicBoolean(false)

        mWidth = AtomicInteger(0)
        mHeight = AtomicInteger(0)

        mQuadrangle = Rect()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
        val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

        mWidth.set(widthSize)
        mHeight.set(heightSize)

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (mDrawDocumentPlaceholder) {
            drawDocument()
            mDrawDocumentPlaceholder = false
        }
        if (mNeedToDrawDocument.get()) {
            canvas.drawRect(mQuadrangle, mDocumentPaint)
        }
    }

    fun drawDocumentPlaceholder(drawDocumentPlaceholder: Boolean) {
        mDrawDocumentPlaceholder = drawDocumentPlaceholder
    }

    private fun drawDocument() {

        val viewW = mWidth.toFloat().toDouble()
        val viewH = mHeight.toFloat().toDouble()
        val viewAR = viewW / viewH
        val docAR = 1.58 //(idSelected) ? 1.58 : 1.42;
        val min = if (viewW < viewH) viewW else viewH
        val shortSide = min * 80 / 100
        val computedSide = if (viewAR > 1.0) shortSide * docAR else shortSide / docAR

        val w = if (viewW < viewH) shortSide else computedSide
        val h = if (viewW < viewH) computedSide else shortSide

        val offsetX = ((viewW - w) / 2).toInt()
        val offsetY = ((viewH - h) / 2).toInt()
        mQuadrangle.set(offsetX, offsetY, offsetX + w.toInt(), offsetY + h.toInt())

        mNeedToDrawDocument.set(true)

        post { invalidate() }
    }

    fun clear() {
        clearDocument()
    }

    fun clearDocument() {
        mNeedToDrawDocument.set(true)
        mQuadrangle.set(0, 0, 0, 0)
        post { invalidate() }
    }

    internal fun getQuadrangle():Rect{
        return mQuadrangle
    }
}
