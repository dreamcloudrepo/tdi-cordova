/**
 * TdiSdkAgent.swift
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

import Foundation
import TDI
import main
import IDV_DOC
import Instabug

@objc class TdiSdkAgent: UIViewController {
    var tdiMobileSdk : TdiMobileSdk!
    private var m_session: Session? = nil
    private var m_isNotifyCaptor: Bool = false
    private var m_captorType: String = ""
    private var m_isLicenseApiUsed: Bool = false
    private var m_profileApiCall: Bool = false
    
    @objc var m_callbackId: String?
    @objc var m_commandDelegate: CDVCommandDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func wrapCR(){
        IDVDOCCaptureSDK.wrapCrashReporting({
            Instabug.init()
        })
    }
    
    func tdiSdkInit(_view: UIViewController, _idv_license: String) {
        PLog(self, "TDI init")
        tdiMobileSdk = TdiMobileSdk.shared
        tdiMobileSdk.setViewController(myViewController: _view)
        tdiMobileSdk.delegate = self
        
        wrapCR()
        
        var pluginResult: CDVPluginResult? = nil

        if let _:TdiMobileSdk = tdiMobileSdk {
            if (!_idv_license.isEmpty) {
                tdiMobileSdk.setLicenses(licenses: ["EXTRA_IDV_LICENSE":_idv_license])
                m_isLicenseApiUsed = true
            }

            PLog(self, "TDI init OK")
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "TDI init OK")
        } else {
            PLog(self, "TDI init failed")
            pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "TDI init failed")
        }
        
        self.m_commandDelegate.send(pluginResult, callbackId: self.m_callbackId)
    }
    
    func getProfile(_base_url: String, _jwt_token: String, _tenant_id: String) {
        PLog(self, "TDI get profile: url= \(_base_url) tenant= \(_tenant_id)")
        if let _:TdiMobileSdk = tdiMobileSdk {
            tdiMobileSdk.getProfile(tdiServerURL: _base_url, jwtToken: _jwt_token, tenantId: _tenant_id)
            m_profileApiCall = true
        } else {
            PLog(self, "TDI not initialized!")
            self.m_commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "TDI not initialized!"), callbackId: self.m_callbackId)
        }
    }
    
    func newSession(
        _base_url: String,
        _scenario_name: String,
        _jwt_token: String,
        _tenant_id: String
    ) {
        PLog(self, "TDI new session: url= \(_base_url) scenario= \(_scenario_name) tenant= \(_tenant_id)")
        let server_tenant_id = ""
        var pluginResult: CDVPluginResult? = nil
        
        if let _:TdiMobileSdk = tdiMobileSdk {
            m_session = tdiMobileSdk.newSession(tdiServerURL: _base_url, scenarioName: _scenario_name, jwtToken: _jwt_token, tenantId: server_tenant_id, correlationId: _tenant_id)
            
            if (m_session != nil) {
                PLog(self, "TDI new session OK")
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "New session OK")
            } else {
                PLog(self, "TDI new session: Invalid session")
                pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "new session: Invalid session")
            }
        } else {
           PLog(self, "TDI not initialized!")
           pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "TDI not initialized!")
        }

        self.m_commandDelegate.send(pluginResult, callbackId: self.m_callbackId)
    }
    
    func sessionStart() {
        PLog(self, "TDI session start")
        if (m_session != nil) {
            do {
                try m_session?.start()
            } catch {
                PLog(self, error.localizedDescription)
            }
        } else {
            PLog(self, "TDI session start: Invalid session")
            self.m_commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "session start: Invalid session"), callbackId: self.m_callbackId)
        }
    }

    func sessionResume(_taskId: String) {
        PLog(self, "TDI session resume: task id= \(_taskId)")
        if (m_session != nil) {
            do {
                try m_session?.resume(taskId: _taskId)
            } catch {
                PLog(self, error.localizedDescription)
            }
            
        } else {
            PLog(self, "TDI session resume: Invalid session")
            self.m_commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "session resume: Invalid session"), callbackId: self.m_callbackId)
        }
    }

    func sessionResult() {
        PLog(self, "TDI session result")
        if (m_session != nil) {
            do {
                try m_session?.result()
            } catch {
                PLog(self, error.localizedDescription)
            }
            
        } else {
            PLog(self, "TDI session result: Invalid session")
            self.m_commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "session result: Invalid session"), callbackId: self.m_callbackId)
        }
    }
    
    func sessionUpdate(_taskId: String, _pages: Dictionary<String, Any>) {
        PLog(self, "TDI session update: task id= \(_taskId)  has pages= \(!_pages.isEmpty)")
        let pageData = KotlinMutableDictionary<NSString,NSString>.init()
        
        if (_pages.count > 0) {
            for (idx,metadata) in _pages {
                pageData[idx] = metadata
            }
        }
        
        let _input = InputImpl.init(pages: pageData)
        
        if (m_session != nil) {
            do {
                try m_session?.update(taskId: _taskId, input: _input)
            } catch {
                PLog(self, error.localizedDescription)
            }
            
        } else {
            PLog(self, "TDI session update: Invalid session")
            self.m_commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "session update: Invalid session"), callbackId: self.m_callbackId)
        }
    }

    func sessionStop() {
        PLog(self, "TDI session stop")
        if (m_session != nil) {
            do {
                try m_session?.stop()
            } catch {
                PLog(self, error.localizedDescription)
            }
            
            m_session = nil
            PLog(self, "TDI stop session OK")
            self.m_commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "Stop session OK"), callbackId: self.m_callbackId)
        } else {
            PLog(self, "TDI session stop: Invalid session")
            self.m_commandDelegate.send(CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: "Stop session: Invalid session"), callbackId: self.m_callbackId)
        }
    }
    
    func getTdiCaptor(_config: Dictionary<String,Any>) {        
        if (tdiMobileSdk != nil) {
            if let captureType = _config["CAPTURE_TYPE"] as? String {
                m_captorType = captureType
                if let captorCallback = _config["EXTRA_CAPTOR_CALLBACK"] as? Bool {
                    self.m_isNotifyCaptor = captorCallback
                } else {
                    self.m_isNotifyCaptor = false
                }
                
                let configData = KotlinMutableDictionary<NSString,AnyObject>.init()                
                if (_config.count > 0) {
                    for (idx,data) in _config {
                        if (idx != "EXTRA_CAPTOR_CALLBACK" && idx != "CAPTURE_TYPE") {
                            if (idx == "EXTRA_IDV_LICENSE" && m_isLicenseApiUsed) {
                                continue
                            } else {
                                configData[idx] = data
                            }
                        }
                    }
                }
                
                if (m_isNotifyCaptor) {
                    configData["EXTRA_CAPTOR_CALLBACK"] = self
                }
                
                let captor = tdiMobileSdk.captorProvider.get(name: m_captorType)
                captor?.config = configData
                if (m_isNotifyCaptor) {
                    captor?.execute(callback: self)
                }
            }
        }
    }
    
    func getSdkVersion() -> String {
        var sdkVer = ""
        if (tdiMobileSdk != nil) {
            sdkVer = tdiMobileSdk.getVersion()
        }
        return sdkVer
    }
}

extension TdiSdkAgent : CaptorCallback {
    func onDataCaptured(input: Input) {
        PLog(self, "\(m_captorType) Captor OK")
        let jsonObject: [String:Any] = ["pages": input.pages]
        var pluginResult: CDVPluginResult? = nil
        pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: jsonObject)
        self.m_commandDelegate.send(pluginResult, callbackId: self.m_callbackId)
    }
    func onFailure(failure: Failure) {
        let failMsg = "\(m_captorType) Captor failure. code=\(failure.errorCode) msg=\(failure.errorMessage)"
        PLog(self, failMsg)
        var pluginResult: CDVPluginResult? = nil
        pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR, messageAs: failMsg)
        self.m_commandDelegate.send(pluginResult, callbackId: self.m_callbackId)
    }
}

extension TdiSdkAgent : TdiMobileSdkDelegate {
    func taskStateListener(status _status: Status, task _task: NSMutableArray?, result _result: Result?, profileInfo _profileInfo: TdiProfileInfo?) {
        
        var mainJsonDict = [String:Any]()
        
        switch _status {
        case .created:
            PLog(self, "Session created")
        case .action:
            PLog(self, "Session in action")
        case .pending:
            PLog(self, "Session pending")
        case .completed:
            PLog(self, "Session completed")
        default:
            PLog(self, "invalid session")
        }        
        mainJsonDict["status"] = _status.name
		
		if let taskList = _task {
			PLog(self, "Tasks: \n \(taskList)")
			mainJsonDict["tasks"] = extractTasks(_taskList: taskList)
		}
      
        if let profile = _profileInfo, m_profileApiCall {
            PLog(self, "profileInfo: \n \(profile)")
            
            // Scenarios
            var proScenarios = [PScenarios]()
            var idx = 0
            
            for scene in profile.scenarios {
                let tmpScen: Scenario = scene as! Scenario
                var newCaptures = [PProfileCapture]()
                var idx1 = 0
                
                // Captures
                for cap in tmpScen.captures {
                    let tmpCap: ProfileCapture = cap as! ProfileCapture
                    var newSteps = [PCapture]()
                    var idx2 = 0
                    
                    for tmpStep in tmpCap.steps {
                        let tmpCap1 = tmpStep as! Capture
                        var tempCaptures = [[String:String]]()
                        
                        for tmpCap2 in tmpCap1.captures {
                            let tmpCapDict: [String:String] = tmpCap2 as! [String:String]
                            var idx3 = 0
                            
                            if (idx3 < tmpCap1.captures.count) {
                                tempCaptures.insert(tmpCapDict, at: idx3)
                                idx3 += 1
                            }
                        }
                        
                        if (idx2 < tmpCap.steps.count) {
                            newSteps.insert(
                                PCapture(
                                    captures: tempCaptures
                                ),
                                at: idx2
                            )
                            idx2 += 1
                        }
                    }
                    
                    if (idx1 < tmpScen.captures.count) {
                        newCaptures.insert(
                            PProfileCapture(
                                name: tmpCap.name,
                                isSkipAllowed: tmpCap.isSkipAllowed,
                                returnRisk: tmpCap.returnRisk,
                                idpIdentifier: tmpCap.idpIdentifier,
                                response: tmpCap.response as? [String],
                                steps: newSteps
                            ),
                            at: idx1
                        )
                        idx1 += 1
                    }
                }
                
                // Responses
                var newResponses = [PResponsesCategory]()
                let tmpRspCat = tmpScen.responses as! [ResponsesCategory]
                var idx4 = 0
                
                for rspCat in tmpRspCat {
                    
                    let tmpRspCatField = rspCat.fields as! [ResponsesCategoryField]
                    var newRspCatField = [PResponsesCategoryField]()
                    var idx5 = 0
                    
                    for resp2 in tmpRspCatField {
                        if (idx5 < tmpRspCatField.count) {
                            newRspCatField.insert(
                                PResponsesCategoryField(
                                    name: resp2.name,
                                    field: PField(
                                        format: resp2.field.format,
                                        isDisplayed: resp2.field.isDisplayed,
                                        mandatory: resp2.field.mandatory,
                                        pattern: resp2.field.pattern,
                                        editable: resp2.field.editable as? [String]
                                    )
                                ),
                                at: idx5
                            )
                            idx5 += 1
                        }
                    }
                    
                    if (idx4 < tmpRspCat.count) {
                        newResponses.insert(
                            PResponsesCategory(
                                name: rspCat.name,
                                fields: newRspCatField
                            ),
                            at: idx4
                        )
                        idx4 += 1
                    }
                }
                
                // Infodata
                var newInfoData = [String:String]()
                if let tmpInfo = tmpScen.infoData! as? [String:Any] {
                    for (rKey1,rVal1) in tmpInfo {
                        if let info = rVal1 as? String {
                            newInfoData.updateValue(info, forKey: rKey1)
                        }
                    }
                }
                
                let newIdx: String? = tmpScen.idx
                let newName: String? = tmpScen.name
                
                if (idx < profile.scenarios.count) {
                    proScenarios.insert(
                        PScenarios(
                            idx: newIdx,
                            name: newName,
                            captures: newCaptures,
                            responses: newResponses,
                            infoData: newInfoData
                        ),
                        at: idx
                    )
                    idx += 1
                }
            }
            
            // ProfileCaptor
            var proCaptor = [String:PProfileCaptor]()
            for (idx,data) in profile.captors {
                let tmpProfCaptor = data as? ProfileCaptor
                var tempMeta: [String:Any]? = nil
                if let tmpMetaData = tmpProfCaptor?.metadata as? [String:Any] {
                    tempMeta = [String:Any]()
                    for (mKey,mVal) in tmpMetaData {
                        if let dictTree = extractDictionaries(_val: mVal) {
                            tempMeta?.updateValue(dictTree, forKey: mKey)
                        }
                    }
                }
                                
                proCaptor.updateValue(
                    PProfileCaptor(
                        pages: tmpProfCaptor?.pages as? [String],
                        metadata: tempMeta,
                        isUploadAllowed: tmpProfCaptor?.isUploadAllowed,
                        category: tmpProfCaptor?.category),
                    forKey: idx as! String
                )
            }
            
            // ProfileConfigs
            let proConfig = PProfileConfigs(
                k1: profile.configs.k1,
                k1m: profile.configs.k1m,
                k1e: profile.configs.k1e,
                httpTimeoutSecond: profile.configs.httpTimeoutSecond,
                httpMaxRetryNum: profile.configs.httpMaxRetryNum,
                httpRetryDelaySecond: profile.configs.httpRetryDelaySecond,
                pushNotifTimeoutSecond: profile.configs.pushNotifTimeoutSecond,
                pullMaxRetryNum: profile.configs.pullMaxRetryNum,
                lang: profile.configs.lang,
                sizeLimit: profile.configs.sizeLimit,
                enableRisk: profile.configs.enableRisk
            )
            
            // Theme
            var proTheme: [String:Any]? = nil
            if let tmpMetaData = profile.theme as? [String:Any] {
                proTheme = [String:Any]()
                for (mKey,mVal) in tmpMetaData {
                    if let dictTree = extractDictionaries(_val: mVal) {
                        proTheme?.updateValue(dictTree, forKey: mKey)
                    }
                }
            }
            
            // Localization
            var proLocale: [String:Any]? = nil            
            if let tmpMetaData = profile.localization as? [String:Any] {
                proLocale = [String:Any]()
                for (mKey,mVal) in tmpMetaData {
                    if let dictTree = extractDictionaries(_val: mVal) {
                        proLocale?.updateValue(dictTree, forKey: mKey)
                    }
                }
            }
            
            let newProfile = PTdiProfileInfo(
                version: profile.version,
                scenarios: proScenarios,
                captors: proCaptor,
                configs: proConfig,
                theme: nil,
                localization: proLocale
            )
            
            var profileInfoJsonStr = ""
            let encoder = JSONEncoder()
//            encoder.outputFormatting = .prettyPrinted
            
            do {
                let data = try encoder.encode(newProfile)
                profileInfoJsonStr = String(data: data, encoding: .utf8)!
                PLog(self, "profile json: \n \(profileInfoJsonStr)")
            } catch {
                PLog(self, "JSON encode error")
            }
            
            mainJsonDict["profile"] = profileInfoJsonStr
            
            m_profileApiCall = false
        }
        
        var callbkStatus = CDVCommandStatus_OK
        
        if let resultValue = _result {
            PLog(self, resultValue)
            
            var tempDesc = [String:Any]()
            if let tmpMetaData = resultValue.description_ {
                for (mKey,mVal) in tmpMetaData {
                    if let dictTree = extractDictionaries(_val: mVal) {
                        tempDesc.updateValue(dictTree, forKey: mKey as! String)
                    }
                }
            }
            
            let resultDict : [String: Any] = [
                "code": resultValue.code as Any,
                "message": resultValue.message as Any,
                "description": tempDesc,
            ]
            
            mainJsonDict["result"] = resultDict
            /* TODO:
            if let error = resultValue.code {
                if (Int(error) != 0) {
                    callbkStatus = CDVCommandStatus_ERROR
                }
            }*/
        }
        
        let pluginResult = CDVPluginResult(status: callbkStatus, messageAs: mainJsonDict)
        self.m_commandDelegate.send(pluginResult, callbackId: self.m_callbackId)
    }
    
    private func extractDictionaries(_val: Any) -> Any? {
        if let mainDict = _val as? Dictionary<String, Any> {
            var childDict = Dictionary<String, Any>()
            for (dKey,dVal) in mainDict {
                childDict.updateValue(extractDictionaries(_val: dVal) as Any, forKey: dKey)
            }
            return childDict
        } else {
            if let strVal = _val as? String {
                return strVal
            }
        }
        return nil
    }
    
    private func extractTasks(_taskList: NSMutableArray) -> Any  {
        var taskArray: [[String: Any]] = [[String: Any]]()
        var arrIdx = 0
        for task in _taskList {
            var tempJsonObj : [String: Any] = [:]
            if let tempTask: TaskImpl = task as? TaskImpl {
                tempJsonObj.updateValue(tempTask.id, forKey: "id")
                tempJsonObj.updateValue(tempTask.name as Any, forKey: "name")
                tempJsonObj.updateValue(tempTask.status as Any, forKey: "status")
                tempJsonObj.updateValue(tempTask.category as Any, forKey: "category")
            }
            if (arrIdx < _taskList.count) {
                taskArray.insert(tempJsonObj, at: arrIdx)
                arrIdx += 1
            }
        }
        return taskArray
    }
}
