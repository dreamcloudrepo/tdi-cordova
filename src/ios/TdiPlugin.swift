/**
 * TdiPlugin.swift
 * Cordova plugin wrapper for TDI SDK.
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

import Foundation
import UIKit

@objc(TdiPlugin)
class TdiPlugin: CDVPlugin {
    private var tdiSdkAgent: TdiSdkAgent?
    private var customCaptor: CustomCaptor?
    private var m_callbackId: String?
    
    override func pluginInitialize() {
        tdiSdkAgent = TdiSdkAgent()
    }
    
    @objc(tdiSdkInit:)
    func tdiSdkInit(_ command: CDVInvokedUrlCommand?) {
        PLog(self, "tdiSdkInit")
        tdiSdkAgent?.m_commandDelegate = self.commandDelegate
        tdiSdkAgent?.m_callbackId = command?.callbackId
        tdiSdkAgent?.tdiSdkInit(
            _view: self.viewController,
            _idv_license: command?.argument(at: 0) as? String ?? ""
        )
    }
    
    @objc(getProfile:)
    func getProfile(_ command: CDVInvokedUrlCommand?) {
        PLog(self, "getProfile")
        tdiSdkAgent?.m_commandDelegate = self.commandDelegate
        tdiSdkAgent?.m_callbackId = command?.callbackId
        tdiSdkAgent?.getProfile(
            _base_url: command?.argument(at: 0) as! String,
            _jwt_token: command?.argument(at: 1) as! String,
            _tenant_id: command?.argument(at: 2) as! String
        )
    }
    
    @objc(newSession:)
    func newSession(_ command: CDVInvokedUrlCommand?) {
        pluginVersion()
        PLog(self, "newSession")
        tdiSdkAgent?.m_commandDelegate = self.commandDelegate
        tdiSdkAgent?.m_callbackId = command?.callbackId
        tdiSdkAgent?.newSession(
            _base_url: command?.argument(at: 0) as! String,
            _scenario_name: command?.argument(at: 1) as! String,
            _jwt_token: command?.argument(at: 2) as! String,
            _tenant_id: command?.argument(at: 3) as! String
        )
    }
    
    @objc(sessionStart:)
    func sessionStart(_ command: CDVInvokedUrlCommand?) {
        PLog(self, "sessionStart")
        tdiSdkAgent?.m_commandDelegate = self.commandDelegate
        tdiSdkAgent?.m_callbackId = command?.callbackId
        tdiSdkAgent?.sessionStart()
    }
    
    @objc(sessionResume:)
    func sessionResume(_ command: CDVInvokedUrlCommand?) {
        PLog(self, "sessionResume")
        tdiSdkAgent?.m_commandDelegate = self.commandDelegate
        tdiSdkAgent?.m_callbackId = command?.callbackId
        tdiSdkAgent?.sessionResume(
            _taskId: command?.argument(at: 0) as! String
        )
    }
    
    @objc(sessionResult:)
    func sessionResult(_ command: CDVInvokedUrlCommand?) {
        PLog(self, "sessionResult")
        tdiSdkAgent?.m_commandDelegate = self.commandDelegate
        tdiSdkAgent?.m_callbackId = command?.callbackId
        tdiSdkAgent?.sessionResult()
    }
    
    @objc(sessionUpdate:)
    func sessionUpdate(_ command: CDVInvokedUrlCommand?) {
        PLog(self, "sessionUpdate")
        tdiSdkAgent?.m_commandDelegate = self.commandDelegate
        tdiSdkAgent?.m_callbackId = command?.callbackId
        tdiSdkAgent?.sessionUpdate(
            _taskId: command?.argument(at: 0) as! String,
            _pages: command?.argument(at: 1) as! Dictionary<String, Any>
        )
    }
    
    @objc(sessionStop:)
    func sessionStop(_ command: CDVInvokedUrlCommand?) {
        PLog(self, "sessionStop")
        tdiSdkAgent?.m_commandDelegate = self.commandDelegate
        tdiSdkAgent?.m_callbackId = command?.callbackId
        tdiSdkAgent?.sessionStop()
    }
    
    @objc(getCaptor:)
    func getCaptor(_ command: CDVInvokedUrlCommand?) {
        PLog(self, "getCaptor")
        self.m_callbackId = command?.callbackId
        let capConfig = command?.argument(at: 0) as! Dictionary<String, Any>
        
        guard capConfig.count > 0 else {
            return
        }
        
        for (key, value) in capConfig {
            PLog(self, "JS data: key=\(key) val=\(value)")
        }
        
        if let captureType = capConfig["CAPTURE_TYPE"] {
            if captureType as! String == "Custom" {
                getCustomCaptor(config: capConfig)
            } else {
                getTdiCaptor(config: capConfig)
            }
        }        
    }
    
    private func getTdiCaptor(config: Dictionary<String,Any>) {
        PLog(self, "getTdiCaptor")
        tdiSdkAgent?.m_callbackId = self.m_callbackId
        tdiSdkAgent?.getTdiCaptor(_config: config)
    }
    
    private func getCustomCaptor(config: Dictionary<String,Any>) {
        PLog(self, "getCustomCaptor")
        let storyBoard: UIStoryboard = UIStoryboard(name: "Captor", bundle: nil)
        let captor = storyBoard.instantiateViewController(withIdentifier: "CustomCaptor") as? CustomCaptor
        captor?.m_commandDelegate = self.commandDelegate
        captor?.m_callbackId = self.m_callbackId
        captor?.prepareCaptor(_config: config)
        captor?.modalPresentationStyle = .fullScreen
        self.viewController?.present(captor!, animated: true, completion: nil)
    }

    private func pluginVersion() {
        let verStr = plugin_version
        let sdkVer = tdiSdkAgent?.getSdkVersion() ?? ""
        PLog(self, "TDI-\(verStr)-\(sdkVer)")
    }
}
