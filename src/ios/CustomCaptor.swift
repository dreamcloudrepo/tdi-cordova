/**
 * CustomCaptor.swift
 * Custom captor using IDV SDK.
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

import Foundation
import IDV_DOC
import main

let DOCUMENT_TD1 = IDVDOCDocument.init(width: 85.6, andHeight: 53.98)
let DOCUMENT_TD2 = IDVDOCDocument.init(width: 105.00, andHeight: 74.00)
let DOCUMENT_TD3 = IDVDOCDocument.init(width: 125.00, andHeight: 88.00)

let DOCUMENT_MODE_IDDOCUMENT : [IDVDOCDocument?] = [DOCUMENT_TD1, DOCUMENT_TD2]
let DOCUMENT_MODE_PASSPORT : [IDVDOCDocument?] = [DOCUMENT_TD3]
let DOCUMENT_MODE_ICAO : [IDVDOCDocument?] = [DOCUMENT_TD1, DOCUMENT_TD2, DOCUMENT_TD3]

class CustomCaptor : UIViewController, IDVDOCStartCallback  {
    @IBOutlet weak var camera : UIView!
    @IBOutlet weak var captureImageView : UIImageView!
    @IBOutlet weak var confirmBox : UIView!
    @IBOutlet weak var drawOverlay : DrawOverlayVw!
    @IBOutlet weak var lblFlashMessage : UILabel!
    @IBOutlet weak var backButton : UIButton!
    @IBOutlet weak var shutterButton : UIButton!
    @IBOutlet weak var feedbackText : UILabel!
    @IBOutlet weak var feedbackText2 : UILabel!
    @IBOutlet weak var buttonsStackview : UIStackView!
    @IBOutlet weak var cropButton: UIButton!
    @IBOutlet weak var cropCancelBtn: UIButton!
    // CropViews
    @IBOutlet weak var cropGroupView : UIView!
    @IBOutlet weak var cropView: CropVw!
    @IBOutlet weak var cropImagePreview : UIImageView!
        
    private var edgeMode: IDVDOCDetectionMode!
    private var blurMode : BlurMode!
    private var glareMode : GlareMode!
    private var darknessMode : DarknessMode!
    private var photocopyMode : PhotocopyMode!
    private var totalSide = 1
    private var imageArray = [Data?]()
    private var currentSide = 1
    private let sdk = IDVDOCCaptureSDK.init()
    private let config = IDVDOCConfiguration.init()
    private var lastCropPoints : IDVDOCQuadrangle?
    private var firstCapResult : IDVDOCCaptureResult?
    private var configuration = [String: Any]()
    private var isMachineLearning : Bool = false
    private var license : String = ""
    private var isOverLay : Bool = true
    private var jpegQuality : Float = 0.8 // 80 Percent
    private var m_input : InputImpl?
    private var m_pageDict = [AnyHashable:Any]()
    private var shutterTimer : Int = 10 // Seconds
    private var shutterPressed : Bool = false
    private var imageNotTaken : Bool = true
    private var isFeedbackOn : Bool = false
    private var countDownTimer: Timer? = nil
    
    private var feedbackStr1 : String = "Please verify that the document is not cropped, is clear without shadow, and text is visible"
    private var feedbackStr2 : String = "Confirm the capture?"
    
    private let EXTRA_CAPTURE_MODE = "EXTRA_CAPTURE_MODE"
    private let EXTRA_CAPTURE_NB_SIDES = "EXTRA_CAPTURE_NB_SIDES"
    private let EXTRA_CAPTURE_OVERLAY = "EXTRA_CAPTURE_OVERLAY"
    private let EXTRA_IDV_LICENSE = "EXTRA_IDV_LICENSE"
    private let EXTRA_CAPTURE_DETECTION_MODE = "EXTRA_CAPTURE_DETECTION_MODE"
    private let EXTRA_CAPTURE_QUALITY_CHECK_BLUR = "EXTRA_CAPTURE_QUALITY_CHECK_BLUR"
    private let EXTRA_CAPTURE_QUALITY_CHECK_GLARE = "EXTRA_CAPTURE_QUALITY_CHECK_GLARE"
    private let EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS = "EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS"
    private let EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY = "EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY"
    private let EXTRA_DOC_SIZE = "EXTRA_DOC_SIZE"
    private let EXTRA_CAPTURE_JPEG_COMPRESSION = "EXTRA_CAPTURE_JPEG_COMPRESSION"
    private let EXTRA_CAPTURE_SHUTTER_BUTTON_DELAY = "EXTRA_CAPTURE_SHUTTER_BUTTON_DELAY"
    
    @objc var m_callbackId: String?
    @objc var m_commandDelegate: CDVCommandDelegate!
    
    //MARK: - Views
    override func viewDidLoad() {
        captureImageView.isHidden = true
        cropGroupView.isHidden = true
        cropButton.isHidden = true
        shutterButton.isHidden = true
        
        // Setup qualityText box
        lblFlashMessage.layer.borderWidth = 1.0
        lblFlashMessage.layer.borderColor = UIColor.white.cgColor
        showHideFlashMessage(hide: true)
        
        // Setup confirm box
        confirmBox.isHidden = true
        confirmBox.layer.borderWidth = 1.0
        confirmBox.layer.borderColor = UIColor.white.cgColor
        
        backButton.setTitle("BACK".localized, for: .normal)
        cropCancelBtn.setTitle("CANCEL".localized, for: .normal)
        feedbackText.text = feedbackStr1.localized
        feedbackText2.text = feedbackStr2.localized
        
        configDocument()
        configCamera()
        initializeCamera()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            UINavigationController.attemptRotationToDeviceOrientation()
        }
        sdk.finish()
    }
    
    override public var shouldAutorotate: Bool {
        return false
    }

    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    //MARK: - Inits

    func configDocument() {
        if let numberOfPages = configuration[EXTRA_CAPTURE_NB_SIDES] as? Int {
            totalSide = numberOfPages
        } else {
            totalSide = 1
        }
        
        if let customSize = configuration[EXTRA_DOC_SIZE] as? Dictionary<String,Any> {
            PLog(self, customSize.debugDescription)
            
            var customWidth: Float = 0.0
            if let width = customSize["Width"] as? NSNumber {
                customWidth = width.floatValue
            }
            
            var customHeight: Float = 0.0
            if let height = customSize["Height"] as? NSNumber {
                customHeight = height.floatValue
            }
            PLog(self, "Custom doc: w:\(customWidth) h:\(customHeight)")
            let docSizeList = [IDVDOCDocument.init(width: customWidth, andHeight: customHeight),
                               DOCUMENT_TD1, DOCUMENT_TD2, DOCUMENT_TD3]
            config.captureDocuments = docSizeList as? [IDVDOCDocument]
        } else {
            config.captureDocuments = DOCUMENT_MODE_ICAO as? [IDVDOCDocument]
        }
    }
    
    func prepareCaptor(_config: Dictionary<String, Any>) {
        configuration = _config
         
        if let mode = configuration[EXTRA_CAPTURE_DETECTION_MODE] as? Int {
            switch IDVDOCDetectionMode(rawValue: mode) {
            case .DetectionImageProcessing:
                edgeMode = IDVDOCDetectionMode.DetectionImageProcessing
                isMachineLearning = false
            case .DetectionMachineLearning:
                edgeMode = IDVDOCDetectionMode.DetectionMachineLearning
                isMachineLearning = true
            default:
                edgeMode = IDVDOCDetectionMode.DetectionDisabled
                isMachineLearning = false
            }
        } else {
            edgeMode = IDVDOCDetectionMode.DetectionMachineLearning
            isMachineLearning = true
        }
        
        if let blur = configuration[EXTRA_CAPTURE_QUALITY_CHECK_BLUR] as? Int {
            switch BlurMode(rawValue: blur) {
            case .relaxed:
                blurMode = BlurMode.relaxed
            case .strict:
                blurMode = BlurMode.strict
            case .disabled:
                blurMode = BlurMode.disabled
            default:
                blurMode = BlurMode.textBlock
            }
        } else {
            blurMode = BlurMode.textBlock
        }
        
        if let glare = configuration[EXTRA_CAPTURE_QUALITY_CHECK_GLARE] as? Int {
            switch GlareMode(rawValue: glare) {
            case .white:
                glareMode = GlareMode.white
            case .color:
                glareMode = GlareMode.color
            default:
                glareMode = GlareMode.disabled
            }
        } else {
            glareMode = GlareMode.white
        }
 
        if let darkness = configuration[EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS] as? Int {
            switch DarknessMode(rawValue: darkness) {
            case .relaxed:
                darknessMode = DarknessMode.relaxed
            default:
                darknessMode = DarknessMode.disabled
            }
        } else {
            darknessMode = DarknessMode.relaxed
        }
        
        if let photocopy = configuration[EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY] as? Int {
            switch PhotocopyMode(rawValue: photocopy) {
            case .blackAndWhite:
                photocopyMode = PhotocopyMode.blackAndWhite
            default:
                photocopyMode = PhotocopyMode.disabled
            }
        } else {
            photocopyMode = PhotocopyMode.disabled
        }
        
        if let quality = configuration[EXTRA_CAPTURE_JPEG_COMPRESSION] as? Float {
            jpegQuality = quality / 100.0
        }
        
        if let shutterBtnDelay = configuration[EXTRA_CAPTURE_SHUTTER_BUTTON_DELAY] as? Int {
            shutterTimer = shutterBtnDelay
        }
    }
    
    private func showHideFlashMessage(hide: Bool) {
        lblFlashMessage.isHidden = hide
        guard let labelMsg = lblFlashMessage.text else { return }
        if (labelMsg.isEmpty) { lblFlashMessage.isHidden = true }
    }
    
    func configCamera() {
        PLog(self, "IDV DOC SDK:\(IDVDOCCaptureSDK.version() ?? "") sides:\(totalSide) jpegQ:\(jpegQuality * 100) edge:\(edgeMode.rawValue) blur:\(blurMode.rawValue) glare:\(glareMode.rawValue) dark:\(darknessMode.rawValue) photo:\(photocopyMode.rawValue)")
        
        if let objLicense = configuration[EXTRA_IDV_LICENSE] as? String { license = objLicense }
        if let objOverLay = configuration[EXTRA_CAPTURE_OVERLAY] as? Bool { isOverLay = objOverLay }
        
        config.detectionMode = edgeMode
        config.qualityChecks.blurDetectionMode = blurMode
        config.qualityChecks.glareDetectionMode = glareMode
        config.qualityChecks.darknessDetectionMode = darknessMode
        config.qualityChecks.photocopyDetectionMode = photocopyMode
    }
    
    func initializeCamera() {
        PLog(self, "initialize Camera")
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized: // The user has previously granted access to the camera.
            init_sdk()
        case .notDetermined: // The user has not yet been asked for camera access.
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    self.init_sdk()
                }
            }
        case .denied: // The user has previously denied access.
            return
        case .restricted: // The user can't grant access due to restrictions.
            break
        @unknown default:
            return
        }
    }
    
    func init_sdk() {
        PLog(self, "Init SDK")
        sdk.`init`(license, view: camera, onInit: { completed, error in
            if (completed) {
                self.drawOverlay.draw(DocumentPlaceholder._TD1)
                DispatchQueue.main.async { [weak self] in
                    self?.drawOverlay.isHidden = !(self?.isOverLay ?? true)
                    self?.drawOverlay.setCameraFrame((self?.camera.frame)!)
                    self?.drawOverlay.setNeedsDisplay()
                }
                self.start_sdk()
            } else {
                PLog(self, "Error onInit: \(error)")
            }
        })
    }
    
    func start_sdk() {
        PLog(self, "Start SDK")
        // Clear cache
        if (lastCropPoints != nil) {
            lastCropPoints = nil
        }
        DispatchQueue.main.async {
            self.shutterSettings()
        }
        sdk.start(config, withBlock: self)
    }
    
    func shutterSettings() {
        if (countDownTimer != nil) {
            countDownTimer?.invalidate()
            countDownTimer = nil
        }
        
        PLog(self, "Shutter delay: \(shutterTimer) sec")
        
        if (shutterTimer > 0) { // 1 to X seconds
            var timeLeft = shutterTimer
            countDownTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { _ in
                timeLeft -= 1
//                PLog(self, "timeLeft= \(timeLeft)")
                if (timeLeft <= 0) {
                    self.countDownTimer?.invalidate()
                    if (self.imageNotTaken) {
                        self.shutterButton.isHidden = false
                    }
                } else {
                    self.shutterButton.isHidden = true
                }
            })
        } else if (shutterTimer < 0) { // -1
            shutterButton.isHidden = true
        } else { // 0
            shutterButton.isHidden = false
        }
    }

    //MARK: - Buttons Actions
    
    @IBAction func onShutterButtonClick(){
        shutterPressed = true
        sdk.triggerCapture()
    }

    @IBAction func onBackButtonClick() {
        PLog(self, "back pressed")
        let pluginResult = CDVPluginResult(
            status: CDVCommandStatus_ERROR,
            messageAs: "Failure: code=\(FailureCompanion.init().REASON_DOCUMENT_CAPTURE_ABORT) msg=action cancelled by user"
        )
        self.m_commandDelegate.send(pluginResult, callbackId:self.m_callbackId)
        sdk.stop()
        sdk.finish()
        self.dismiss(animated: true)
    }

    @IBAction func onCancelButtonClick() {
        PLog(self, "cancel pressed")
        camera.isHidden = false
        confirmBox.isHidden = true
        captureImageView.isHidden = true
        drawOverlay.isHidden = !self.isOverLay
        showHideFlashMessage(hide: false)
        imageArray.remove(at: currentSide-1) // remove image data from buffer when user cancels
        imageNotTaken = true
        sdk.finish()
        init_sdk()
    }
    
    @IBAction func onOKButtonClick() {
        PLog(self, "ok pressed: user accepted page= \(currentSide)")
        
        if imageArray.count > 0 {
            m_pageDict["page\(currentSide)"] = imageArray[currentSide-1]?.base64EncodedString()
        }
        
        if currentSide < totalSide { // Next side
            currentSide += 1
            camera.isHidden = false
            confirmBox.isHidden = true
            lblFlashMessage.text = "Turn the document over".localized
            showHideFlashMessage(hide: false)
            captureImageView.isHidden = true
            cropButton.isHidden = true
            imageNotTaken = true
            sdk.finish()
            init_sdk()
        } else { // Finish
            var pages = Dictionary<String,Any>()
            pages["pages"] = m_pageDict
            let pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: pages
            )
            showHideFlashMessage(hide: true)
//            sdk.finish()
            // Delay callback to give enough time for sdk.finish()
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
                self.m_commandDelegate.send(pluginResult, callbackId:self.m_callbackId)
            }
            
            self.dismiss(animated: true)
        }
    }
    
    @IBAction func onCropDoneButtonClick(){
        let points = IDVDOCQuadrangle()
        points.topLeft = cropView.getTopLeft()
        points.topRight = cropView.getTopRight()
        points.bottomRight = cropView.getBottomRight()
        points.bottomLeft = cropView.getBottomLeft()
        lastCropPoints = points
        
        if let img = firstCapResult?.fullFrame {
            PLog(self, "Got img data")
            if let result = sdk.cropFrame(img, with: points, configuration: config) {
                PLog(self, "Got result from crop")
                DispatchQueue.main.async {
                    self.cropGroupView.isHidden = true
                }
                capturedImage(capturedFrame: result.cropFrame, isBlur: result.qualityCheckResults.blur)
            }
        } else {
            showAlert(title: "Warning".localized, message: "No frame detected to be croppped, no changes applied".localized)
        }
    }
        
    @IBAction func onCropButtonClick() {
        if let result = firstCapResult {
            if (result.cropFrame == result.fullFrame) {
                manualCropView(result.fullFrame)
            } else {
                autoCropView(result)
            }
        }
    }
    
    @IBAction func onCropCancelButtonClick() {
        cropGroupView.isHidden = true
        backButton.isHidden = false
        captureImageView.isHidden = false
        confirmBox.isHidden = false
    }
    
    //MARK:- Others
    //Crop image with rect
    private func showAlert(title:String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { action in
            self.cropGroupView.isHidden = true
            self.confirmBox.isHidden = false
            self.backButton.isHidden = false
        }))
        
        DispatchQueue.main.async {
            self.present(alert, animated: false, completion: nil)
        }
    }
    
    func manualCropView(_ fullFrame: Data){
        let placeholderRect = drawOverlay.getQuadrangle()
        let width = placeholderRect.width
        let height = placeholderRect.height
    
        let overlayCenter = CGPoint(x: width / 2.0, y: height / 2.0)
        let left = overlayCenter.x - width/3.0
        let top = overlayCenter.y - height/3.0
        let right = overlayCenter.x + width/3.0
        let bottom = overlayCenter.y + height/3.0
        
        DispatchQueue.main.async {
            self.confirmBox.isHidden = true
            self.backButton.isHidden = true
            self.captureImageView.isHidden = false
            
            if let image = UIImage(data: fullFrame) {
                PLog(self, "Man crop img size: \(String(describing: image.size))")
                let screen = UIScreen.main.bounds
                let newImgSz = CGSize(width: screen.size.width, height: screen.size.height)
                PLog(self, "Man crop new img size: \(String(describing: newImgSz))")
                self.cropView.updateFrame(image.size)

                var topLeft = CGPoint(x: left/width, y: top/height)
                var topRight = CGPoint(x: right/width , y: top/height)
                var bottomRight = CGPoint(x: right/width, y: bottom/height)
                var bottomLeft = CGPoint(x: left/width, y: bottom/height)
                
                if let lastPoints = self.lastCropPoints {
                    topLeft = lastPoints.topLeft
                    topRight = lastPoints.topRight
                    bottomRight = lastPoints.bottomRight
                    bottomLeft = lastPoints.bottomLeft
                }
                
                self.cropView.setDocumentCornersWithTopLeft(
                    topLeft,
                    topRight: topRight,
                    bottomRight: bottomRight,
                    andBottomLeft: bottomLeft
                )
                
                self.cropImagePreview.image = image.getResizedImage(newSize: newImgSz)
            }
            
            self.cropImagePreview.isHidden = false
            self.cropGroupView.isHidden = false
        }
    }
    
    func autoCropView(_ result: IDVDOCCaptureResult){
        DispatchQueue.main.async {
            self.confirmBox.isHidden = true
            self.backButton.isHidden = true
            self.captureImageView.isHidden = false
            
            if let image = UIImage(data: result.fullFrame) {
                PLog(self, "Auto crop img size: \(String(describing: image.size))")
                let screen = UIScreen.main.bounds
                let newImgSz = CGSize(width: screen.size.width, height: screen.size.height)
                PLog(self, "Auto crop new img size: \(String(describing: newImgSz))")
                self.cropView.updateFrame(newImgSz)
                
                var topLeft = result.quadrangle.topLeft
                var topRight = result.quadrangle.topRight
                var bottomRight = result.quadrangle.bottomRight
                var bottomLeft = result.quadrangle.bottomLeft
                
                if let lastPoints = self.lastCropPoints {
                    topLeft = lastPoints.topLeft
                    topRight = lastPoints.topRight
                    bottomRight = lastPoints.bottomRight
                    bottomLeft = lastPoints.bottomLeft
                }
                
                self.cropView.setDocumentCornersWithTopLeft(
                    topLeft,
                    topRight: topRight,
                    bottomRight: bottomRight,
                    andBottomLeft: bottomLeft
                )
                
                self.cropImagePreview.image = image.getResizedImage(newSize: newImgSz)
            }
            
            self.cropImagePreview.isHidden = false
            self.cropGroupView.isHidden = false
        }
    }

    func onProcessedFrame(_ partial: IDVDOCCaptureResult!) {
        if !isFeedbackOn {
            var qualityText = ""

            if partial.deviceStatusInfo.adjustingFocus {
                qualityText = "Auto Focus".localized
            } else if partial.deviceStatusInfo.adjustingExposure {
                qualityText = "Auto Exposure".localized
            } else if partial.deviceStatusInfo.adjustingWhiteBalance {
                qualityText = "Auto WhiteBalance".localized
            }

            if (!partial.qualityCheckResults.all && qualityText.isEmpty) {
                if partial.qualityCheckResults.glare {
                    qualityText = "Glare detected".localized
                } else if partial.qualityCheckResults.blur {
                    qualityText = "Blur detected".localized
                } else if partial.qualityCheckResults.darkness {
                    qualityText = "Shadow detected".localized
                } else if partial.qualityCheckResults.photocopy {
                    qualityText = "Photocopy detected".localized
                } else if partial.qualityCheckResults.contrast {
                    if !self.isMachineLearning {
                        qualityText = "Low contrast".localized
                    }
                }
                else {
                    qualityText = ""
                }
            }

            DispatchQueue.main.async { [weak self] in
                if !qualityText.isEmpty {
                    self?.lblFlashMessage.text = qualityText
                    self?.showHideFlashMessage(hide: false)
                    self?.isFeedbackOn = true
                } else {
                    self?.showHideFlashMessage(hide: true)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1500)) {
                    self?.isFeedbackOn = false
                }
            }
        } else {
            DispatchQueue.main.async { [weak self] in
                self?.showHideFlashMessage(hide: true)
            }
        }
    }

    func onSuccess(_ result: IDVDOCCaptureResult!) {
        // TODO
        /*if (!shutterPressed && !shutterButton.isHidden) { // Auto capture
//            PLog(self, "Auto capture skipped. Starting again the capture process...");
            sdk.start(config, withBlock: self)
            return
        }*/
        
        PLog(self, "Success")
        
        if (shutterPressed) {
            shutterPressed = false
            cropButton.isHidden = false
        }
        
        imageNotTaken = false
        
        if (result.cropFrame == nil && result.fullFrame != nil) {
            PLog(self, "No Crop frame")
            result.cropFrame = result.fullFrame
        }
        
        if let croppedImage = result.cropFrame {
            PLog(self, "Before compression : \(croppedImage.count / 1024) KB")
            firstCapResult = result
            capturedImage(capturedFrame: croppedImage, isBlur: result.qualityCheckResults.blur)
        } else {
            PLog(self, "onSuccess No Crop image")
        }
    }
    
    private func capturedImage(capturedFrame:Data, isBlur: Bool) {
        if let image = UIImage(data: capturedFrame) {
            DispatchQueue.main.async {
                self.backButton.isHidden = false
                self.captureImageView.image = image
                self.captureImageView.isHidden = false
                self.captureImageView.contentMode = .scaleAspectFit
                self.drawOverlay.setCameraFrame(self.captureImageView.frame)
                self.drawOverlay.isHidden = true
//                self.sdk.finish()
                self.camera.isHidden = true
                self.confirmBox.isHidden = false
                self.lblFlashMessage.text = ""
                self.feedbackText.text = isBlur ? "WARNING: Image seems blur".localized : self.feedbackStr1.localized
                self.showHideFlashMessage(hide: true)
                self.shutterButton.isHidden = true
            }
            
            checkImageSize(capturedImage: image)
        } else {
            PLog(self, "Cannot parse image data from IDV library into UIImage")
        }
    }
    
    func checkImageSize(capturedImage : UIImage) {
        if let imageData = capturedImage.jpegData(compressionQuality: CGFloat(jpegQuality)) {
            PLog(self, "After compression : \(imageData.count / 1024) KB")
            imageArray.insert(imageData, at: self.currentSide-1)
        }
    }
}
   
