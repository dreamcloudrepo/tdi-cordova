/**
 * CropVw.m
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

#import "CropVw.h"

#define GET_MID_POINT(p1, p2) CGPointMake(p1.x + (p2.x - p1.x)/2, p1.y + (p2.y - p1.y)/2)
#define EPSILON 0.00001
#define IS_ZERO(v) (fabs((double)v) < (double)EPSILON)
#define CHECK_DISTANCE(p1, p2, d) (sqrtf(pow(p1.x - p2.x, 2.0) + pow(p1.y - p2.y, 2.0)) <= d)

@implementation CropVw

- (void) dealloc
{
    NSLog(@"Dealloc recrop view");
}
- (void) deinitiallize
{
    m_editedCorner = nil;
    m_editedSide = nil;
    m_scrollView = nil;
}

#pragma mark - Trigonometry methods

-(line2d) lineFromPoints:(CGPoint) p1 and:(CGPoint) p2
{
    line2d ret;
    ret.m = 0;
    ret.n = 0;
    
    // (a*x + b*y + c = 0)
    ret.a = p2.y - p1.y;
    ret.b = p1.x - p2.x;
    ret.c = ret.a*p1.x + ret.b*p1.y;
    
    // (y = m*x + n)
    if(!IS_ZERO(ret.b))
    {
        ret.m = -ret.a/ret.b;
        ret.n = -ret.c/ret.b;
    }
    
    return ret;
}

- (CGPoint) getIntersectionPointFor:(line2d) line1 and:(line2d) line2
{
    CGPoint ret;
    // https://www.topcoder.com/community/data-science/data-science-tutorials/geometry-concepts-line-intersection-and-its-applications/
    double x = 0;
    double y = 0;
    double det = line1.a*line2.b - line2.a*line1.b;
    if(!IS_ZERO(det))
    {
        x = (line2.b*line1.c - line1.b*line2.c)/det;
        y = (line1.a*line2.c - line2.a*line1.c)/det;
    }
    ret.x = x;
    ret.y = y;
    return ret;
}

-(line2d) traslateLine:(line2d)line to:(CGPoint) point
{
    line2d l;
    l.a = line.a;
    l.b = line.b;
    l.c = line.a * point.x + line.b * point.y;
    return l;
}

- (float) getDistanceFor:(line2d) line and:(CGPoint) point
{
    float dist = 0;
    // https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
    double numerator = fabs((line.a * point.x) + (line.b * point.y) + line.c);
    double base = sqrt(line.a*line.a + line.b*line.b);
    if(!IS_ZERO(base))
    {
        dist = numerator / base;
    }
    return dist;
}

#pragma mark - Accessor methods

-(void)setDocumentCornersWithTopLeft:(CGPoint)p_topLeft
                            topRight:(CGPoint)p_topRight
                         bottomRight:(CGPoint)p_bottomRight
                       andBottomLeft:(CGPoint)p_bottomLeft
{
    m_topLeftCorner = p_topLeft;
    m_topRightCorner = p_topRight;
    m_bottomRightCorner = p_bottomRight;
    m_bottomLeftCorner = p_bottomLeft;

    m_topSide.side_point    = GET_MID_POINT(m_topLeftCorner, m_topRightCorner);
    m_topSide.move_corner_1 = &m_topLeftCorner;
    m_topSide.move_corner_2 = &m_topRightCorner;
    m_topSide.anchor_corner_1 = &m_bottomLeftCorner;
    m_topSide.anchor_corner_2 = &m_bottomRightCorner;
    
    m_rightSide.side_point    = GET_MID_POINT(m_topRightCorner, m_bottomRightCorner);
    m_rightSide.move_corner_1 = &m_topRightCorner;
    m_rightSide.move_corner_2 = &m_bottomRightCorner;
    m_rightSide.anchor_corner_1 = &m_topLeftCorner;
    m_rightSide.anchor_corner_2 = &m_bottomLeftCorner;
    
    m_bottomSide.side_point    = GET_MID_POINT(m_bottomLeftCorner, m_bottomRightCorner);
    m_bottomSide.move_corner_1 = &m_bottomLeftCorner;
    m_bottomSide.move_corner_2 = &m_bottomRightCorner;
    m_bottomSide.anchor_corner_1 = &m_topLeftCorner;
    m_bottomSide.anchor_corner_2 = &m_topRightCorner;
    
    m_leftSide.side_point    = GET_MID_POINT(m_topLeftCorner, m_bottomLeftCorner);
    m_leftSide.move_corner_1 = &m_topLeftCorner;
    m_leftSide.move_corner_2 = &m_bottomLeftCorner;
    m_leftSide.anchor_corner_1 = &m_topRightCorner;
    m_leftSide.anchor_corner_2 = &m_bottomRightCorner;
    
    m_userUpdate = false;
    
    m_editedCorner = nil;
    m_editedSide = nil;
    
    [self setNeedsDisplay];
}


-(NSArray*) getCropPoints{
   
    // return an array with the crop points, it will return points for 0,0 in case the crop view is not being displayed
    
    NSArray *allPoints = [NSArray arrayWithObjects:
          [NSValue valueWithCGPoint:m_topLeftCorner],
          [NSValue valueWithCGPoint:m_topSide.side_point],
          [NSValue valueWithCGPoint:m_topRightCorner],
          [NSValue valueWithCGPoint:m_bottomLeftCorner],
          [NSValue valueWithCGPoint:m_bottomSide.side_point],
          [NSValue valueWithCGPoint:m_bottomRightCorner],
          [NSValue valueWithCGPoint:m_leftSide.side_point],
          [NSValue valueWithCGPoint:m_rightSide.side_point],
          nil];
    
    return allPoints;
}

-(CGPoint)getTopLeft
{
    return m_topLeftCorner;
}

-(CGPoint)getTopRight
{
    return m_topRightCorner;
}


-(CGPoint)getBottomRight
{
    return m_bottomRightCorner;
}


-(CGPoint)getBottomLeft
{
    return m_bottomLeftCorner;
}

-(bool)isUserUpdate
{
    return (m_userUpdate);
}

-(void)setImageSize:(CGSize)p_size
{
    m_imgSize = p_size;
}

-(void)setImageScale:(float)p_scale
{
    m_imgScale = p_scale;
}

-(void)setScrollViewPointer:(UIScrollView*)p_scrollView
{
    m_scrollView = p_scrollView;
}

#pragma mark - Internal methods

//This method is called after a constrain changes the layout
- (void)layoutSubviews
{
    [super layoutSubviews];
    if(m_frame.size.width>0 && m_frame.size.height>0)
    {
        [self setFrame: m_frame];
    }
}

-(void) updateFrame:(CGSize)imgSize
{
    CGSize scrSize = self.frame.size;
    float scrAR = scrSize.width / scrSize.height;
    float imgAR = imgSize.width / imgSize.height;
    float decX = 0;
    float decY = 0;
    
    if (scrAR > imgAR) {
        decX += (int)(fabs((scrSize.width - (scrSize.height * imgAR)) / 2.0F));
    } else if (scrAR < imgAR) {
        decY += (int)(fabs((scrSize.height - (scrSize.width / imgAR)) / 2.0F));
    }
    
    m_frame = CGRectMake(decX, decY, scrSize.width - decX * 2.0F, scrSize.height - decY * 2.0F);
    [self setFrame:m_frame];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    UIColor *line_color = [UIColor colorWithHue:0.55 saturation:1 brightness:0.28 alpha:0.8];
    float line_width = 2;
    
    UIColor *circle_color = [UIColor colorWithHue:0.65 saturation:0.53 brightness:0.3 alpha:0.8];
    float circle_radio = 15;
    UIColor *selected_circle_color = [UIColor whiteColor];
    float selected_circle_radio = 40;
    
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Draw border
    float width = self.frame.size.width;
    float height = self.frame.size.height;
    
    CGContextSetStrokeColorWithColor(context, line_color.CGColor);
    CGContextSetLineWidth(context, line_width);
    CGContextMoveToPoint(context, m_topLeftCorner.x * width, m_topLeftCorner.y * height); //start at this point
    CGContextAddLineToPoint(context, m_topRightCorner.x * width, m_topRightCorner.y * height);
    CGContextAddLineToPoint(context, m_bottomRightCorner.x * width, m_bottomRightCorner.y * height);
    CGContextAddLineToPoint(context, m_bottomLeftCorner.x * width, m_bottomLeftCorner.y * height);
    CGContextAddLineToPoint(context, m_topLeftCorner.x * width, m_topLeftCorner.y * height);
    CGContextStrokePath(context);

    CGContextSetFillColorWithColor(context, circle_color.CGColor);
    
    float radio = circle_radio;
    
    
    if (m_editedCorner != nil) {
        CGContextSetFillColorWithColor(context, selected_circle_color.CGColor);
        radio = selected_circle_radio;
        CGContextFillEllipseInRect(context, CGRectMake(m_editedCorner->x * width - radio, m_editedCorner->y * height - radio, radio * 2, radio * 2));
    }

    CGContextSetFillColorWithColor(context, circle_color.CGColor);
    radio = circle_radio;
    if(m_editedCorner != &m_topLeftCorner)
        CGContextFillEllipseInRect(context, CGRectMake(m_topLeftCorner.x * width - radio, m_topLeftCorner.y * height - radio, radio * 2, radio * 2));
    if(m_editedCorner != &m_topRightCorner)
        CGContextFillEllipseInRect(context, CGRectMake(m_topRightCorner.x * width - radio, m_topRightCorner.y * height - radio, radio * 2, radio * 2));
    if(m_editedCorner != &m_bottomRightCorner)
        CGContextFillEllipseInRect(context, CGRectMake(m_bottomRightCorner.x * width - radio, m_bottomRightCorner.y * height - radio, radio * 2, radio * 2));
    if(m_editedCorner != &m_bottomLeftCorner)
        CGContextFillEllipseInRect(context, CGRectMake(m_bottomLeftCorner.x * width - radio, m_bottomLeftCorner.y * height - radio, radio * 2, radio * 2));
    
    // Side circles
    /*
    m_topSide.side_point    = GET_MID_POINT(m_topLeftCorner, m_topRightCorner);
    m_rightSide.side_point    = GET_MID_POINT(m_topRightCorner, m_bottomRightCorner);
    m_bottomSide.side_point    = GET_MID_POINT(m_bottomLeftCorner, m_bottomRightCorner);
    m_leftSide.side_point    = GET_MID_POINT(m_topLeftCorner, m_bottomLeftCorner);
    
    if (m_editedSide != nil) {
        CGContextSetFillColorWithColor(context, selected_circle_color.CGColor);
        radio = selected_circle_radio;
        CGContextFillEllipseInRect(context, CGRectMake(m_editedSide->side_point.x * width - radio, m_editedSide->side_point.y * height - radio, radio * 2, radio * 2));
    }
    
    CGContextSetFillColorWithColor(context, circle_color.CGColor);
    radio = circle_radio;
    if(m_editedSide != &m_topSide)
        CGContextFillEllipseInRect(context, CGRectMake(m_topSide.side_point.x * width - radio, m_topSide.side_point.y * height - radio, radio * 2, radio * 2));
    if(m_editedSide != &m_bottomSide)
        CGContextFillEllipseInRect(context, CGRectMake(m_bottomSide.side_point.x * width - radio, m_bottomSide.side_point.y * height - radio, radio * 2, radio * 2));
    if(m_editedSide != &m_leftSide)
        CGContextFillEllipseInRect(context, CGRectMake(m_leftSide.side_point.x * width - radio, m_leftSide.side_point.y * height - radio, radio * 2, radio * 2));
    if(m_editedSide != &m_rightSide)
        CGContextFillEllipseInRect(context, CGRectMake(m_rightSide.side_point.x * width - radio, m_rightSide.side_point.y * height - radio, radio * 2, radio * 2));
     */
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchBegan");
    CGPoint point = [[[touches allObjects] objectAtIndex:0] locationInView:self];
    [self checkPointInside:point];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint location = [[[touches allObjects] objectAtIndex:0] locationInView:self];
    
    float w = self.frame.size.width;
    float h = self.frame.size.height;
    float width = w;
    float height = h;
    
    CGFloat newX = location.x / width;
    CGFloat newY = location.y / height;
    
    // Clamp moves!!
    newX = fmax(fmin(newX, 1.0F), 0.0F);
    newY = fmax(fmin(newY, 1.0F), 0.0F);

    if (m_editedCorner != nil)
    {
        m_editedCorner->x = newX;
        m_editedCorner->y = newY;
        m_userUpdate = true;
        [self setNeedsDisplay];
    }
    else if (m_editedSide != nil)
    {
        CGPoint point = CGPointMake(newX, newY);
        line2d line = [self lineFromPoints:*m_editedSide->move_corner_1 and:*m_editedSide->move_corner_2];
        line2d line1 = [self lineFromPoints:*m_editedSide->move_corner_1 and:*m_editedSide->anchor_corner_1];
        line2d line2 = [self lineFromPoints:*m_editedSide->move_corner_2 and:*m_editedSide->anchor_corner_2];
        line2d lineMoved = [self traslateLine:line to:point];
        CGPoint pos1 = [self getIntersectionPointFor:lineMoved and:line1];
        CGPoint pos2 = [self getIntersectionPointFor:lineMoved and:line2];

        // TODO: We need to check that all the points stay inside the frame!!
        m_editedSide->move_corner_1->x = pos1.x;
        m_editedSide->move_corner_1->y = pos1.y;
        m_editedSide->move_corner_2->x = pos2.x;
        m_editedSide->move_corner_2->y = pos2.y;
        
        m_userUpdate = true;
        [self setNeedsDisplay];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    m_editedCorner = nil;
    m_editedSide = nil;
    [m_scrollView setScrollEnabled:TRUE];
    [self setNeedsDisplay];
    NSLog(@"touchesEnded");
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    m_editedCorner = nil;
    m_editedSide = nil;
    [m_scrollView setScrollEnabled:TRUE];
    [self setNeedsDisplay];
    NSLog(@"touchesCancelled");
}

- (BOOL)checkPointInside:(CGPoint)point
{
    float w = self.frame.size.width;
    float h = self.frame.size.height;
    
    float width = w;
    float height = h;
    float touchDist = 25/ width;
    
    point.x /= width;
    point.y /= height;

    if (CHECK_DISTANCE(m_topLeftCorner, point, touchDist))
    {
        m_editedCorner = &m_topLeftCorner;
        NSLog(@"Edit topLeft corner");
    }
    else if (CHECK_DISTANCE(m_topRightCorner, point, touchDist))
    {
        m_editedCorner = &m_topRightCorner;
        NSLog(@"Edit topRight corner");
    }
    else if (CHECK_DISTANCE(m_bottomRightCorner, point, touchDist))
    {
        m_editedCorner = &m_bottomRightCorner;
        NSLog(@"Edit bottomRight corner");
    }
    else if (CHECK_DISTANCE(m_bottomLeftCorner, point, touchDist))
    {
        m_editedCorner = &m_bottomLeftCorner;
        NSLog(@"Edit bottomLeft corner");
    }
    else if (CHECK_DISTANCE(m_topSide.side_point, point, touchDist))
    {
        m_editedSide = &m_topSide;
        NSLog(@"Edit top side");
    }
    else if (CHECK_DISTANCE(m_bottomSide.side_point, point, touchDist))
    {
        m_editedSide = &m_bottomSide;
        NSLog(@"Edit bottom side");
    }
    else if (CHECK_DISTANCE(m_rightSide.side_point, point, touchDist))
    {
        m_editedSide = &m_rightSide;
        NSLog(@"Edit right side");
    }
    else if (CHECK_DISTANCE(m_leftSide.side_point, point, touchDist))
    {
        m_editedSide = &m_leftSide;
        NSLog(@"Edit left side");
    }
    
    
    [m_scrollView setScrollEnabled: m_editedCorner == nil && m_editedSide == nil];
    return (m_editedCorner != nil) || (m_editedSide != nil);
}



@end
