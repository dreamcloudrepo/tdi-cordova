/**
 * PLog.swift
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

import Foundation

@inline(__always)
func PLog(_ _msg: Any) {
#if DEBUG
    print(_msg)
#endif
}

@inline(__always)
func PLog(_ _tag: Any, _ _msg: Any) {
#if DEBUG
    print("[\(String(describing: type(of: _tag)))] \(_msg)")
#endif
}
