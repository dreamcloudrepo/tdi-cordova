/**
 * DrawOverlayVw.h
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

#ifndef DrawOverlayVw_h
#define DrawOverlayVw_h
#import <UIKit/UIKit.h>

typedef NS_ENUM( NSInteger, DocumentPlaceholder ) {
    DocumentPlaceholder_TD1 = 0,
    DocumentPlaceholder_TD2 = 1,
    DocumentPlaceholder_TD3 = 2
};

@interface DrawOverlayVw : UIView

- (void)setCameraFrame:(CGRect)rect;
- (void)drawDocumentPlaceholder:(DocumentPlaceholder)placeholder;
- (void)drawLinePoints:(CGPoint)topLeft :(CGPoint)topRight :(CGPoint)bottomRight :(CGPoint)bottomLeft;
- (void)clear;
- (CGRect)getQuadrangle;

@end

#endif
