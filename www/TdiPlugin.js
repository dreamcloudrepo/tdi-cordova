
var exec = require('cordova/exec');
var PLUGIN_NAME = 'TdiPlugin';

exports.tdiSdkInit = function(LICENSE, successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'tdiSdkInit', [LICENSE]);
};

exports.getProfile = function(BASE_URL, JWT_TOKEN, TENANT_ID, successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'getProfile', [BASE_URL, JWT_TOKEN, TENANT_ID]);
};

exports.newSession = function(BASE_URL, SCENARIO_NAME, JWT_TOKEN, TENANT_ID, successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'newSession', [BASE_URL, SCENARIO_NAME, JWT_TOKEN, TENANT_ID]);
};

exports.sessionStart = function(successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'sessionStart', []);
};

exports.sessionResume = function(TASK_ID, successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'sessionResume', [TASK_ID]);
};

exports.sessionResult = function(successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'sessionResult', []);
};

exports.sessionUpdate = function(TASK_ID, PAGES, successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'sessionUpdate', [TASK_ID, PAGES]);
};

exports.sessionStop = function(successCall, errorCall) {
    exec(successCall, errorCall, PLUGIN_NAME, 'sessionStop', []);
};

exports.getCaptor = function(CONFIG, successCall, errorCall) {
	exec(successCall, errorCall, PLUGIN_NAME, 'getCaptor', [CONFIG]);
};

const generalConfig = Object.freeze({
    CAPTURE_TYPE: "CAPTURE_TYPE", // Refer doc for list of default captors from TDI
    EXTRA_CAPTOR_CALLBACK: "EXTRA_CAPTOR_CALLBACK", // Refer doc on captor callback section
    EXTRA_CAPTOR_SKIPPABLE: "EXTRA_CAPTOR_SKIPPABLE", // Optional config used in Non-ID/Voice captors to skip running of relevant workflow tasks
    EXTRA_CAPTURE_SHUTTER_BUTTON_DELAY: "EXTRA_CAPTURE_SHUTTER_BUTTON_DELAY",

    // Config for Passport, ID Card, ICAO
    EXTRA_IDV_LICENSE: "EXTRA_IDV_LICENSE", // IDV license string. Contact Thales to obtain a valid license.
    EXTRA_CAPTURE_OVERLAY: "EXTRA_CAPTURE_OVERLAY", // Guiding frame during Capture - true or false
    EXTRA_CAPTURE_NB_SIDES: "EXTRA_CAPTURE_NB_SIDES", // No of pages, applicable for ID Card and ICAO
    EXTRA_CAPTURE_FRONT_HINT: "EXTRA_CAPTURE_FRONT_HINT", // Optional, Display hint message to capture front side
    EXTRA_CAPTURE_DETECTION_MODE: "EXTRA_CAPTURE_DETECTION_MODE", // MachineLearning, ImageProcessing
    EXTRA_CAPTURE_QUALITY_CHECK_BLUR: "EXTRA_CAPTURE_QUALITY_CHECK_BLUR", // Strict, Relaxed, Disabled
    EXTRA_CAPTURE_QUALITY_CHECK_GLARE: "EXTRA_CAPTURE_QUALITY_CHECK_GLARE", // Color, White, Disabled
    EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS: "EXTRA_CAPTURE_QUALITY_CHECK_DARKNESS", // Relaxed, Disabled
    EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY: "EXTRA_CAPTURE_QUALITY_CHECK_PHOTOCOPY", // BlackAndWhite, Disabled

    // Config for Aware Liveness
    EXTRA_CAPTURE_WORKFLOW: "EXTRA_CAPTURE_WORKFLOW", // Charlie2, Charlie4, Charlie6
    EXTRA_CAPTURE_SAVE_JPEG_QUALITY: "EXTRA_CAPTURE_SAVE_JPEG_QUALITY", // to true,
    EXTRA_CAPTURE_TIME_OUT: "EXTRA_CAPTURE_TIME_OUT", // to 0.0,
    EXTRA_CAPTURE_GET_PORTRAIT: "EXTRA_CAPTURE_GET_PORTRAIT", // to true,

    // Config for Signature
    EXTRA_SCROLL_BOTTOM_MANDATORY: "EXTRA_SCROLL_BOTTOM_MANDATORY", // to true,

    // Config for Non-ID
    // EXTRA_IDV_LICENSE: "EXTRA_IDV_LICENSE", // IDV license string for Non-ID captor on iOS only. Contact Thales to obtain a valid license.
    EXTRA_CAPTURE_TIMEOUT: "EXTRA_CAPTURE_TIMEOUT", // to 120, set Timeout to 120s, 0 to disable timeout, default = 60s
    EXTRA_CAPTURE_RESOLUTION: "EXTRA_CAPTURE_RESOLUTION", // to 4, Image resolution in mega pixels, default= 4MP
    EXTRA_CAPTURE_QUALITY: "EXTRA_CAPTURE_QUALITY", // to 90, JPEG compression quality, default= 80

    // Config for MRZ
    EXTRA_MRZ_DISPLAY_HELP: "EXTRA_MRZ_DISPLAY_HELP", // to true, // Display or not help
    EXTRA_MRZ_CAPTURE_TIMEOUT: "EXTRA_MRZ_CAPTURE_TIMEOUT", // to 60, // Timeout, default 60s

    // Config for NFC
    EXTRA_NFC_DISPLAY_WAIT_TAP_ANIMATION: "EXTRA_NFC_DISPLAY_WAIT_TAP_ANIMATION", // to true, Display or not an animation while waiting after NFC tap
    EXTRA_NFC_DISPLAY_MOVE_DETECTION: "EXTRA_NFC_DISPLAY_MOVE_DETECTION", // to true, Display help in case of handset movement
    EXTRA_MRZ_INPUT_DOC_NUMBER: "EXTRA_MRZ_INPUT_DOC_NUMBER", // to "xxxxxxxxxx", Optional if MRZ captor is used before NFC reading
    EXTRA_MRZ_INPUT_DATE_OF_BIRTHDAY: "EXTRA_MRZ_INPUT_DATE_OF_BIRTHDAY", // to "YYMMDD", Optional if MRZ captor is used before NFC reading
    EXTRA_MRZ_INPUT_DATE_OF_EXPIRATION: "EXTRA_MRZ_INPUT_DATE_OF_EXPIRATION", // to "YYMMDD", Optional if MRZ captor is used before NFC reading
    EXTRA_NFC_READING_TIMEOUT: "EXTRA_NFC_READING_TIMEOUT", // to 120, Timeout, default 60s

    // Config for Voice
    EXTRA_SENTENCE_COUNT: "EXTRA_SENTENCE_COUNT", // to 3, Optional, Number of repetitions user should read the phrase
    EXTRA_RECORD_VOICE_SILENCE_THREESHOLD: "EXTRA_RECORD_VOICE_SILENCE_THREESHOLD", // to 300, Optional, time duration (in milliseconds) of silent recording to recognize the end of one repetition

    // Config for Thales Liveness
    EXTRA_FRONTAL_FACE_CHECK: "EXTRA_FRONTAL_FACE_CHECK", // Enabled, Disabled
    EXTRA_EYES_OPEN_CHECK: "EXTRA_EYES_OPEN_CHECK", // Enabled, Disabled
    EXTRA_MOUTH_CLOSE_CHECK: "EXTRA_MOUTH_CLOSE_CHECK", // Enabled, Disabled
    EXTRA_NO_EYE_SHADOW_CHECK: "EXTRA_NO_EYE_SHADOW_CHECK", // Enabled, Disabled
    EXTRA_NO_GLARE_CHECK: "EXTRA_NO_GLARE_CHECK", // Enabled, Disabled
    EXTRA_NO_GLASSES_CHECK: "EXTRA_NO_GLASSES_CHECK", // Enabled, Disabled
    EXTRA_NO_TINTED_GLASSES_CHECK: "EXTRA_NO_TINTED_GLASSES_CHECK", // Enabled, Disabled
    EXTRA_EXPOSURE_CHECK: "EXTRA_EXPOSURE_CHECK", // Enabled, Disabled
    EXTRA_SHARP_CHECK: "EXTRA_SHARP_CHECK", // Enabled, Disabled
    EXTRA_H_CENTERED_CHECK: "EXTRA_H_CENTERED_CHECK", // Enabled, Disabled
    EXTRA_V_CENTERED_CHECK: "EXTRA_V_CENTERED_CHECK", // Enabled, Disabled
    EXTRA_GAZE_CHECK: "EXTRA_GAZE_CHECK", // Enabled, Disabled
    EXTRA_NUMBER_OF_FRAMES: "EXTRA_NUMBER_OF_FRAMES", // Optional. Number of consecutive face detections required for successful capture
    EXTRA_JPEG_QUALITY: "EXTRA_JPEG_QUALITY", // Captured image JPEG compression quality, default= 90

    // Config for Web Contract
    EXTRA_CAPTURE_CONTRACT_URL: "EXTRA_CAPTURE_CONTRACT_URL", // URL to open on webview
    EXTRA_CAPTURE_ASYNC_TEXT_DELAY: "EXTRA_CAPTURE_ASYNC_TEXT_DELAY", // Time delay to change instruction message

    // Config for Custom captor using IDV
    EXTRA_DOC_SIZE: "EXTRA_DOC_SIZE", // Document width and height
    EXTRA_CAPTURE_JPEG_COMPRESSION: "EXTRA_CAPTURE_JPEG_COMPRESSION", // Document image compression in JPEG format, default= 80. Expected value range 0-100. 0 meaning compress for small size, 100 meaning compress for max quality

    // Config for Fingerprint
    EXTRA_FP_LICENSE: "EXTRA_FP_LICENSE", // License string, for iOS only. Contact Thales to obtain a valid license.
    EXTRA_CAPTURE_LIVENESS: "EXTRA_CAPTURE_LIVENESS", // to true, Activate or not the liveness passive mode. true by default
    EXTRA_FP_LIVENESS_FACTOR: "EXTRA_FP_LIVENESS_FACTOR" // to 80, Set the liveness factor. 80 by default
});

const docConfig = Object.freeze({
   DETECTION_MODE: { ImageProcessing: 0, MachineLearning: 1, Disabled: -1 },
   BLUR: { Relaxed: 0, Strict: 1, TextBlock: 2, Disabled: -1 },
   GLARE: { White: 0, Color: 1, Disabled: -1 },
   DARKNESS: { Relaxed: 0, Disabled: -1 },
   PHOTOCOPY: { BlackAndWhite: 0, Disabled: -1 }
});

const selfieConfig = Object.freeze({
    WORKFLOW: { Charlie2: "Charlie2", Charlie4: "Charlie4", Charlie6: "Charlie6" },
    Enabled: 1,
    Disabled: 0
});

const customConfig = Object.freeze({
   DOC_SIZE: { Width: "Width", Height: "Height" }
});

exports.captureConfig = {
   generalConfig,
   docConfig,
   selfieConfig,
   customConfig
};
