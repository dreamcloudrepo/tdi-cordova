# Changelog

<br>

### **0.9.1**
----
28 Feb 2022

##### **Changed**
>
- [iOS] Cleanup and repackage TDI 1.5.0 ios SDK.
>
<br>

### **0.9.0**
----
09 Feb 2022

##### **Added**
>
1. Support TDI 1.5.0 android and ios SDKs.
2. [iOS] Hook script to configure Xcode build settings.
3. Captor configurations for fingerprint captor.
>
##### **Changed**
>
1. [Android|iOS] Profile API datastructures for PProfileCapture and PField.
2. [Android|iOS] Supported min/max mobile OS versions.
3. [Android|iOS] Custom captor blur and photocopy default modes.
>
##### **Fixed**
>
- [iOS] Handle exceptions for session APIs.
>
##### **Removed**
>
- [Android|iOS] Support for consecutive SDK callbacks.
>

*Known Issues / Limitations:*
>
- Other issues inherited from previous versions.
>
<br>

### **0.8.1**
----
20 Dec 2021

##### **Added**
>
1. Support TDI android 1.4.3 and ios 1.4.2 SDKs.
2. [Android|iOS] Hook script to inject plugin version.
>
##### **Changed**
>
- [Android|iOS] Allow auto capture even when manual capture button is active/visible in custom captor inline with SDK default ID document captor.
>
##### **Fixed**
>
1. [iOS] Random crash when using custom captor due to memory leak.
2. [iOS] Timer reset for manual capture button in Custom captor.
>

*Known Issues / Limitations:*
>
- Other issues inherited from previous versions.
>
<br>

### **0.8.0**
----
01 Dec 2021

##### **Added**
>
1. Support TDI 1.4.2 android and ios SDKs.
2. [Android|iOS] Configurable manual capture button with cropping feature. Refer custom captor on shutter button configuration from developer guide.
>
##### **Changed**
>
1. [Android|iOS] Introduced delay in sending custom captor success callback to allow application to loop successive captor calls.
2. [Android|iOS] Updated additional camera quality feedbacks.
>
##### **Fixed**
>
- [Android|iOS] Updated profile API datastructure for configs.k1 property.
>

*Known Issues / Limitations:*
>
1. [Android] Application may not be able to loop custom captor calls more than 5 times due to crash caused by cordova/ionic webview (part of application) rendering process.
2. Other issues inherited from previous versions.
>
<br>

### **0.7.2**
----
15 Nov 2021

##### **Fixed**
>
- [Android] Resolve NullPointerException when not setting the captor callback config.
>
##### **Removed**
>
1. [Android|iOS] Route error response from main callback to plugin's failure callback.
2. [Android|iOS] Return completed status from sessionStop callback.
>

*Known Issues / Limitations:*
>
- Other issues inherited from previous versions.
>
<br>

### **0.7.1**
----
29 Oct 2021

##### **Fixed**
>
1. [Android|iOS] Adjust custom captor config mappings and datatypes. Also applies for default ID captors. No change in config names.
2. [Android|iOS] Sometimes crash happens after sessionResult call and handling of invalid error code.
>
##### **Removed**
>
- [iOS] Duplicate references of linked frameworks are removed to support Xcode 13.
>

*Known Issues / Limitations:*
>
- Other issues inherited from previous versions.
>
<br>

### **0.7.0**
----
30 Sep 2021

##### **Added**
>
- Support TDI 1.4.1 android and ios SDKs.
>
##### **Changed**
>
1. [Android|iOS] Route error response from main callback to plugin's failure callback.
2. [Android|iOS] Support for consecutive SDK callbacks.
3. [Android|iOS] Return completed status from sessionStop callback.
4. [Android] Required kotlin version 1.5.30 or minimum 1.4.0 to be set in application root gradle file.
>
##### **Removed**
>
- [Android|iOS] ProfileAPI call from newSession.
>

*Known Issues / Limitations:*
>
- Other issues inherited from previous versions.
>
<br>

### **0.6.0**
----
03 Sep 2021

##### **Added**
>
1. Support TDI 1.4.0 android and ios SDKs.
2. Captor configurations for Thales selfie, voice and web contract captors.
>
##### **Changed**
>
- Updated Scenario datastructure parsing ProfileAPI data.
>
##### **Fixed**
>
- [Android] Custom captor turn document screen.
>

*Known Issues / Limitations:*
>
- Other issues inherited from previous versions.
>
<br>

### **0.5.0**
----
10 Jun 2021

##### **Added**
>
1. Support TDI 1.3.1 android and 1.3.2 ios SDKs.
2. [Android|iOS] JPEG compression configuration for custom captor.
>
##### **Changed**
>
- Updated required Kotlin and Swift versions in plugin.xml file.
>
##### **Fixed**
>
1. [Android] custom captor on use of layout resource IDs from app package name.
2. [Android] hook script to overwrite app package name in custom captor.
>
##### **Removed**
>
- [iOS] importing of SwiftSignatureView dependency for iOS app.
>

*Known Issues / Limitations:*
>
- Other issues inherited from previous versions.
>
<br>

### **0.4.1**
----
17 May 2021

##### **Changed**
>
- [Android|iOS] Minor updates for plugin integration.
>

*Known Issues / Limitations:*
>
- Other issues inherited from previous versions.
>
<br>

### **0.4.0**
----
30 Apr 2021

##### **Added**
>
1. Support TDI 1.3.1 android and ios SDKs.
2. [Android|iOS] Logs shown based on build configuration.
>
##### **Changed**
>
1. [Android|iOS] Updated tdiSdkInit, which is the preferred way to input license (ex: IDV license) as parameter.
2. [Android|iOS] Custom captor to launch in landscape mode.
3. [Android|iOS] By default call profile API in newSession.
4. Minor updates to captor configuration.
>
##### **Fixed**
>
- [iOS] contract/signature captor launches with blank contract and not able to proceed next to get the signature.
>
##### **Removed**
>
- [Android] Activity related code and runtime permission requests.
>

*Known Issues / Limitations:*
>
- Profile API in ios does not return "theme" property.
>
<br>

### **0.3.0 (beta01)**
----
01 Apr 2021

##### **Added**
>
- Add full support for all default captors for both android and ios.
>
##### **Changed**
>
- Updated custom captor ok, cancel images for android inline with ios.
>
##### **Fixed**
>
1. [Android|iOS] Profile API converting raw data to json.
2. [Android] custom captor layout conflict issue.
>

*Known Issues / Limitations:*
>
1. [iOS] contract/signature captor launches with blank contract and not able to proceed next to get the signature.
2. [iOS] Profile API does not return "theme" property.
3. [Android|iOS] NFC captor not fully tested.
>
<br>

### **0.2.0 (alpha02)**
----
09 Mar 2021

##### **Added**
>
1. Support TDI 1.3.0 android and ios SDKs.
2. [Android|iOS] Add custom captor with configurable document form factor.
3. Add captor configuration for default and custom captor.
>

*Known Issues / Limitations:*
>
1. Profile API does not return expected json data.
2. Few default captors like MRZ, NFC, contract captors not tested.
>
<br>

### **0.1.0 (alpha01)**
----
08 Feb 2021

>
- Initial release with definition of basic plugin APIs.
>

*Known Issues / Limitations:*
>
- Minimal or no functionality implemented.
>
