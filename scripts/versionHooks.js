/**
 * versionHooks.js
 * This hook adds the plugin version for Cordova TDI plugin.
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

const fs = require('fs');
const path = require('path');

module.exports = context => {
  console.log("Running hooks script before_plugin_install");
  
  const pluginPath = path.join(context.opts.projectRoot, 'plugins/', context.opts.plugin.id);
  const pluginVer = context.opts.plugin.pluginInfo.version;
  const platform = context.opts.plugin.platform;
  let srcFile = ''; 

  switch (platform) {
    case "android":
        srcFile = path.join(pluginPath, 'src/android/TdiPlugin.kt');
        console.log("[before_plugin_install]: Android srcFile: " + srcFile);
        break;
    case "ios":
        srcFile = path.join(pluginPath, 'src/ios/TdiPlugin.swift');
        console.log("[before_plugin_install]: iOS srcFile: " + srcFile);
        break;
    default:
        console.log('Platform not supported!');
        return;
  }

  let file = fs.readFileSync(srcFile, 'utf8');
  let result = file.replace(/plugin_version/g, '\"'+pluginVer+'\"');
  fs.writeFileSync(srcFile, result);
  console.log('Installing plugin version: ' + pluginVer);
};
