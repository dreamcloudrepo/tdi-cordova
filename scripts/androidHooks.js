/**
 * androidHooks.js
 * This hook adds all the required config for Android Cordova TDI plugin.
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

const fs = require('fs');
const path = require('path');
const et = require('elementtree');

module.exports = context => {    
    const config_xml = path.join(context.opts.projectRoot, 'config.xml');
    const data = fs.readFileSync(config_xml).toString();
    const etree = et.parse(data);
    const app_id = etree.getroot().attrib.id;    
    const logFile = 'platforms/android/app/src/main/java/cordova/plugin/tdi/PLog.kt';
    const customCaptorFile = 'platforms/android/app/src/main/java/cordova/plugin/tdi/CustomCaptor.kt';

    console.log("Running hooks script after_plugin_add for Android");

    var file = fs.readFileSync(logFile, 'utf8');
    var result = file.replace(/applicationId/g, app_id);
    fs.writeFileSync(logFile, result);
    file = fs.readFileSync(customCaptorFile, 'utf8');
    result = file.replace(/applicationId/g, app_id);
    fs.writeFileSync(customCaptorFile, result);

    console.log('Extracted app id: ' + app_id);
};
