/**
 * iosHooks.js
 * This hook adds all the required Xcode config for iOS Cordova TDI plugin.
 * Copyright © 2021 Thales DIS. All rights reserved.
 */

const fs = require('fs');
const path = require('path');
const xcode = require('xcode');
const childProcess = require('child_process');
const semver = require('semver');
// const glob = require('glob');

module.exports = context => {
	console.log("Running hooks script before_build for iOS");
  const projectRoot = context.opts.projectRoot;

  // This script has to be executed depending on the command line arguments, not
  // on the hook execution cycle.
  if ((context.hook === 'after_platform_add' && context.cmdLine.includes('platform add')) ||
    (context.hook === 'after_prepare' && context.cmdLine.includes('prepare')) ||
 (context.hook === 'after_plugin_add' && context.cmdLine.includes('plugin add')) ||
    (context.hook === 'before_build' && context.cmdLine.includes('build'))) {
	
    console.log('context.cmdLine: ', context.cmdLine);
	console.log('context.hook: ', context.hook);
    getPlatformVersionsFromFileSystem(context, projectRoot).then(platformVersions => {
		const IOS_MIN_VERSION = '12.1';
		const platformPath = path.join(projectRoot, 'platforms', 'ios');
		const rootConfig = getConfigParser(context, path.join(projectRoot, 'config.xml'));
		console.log('rootConfig: ', rootConfig);
		
		let projectName;
		let projectPath;
		let iosPlatformVersion;
		let pbxprojPath;
		let xcodeProject;

		const COMMENT_KEY = /_comment$/;
		let buildConfigs;
		let buildConfig;
		let configName;

		platformVersions.forEach((platformVersion) => {
			if (platformVersion.platform === 'ios') {
			  iosPlatformVersion = platformVersion.version;
			  console.log('cordova-ios: ', iosPlatformVersion); // TODO
			}
		});

		if (!iosPlatformVersion) {
			return;
		}

		projectName = rootConfig.name();
		projectPath = path.join(platformPath, projectName);
		let config = getConfigParser(context, path.join(projectPath, 'config.xml'));
		pbxprojPath = path.join(platformPath, projectName + '.xcodeproj', 'project.pbxproj');
		xcodeProject = xcode.project(pbxprojPath);
		xcodeProject.parseSync();
		buildConfigs = xcodeProject.pbxXCBuildConfigurationSection();

		for (configName in buildConfigs) {
		  if (!COMMENT_KEY.test(configName)) {
			buildConfig = buildConfigs[configName];

			if (parseFloat(xcodeProject.getBuildProperty('IPHONEOS_DEPLOYMENT_TARGET', buildConfig.name)) < parseFloat(IOS_MIN_VERSION)) {
				xcodeProject.updateBuildProperty('IPHONEOS_DEPLOYMENT_TARGET', IOS_MIN_VERSION, buildConfig.name);
				console.log('Update IOS project deployment target to:', IOS_MIN_VERSION, 'for build configuration', buildConfig.name);
			}

			if (xcodeProject.getBuildProperty('ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES', buildConfig.name) !== 'YES') {
				xcodeProject.updateBuildProperty('ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES', 'YES', buildConfig.name);
				console.log('Update IOS build setting ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES to: YES', 'for build configuration', buildConfig.name);
			}

			// TODO
			const import_path = `"\\"${path.join(projectName, "Plugins", context.opts.plugin.id)}\\""`; 
			if (xcodeProject.getBuildProperty('SWIFT_INCLUDE_PATHS', buildConfig.name) !== import_path) { 
				xcodeProject.addBuildProperty('SWIFT_INCLUDE_PATHS', import_path, buildConfig.name); 
				console.log('Update IOS build setting SWIFT_INCLUDE_PATHS to: ' + import_path, 'for build configuration', buildConfig.name); 
			}

			if (buildConfig.name === 'Debug') {
				if (xcodeProject.getBuildProperty('SWIFT_ACTIVE_COMPILATION_CONDITIONS', buildConfig.name) !== 'DEBUG') {
				  xcodeProject.updateBuildProperty('SWIFT_ACTIVE_COMPILATION_CONDITIONS', 'DEBUG', buildConfig.name);
				  console.log('Update IOS build setting swift debug flag', buildConfig.name);
				}
			}

			if (typeof xcodeProject.getBuildProperty('SWIFT_VERSION', buildConfig.name) === 'undefined') {
			  if (config.getPreference('UseLegacySwiftLanguageVersion', 'ios')) {
				xcodeProject.updateBuildProperty('SWIFT_VERSION', '2.3', buildConfig.name);
				console.log('Use legacy Swift language version', buildConfig.name);
			  } else if (config.getPreference('UseSwiftLanguageVersion', 'ios')) {
				const swiftVersion = config.getPreference('UseSwiftLanguageVersion', 'ios');
				xcodeProject.updateBuildProperty('SWIFT_VERSION', swiftVersion, buildConfig.name);
				console.log('Update IOS build setting Swift language version', swiftVersion);
			  } else {
				xcodeProject.updateBuildProperty('SWIFT_VERSION', '4.0', buildConfig.name);
				console.log('Update SWIFT version to 4.0', buildConfig.name);
			  }
			}else{
				xcodeProject.updateBuildProperty('SWIFT_VERSION', '5.0', buildConfig.name);
				console.log('Update SWIFT version to 5.0', buildConfig.name);
			}
		  }
		}
		
		console.log('pbxprojPath: ', pbxprojPath);
		fs.writeFileSync(pbxprojPath, xcodeProject.writeSync());
		console.log("Completed hooks script before_build for iOS");
    });
  }
};

const getConfigParser = (context, configPath) => {
  let ConfigParser;

  if (semver.lt(context.opts.cordova.version, '5.4.0')) {
    ConfigParser = context.requireCordovaModule('cordova-lib/src/ConfigParser/ConfigParser');
  } else {
    ConfigParser = context.requireCordovaModule('cordova-common/src/ConfigParser/ConfigParser');
  }

  return new ConfigParser(configPath);
};

const getPlatformVersionsFromFileSystem = (context, projectRoot) => {
  const cordovaUtil = context.requireCordovaModule('cordova-lib/src/cordova/util');
  const platformsOnFs = cordovaUtil.listPlatforms(projectRoot);
  const platformVersions = platformsOnFs.map(platform => {
    const script = path.join(projectRoot, 'platforms', platform, 'cordova', 'version');
    return new Promise((resolve, reject) => {
      childProcess.exec('"' + script + '"', {}, (error, stdout, _) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(stdout.trim());
      });
    }).then(result => {
      const version = result.replace(/\r?\n|\r/g, '');
      return { platform, version };
    }, (error) => {
      console.log(error);
      process.exit(1);
    });
  });

  return Promise.all(platformVersions);
};
