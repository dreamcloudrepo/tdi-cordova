//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for IDV_DOC.
FOUNDATION_EXPORT double IDV_DOCVersionNumber;

//! Project version string for IDV_DOC.
FOUNDATION_EXPORT const unsigned char IDV_DOCVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IDV_DOC/PublicHeader.h>

#import "IDV_DOC/IDVDOCStartCallback.h"
#import "IDV_DOC/IDVDOCCaptureSDK.h"
#import "IDV_DOC/IDVDOCCaptureResult.h"
#import "IDV_DOC/IDVDOCConfiguration.h"
