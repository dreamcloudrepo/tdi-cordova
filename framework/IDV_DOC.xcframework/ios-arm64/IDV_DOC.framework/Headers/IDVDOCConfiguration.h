//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVDOCConfiguration_h
#define IDVDOCConfiguration_h

#import "IDVDOCDocument.h"
#import "IDVDOCQualityChecks.h"
#import <AVFoundation/AVFoundation.h>


/**
 Detection modes. These values are used on the detectionMode field from IDVDOCConfiguration
*/
typedef NS_ENUM(NSInteger, IDVDOCDetectionMode) {
    /**
     Disable detecting documents.
    */
    DetectionDisabled = -1,
    /**
     Detection based on image processing, can be faster but less accurate.
    */
    DetectionImageProcessing,
    /**
     Detection based on machine learning, can more accurate but more slow.
    */
    DetectionMachineLearning
};

/**
 Camera orientation options. These values are used on the cameraOrientation field from IDVDOCConfiguration
*/
typedef NS_ENUM(NSInteger, IDVDOCCameraOrientation) {
    /**
     Inteface sets the camera to the current app orientation.
    */
    Interface = 0,
    /**
     Device set the camera on the device actual orientation.
    */
    Device = 1
};

/**
 Object containing configuration info to initialize the SDK.
*/
__attribute__((visibility("default"))) @interface IDVDOCConfiguration : NSObject {
    NSArray<IDVDOCDocument*>    *captureDocuments;
    IDVDOCQualityChecks         *qualityChecks;
}

/**
 Array of valid documents to be detected, if empty all ICAO documents will be detected. IDVDOCDocument
*/
@property (nonatomic, readwrite) NSArray<IDVDOCDocument*>* captureDocuments;

/**
 Array of valid documents to be detected, is empty all ICAO documents will be detected. IDVDOCDetectionMode
*/
@property (nonatomic, readwrite) IDVDOCDetectionMode detectionMode;

/**
 Configuration of quality checks to apply on the capture. IDVDOCQualityChecks
*/
@property (nonatomic, readwrite) IDVDOCQualityChecks* qualityChecks;

/**
 Camera orientation behaviour. IDVDOCCameraOrientation
*/
@property (nonatomic, readwrite) IDVDOCCameraOrientation cameraOrientation;

@end

#endif
