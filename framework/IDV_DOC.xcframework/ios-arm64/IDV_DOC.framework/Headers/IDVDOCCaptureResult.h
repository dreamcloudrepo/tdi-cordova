//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVDOCCaptureResult_h
#define IDVDOCCaptureResult_h

#import <AVFoundation/AVFoundation.h>

/**
 Object containing coordinates
*/
__attribute__((visibility("default"))) @interface IDVDOCQuadrangle : NSObject

@property (readwrite) CGPoint topLeft;
@property (readwrite) CGPoint topRight;
@property (readwrite) CGPoint bottomLeft;
@property (readwrite) CGPoint bottomRight;

- (BOOL) isEmpty;

@end

/**
 Object containing quality check info
*/
__attribute__((visibility("default"))) @interface IDVDOCQualityCheckResults : NSObject

@property (readwrite) bool all;
@property (readwrite) bool contrast;
@property (readwrite) bool glare;
@property (readwrite) bool darkness;
@property (readwrite) bool blur;
@property (readwrite) bool photocopy;
@property (readwrite) bool noFocused __deprecated;

@end

/**
 Object containing device status info
*/
__attribute__((visibility("default"))) @interface IDVDOCDeviceStatusInfo : NSObject

@property (readwrite) bool adjustingFocus;
@property (readwrite) bool adjustingExposure;
@property (readwrite) bool adjustingWhiteBalance;
@property (readwrite) int  uiRotation;

@end

/**
 Object containing process results
*/
__attribute__((visibility("default"))) @interface IDVDOCCaptureResult : NSObject

@property (readwrite, strong) NSData *fullFrame;
@property (readwrite, strong) NSData *cropFrame;
@property (readwrite, strong) IDVDOCQuadrangle *quadrangle;
@property (readwrite, strong) IDVDOCQualityCheckResults *qualityCheckResults;
@property (readwrite, strong) IDVDOCDeviceStatusInfo *deviceStatusInfo;

@property (nonatomic) int responseCode;

@end

#endif
