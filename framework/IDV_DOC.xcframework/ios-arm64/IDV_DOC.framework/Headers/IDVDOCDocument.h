//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVDOCDocument_h
#define IDVDOCDocument_h

#import <Foundation/Foundation.h>

/**
 Object containing ID document representation
*/

__attribute__((visibility("default"))) @interface IDVDOCDocument : NSObject {
    @public float width;
    @public float height;
    @public bool isICAO;
    @public double aspectRatio;
}

/**
 Create an instance of a document

 @param p_width width of the document
 @param p_height height of the document
 @return a document instance
 */
-(id)initWithWidth:(float)p_width andHeight:(float)p_height;

/**
 Compare two documentsw

 @param p_document document to compare
 @return true if the documents are equal
 */
-(BOOL)isEqual:(IDVDOCDocument*)p_document;

@end

__attribute__((visibility("default"))) extern IDVDOCDocument* DOCUMENT_TD1;
__attribute__((visibility("default"))) extern IDVDOCDocument* DOCUMENT_TD2;
__attribute__((visibility("default"))) extern IDVDOCDocument* DOCUMENT_TD3;

__attribute__((visibility("default"))) extern NSArray *DOCUMENT_MODE_IDDOCUMENT;
__attribute__((visibility("default"))) extern NSArray *DOCUMENT_MODE_PASSPORT;
__attribute__((visibility("default"))) extern NSArray *DOCUMENT_MODE_ICAO;

#endif
