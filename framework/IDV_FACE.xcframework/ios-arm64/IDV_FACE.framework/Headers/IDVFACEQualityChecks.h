//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, IDVFACEQualityCheckMode) {
    IDVFACEQualityCheckModeDisabled = 0,
    IDVFACEQualityCheckModeEnabled = 1
};

/**
 Object containing quality check configuration
*/
__attribute__((visibility("default"))) @interface IDVFACEQualityChecks : NSObject {
}

@property (nonatomic, readwrite) IDVFACEQualityCheckMode frontalDetectionMode;
@property (nonatomic, readwrite) IDVFACEQualityCheckMode eyeOpenDetectionMode;
@property (nonatomic, readwrite) IDVFACEQualityCheckMode mouthCloseDetectionMode;
@property (nonatomic, readwrite) IDVFACEQualityCheckMode onlyOneFaceDetectionMode;
@property (nonatomic, readwrite) IDVFACEQualityCheckMode noEyeShadowDetectionMode;
@property (nonatomic, readwrite) IDVFACEQualityCheckMode noGlareDetectionMode;
@property (nonatomic, readwrite) IDVFACEQualityCheckMode noGlassesDetectionMode;
@property (nonatomic, readwrite) IDVFACEQualityCheckMode noTintedGlassesDetectionMode;
@property (nonatomic, readwrite) IDVFACEQualityCheckMode exposureDetectionMode;
@property (nonatomic, readwrite) IDVFACEQualityCheckMode sharpDetectionMode;
@property (nonatomic, readwrite) IDVFACEQualityCheckMode hCenteredDetectionMode;
@property (nonatomic, readwrite) IDVFACEQualityCheckMode vCenteredDetectionMode;
@property (nonatomic, readwrite) IDVFACEQualityCheckMode gazeDetectionMode;

@end

NS_ASSUME_NONNULL_END
