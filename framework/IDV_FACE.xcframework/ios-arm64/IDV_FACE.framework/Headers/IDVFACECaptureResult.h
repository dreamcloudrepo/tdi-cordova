//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVFACECaptureResult_h
#define IDVFACECaptureResult_h

#import <AVFoundation/AVFoundation.h>

/**
 Object containing coordinates
*/
__attribute__((visibility("default"))) @interface IDVFACEQuadrangle : NSObject
    @property (readwrite) CGPoint topLeft;
    @property (readwrite) CGPoint topRight;
    @property (readwrite) CGPoint bottomLeft;
    @property (readwrite) CGPoint bottomRight;

- (BOOL) isEmpty;
@end

/**
 Object containing face qa info
*/
__attribute__((visibility("default"))) @interface IDVFACEQualityCheckResults : NSObject

    @property (readwrite) bool frontal;
    @property (readwrite) bool eyeOpen;
    @property (readwrite) bool mouthClose;
    @property (readwrite) bool onlyOneFace;
    @property (readwrite) bool noEyeShadow;
    @property (readwrite) bool noGlare;
    @property (readwrite) bool noGlasses;
    @property (readwrite) bool noTintedGlasses;
    @property (readwrite) bool exposure;
    @property (readwrite) bool sharp;
    @property (readwrite) bool hCentered;
    @property (readwrite) bool vCentered;
    @property (readwrite) bool gaze;
    @property (readwrite) bool focused __deprecated;
    @property (readwrite) bool all;
@end

/**
 Object containing image info
*/
__attribute__((visibility("default"))) @interface IDVFACEFrameData : NSObject
    @property (readwrite, strong) NSData *image;
    @property (readwrite) long timestamp;
    @property (readwrite, strong) IDVFACEQuadrangle *quadrangle;
    @property (readwrite, strong) IDVFACEQualityCheckResults *qualityCheckResults;
@end

/**
 Object containing device status info
*/
__attribute__((visibility("default"))) @interface IDVFACEDeviceStatusInfo : NSObject

@property (readwrite) bool adjustingFocus;
@property (readwrite) bool adjustingExposure;
@property (readwrite) bool adjustingWhiteBalance;
@property (readwrite) int  uiRotation;

@end

/**
 Object containing process results
*/
__attribute__((visibility("default"))) @interface IDVFACECaptureResult : NSObject
    @property (readwrite, strong) NSArray<IDVFACEFrameData *> *frames;
    @property (readwrite, strong) IDVFACEQuadrangle *quadrangle;
    @property (readwrite, strong) IDVFACEQualityCheckResults *qualityCheckResults;
    @property (readwrite, strong) IDVFACEDeviceStatusInfo *deviceStatusInfo;

@end

#endif
