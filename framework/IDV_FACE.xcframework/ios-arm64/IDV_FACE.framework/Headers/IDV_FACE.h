//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for IDV_FACE.
FOUNDATION_EXPORT double IDV_FACEVersionNumber;

//! Project version string for IDV_FACE.
FOUNDATION_EXPORT const unsigned char IDV_FACEVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import
// <IDV_FACE/PublicHeader.h>

#import "IDV_FACE/IDVFACECaptureCallback.h"
#import "IDV_FACE/IDVFACECaptureSDK.h"
#import "IDV_FACE/IDVFACECaptureResult.h"
#import "IDV_FACE/IDVFACEConfiguration.h"
#import "IDV_FACE/IDVFACEStartCallback.h"
#import "IDV_FACE/IDVFACEQualityChecks.h"
