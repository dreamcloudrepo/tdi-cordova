//
//  Copyright © 2021 Thales Group. All rights reserved.
//

#ifndef FACEStartCallback_h
#define FACEStartCallback_h

#import "IDVFACECaptureResult.h"

/**
 SDK frame process delegate
*/
@protocol IDVFACEStartCallback <NSObject>

/**
 Callback for each frame processed without a valid detected document
 @param partial IDVFACECaptureResult reference with partial results.
*/
- (void) onProcessedFrame:(IDVFACECaptureResult *)partial;

/**
 Callback called once when a valid detected capture
 @param result IDVFACECaptureResult reference with results.
*/
- (void) onSuccess:(IDVFACECaptureResult *)result;

@end

#endif
