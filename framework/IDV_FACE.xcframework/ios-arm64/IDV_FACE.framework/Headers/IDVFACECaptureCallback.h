//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef FACECaptureCallback_h
#define FACECaptureCallback_h

#import "IDVFACECaptureResult.h"
#import "IDVFACEStartCallback.h"

/**
 SDK frame process delegate
 IDVFACECaptureCallback is deprecated, use IDVFACEStartCallback instead
*/
__deprecated_msg("Use IDVFACEStartCallback instead.")
@protocol IDVFACECaptureCallback <IDVFACEStartCallback>

/**
 Callback for each frame processed without a valid detected document
 @param partial IDVFACECaptureResult reference with partial results.
*/
- (void) onProcessedFrame:(IDVFACECaptureResult *)partial __deprecated;

/**
 Callback called once when a valid detected capture
 @param result IDVFACECaptureResult reference with results.
*/
- (void) onSuccess:(IDVFACECaptureResult *)result __deprecated;

@end

#endif
