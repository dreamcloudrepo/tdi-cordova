//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef FACECaptureSDK_h
#define FACECaptureSDK_h

#import <UIKit/UIKit.h>

#import "IDVFACEStartCallback.h"
#import "IDVFACEConfiguration.h"
#import "IDVFACECaptureCallback.h"
 

__attribute__ ((visibility ("default"))) @interface IDVFACECaptureSDK : NSObject

/**
Callback to pass when using the wrapCrashReporting method.
This callback method shall call the initialization of any 3rd party crash reporting tool
that might cause conflicts with the SDK security mechanisms.
*/
typedef void (^ wrapCrashReporting)(void);

/**
 Called when init is completed.
 This method indicates whether SDK initialization is completed.
 To see the complete list of error codes please go to documentation.
 @param isCompleted  Boolean saying initialization was ok or not
 @param errorCode Error description. To see the complete list of error codes please go to documentation.
*/
typedef void (^ onInitBlock)(BOOL isCompleted, int errorCode);

/**
 @return current SDK version as NSString
 */
+ (NSString*) version;

/**
 This method is needed to avoid false positives on Antidebug detection.
 Some 3rd party crash reporting libraries conflicts with our security tools, to integrate them
 nicely we need this method to be called before these libraries are initallized.
 So when the *init* method is invoked it can avoid the collisions.
 So the correct way of initiallization would be:
   IDV wrapCrashReporting {
     third party (crash reporting tool) init
   }
 @param wrapper Block to invoke the third party.
 */
+ (void) wrapCrashReporting:(wrapCrashReporting)wrapper;

/**
 Initializes the sdk. Shows the camera but doesn't start the capture process.

 @param license valid license to use the SDK.
 @param cameraSide  Camera side to initialize..
 @param view UIView to show the camera preview.
 @param onInit Block called after initialization is done.
*/
- (void) init:(NSString*)license cameraSide:(IDVFACECameraSide)cameraSide view:(UIView *)view onInit:(onInitBlock)onInit;

/**
 Initializes the sdk. Shows the camera but doesn't start the capture process.

 @param license valid license to use the SDK.
 @param view UIView to show the camera preview.
 @param onInit Block called after initialization is done.
*/
- (void) init:(NSString*)license view:(UIView *)view onInit:(onInitBlock)onInit;

/**
 Starts the capture process

 @param configuration Configuration to start the SDK.
 @param callback Listener to be called when a frame is processed.
 @return An error code if can't start. To see the complete list of error codes please go to documentation.
*/
- (int) start:(IDVFACEConfiguration *)configuration withBlock:(id<IDVFACEStartCallback>)callback;


/**
 Stops the capture process

 @return An error code if can't stop. To see the complete list of error codes please go to documentation.
*/
- (int) stop;

/**
 Finish the SDK, releasing resources.

 @return An error code if can't finish. To see the complete list of error codes please go to documentation.
*/
- (int) finish;

/**
 Triggers a manual capture.
*/
- (void) triggerCapture;

@end

#endif
