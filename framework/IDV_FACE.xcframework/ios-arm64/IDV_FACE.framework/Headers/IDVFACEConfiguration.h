//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef FACEConfiguration_h
#define FACEConfiguration_h

#import <AVFoundation/AVFoundation.h>
#import "IDVFACEQualityChecks.h"

/**
 Camera orientation options. These values are used on the cameraOrientation field from IDVFACEConfiguration
*/
typedef NS_ENUM(NSInteger, IDVFACECameraOrientation) {
    /**
     Inteface sets the camera to the current app orientation.
    */
    Interface = 0,
    /**
     Device set the camera on the device actual orientation.
    */
    Device = 1
};

/**
 Camera side.
 */
typedef NS_ENUM(NSInteger, IDVFACECameraSide) {
    /**
     Inteface sets the back camera side.
    */
    Back = 1,
    /**
     Inteface sets the front camera side.
    */
    Front = 2
};

/**
 Object containing configuration info to initialize the SDK.
*/
__attribute__ ((visibility ("default"))) @interface IDVFACEConfiguration : NSObject {
    int                   numFrames;
    IDVFACEQualityChecks *qualityChecks;
}

@property (nonatomic, readwrite) int                numFrames;
@property (nonatomic, readwrite) IDVFACEQualityChecks* qualityChecks;

/**
 Camera orientation behaviour. IDVFACECameraOrientation
*/
@property (nonatomic, readwrite) IDVFACECameraOrientation cameraOrientation;

@end

#endif
