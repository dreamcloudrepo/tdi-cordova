#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSError.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSSet.h>
#import <Foundation/NSString.h>
#import <Foundation/NSValue.h>

@class MainCapture, MainFailureCompanion, MainField, MainProfileCaptor, MainProfileCapture, MainProfileConfigs, MainResponsesCategoryField, MainResponsesCategory, MainScenario, MainStatus, MainKotlinEnumCompanion, MainKotlinEnum<E>, MainKotlinArray<T>, MainKotlinx_serialization_jsonJsonElement, MainTdiProfileInfo, MainAbstractCaptorProviderImplCompanion, MainKotlinx_coroutines_coreCoroutineDispatcher, MainScenarioInfo, MainAbstractSessionImplCompanion, MainSessionStateManager, MainKotlinByteArray, MainStatePatchRequest, MainHttpApiFactory, MainBuildUtils, MainAbstractSessionImpl, MainSessionStateManagerCompanion, MainKotlinPair<__covariant A, __covariant B>, MainInputImpl, MainTdiConstants, MainKtor_ioCharset, MainResultAdapter, MainTaskAdapter, MainTaskImpl, MainLoggerCompanion, MainContractProfileSignature, MainContractProfile, MainResult, MainState, MainStep, MainInitRequest, MainInitRequesterCompanion, MainProfileLookAndFeelCompanion, MainInitRequestCompanion, MainStatePatchRequestCompanion, MainTdiClient, MainKtor_client_coreHttpClient, MainKotlinByteIterator, NSData, MainKotlinThrowable, MainKotlinx_serialization_jsonJsonElementCompanion, MainKotlinAbstractCoroutineContextElement, MainKotlinx_coroutines_coreCoroutineDispatcherKey, MainKtor_ioCharsetCompanion, MainKtor_ioCharsetDecoder, MainKtor_ioCharsetEncoder, MainKtor_client_coreHttpClientEngineConfig, MainKtor_client_coreHttpClientConfig<T>, MainKtor_client_coreHttpRequestBuilder, MainKtor_client_coreHttpClientCall, MainKtor_client_coreHttpReceivePipeline, MainKtor_client_coreHttpRequestPipeline, MainKtor_client_coreHttpResponsePipeline, MainKtor_client_coreHttpSendPipeline, MainKotlinAbstractCoroutineContextKey<B, E>, MainKotlinx_serialization_coreSerializersModule, MainKotlinx_serialization_coreSerialKind, MainKotlinNothing, MainKtor_client_coreHttpRequestData, MainKtor_client_coreHttpResponseData, MainKtor_client_coreProxyConfig, MainKotlinException, MainKotlinRuntimeException, MainKotlinIllegalStateException, MainKtor_httpHeadersBuilder, MainKtor_client_coreHttpRequestBuilderCompanion, MainKtor_httpURLBuilder, MainKtor_httpHttpMethod, MainKtor_client_coreHttpClientCallCompanion, MainKtor_client_coreTypeInfo, MainKtor_client_coreHttpResponse, MainKtor_utilsAttributeKey<T>, MainKtor_utilsPipelinePhase, MainKtor_utilsPipeline<TSubject, TContext>, MainKtor_client_coreHttpReceivePipelinePhases, MainKtor_client_coreHttpRequestPipelinePhases, MainKtor_client_coreHttpResponsePipelinePhases, MainKtor_client_coreHttpResponseContainer, MainKtor_client_coreHttpSendPipelinePhases, MainKtor_httpUrl, MainKtor_httpOutgoingContent, MainKtor_httpHttpStatusCode, MainKtor_utilsGMTDate, MainKtor_httpHttpProtocolVersion, MainKtor_utilsStringValuesBuilder, MainKtor_httpURLProtocol, MainKtor_httpParametersBuilder, MainKtor_httpURLBuilderCompanion, MainKotlinCancellationException, MainKotlinUnit, MainKtor_httpHttpMethodCompanion, MainKtor_ioMemory, MainKtor_ioIoBuffer, MainKtor_ioByteReadPacket, MainKtor_ioByteOrder, MainKtor_httpUrlCompanion, MainKtor_httpContentType, MainKtor_httpHttpStatusCodeCompanion, MainKtor_utilsGMTDateCompanion, MainKtor_utilsWeekDay, MainKtor_utilsMonth, MainKtor_httpHttpProtocolVersionCompanion, MainKtor_httpURLProtocolCompanion, MainKtor_httpUrlEncodingOption, MainKtor_ioMemoryCompanion, MainKtor_ioBufferCompanion, MainKtor_ioBuffer, MainKtor_ioChunkBuffer, MainKtor_ioChunkBufferCompanion, MainKotlinCharArray, MainKtor_ioIoBufferCompanion, MainKtor_ioAbstractInputCompanion, MainKtor_ioAbstractInput, MainKtor_ioByteReadPacketBaseCompanion, MainKtor_ioByteReadPacketBase, MainKtor_ioByteReadPacketPlatformBase, MainKtor_ioByteReadPacketCompanion, MainKtor_ioByteOrderCompanion, MainKotlinKTypeProjection, MainKtor_httpHeaderValueParam, MainKtor_httpHeaderValueWithParametersCompanion, MainKtor_httpHeaderValueWithParameters, MainKtor_httpContentTypeCompanion, MainKtor_utilsWeekDayCompanion, MainKtor_utilsMonthCompanion, MainKotlinx_coroutines_coreAtomicDesc, MainKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp, MainKotlinCharIterator, MainKotlinKVariance, MainKotlinKTypeProjectionCompanion, MainKotlinx_coroutines_coreAtomicOp<__contravariant T>, MainKotlinx_coroutines_coreOpDescriptor, MainKotlinx_coroutines_coreLockFreeLinkedListNode, MainKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc, MainKotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc<T>, MainKotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc<T>;

@protocol MainCaptorCallback, MainInput, MainFailure, MainCaptor, MainCaptorProvider, MainTask, MainKotlinComparable, MainCaptorFactory, MainSession, MainCaptorInvoker, MainTdiRequester, MainTdiProfileRequester, MainPushTokenProvider, MainDep, MainIdCloudHelper, MainResult, MainKotlinx_serialization_coreKSerializer, MainKotlinIterator, MainKotlinCoroutineContextKey, MainKotlinCoroutineContextElement, MainKotlinCoroutineContext, MainKotlinContinuation, MainKotlinContinuationInterceptor, MainKotlinx_coroutines_coreRunnable, MainKotlinx_serialization_coreEncoder, MainKotlinx_serialization_coreSerialDescriptor, MainKotlinx_serialization_coreSerializationStrategy, MainKotlinx_serialization_coreDecoder, MainKotlinx_serialization_coreDeserializationStrategy, MainKotlinx_coroutines_coreCoroutineScope, MainKtor_ioCloseable, MainKtor_client_coreHttpClientEngine, MainKtor_client_coreHttpClientEngineCapability, MainKtor_utilsAttributes, MainKotlinx_serialization_coreCompositeEncoder, MainKotlinAnnotation, MainKotlinx_serialization_coreCompositeDecoder, MainKtor_client_coreHttpClientFeature, MainKtor_httpHttpMessageBuilder, MainKotlinx_coroutines_coreJob, MainKtor_ioByteReadChannel, MainKtor_utilsTypeInfo, MainKtor_client_coreHttpRequest, MainKotlinSuspendFunction2, MainKotlinx_serialization_coreSerializersModuleCollector, MainKotlinKClass, MainKtor_httpHeaders, MainKtor_utilsStringValues, MainKotlinMapEntry, MainKotlinx_coroutines_coreChildHandle, MainKotlinx_coroutines_coreChildJob, MainKotlinx_coroutines_coreDisposableHandle, MainKotlinSequence, MainKotlinx_coroutines_coreSelectClause0, MainKtor_ioReadSession, MainKotlinSuspendFunction1, MainKotlinAppendable, MainKotlinKType, MainKtor_httpHttpMessage, MainKotlinFunction, MainKotlinKDeclarationContainer, MainKotlinKAnnotatedElement, MainKotlinKClassifier, MainKtor_httpParameters, MainKotlinx_coroutines_coreParentJob, MainKotlinx_coroutines_coreSelectInstance, MainKotlinSuspendFunction0, MainKtor_ioObjectPool, MainKtor_ioInput, MainKtor_ioOutput;

NS_ASSUME_NONNULL_BEGIN
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-warning-option"
#pragma clang diagnostic ignored "-Wincompatible-property-type"
#pragma clang diagnostic ignored "-Wnullability"

#pragma push_macro("_Nullable_result")
#if !__has_feature(nullability_nullable_result)
#undef _Nullable_result
#define _Nullable_result _Nullable
#endif

__attribute__((swift_name("KotlinBase")))
@interface MainBase : NSObject
- (instancetype)init __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
+ (void)initialize __attribute__((objc_requires_super));
@end;

@interface MainBase (MainBaseCopying) <NSCopying>
@end;

__attribute__((swift_name("KotlinMutableSet")))
@interface MainMutableSet<ObjectType> : NSMutableSet<ObjectType>
@end;

__attribute__((swift_name("KotlinMutableDictionary")))
@interface MainMutableDictionary<KeyType, ObjectType> : NSMutableDictionary<KeyType, ObjectType>
@end;

@interface NSError (NSErrorMainKotlinException)
@property (readonly) id _Nullable kotlinException;
@end;

__attribute__((swift_name("KotlinNumber")))
@interface MainNumber : NSNumber
- (instancetype)initWithChar:(char)value __attribute__((unavailable));
- (instancetype)initWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
- (instancetype)initWithShort:(short)value __attribute__((unavailable));
- (instancetype)initWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
- (instancetype)initWithInt:(int)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
- (instancetype)initWithLong:(long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
- (instancetype)initWithLongLong:(long long)value __attribute__((unavailable));
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
- (instancetype)initWithFloat:(float)value __attribute__((unavailable));
- (instancetype)initWithDouble:(double)value __attribute__((unavailable));
- (instancetype)initWithBool:(BOOL)value __attribute__((unavailable));
- (instancetype)initWithInteger:(NSInteger)value __attribute__((unavailable));
- (instancetype)initWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
+ (instancetype)numberWithChar:(char)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedChar:(unsigned char)value __attribute__((unavailable));
+ (instancetype)numberWithShort:(short)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedShort:(unsigned short)value __attribute__((unavailable));
+ (instancetype)numberWithInt:(int)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInt:(unsigned int)value __attribute__((unavailable));
+ (instancetype)numberWithLong:(long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLong:(unsigned long)value __attribute__((unavailable));
+ (instancetype)numberWithLongLong:(long long)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value __attribute__((unavailable));
+ (instancetype)numberWithFloat:(float)value __attribute__((unavailable));
+ (instancetype)numberWithDouble:(double)value __attribute__((unavailable));
+ (instancetype)numberWithBool:(BOOL)value __attribute__((unavailable));
+ (instancetype)numberWithInteger:(NSInteger)value __attribute__((unavailable));
+ (instancetype)numberWithUnsignedInteger:(NSUInteger)value __attribute__((unavailable));
@end;

__attribute__((swift_name("KotlinByte")))
@interface MainByte : MainNumber
- (instancetype)initWithChar:(char)value;
+ (instancetype)numberWithChar:(char)value;
@end;

__attribute__((swift_name("KotlinUByte")))
@interface MainUByte : MainNumber
- (instancetype)initWithUnsignedChar:(unsigned char)value;
+ (instancetype)numberWithUnsignedChar:(unsigned char)value;
@end;

__attribute__((swift_name("KotlinShort")))
@interface MainShort : MainNumber
- (instancetype)initWithShort:(short)value;
+ (instancetype)numberWithShort:(short)value;
@end;

__attribute__((swift_name("KotlinUShort")))
@interface MainUShort : MainNumber
- (instancetype)initWithUnsignedShort:(unsigned short)value;
+ (instancetype)numberWithUnsignedShort:(unsigned short)value;
@end;

__attribute__((swift_name("KotlinInt")))
@interface MainInt : MainNumber
- (instancetype)initWithInt:(int)value;
+ (instancetype)numberWithInt:(int)value;
@end;

__attribute__((swift_name("KotlinUInt")))
@interface MainUInt : MainNumber
- (instancetype)initWithUnsignedInt:(unsigned int)value;
+ (instancetype)numberWithUnsignedInt:(unsigned int)value;
@end;

__attribute__((swift_name("KotlinLong")))
@interface MainLong : MainNumber
- (instancetype)initWithLongLong:(long long)value;
+ (instancetype)numberWithLongLong:(long long)value;
@end;

__attribute__((swift_name("KotlinULong")))
@interface MainULong : MainNumber
- (instancetype)initWithUnsignedLongLong:(unsigned long long)value;
+ (instancetype)numberWithUnsignedLongLong:(unsigned long long)value;
@end;

__attribute__((swift_name("KotlinFloat")))
@interface MainFloat : MainNumber
- (instancetype)initWithFloat:(float)value;
+ (instancetype)numberWithFloat:(float)value;
@end;

__attribute__((swift_name("KotlinDouble")))
@interface MainDouble : MainNumber
- (instancetype)initWithDouble:(double)value;
+ (instancetype)numberWithDouble:(double)value;
@end;

__attribute__((swift_name("KotlinBoolean")))
@interface MainBoolean : MainNumber
- (instancetype)initWithBool:(BOOL)value;
+ (instancetype)numberWithBool:(BOOL)value;
@end;

__attribute__((swift_name("Captor")))
@protocol MainCaptor
@required
- (void)executeCallback:(id<MainCaptorCallback>)callback __attribute__((swift_name("execute(callback:)")));
@property MainMutableDictionary<NSString *, id> *config __attribute__((swift_name("config")));
@end;

__attribute__((swift_name("CaptorCallback")))
@protocol MainCaptorCallback
@required
- (void)onDataCapturedInput:(id<MainInput>)input __attribute__((swift_name("onDataCaptured(input:)")));
- (void)onFailureFailure:(id<MainFailure>)failure __attribute__((swift_name("onFailure(failure:)")));
@end;

__attribute__((swift_name("CaptorProvider")))
@protocol MainCaptorProvider
@required
- (id<MainCaptor> _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSDictionary<NSString *, id<MainCaptor>> * _Nullable)getAll __attribute__((swift_name("getAll()")));
- (void)setName:(NSString *)name captor:(id<MainCaptor>)captor __attribute__((swift_name("set(name:captor:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Capture")))
@interface MainCapture : MainBase
- (instancetype)initWithCaptures:(NSMutableArray<MainMutableDictionary<NSString *, NSString *> *> *)captures __attribute__((swift_name("init(captures:)"))) __attribute__((objc_designated_initializer));
- (NSMutableArray<MainMutableDictionary<NSString *, NSString *> *> *)component1 __attribute__((swift_name("component1()")));
- (MainCapture *)doCopyCaptures:(NSMutableArray<MainMutableDictionary<NSString *, NSString *> *> *)captures __attribute__((swift_name("doCopy(captures:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSMutableArray<MainMutableDictionary<NSString *, NSString *> *> *captures __attribute__((swift_name("captures")));
@end;

__attribute__((swift_name("Failure")))
@protocol MainFailure
@required
@property (readonly) int32_t errorCode __attribute__((swift_name("errorCode")));
@property (readonly) NSString *errorMessage __attribute__((swift_name("errorMessage")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("FailureCompanion")))
@interface MainFailureCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainFailureCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) int32_t REASON_CAPTOR_INIT_FAILED __attribute__((swift_name("REASON_CAPTOR_INIT_FAILED")));
@property (readonly) int32_t REASON_CAPTOR_JNA_JNI_ISSUE __attribute__((swift_name("REASON_CAPTOR_JNA_JNI_ISSUE")));
@property (readonly) int32_t REASON_CAPTOR_LICENCE_FAILED __attribute__((swift_name("REASON_CAPTOR_LICENCE_FAILED")));
@property (readonly) int32_t REASON_CAPTOR_OUTPUT_DATA_ERROR __attribute__((swift_name("REASON_CAPTOR_OUTPUT_DATA_ERROR")));
@property (readonly) int32_t REASON_CAPTOR_PERMISSION_DENY __attribute__((swift_name("REASON_CAPTOR_PERMISSION_DENY")));
@property (readonly) int32_t REASON_CAPTOR_PROCESS_ERROR __attribute__((swift_name("REASON_CAPTOR_PROCESS_ERROR")));
@property (readonly) int32_t REASON_CONTRACT_CAPTURE_ABORT __attribute__((swift_name("REASON_CONTRACT_CAPTURE_ABORT")));
@property (readonly) int32_t REASON_CONTRACT_CAPTURE_FAILURE __attribute__((swift_name("REASON_CONTRACT_CAPTURE_FAILURE")));
@property (readonly) int32_t REASON_CONTRACT_WEBVIEW_ERROR __attribute__((swift_name("REASON_CONTRACT_WEBVIEW_ERROR")));
@property (readonly) int32_t REASON_CONTRACT_WEBVIEW_HTTP_ERROR __attribute__((swift_name("REASON_CONTRACT_WEBVIEW_HTTP_ERROR")));
@property (readonly) int32_t REASON_DEP_DECRYPT_FAILED_CODE __attribute__((swift_name("REASON_DEP_DECRYPT_FAILED_CODE")));
@property (readonly) int32_t REASON_DEP_DECRYPT_HTTP_EMPTY_BODY __attribute__((swift_name("REASON_DEP_DECRYPT_HTTP_EMPTY_BODY")));
@property (readonly) int32_t REASON_DEP_DECRYPT_HTTP_HEADER_MISSING __attribute__((swift_name("REASON_DEP_DECRYPT_HTTP_HEADER_MISSING")));
@property (readonly) int32_t REASON_DEP_ENCRYPT_FAILED_CODE __attribute__((swift_name("REASON_DEP_ENCRYPT_FAILED_CODE")));
@property (readonly) int32_t REASON_DEVICE_HOOKED __attribute__((swift_name("REASON_DEVICE_HOOKED")));
@property (readonly) int32_t REASON_DEVICE_ROOTED __attribute__((swift_name("REASON_DEVICE_ROOTED")));
@property (readonly) int32_t REASON_DEVICE_SUSPICIOUS __attribute__((swift_name("REASON_DEVICE_SUSPICIOUS")));
@property (readonly) int32_t REASON_DOCUMENT_CAPTURE_ABORT __attribute__((swift_name("REASON_DOCUMENT_CAPTURE_ABORT")));
@property (readonly) int32_t REASON_DOCUMENT_CAPTURE_FAILURE __attribute__((swift_name("REASON_DOCUMENT_CAPTURE_FAILURE")));
@property (readonly) int32_t REASON_FACE_CAPTURE_ABORT __attribute__((swift_name("REASON_FACE_CAPTURE_ABORT")));
@property (readonly) int32_t REASON_FACE_CAPTURE_FAILURE __attribute__((swift_name("REASON_FACE_CAPTURE_FAILURE")));
@property (readonly) int32_t REASON_FINGERPRINT_BACKGROUND __attribute__((swift_name("REASON_FINGERPRINT_BACKGROUND")));
@property (readonly) int32_t REASON_FINGERPRINT_CAMERA_CAPABILITIES __attribute__((swift_name("REASON_FINGERPRINT_CAMERA_CAPABILITIES")));
@property (readonly) int32_t REASON_FINGERPRINT_CAPTURE_ABORT __attribute__((swift_name("REASON_FINGERPRINT_CAPTURE_ABORT")));
@property (readonly) int32_t REASON_FINGERPRINT_FAILED __attribute__((swift_name("REASON_FINGERPRINT_FAILED")));
@property (readonly) int32_t REASON_FINGERPRINT_FAILURE __attribute__((swift_name("REASON_FINGERPRINT_FAILURE")));
@property (readonly) int32_t REASON_FINGERPRINT_LICENCE __attribute__((swift_name("REASON_FINGERPRINT_LICENCE")));
@property (readonly) int32_t REASON_FINGERPRINT_LIVENESS_FAILED __attribute__((swift_name("REASON_FINGERPRINT_LIVENESS_FAILED")));
@property (readonly) int32_t REASON_FINGERPRINT_TIME_OUT __attribute__((swift_name("REASON_FINGERPRINT_TIME_OUT")));
@property (readonly) int32_t REASON_HTTP_CLIENT_FAILED_CODE __attribute__((swift_name("REASON_HTTP_CLIENT_FAILED_CODE")));
@property (readonly) int32_t REASON_JSON_PARSING_FAILED_CODE __attribute__((swift_name("REASON_JSON_PARSING_FAILED_CODE")));
@property (readonly) NSString *REASON_JSON_PARSING_FAILED_MESSAGE __attribute__((swift_name("REASON_JSON_PARSING_FAILED_MESSAGE")));
@property (readonly) int32_t REASON_NFC_CAPTURE_ABORT __attribute__((swift_name("REASON_NFC_CAPTURE_ABORT")));
@property (readonly) int32_t REASON_NFC_CAPTURE_FAILURE __attribute__((swift_name("REASON_NFC_CAPTURE_FAILURE")));
@property (readonly) int32_t REASON_QR_CODE_CAPTURE_FAILURE __attribute__((swift_name("REASON_QR_CODE_CAPTURE_FAILURE")));
@property (readonly) int32_t REASON_SECURITY_TAMPERED __attribute__((swift_name("REASON_SECURITY_TAMPERED")));
@property (readonly) int32_t REASON_VOICE_CAPTURE_ABORT __attribute__((swift_name("REASON_VOICE_CAPTURE_ABORT")));
@property (readonly) int32_t REASON_VOICE_CAPTURE_FAILURE __attribute__((swift_name("REASON_VOICE_CAPTURE_FAILURE")));
@end;

__attribute__((swift_name("FcmMessagingHandler")))
@protocol MainFcmMessagingHandler
@required
- (void)onMessageReceivedFrom:(NSString *)from data:(NSDictionary<NSString *, NSString *> *)data __attribute__((swift_name("onMessageReceived(from:data:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Field")))
@interface MainField : MainBase
- (instancetype)initWithFormat:(NSString *)format isDisplayed:(BOOL)isDisplayed mandatory:(BOOL)mandatory pattern:(NSString *)pattern editable:(NSMutableArray<id> * _Nullable)editable __attribute__((swift_name("init(format:isDisplayed:mandatory:pattern:editable:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (NSMutableArray<id> * _Nullable)component5 __attribute__((swift_name("component5()")));
- (MainField *)doCopyFormat:(NSString *)format isDisplayed:(BOOL)isDisplayed mandatory:(BOOL)mandatory pattern:(NSString *)pattern editable:(NSMutableArray<id> * _Nullable)editable __attribute__((swift_name("doCopy(format:isDisplayed:mandatory:pattern:editable:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSMutableArray<id> * _Nullable editable __attribute__((swift_name("editable")));
@property (readonly) NSString *format __attribute__((swift_name("format")));
@property (readonly) BOOL isDisplayed __attribute__((swift_name("isDisplayed")));
@property (readonly) BOOL mandatory __attribute__((swift_name("mandatory")));
@property (readonly) NSString *pattern __attribute__((swift_name("pattern")));
@end;

__attribute__((swift_name("Input")))
@protocol MainInput
@required
@property (readonly) MainMutableDictionary<NSString *, NSString *> *pages __attribute__((swift_name("pages")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ProfileCaptor")))
@interface MainProfileCaptor : MainBase
- (instancetype)initWithPages:(NSMutableArray<NSString *> * _Nullable)pages metadata:(MainMutableDictionary<NSString *, id> * _Nullable)metadata isUploadAllowed:(BOOL)isUploadAllowed category:(NSString * _Nullable)category __attribute__((swift_name("init(pages:metadata:isUploadAllowed:category:)"))) __attribute__((objc_designated_initializer));
- (NSMutableArray<NSString *> * _Nullable)component1 __attribute__((swift_name("component1()")));
- (MainMutableDictionary<NSString *, id> * _Nullable)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (MainProfileCaptor *)doCopyPages:(NSMutableArray<NSString *> * _Nullable)pages metadata:(MainMutableDictionary<NSString *, id> * _Nullable)metadata isUploadAllowed:(BOOL)isUploadAllowed category:(NSString * _Nullable)category __attribute__((swift_name("doCopy(pages:metadata:isUploadAllowed:category:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable category __attribute__((swift_name("category")));
@property (readonly) BOOL isUploadAllowed __attribute__((swift_name("isUploadAllowed")));
@property (readonly) MainMutableDictionary<NSString *, id> * _Nullable metadata __attribute__((swift_name("metadata")));
@property (readonly) NSMutableArray<NSString *> * _Nullable pages __attribute__((swift_name("pages")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ProfileCapture")))
@interface MainProfileCapture : MainBase
- (instancetype)initWithName:(NSString *)name isSkipAllowed:(BOOL)isSkipAllowed returnRisk:(BOOL)returnRisk idpIdentifier:(NSString * _Nullable)idpIdentifier response:(NSMutableArray<id> * _Nullable)response steps:(NSMutableArray<MainCapture *> *)steps __attribute__((swift_name("init(name:isSkipAllowed:returnRisk:idpIdentifier:response:steps:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (BOOL)component2 __attribute__((swift_name("component2()")));
- (BOOL)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSMutableArray<id> * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSMutableArray<MainCapture *> *)component6 __attribute__((swift_name("component6()")));
- (MainProfileCapture *)doCopyName:(NSString *)name isSkipAllowed:(BOOL)isSkipAllowed returnRisk:(BOOL)returnRisk idpIdentifier:(NSString * _Nullable)idpIdentifier response:(NSMutableArray<id> * _Nullable)response steps:(NSMutableArray<MainCapture *> *)steps __attribute__((swift_name("doCopy(name:isSkipAllowed:returnRisk:idpIdentifier:response:steps:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable idpIdentifier __attribute__((swift_name("idpIdentifier")));
@property (readonly) BOOL isSkipAllowed __attribute__((swift_name("isSkipAllowed")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSMutableArray<id> * _Nullable response __attribute__((swift_name("response")));
@property (readonly) BOOL returnRisk __attribute__((swift_name("returnRisk")));
@property (readonly) NSMutableArray<MainCapture *> *steps __attribute__((swift_name("steps")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ProfileConfigs")))
@interface MainProfileConfigs : MainBase
- (instancetype)initWithK1:(NSString * _Nullable)k1 k1m:(NSString * _Nullable)k1m k1e:(NSString * _Nullable)k1e httpTimeoutSecond:(NSString * _Nullable)httpTimeoutSecond httpMaxRetryNum:(NSString * _Nullable)httpMaxRetryNum httpRetryDelaySecond:(NSString * _Nullable)httpRetryDelaySecond pushNotifTimeoutSecond:(NSString * _Nullable)pushNotifTimeoutSecond pullMaxRetryNum:(NSString * _Nullable)pullMaxRetryNum lang:(NSString * _Nullable)lang sizeLimit:(NSString * _Nullable)sizeLimit enableRisk:(NSString *)enableRisk __attribute__((swift_name("init(k1:k1m:k1e:httpTimeoutSecond:httpMaxRetryNum:httpRetryDelaySecond:pushNotifTimeoutSecond:pullMaxRetryNum:lang:sizeLimit:enableRisk:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component10 __attribute__((swift_name("component10()")));
- (NSString *)component11 __attribute__((swift_name("component11()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSString * _Nullable)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (NSString * _Nullable)component9 __attribute__((swift_name("component9()")));
- (MainProfileConfigs *)doCopyK1:(NSString * _Nullable)k1 k1m:(NSString * _Nullable)k1m k1e:(NSString * _Nullable)k1e httpTimeoutSecond:(NSString * _Nullable)httpTimeoutSecond httpMaxRetryNum:(NSString * _Nullable)httpMaxRetryNum httpRetryDelaySecond:(NSString * _Nullable)httpRetryDelaySecond pushNotifTimeoutSecond:(NSString * _Nullable)pushNotifTimeoutSecond pullMaxRetryNum:(NSString * _Nullable)pullMaxRetryNum lang:(NSString * _Nullable)lang sizeLimit:(NSString * _Nullable)sizeLimit enableRisk:(NSString *)enableRisk __attribute__((swift_name("doCopy(k1:k1m:k1e:httpTimeoutSecond:httpMaxRetryNum:httpRetryDelaySecond:pushNotifTimeoutSecond:pullMaxRetryNum:lang:sizeLimit:enableRisk:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *enableRisk __attribute__((swift_name("enableRisk")));
@property (readonly) NSString * _Nullable httpMaxRetryNum __attribute__((swift_name("httpMaxRetryNum")));
@property (readonly) NSString * _Nullable httpRetryDelaySecond __attribute__((swift_name("httpRetryDelaySecond")));
@property (readonly) NSString * _Nullable httpTimeoutSecond __attribute__((swift_name("httpTimeoutSecond")));
@property (readonly) NSString * _Nullable k1 __attribute__((swift_name("k1")));
@property (readonly) NSString * _Nullable k1e __attribute__((swift_name("k1e")));
@property (readonly) NSString * _Nullable k1m __attribute__((swift_name("k1m")));
@property (readonly) NSString * _Nullable lang __attribute__((swift_name("lang")));
@property (readonly) NSString * _Nullable pullMaxRetryNum __attribute__((swift_name("pullMaxRetryNum")));
@property (readonly) NSString * _Nullable pushNotifTimeoutSecond __attribute__((swift_name("pushNotifTimeoutSecond")));
@property (readonly) NSString * _Nullable sizeLimit __attribute__((swift_name("sizeLimit")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResponsesCategory")))
@interface MainResponsesCategory : MainBase
- (instancetype)initWithName:(NSString *)name fields:(NSMutableArray<MainResponsesCategoryField *> *)fields __attribute__((swift_name("init(name:fields:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSMutableArray<MainResponsesCategoryField *> *)component2 __attribute__((swift_name("component2()")));
- (MainResponsesCategory *)doCopyName:(NSString *)name fields:(NSMutableArray<MainResponsesCategoryField *> *)fields __attribute__((swift_name("doCopy(name:fields:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSMutableArray<MainResponsesCategoryField *> *fields __attribute__((swift_name("fields")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResponsesCategoryField")))
@interface MainResponsesCategoryField : MainBase
- (instancetype)initWithName:(NSString *)name field:(MainField *)field __attribute__((swift_name("init(name:field:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (MainField *)component2 __attribute__((swift_name("component2()")));
- (MainResponsesCategoryField *)doCopyName:(NSString *)name field:(MainField *)field __attribute__((swift_name("doCopy(name:field:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) MainField *field __attribute__((swift_name("field")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("Result")))
@protocol MainResult
@required
@property (readonly) NSString * _Nullable code __attribute__((swift_name("code")));
@property (readonly) MainMutableDictionary<NSString *, NSString *> * _Nullable description_ __attribute__((swift_name("description_")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@property (readonly) int64_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Scenario")))
@interface MainScenario : MainBase
- (instancetype)initWithIdx:(NSString *)idx name:(NSString *)name captures:(NSMutableArray<MainProfileCapture *> *)captures responses:(NSMutableArray<MainResponsesCategory *> * _Nullable)responses infoData:(MainMutableDictionary<NSString *, id> * _Nullable)infoData __attribute__((swift_name("init(idx:name:captures:responses:infoData:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSMutableArray<MainProfileCapture *> *)component3 __attribute__((swift_name("component3()")));
- (NSMutableArray<MainResponsesCategory *> * _Nullable)component4 __attribute__((swift_name("component4()")));
- (MainMutableDictionary<NSString *, id> * _Nullable)component5 __attribute__((swift_name("component5()")));
- (MainScenario *)doCopyIdx:(NSString *)idx name:(NSString *)name captures:(NSMutableArray<MainProfileCapture *> *)captures responses:(NSMutableArray<MainResponsesCategory *> * _Nullable)responses infoData:(MainMutableDictionary<NSString *, id> * _Nullable)infoData __attribute__((swift_name("doCopy(idx:name:captures:responses:infoData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSMutableArray<MainProfileCapture *> *captures __attribute__((swift_name("captures")));
@property (readonly) NSString *idx __attribute__((swift_name("idx")));
@property (readonly) MainMutableDictionary<NSString *, id> * _Nullable infoData __attribute__((swift_name("infoData")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSMutableArray<MainResponsesCategory *> * _Nullable responses __attribute__((swift_name("responses")));
@end;

__attribute__((swift_name("Session")))
@protocol MainSession
@required
- (id<MainCaptorProvider> _Nullable)getCaptorsList __attribute__((swift_name("getCaptorsList()")));
- (NSString *)getScenarioId __attribute__((swift_name("getScenarioId()")));
- (BOOL)isStarted __attribute__((swift_name("isStarted()")));

/**
 @note This method converts instances of Exception to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)profileAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("profile()")));

/**
 @note This method converts instances of Exception to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)resultAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("result()")));

/**
 @note This method converts instances of Exception to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)resumeTaskId:(NSString *)taskId error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("resume(taskId:)")));

/**
 @note This method converts instances of Exception to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)startAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("start()")));

/**
 @note This method converts instances of Exception to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)stopAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("stop()")));

/**
 @note This method converts instances of Exception to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)updateTaskId:(NSString *)taskId input:(id<MainInput>)input error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("update(taskId:input:)")));
@property (readonly) MainStatus *status __attribute__((swift_name("status")));
@property (readonly) NSMutableArray<id<MainTask>> *tasks __attribute__((swift_name("tasks")));
@property (readonly) NSString *token __attribute__((swift_name("token")));
@end;

__attribute__((swift_name("KotlinComparable")))
@protocol MainKotlinComparable
@required
- (int32_t)compareToOther:(id _Nullable)other __attribute__((swift_name("compareTo(other:)")));
@end;

__attribute__((swift_name("KotlinEnum")))
@interface MainKotlinEnum<E> : MainBase <MainKotlinComparable>
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainKotlinEnumCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(E)other __attribute__((swift_name("compareTo(other:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) int32_t ordinal __attribute__((swift_name("ordinal")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Status")))
@interface MainStatus : MainKotlinEnum<MainStatus *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) MainStatus *created __attribute__((swift_name("created")));
@property (class, readonly) MainStatus *action __attribute__((swift_name("action")));
@property (class, readonly) MainStatus *pending __attribute__((swift_name("pending")));
@property (class, readonly) MainStatus *completed __attribute__((swift_name("completed")));
+ (MainKotlinArray<MainStatus *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((swift_name("Task")))
@protocol MainTask
@required
@property (readonly) NSString * _Nullable category __attribute__((swift_name("category")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TdiProfileInfo")))
@interface MainTdiProfileInfo : MainBase
- (instancetype)initWithVersion:(NSString *)version scenarios:(NSMutableArray<MainScenario *> *)scenarios captors:(MainMutableDictionary<NSString *, MainProfileCaptor *> *)captors configs:(MainProfileConfigs *)configs theme:(MainMutableDictionary<NSString *, MainKotlinx_serialization_jsonJsonElement *> * _Nullable)theme localization:(MainMutableDictionary<NSString *, id> *)localization __attribute__((swift_name("init(version:scenarios:captors:configs:theme:localization:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSMutableArray<MainScenario *> *)component2 __attribute__((swift_name("component2()")));
- (MainMutableDictionary<NSString *, MainProfileCaptor *> *)component3 __attribute__((swift_name("component3()")));
- (MainProfileConfigs *)component4 __attribute__((swift_name("component4()")));
- (MainMutableDictionary<NSString *, MainKotlinx_serialization_jsonJsonElement *> * _Nullable)component5 __attribute__((swift_name("component5()")));
- (MainMutableDictionary<NSString *, id> *)component6 __attribute__((swift_name("component6()")));
- (MainTdiProfileInfo *)doCopyVersion:(NSString *)version scenarios:(NSMutableArray<MainScenario *> *)scenarios captors:(MainMutableDictionary<NSString *, MainProfileCaptor *> *)captors configs:(MainProfileConfigs *)configs theme:(MainMutableDictionary<NSString *, MainKotlinx_serialization_jsonJsonElement *> * _Nullable)theme localization:(MainMutableDictionary<NSString *, id> *)localization __attribute__((swift_name("doCopy(version:scenarios:captors:configs:theme:localization:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) MainMutableDictionary<NSString *, MainProfileCaptor *> *captors __attribute__((swift_name("captors")));
@property (readonly) MainProfileConfigs *configs __attribute__((swift_name("configs")));
@property (readonly) MainMutableDictionary<NSString *, id> *localization __attribute__((swift_name("localization")));
@property (readonly) NSMutableArray<MainScenario *> *scenarios __attribute__((swift_name("scenarios")));
@property (readonly) MainMutableDictionary<NSString *, MainKotlinx_serialization_jsonJsonElement *> * _Nullable theme __attribute__((swift_name("theme")));
@property (readonly) NSString *version __attribute__((swift_name("version")));
@end;

__attribute__((swift_name("AbstractCaptorProviderImpl")))
@interface MainAbstractCaptorProviderImpl : MainBase <MainCaptorProvider>
- (instancetype)initWithCaptorFactory:(id<MainCaptorFactory>)captorFactory __attribute__((swift_name("init(captorFactory:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainAbstractCaptorProviderImplCompanion *companion __attribute__((swift_name("companion")));
- (id<MainCaptor> _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSDictionary<NSString *, id<MainCaptor>> * _Nullable)getAll __attribute__((swift_name("getAll()")));
- (void)setName:(NSString *)name captor:(id<MainCaptor>)captor __attribute__((swift_name("set(name:captor:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AbstractCaptorProviderImpl.Companion")))
@interface MainAbstractCaptorProviderImplCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainAbstractCaptorProviderImplCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("AbstractSessionImpl")))
@interface MainAbstractSessionImpl : MainBase <MainSession>
- (instancetype)initWithDispatcher:(MainKotlinx_coroutines_coreCoroutineDispatcher *)dispatcher captorProvider:(id<MainCaptorProvider>)captorProvider captorInvoker:(id<MainCaptorInvoker>)captorInvoker eventListener:(void (^)(MainTdiProfileInfo * _Nullable, MainScenarioInfo * _Nullable))eventListener __attribute__((swift_name("init(dispatcher:captorProvider:captorInvoker:eventListener:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainAbstractSessionImplCompanion *companion __attribute__((swift_name("companion")));
- (MainSessionStateManager *)createSessionStateManager __attribute__((swift_name("createSessionStateManager()")));
- (id<MainTask> _Nullable)getCurrentTask __attribute__((swift_name("getCurrentTask()")));
- (void)initialize __attribute__((swift_name("initialize()")));
- (void)onEventReceivedScenarioInfo:(MainScenarioInfo *)scenarioInfo __attribute__((swift_name("onEventReceived(scenarioInfo:)")));
- (void)profileImpl __attribute__((swift_name("profileImpl()")));
- (void)profileReceivedTdiProfileInfo:(MainTdiProfileInfo *)tdiProfileInfo __attribute__((swift_name("profileReceived(tdiProfileInfo:)")));
- (void)resultImpl __attribute__((swift_name("resultImpl()")));
- (void)resumeImplTaskId:(NSString *)taskId __attribute__((swift_name("resumeImpl(taskId:)")));
- (void)scenarioReceivedScenarioInfo:(MainScenarioInfo *)scenarioInfo __attribute__((swift_name("scenarioReceived(scenarioInfo:)")));
- (void)setCurrentTaskTask:(id<MainTask> _Nullable)task __attribute__((swift_name("setCurrentTask(task:)")));
- (void)startImpl __attribute__((swift_name("startImpl()")));
- (void)stopImpl __attribute__((swift_name("stopImpl()")));
- (void)updateImplTaskId:(NSString *)taskId input:(id<MainInput>)input __attribute__((swift_name("updateImpl(taskId:input:)")));
@property (readonly) id<MainCaptorProvider> captorProvider __attribute__((swift_name("captorProvider")));
@property (readonly) MainKotlinx_coroutines_coreCoroutineDispatcher *dispatcher __attribute__((swift_name("dispatcher")));
@property (readonly) MainStatus *status __attribute__((swift_name("status")));
@property (readonly) NSMutableArray<id<MainTask>> *tasks __attribute__((swift_name("tasks")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("AbstractSessionImpl.Companion")))
@interface MainAbstractSessionImplCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainAbstractSessionImplCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("CaptorFactory")))
@protocol MainCaptorFactory
@required
- (id<MainCaptor>)getConnectiveAsyncContractSignatureDocumentCaptor __attribute__((swift_name("getConnectiveAsyncContractSignatureDocumentCaptor()")));
- (id<MainCaptor>)getConnectiveSyncContractSignatureDocumentCaptor __attribute__((swift_name("getConnectiveSyncContractSignatureDocumentCaptor()")));
- (id<MainCaptor>)getContractSignatureDocumentCaptor __attribute__((swift_name("getContractSignatureDocumentCaptor()")));
- (id<MainCaptor>)getDocumentCaptor __attribute__((swift_name("getDocumentCaptor()")));
- (id<MainCaptor>)getFingerPrintCaptureCaptor __attribute__((swift_name("getFingerPrintCaptureCaptor()")));
- (id<MainCaptor>)getFullDocumentCaptor __attribute__((swift_name("getFullDocumentCaptor()")));
- (id<MainCaptor>)getIdDocumentCaptor __attribute__((swift_name("getIdDocumentCaptor()")));
- (id<MainCaptor>)getIdvFaceCaptor __attribute__((swift_name("getIdvFaceCaptor()")));
- (id<MainCaptor>)getKnomiLivenessCaptor __attribute__((swift_name("getKnomiLivenessCaptor()")));
- (id<MainCaptor>)getMrzDocumentCaptor __attribute__((swift_name("getMrzDocumentCaptor()")));
- (id<MainCaptor>)getMrzNfcDocumentCaptor __attribute__((swift_name("getMrzNfcDocumentCaptor()")));
- (id<MainCaptor>)getNfcDocumentCaptor __attribute__((swift_name("getNfcDocumentCaptor()")));
- (id<MainCaptor>)getPassportCaptor __attribute__((swift_name("getPassportCaptor()")));
- (id<MainCaptor>)getVoiceCaptor __attribute__((swift_name("getVoiceCaptor()")));
@end;

__attribute__((swift_name("CaptorInvoker")))
@protocol MainCaptorInvoker
@required
- (void)executeCaptor:(id<MainCaptor>)captor listener:(void (^)(id<MainInput>))listener __attribute__((swift_name("execute(captor:listener:)")));
@end;

__attribute__((swift_name("Dep")))
@protocol MainDep
@required
- (void)clearSession __attribute__((swift_name("clearSession()")));
- (MainKotlinByteArray * _Nullable)decodeRequestEncoded:(MainKotlinByteArray *)encoded __attribute__((swift_name("decodeRequest(encoded:)")));
- (MainKotlinByteArray * _Nullable)encodeRequestContentType:(NSString *)contentType body:(MainKotlinByteArray * _Nullable)body __attribute__((swift_name("encodeRequest(contentType:body:)")));
- (void)generateDEPSession __attribute__((swift_name("generateDEPSession()")));
@end;

__attribute__((swift_name("HttpApiFactory")))
@interface MainHttpApiFactory : MainBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id<MainTdiRequester>)getInitRequester __attribute__((swift_name("getInitRequester()")));
- (id<MainTdiProfileRequester>)getProfileRequester __attribute__((swift_name("getProfileRequester()")));
- (id<MainTdiRequester>)getScenarioRequesterScenarioId:(NSString *)scenarioId __attribute__((swift_name("getScenarioRequester(scenarioId:)")));
- (id<MainTdiRequester>)getStatePatcherScenarioId:(NSString *)scenarioId stepId:(NSString *)stepId statePatchRequest:(MainStatePatchRequest *)statePatchRequest __attribute__((swift_name("getStatePatcher(scenarioId:stepId:statePatchRequest:)")));
@property (readonly) NSString *scenarioName __attribute__((swift_name("scenarioName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("HttpApiFactoryImpl")))
@interface MainHttpApiFactoryImpl : MainHttpApiFactory
- (instancetype)initWithBaseURL:(NSString *)baseURL scenarioName:(NSString *)scenarioName buildUtils:(MainBuildUtils *)buildUtils token:(NSString *)token correlationId:(NSString *)correlationId tenantId:(NSString *)tenantId pushTokenProvider:(id<MainPushTokenProvider>)pushTokenProvider dep:(id<MainDep>)dep __attribute__((swift_name("init(baseURL:scenarioName:buildUtils:token:correlationId:tenantId:pushTokenProvider:dep:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (id<MainTdiRequester>)getInitRequester __attribute__((swift_name("getInitRequester()")));
- (id<MainTdiProfileRequester>)getProfileRequester __attribute__((swift_name("getProfileRequester()")));
- (id<MainTdiRequester>)getScenarioRequesterScenarioId:(NSString *)scenarioId __attribute__((swift_name("getScenarioRequester(scenarioId:)")));
- (id<MainTdiRequester>)getStatePatcherScenarioId:(NSString *)scenarioId stepId:(NSString *)stepId statePatchRequest:(MainStatePatchRequest *)statePatchRequest __attribute__((swift_name("getStatePatcher(scenarioId:stepId:statePatchRequest:)")));
@property (readonly) NSString *scenarioName __attribute__((swift_name("scenarioName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IOSSessionImpl")))
@interface MainIOSSessionImpl : MainAbstractSessionImpl
- (instancetype)initWithToken:(NSString *)token captorProvider:(id<MainCaptorProvider>)captorProvider httpApiFactory:(MainHttpApiFactory *)httpApiFactory idCloudHelper:(id<MainIdCloudHelper>)idCloudHelper eventListener:(void (^)(MainTdiProfileInfo * _Nullable, MainScenarioInfo * _Nullable))eventListener captorInvoker:(id<MainCaptorInvoker>)captorInvoker __attribute__((swift_name("init(token:captorProvider:httpApiFactory:idCloudHelper:eventListener:captorInvoker:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithDispatcher:(MainKotlinx_coroutines_coreCoroutineDispatcher *)dispatcher captorProvider:(id<MainCaptorProvider>)captorProvider captorInvoker:(id<MainCaptorInvoker>)captorInvoker eventListener:(void (^)(MainTdiProfileInfo * _Nullable, MainScenarioInfo * _Nullable))eventListener __attribute__((swift_name("init(dispatcher:captorProvider:captorInvoker:eventListener:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (MainSessionStateManager *)createSessionStateManager __attribute__((swift_name("createSessionStateManager()")));
- (id<MainCaptorProvider> _Nullable)getCaptorsList __attribute__((swift_name("getCaptorsList()")));
- (NSString *)getScenarioId __attribute__((swift_name("getScenarioId()")));
- (BOOL)isStarted __attribute__((swift_name("isStarted()")));

/**
 @note This method converts instances of Exception to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)profileAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("profile()")));

/**
 @note This method converts instances of Exception to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)resultAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("result()")));

/**
 @note This method converts instances of Exception to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)resumeTaskId:(NSString *)taskId error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("resume(taskId:)")));

/**
 @note This method converts instances of Exception to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)startAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("start()")));

/**
 @note This method converts instances of Exception to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)stopAndReturnError:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("stop()")));

/**
 @note This method converts instances of Exception to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (BOOL)updateTaskId:(NSString *)taskId input:(id<MainInput>)input error:(NSError * _Nullable * _Nullable)error __attribute__((swift_name("update(taskId:input:)")));
@property (readonly) NSString *token __attribute__((swift_name("token")));
@end;

__attribute__((swift_name("SessionStateManager")))
@interface MainSessionStateManager : MainBase
- (instancetype)initWithHttpApiFactory:(MainHttpApiFactory *)httpApiFactory __attribute__((swift_name("init(httpApiFactory:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainSessionStateManagerCompanion *companion __attribute__((swift_name("companion")));
- (MainKotlinPair<NSMutableArray<id<MainTask>> *, NSString *> *)onEventReceivedScenarioInfo:(MainScenarioInfo *)scenarioInfo __attribute__((swift_name("onEventReceived(scenarioInfo:)")));
- (void)profileSession:(MainAbstractSessionImpl *)session __attribute__((swift_name("profile(session:)")));
- (void)resultScenarioId:(NSString *)scenarioId session:(MainAbstractSessionImpl *)session __attribute__((swift_name("result(scenarioId:session:)")));
- (void)resumeStepId:(NSString *)stepId input:(id<MainInput>)input session:(MainAbstractSessionImpl *)session __attribute__((swift_name("resume(stepId:input:session:)")));
- (void)startSession:(MainAbstractSessionImpl *)session __attribute__((swift_name("start(session:)")));
- (void)stop __attribute__((swift_name("stop_()")));
- (void)updateStepId:(NSString *)stepId input:(id<MainInput>)input session:(MainAbstractSessionImpl *)session __attribute__((swift_name("update(stepId:input:session:)")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) MainStatus *status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("IOSSessionStateManager")))
@interface MainIOSSessionStateManager : MainSessionStateManager
- (instancetype)initWithHttpApiFactory:(MainHttpApiFactory *)httpApiFactory __attribute__((swift_name("init(httpApiFactory:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InputImpl")))
@interface MainInputImpl : MainBase <MainInput>
- (instancetype)initWithPages:(MainMutableDictionary<NSString *, NSString *> *)pages __attribute__((swift_name("init(pages:)"))) __attribute__((objc_designated_initializer));
- (MainMutableDictionary<NSString *, NSString *> *)component1 __attribute__((swift_name("component1()")));
- (MainInputImpl *)doCopyPages:(MainMutableDictionary<NSString *, NSString *> *)pages __attribute__((swift_name("doCopy(pages:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) MainMutableDictionary<NSString *, NSString *> *pages __attribute__((swift_name("pages")));
@end;

__attribute__((swift_name("PushTokenProvider")))
@protocol MainPushTokenProvider
@required
- (NSString * _Nullable)getToken __attribute__((swift_name("getToken()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TdiConstants")))
@interface MainTdiConstants : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)tdiConstants __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainTdiConstants *shared __attribute__((swift_name("shared")));
@property (readonly) BOOL ALLOW_SCREENSHOT __attribute__((swift_name("ALLOW_SCREENSHOT")));
@property (readonly) MainKtor_ioCharset *BODY_ENCODING_CHARACTERS __attribute__((swift_name("BODY_ENCODING_CHARACTERS")));
@property (readonly) BOOL DEBUG_DUMP_BODY __attribute__((swift_name("DEBUG_DUMP_BODY")));
@property BOOL DEBUG_FORCE_ENGLISH __attribute__((swift_name("DEBUG_FORCE_ENGLISH")));
@property (readonly) BOOL DEBUG_NETWORK_CHECK __attribute__((swift_name("DEBUG_NETWORK_CHECK")));
@property int32_t DEBUG_PATCH_HTTP_STATUS __attribute__((swift_name("DEBUG_PATCH_HTTP_STATUS")));
@property BOOL DEBUG_PATCH_PROFILE __attribute__((swift_name("DEBUG_PATCH_PROFILE")));
@property int32_t DEBUG_PATCH_TDI_ERROR __attribute__((swift_name("DEBUG_PATCH_TDI_ERROR")));
@property (readonly) BOOL DEBUG_TEST_ROOT __attribute__((swift_name("DEBUG_TEST_ROOT")));
@property (readonly) NSString *DEFAULT_LANGUAGE __attribute__((swift_name("DEFAULT_LANGUAGE")));
@property (readonly) BOOL DEV_MODE __attribute__((swift_name("DEV_MODE")));
@property (readonly) NSString *EXTRA_CAPTOR_CALLBACK __attribute__((swift_name("EXTRA_CAPTOR_CALLBACK")));
@property (readonly) NSString *FINISHED __attribute__((swift_name("FINISHED")));
@property (readonly) NSString *FINISHED_ERROR __attribute__((swift_name("FINISHED_ERROR")));
@property (readonly) NSString *FINISHED_FAILURE __attribute__((swift_name("FINISHED_FAILURE")));
@property (readonly) NSString *FINISHED_FALSE __attribute__((swift_name("FINISHED_FALSE")));
@property (readonly) NSString *FINISHED_SUCCESS __attribute__((swift_name("FINISHED_SUCCESS")));
@property (readonly) NSString *FINISHED_TRUE __attribute__((swift_name("FINISHED_TRUE")));
@property (readonly) NSString *GTO __attribute__((swift_name("GTO")));
@property (readonly) BOOL HTTP_DEBUG_MODE __attribute__((swift_name("HTTP_DEBUG_MODE")));
@property (readonly) BOOL HTTP_SOFT_LOG __attribute__((swift_name("HTTP_SOFT_LOG")));
@property (readonly) NSString *METADATA_ACCESS __attribute__((swift_name("METADATA_ACCESS")));
@property (readonly) NSString *METADATA_CONTRACT_ACCEPTANCE __attribute__((swift_name("METADATA_CONTRACT_ACCEPTANCE")));
@property (readonly) NSString *METADATA_MRZ_DOB __attribute__((swift_name("METADATA_MRZ_DOB")));
@property (readonly) NSString *METADATA_MRZ_DOC __attribute__((swift_name("METADATA_MRZ_DOC")));
@property (readonly) NSString *METADATA_MRZ_DOE __attribute__((swift_name("METADATA_MRZ_DOE")));
@property (readonly) NSString *METADATA_MRZ_LINES __attribute__((swift_name("METADATA_MRZ_LINES")));
@property (readonly) NSString *METADATA_PROTOCOL __attribute__((swift_name("METADATA_PROTOCOL")));
@property (readonly) NSString *METADATA_SDK_LIBRARY __attribute__((swift_name("METADATA_SDK_LIBRARY")));
@property (readonly) NSString *METADATA_SDK_VERSION __attribute__((swift_name("METADATA_SDK_VERSION")));
@property (readonly) BOOL POC_MODE __attribute__((swift_name("POC_MODE")));
@property (readonly) NSString *PROF_CAPTURES __attribute__((swift_name("PROF_CAPTURES")));
@property (readonly) NSString *PROF_CAPTURE_TYPE __attribute__((swift_name("PROF_CAPTURE_TYPE")));
@property (readonly) NSString *PROF_CATEGORY_DOC __attribute__((swift_name("PROF_CATEGORY_DOC")));
@property (readonly) NSString *PROF_CATEGORY_RESULT __attribute__((swift_name("PROF_CATEGORY_RESULT")));
@property (readonly) NSString *PROF_CATEGORY_USER __attribute__((swift_name("PROF_CATEGORY_USER")));
@property (readonly) NSString *PROF_COMMON __attribute__((swift_name("PROF_COMMON")));
@property (readonly) NSString *PROF_DEFAULT __attribute__((swift_name("PROF_DEFAULT")));
@property (readonly) NSString *PROF_ICON __attribute__((swift_name("PROF_ICON")));
@property (readonly) NSString *PROF_LOC_BUTTON_BACK __attribute__((swift_name("PROF_LOC_BUTTON_BACK")));
@property (readonly) NSString *PROF_LOC_BUTTON_NEXT __attribute__((swift_name("PROF_LOC_BUTTON_NEXT")));
@property (readonly) NSString *PROF_LOC_BUTTON_SKIP __attribute__((swift_name("PROF_LOC_BUTTON_SKIP")));
@property (readonly) NSString *PROF_LOC_DESCRIPTION __attribute__((swift_name("PROF_LOC_DESCRIPTION")));
@property (readonly) NSString *PROF_LOC_LABEL __attribute__((swift_name("PROF_LOC_LABEL")));
@property (readonly) NSString *PROF_LOC_PAGE __attribute__((swift_name("PROF_LOC_PAGE")));
@property (readonly) NSString *PROF_LOC_SEPARATOR __attribute__((swift_name("PROF_LOC_SEPARATOR")));
@property (readonly) NSString *PROF_PATTERN __attribute__((swift_name("PROF_PATTERN")));
@property (readonly) NSString *PROF_RESPONSES __attribute__((swift_name("PROF_RESPONSES")));
@property (readonly) NSString *RUNNING __attribute__((swift_name("RUNNING")));
@property (readonly) BOOL SOFT_LOG __attribute__((swift_name("SOFT_LOG")));
@property (readonly) BOOL SSL_PINNING __attribute__((swift_name("SSL_PINNING")));
@property NSString *STATIC_HTTP_RESPONSE __attribute__((swift_name("STATIC_HTTP_RESPONSE")));
@property NSString *STATIC_PROFILE_API __attribute__((swift_name("STATIC_PROFILE_API")));
@property (readonly) BOOL TEST_MODE __attribute__((swift_name("TEST_MODE")));
@property (readonly) BOOL TEST_SENSOR_LIGHT __attribute__((swift_name("TEST_SENSOR_LIGHT")));
@property (readonly) NSString *WAITING __attribute__((swift_name("WAITING")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ResultAdapter")))
@interface MainResultAdapter : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)resultAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainResultAdapter *shared __attribute__((swift_name("shared")));
- (id<MainResult> _Nullable)adaptFromScenarioInfoScenarioInfo:(MainScenarioInfo *)scenarioInfo __attribute__((swift_name("adaptFromScenarioInfo(scenarioInfo:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("SessionStateManager.Companion")))
@interface MainSessionStateManagerCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainSessionStateManagerCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TaskAdapter")))
@interface MainTaskAdapter : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)taskAdapter __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainTaskAdapter *shared __attribute__((swift_name("shared")));
- (NSMutableArray<id<MainTask>> * _Nullable)adaptFromScenarioInfoScenarioInfo:(MainScenarioInfo *)scenarioInfo __attribute__((swift_name("adaptFromScenarioInfo(scenarioInfo:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TaskImpl")))
@interface MainTaskImpl : MainBase <MainTask>
- (instancetype)initWithId:(NSString *)id name:(NSString * _Nullable)name status:(NSString * _Nullable)status category:(NSString * _Nullable)category __attribute__((swift_name("init(id:name:status:category:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (MainTaskImpl *)doCopyId:(NSString *)id name:(NSString * _Nullable)name status:(NSString * _Nullable)status category:(NSString * _Nullable)category __attribute__((swift_name("doCopy(id:name:status:category:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable category __attribute__((swift_name("category")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable status __attribute__((swift_name("status")));
@end;

__attribute__((swift_name("BuildUtils")))
@interface MainBuildUtils : MainBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property NSString *appAppVersionCode __attribute__((swift_name("appAppVersionCode")));
@property NSString *appId __attribute__((swift_name("appId")));
@property NSString *appVersionName __attribute__((swift_name("appVersionName")));
@property NSString *sdkVersionName __attribute__((swift_name("sdkVersionName")));
@end;

__attribute__((swift_name("IdCloudHelper")))
@protocol MainIdCloudHelper
@required
- (void)checkEnableRiskManagementScenarioName:(NSString *)scenarioName __attribute__((swift_name("checkEnableRiskManagement(scenarioName:)")));
- (void)resetRiskSDK __attribute__((swift_name("resetRiskSDK()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Logger")))
@interface MainLogger : MainBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) MainLoggerCompanion *companion __attribute__((swift_name("companion")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Logger.Companion")))
@interface MainLoggerCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainLoggerCompanion *shared __attribute__((swift_name("shared")));
- (void)debugTag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("debug(tag:message:)")));
- (void)debugHttpOut:(BOOL)out tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("debugHttp(out:tag:message:)")));
- (void)errorTag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("error(tag:message:)")));
- (void)errorHttpOut:(BOOL)out tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("errorHttp(out:tag:message:)")));
- (void)infoHttpOut:(BOOL)out tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("infoHttp(out:tag:message:)")));
- (void)softTag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("soft(tag:message:)")));
- (void)softHttpOut:(BOOL)out tag:(NSString *)tag message:(NSString *)message __attribute__((swift_name("softHttp(out:tag:message:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ContractProfile")))
@interface MainContractProfile : MainBase
- (instancetype)initWithTitle:(NSString * _Nullable)title description:(NSString * _Nullable)description buttonBack:(NSString * _Nullable)buttonBack buttonNext:(NSString * _Nullable)buttonNext icon:(NSString * _Nullable)icon steps:(NSArray<MainContractProfileSignature *> * _Nullable)steps __attribute__((swift_name("init(title:description:buttonBack:buttonNext:icon:steps:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (NSArray<MainContractProfileSignature *> * _Nullable)component6 __attribute__((swift_name("component6()")));
- (MainContractProfile *)doCopyTitle:(NSString * _Nullable)title description:(NSString * _Nullable)description buttonBack:(NSString * _Nullable)buttonBack buttonNext:(NSString * _Nullable)buttonNext icon:(NSString * _Nullable)icon steps:(NSArray<MainContractProfileSignature *> * _Nullable)steps __attribute__((swift_name("doCopy(title:description:buttonBack:buttonNext:icon:steps:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable buttonBack __attribute__((swift_name("buttonBack")));
@property (readonly) NSString * _Nullable buttonNext __attribute__((swift_name("buttonNext")));
@property (readonly) NSString * _Nullable description_ __attribute__((swift_name("description_")));
@property (readonly) NSString * _Nullable icon __attribute__((swift_name("icon")));
@property (readonly) NSArray<MainContractProfileSignature *> * _Nullable steps __attribute__((swift_name("steps")));
@property (readonly) NSString * _Nullable title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ContractProfile.Signature")))
@interface MainContractProfileSignature : MainBase
- (instancetype)initWithTitle:(NSString * _Nullable)title description:(NSString * _Nullable)description cancelLabel:(NSString * _Nullable)cancelLabel acceptLabel:(NSString * _Nullable)acceptLabel signatureData:(MainKotlinByteArray * _Nullable)signatureData __attribute__((swift_name("init(title:description:cancelLabel:acceptLabel:signatureData:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (MainKotlinByteArray * _Nullable)component5 __attribute__((swift_name("component5()")));
- (MainContractProfileSignature *)doCopyTitle:(NSString * _Nullable)title description:(NSString * _Nullable)description cancelLabel:(NSString * _Nullable)cancelLabel acceptLabel:(NSString * _Nullable)acceptLabel signatureData:(MainKotlinByteArray * _Nullable)signatureData __attribute__((swift_name("doCopy(title:description:cancelLabel:acceptLabel:signatureData:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable acceptLabel __attribute__((swift_name("acceptLabel")));
@property (readonly) NSString * _Nullable cancelLabel __attribute__((swift_name("cancelLabel")));
@property (readonly) NSString * _Nullable description_ __attribute__((swift_name("description_")));
@property MainKotlinByteArray * _Nullable signatureData __attribute__((swift_name("signatureData")));
@property (readonly) NSString * _Nullable title __attribute__((swift_name("title")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Result_")))
@interface MainResult : MainBase
- (instancetype)initWithCode:(NSString * _Nullable)code message:(NSString * _Nullable)message description:(MainMutableDictionary<NSString *, NSString *> * _Nullable)description __attribute__((swift_name("init(code:message:description:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (MainMutableDictionary<NSString *, NSString *> * _Nullable)component3 __attribute__((swift_name("component3()")));
- (MainResult *)doCopyCode:(NSString * _Nullable)code message:(NSString * _Nullable)message description:(MainMutableDictionary<NSString *, NSString *> * _Nullable)description __attribute__((swift_name("doCopy(code:message:description:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable code __attribute__((swift_name("code")));
@property (readonly) MainMutableDictionary<NSString *, NSString *> * _Nullable description_ __attribute__((swift_name("description_")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ScenarioInfo")))
@interface MainScenarioInfo : MainBase
- (instancetype)initWithId:(NSString *)id name:(NSString * _Nullable)name state:(MainState * _Nullable)state status:(NSString *)status __attribute__((swift_name("init(id:name:state:status:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString * _Nullable)component2 __attribute__((swift_name("component2()")));
- (MainState * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (MainScenarioInfo *)doCopyId:(NSString *)id name:(NSString * _Nullable)name state:(MainState * _Nullable)state status:(NSString *)status __attribute__((swift_name("doCopy(id:name:state:status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) MainState * _Nullable state __attribute__((swift_name("state")));
@property (readonly) NSString *status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("State")))
@interface MainState : MainBase
- (instancetype)initWithResult:(MainResult * _Nullable)result steps:(NSMutableArray<MainStep *> * _Nullable)steps __attribute__((swift_name("init(result:steps:)"))) __attribute__((objc_designated_initializer));
- (MainResult * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSMutableArray<MainStep *> * _Nullable)component2 __attribute__((swift_name("component2()")));
- (MainState *)doCopyResult:(MainResult * _Nullable)result steps:(NSMutableArray<MainStep *> * _Nullable)steps __attribute__((swift_name("doCopy(result:steps:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) MainResult * _Nullable result __attribute__((swift_name("result")));
@property (readonly) NSMutableArray<MainStep *> * _Nullable steps __attribute__((swift_name("steps")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Step")))
@interface MainStep : MainBase
- (instancetype)initWithDescription:(NSString * _Nullable)description id:(NSString *)id name:(NSString * _Nullable)name category:(NSString * _Nullable)category status:(NSString * _Nullable)status __attribute__((swift_name("init(description:id:name:category:status:)"))) __attribute__((objc_designated_initializer));
- (NSString * _Nullable)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (NSString * _Nullable)component3 __attribute__((swift_name("component3()")));
- (NSString * _Nullable)component4 __attribute__((swift_name("component4()")));
- (NSString * _Nullable)component5 __attribute__((swift_name("component5()")));
- (MainStep *)doCopyDescription:(NSString * _Nullable)description id:(NSString *)id name:(NSString * _Nullable)name category:(NSString * _Nullable)category status:(NSString * _Nullable)status __attribute__((swift_name("doCopy(description:id:name:category:status:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString * _Nullable category __attribute__((swift_name("category")));
@property (readonly) NSString * _Nullable description_ __attribute__((swift_name("description_")));
@property (readonly) NSString *id __attribute__((swift_name("id")));
@property (readonly) NSString * _Nullable name __attribute__((swift_name("name")));
@property (readonly) NSString * _Nullable status __attribute__((swift_name("status")));
@end;

__attribute__((swift_name("TdiRequester")))
@protocol MainTdiRequester
@required
- (void)executeSession:(MainAbstractSessionImpl *)session __attribute__((swift_name("execute(session:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitRequester")))
@interface MainInitRequester : MainBase <MainTdiRequester>
- (instancetype)initWithBaseURL:(NSString *)baseURL tenantId:(NSString *)tenantId headerMap:(NSDictionary<NSString *, NSString *> *)headerMap initRequest:(MainInitRequest *)initRequest dep:(id<MainDep>)dep __attribute__((swift_name("init(baseURL:tenantId:headerMap:initRequest:dep:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainInitRequesterCompanion *companion __attribute__((swift_name("companion")));
- (void)executeSession:(MainAbstractSessionImpl *)session __attribute__((swift_name("execute(session:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitRequester.Companion")))
@interface MainInitRequesterCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainInitRequesterCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("TdiProfileRequester")))
@protocol MainTdiProfileRequester
@required
- (void)executeSession:(MainAbstractSessionImpl *)session __attribute__((swift_name("execute(session:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ProfileLookAndFeel")))
@interface MainProfileLookAndFeel : MainBase
- (instancetype)initWithMobileApp:(MainMutableDictionary<NSString *, MainKotlinx_serialization_jsonJsonElement *> * _Nullable)mobileApp __attribute__((swift_name("init(mobileApp:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainProfileLookAndFeelCompanion *companion __attribute__((swift_name("companion")));
@property (readonly) MainMutableDictionary<NSString *, MainKotlinx_serialization_jsonJsonElement *> * _Nullable mobileApp __attribute__((swift_name("mobileApp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("ProfileLookAndFeel.Companion")))
@interface MainProfileLookAndFeelCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainProfileLookAndFeelCompanion *shared __attribute__((swift_name("shared")));
- (id<MainKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitRequest")))
@interface MainInitRequest : MainBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainInitRequestCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (MainInitRequest *)doCopyName:(NSString *)name __attribute__((swift_name("doCopy(name:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("InitRequest.Companion")))
@interface MainInitRequestCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainInitRequestCompanion *shared __attribute__((swift_name("shared")));
- (id<MainKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StatePatchRequest")))
@interface MainStatePatchRequest : MainBase
- (instancetype)initWithName:(NSString *)name input:(MainMutableDictionary<NSString *, NSString *> *)input __attribute__((swift_name("init(name:input:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainStatePatchRequestCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (MainMutableDictionary<NSString *, NSString *> *)component2 __attribute__((swift_name("component2()")));
- (MainStatePatchRequest *)doCopyName:(NSString *)name input:(MainMutableDictionary<NSString *, NSString *> *)input __attribute__((swift_name("doCopy(name:input:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) MainMutableDictionary<NSString *, NSString *> *input __attribute__((swift_name("input")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("StatePatchRequest.Companion")))
@interface MainStatePatchRequestCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainStatePatchRequestCompanion *shared __attribute__((swift_name("shared")));
- (id<MainKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("TdiClient")))
@interface MainTdiClient : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)tdiClient __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainTdiClient *shared __attribute__((swift_name("shared")));
- (MainKtor_client_coreHttpClient *)getPlatformClientUrl:(NSString *)url __attribute__((swift_name("getPlatformClient(url:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinByteArray")))
@interface MainKotlinByteArray : MainBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(MainByte *(^)(MainInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (int8_t)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (MainKotlinByteIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

@interface MainKotlinByteArray (Extensions)
- (NSData *)toData __attribute__((swift_name("toData()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("NSDataByteArrayKt")))
@interface MainNSDataByteArrayKt : MainBase
+ (MainKotlinByteArray *)toByteArray:(NSData *)receiver __attribute__((swift_name("toByteArray(_:)")));
@end;

__attribute__((swift_name("KotlinThrowable")))
@interface MainKotlinThrowable : MainBase
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (MainKotlinArray<NSString *> *)getStackTrace __attribute__((swift_name("getStackTrace()")));
- (void)printStackTrace __attribute__((swift_name("printStackTrace()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) MainKotlinThrowable * _Nullable cause __attribute__((swift_name("cause")));
@property (readonly) NSString * _Nullable message __attribute__((swift_name("message")));
- (NSError *)asError __attribute__((swift_name("asError()")));
@end;

__attribute__((swift_name("KotlinException")))
@interface MainKotlinException : MainKotlinThrowable
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinEnumCompanion")))
@interface MainKotlinEnumCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKotlinEnumCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinArray")))
@interface MainKotlinArray<T> : MainBase
+ (instancetype)arrayWithSize:(int32_t)size init:(T _Nullable (^)(MainInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (T _Nullable)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (id<MainKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(T _Nullable)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((swift_name("Kotlinx_serialization_jsonJsonElement")))
@interface MainKotlinx_serialization_jsonJsonElement : MainBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) MainKotlinx_serialization_jsonJsonElementCompanion *companion __attribute__((swift_name("companion")));
@end;

__attribute__((swift_name("KotlinCoroutineContext")))
@protocol MainKotlinCoroutineContext
@required
- (id _Nullable)foldInitial:(id _Nullable)initial operation:(id _Nullable (^)(id _Nullable, id<MainKotlinCoroutineContextElement>))operation __attribute__((swift_name("fold(initial:operation:)")));
- (id<MainKotlinCoroutineContextElement> _Nullable)getKey:(id<MainKotlinCoroutineContextKey>)key __attribute__((swift_name("get(key:)")));
- (id<MainKotlinCoroutineContext>)minusKeyKey:(id<MainKotlinCoroutineContextKey>)key __attribute__((swift_name("minusKey(key:)")));
- (id<MainKotlinCoroutineContext>)plusContext:(id<MainKotlinCoroutineContext>)context __attribute__((swift_name("plus(context:)")));
@end;

__attribute__((swift_name("KotlinCoroutineContextElement")))
@protocol MainKotlinCoroutineContextElement <MainKotlinCoroutineContext>
@required
@property (readonly) id<MainKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("KotlinAbstractCoroutineContextElement")))
@interface MainKotlinAbstractCoroutineContextElement : MainBase <MainKotlinCoroutineContextElement>
- (instancetype)initWithKey:(id<MainKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer));
@property (readonly) id<MainKotlinCoroutineContextKey> key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("KotlinContinuationInterceptor")))
@protocol MainKotlinContinuationInterceptor <MainKotlinCoroutineContextElement>
@required
- (id<MainKotlinContinuation>)interceptContinuationContinuation:(id<MainKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (void)releaseInterceptedContinuationContinuation:(id<MainKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineDispatcher")))
@interface MainKotlinx_coroutines_coreCoroutineDispatcher : MainKotlinAbstractCoroutineContextElement <MainKotlinContinuationInterceptor>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithKey:(id<MainKotlinCoroutineContextKey>)key __attribute__((swift_name("init(key:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) MainKotlinx_coroutines_coreCoroutineDispatcherKey *companion __attribute__((swift_name("companion")));
- (void)dispatchContext:(id<MainKotlinCoroutineContext>)context block:(id<MainKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatch(context:block:)")));
- (void)dispatchYieldContext:(id<MainKotlinCoroutineContext>)context block:(id<MainKotlinx_coroutines_coreRunnable>)block __attribute__((swift_name("dispatchYield(context:block:)")));
- (id<MainKotlinContinuation>)interceptContinuationContinuation:(id<MainKotlinContinuation>)continuation __attribute__((swift_name("interceptContinuation(continuation:)")));
- (BOOL)isDispatchNeededContext:(id<MainKotlinCoroutineContext>)context __attribute__((swift_name("isDispatchNeeded(context:)")));
- (MainKotlinx_coroutines_coreCoroutineDispatcher *)plusOther:(MainKotlinx_coroutines_coreCoroutineDispatcher *)other __attribute__((swift_name("plus(other:)"))) __attribute__((unavailable("Operator '+' on two CoroutineDispatcher objects is meaningless. CoroutineDispatcher is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The dispatcher to the right of `+` just replaces the dispatcher to the left.")));
- (void)releaseInterceptedContinuationContinuation:(id<MainKotlinContinuation>)continuation __attribute__((swift_name("releaseInterceptedContinuation(continuation:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinPair")))
@interface MainKotlinPair<__covariant A, __covariant B> : MainBase
- (instancetype)initWithFirst:(A _Nullable)first second:(B _Nullable)second __attribute__((swift_name("init(first:second:)"))) __attribute__((objc_designated_initializer));
- (A _Nullable)component1 __attribute__((swift_name("component1()")));
- (B _Nullable)component2 __attribute__((swift_name("component2()")));
- (MainKotlinPair<A, B> *)doCopyFirst:(A _Nullable)first second:(B _Nullable)second __attribute__((swift_name("doCopy(first:second:)")));
- (BOOL)equalsOther:(id _Nullable)other __attribute__((swift_name("equals(other:)")));
- (int32_t)hashCode __attribute__((swift_name("hashCode()")));
- (NSString *)toString __attribute__((swift_name("toString()")));
@property (readonly) A _Nullable first __attribute__((swift_name("first")));
@property (readonly) B _Nullable second __attribute__((swift_name("second")));
@end;

__attribute__((swift_name("Ktor_ioCharset")))
@interface MainKtor_ioCharset : MainBase
- (instancetype)initWith_name:(NSString *)_name __attribute__((swift_name("init(_name:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainKtor_ioCharsetCompanion *companion __attribute__((swift_name("companion")));
- (MainKtor_ioCharsetDecoder *)doNewDecoder __attribute__((swift_name("doNewDecoder()")));
- (MainKtor_ioCharsetEncoder *)doNewEncoder __attribute__((swift_name("doNewEncoder()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializationStrategy")))
@protocol MainKotlinx_serialization_coreSerializationStrategy
@required
- (void)serializeEncoder:(id<MainKotlinx_serialization_coreEncoder>)encoder value:(id _Nullable)value __attribute__((swift_name("serialize(encoder:value:)")));
@property (readonly) id<MainKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreDeserializationStrategy")))
@protocol MainKotlinx_serialization_coreDeserializationStrategy
@required
- (id _Nullable)deserializeDecoder:(id<MainKotlinx_serialization_coreDecoder>)decoder __attribute__((swift_name("deserialize(decoder:)")));
@property (readonly) id<MainKotlinx_serialization_coreSerialDescriptor> descriptor __attribute__((swift_name("descriptor")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreKSerializer")))
@protocol MainKotlinx_serialization_coreKSerializer <MainKotlinx_serialization_coreSerializationStrategy, MainKotlinx_serialization_coreDeserializationStrategy>
@required
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineScope")))
@protocol MainKotlinx_coroutines_coreCoroutineScope
@required
@property (readonly) id<MainKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@end;

__attribute__((swift_name("Ktor_ioCloseable")))
@protocol MainKtor_ioCloseable
@required
- (void)close __attribute__((swift_name("close()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClient")))
@interface MainKtor_client_coreHttpClient : MainBase <MainKotlinx_coroutines_coreCoroutineScope, MainKtor_ioCloseable>
- (instancetype)initWithEngine:(id<MainKtor_client_coreHttpClientEngine>)engine userConfig:(MainKtor_client_coreHttpClientConfig<MainKtor_client_coreHttpClientEngineConfig *> *)userConfig __attribute__((swift_name("init(engine:userConfig:)"))) __attribute__((objc_designated_initializer));
- (void)close __attribute__((swift_name("close()")));
- (MainKtor_client_coreHttpClient *)configBlock:(void (^)(MainKtor_client_coreHttpClientConfig<id> *))block __attribute__((swift_name("config(block:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeBuilder:(MainKtor_client_coreHttpRequestBuilder *)builder completionHandler:(void (^)(MainKtor_client_coreHttpClientCall * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(builder:completionHandler:)"))) __attribute__((unavailable("Unbound [HttpClientCall] is deprecated. Consider using [request<HttpResponse>(builder)] instead.")));
- (BOOL)isSupportedCapability:(id<MainKtor_client_coreHttpClientEngineCapability>)capability __attribute__((swift_name("isSupported(capability:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<MainKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) id<MainKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@property (readonly) MainKotlinx_coroutines_coreCoroutineDispatcher *dispatcher __attribute__((swift_name("dispatcher"))) __attribute__((unavailable("[dispatcher] is deprecated. Use coroutineContext instead.")));
@property (readonly) id<MainKtor_client_coreHttpClientEngine> engine __attribute__((swift_name("engine")));
@property (readonly) MainKtor_client_coreHttpClientEngineConfig *engineConfig __attribute__((swift_name("engineConfig")));
@property (readonly) MainKtor_client_coreHttpReceivePipeline *receivePipeline __attribute__((swift_name("receivePipeline")));
@property (readonly) MainKtor_client_coreHttpRequestPipeline *requestPipeline __attribute__((swift_name("requestPipeline")));
@property (readonly) MainKtor_client_coreHttpResponsePipeline *responsePipeline __attribute__((swift_name("responsePipeline")));
@property (readonly) MainKtor_client_coreHttpSendPipeline *sendPipeline __attribute__((swift_name("sendPipeline")));
@end;

__attribute__((swift_name("KotlinIterator")))
@protocol MainKotlinIterator
@required
- (BOOL)hasNext __attribute__((swift_name("hasNext()")));
- (id _Nullable)next __attribute__((swift_name("next()")));
@end;

__attribute__((swift_name("KotlinByteIterator")))
@interface MainKotlinByteIterator : MainBase <MainKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (MainByte *)next __attribute__((swift_name("next()")));
- (int8_t)nextByte __attribute__((swift_name("nextByte()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_serialization_jsonJsonElement.Companion")))
@interface MainKotlinx_serialization_jsonJsonElementCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKotlinx_serialization_jsonJsonElementCompanion *shared __attribute__((swift_name("shared")));
- (id<MainKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("serializer()")));
@end;

__attribute__((swift_name("KotlinCoroutineContextKey")))
@protocol MainKotlinCoroutineContextKey
@required
@end;

__attribute__((swift_name("KotlinContinuation")))
@protocol MainKotlinContinuation
@required
- (void)resumeWithResult:(id _Nullable)result __attribute__((swift_name("resumeWith(result:)")));
@property (readonly) id<MainKotlinCoroutineContext> context __attribute__((swift_name("context")));
@end;

__attribute__((swift_name("KotlinAbstractCoroutineContextKey")))
@interface MainKotlinAbstractCoroutineContextKey<B, E> : MainBase <MainKotlinCoroutineContextKey>
- (instancetype)initWithBaseKey:(id<MainKotlinCoroutineContextKey>)baseKey safeCast:(E _Nullable (^)(id<MainKotlinCoroutineContextElement>))safeCast __attribute__((swift_name("init(baseKey:safeCast:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_coroutines_coreCoroutineDispatcher.Key")))
@interface MainKotlinx_coroutines_coreCoroutineDispatcherKey : MainKotlinAbstractCoroutineContextKey<id<MainKotlinContinuationInterceptor>, MainKotlinx_coroutines_coreCoroutineDispatcher *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithBaseKey:(id<MainKotlinCoroutineContextKey>)baseKey safeCast:(id<MainKotlinCoroutineContextElement> _Nullable (^)(id<MainKotlinCoroutineContextElement>))safeCast __attribute__((swift_name("init(baseKey:safeCast:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)key __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKotlinx_coroutines_coreCoroutineDispatcherKey *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreRunnable")))
@protocol MainKotlinx_coroutines_coreRunnable
@required
- (void)run __attribute__((swift_name("run()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioCharset.Companion")))
@interface MainKtor_ioCharsetCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_ioCharsetCompanion *shared __attribute__((swift_name("shared")));
- (MainKtor_ioCharset *)forNameName:(NSString *)name __attribute__((swift_name("forName(name:)")));
@end;

__attribute__((swift_name("Ktor_ioCharsetDecoder")))
@interface MainKtor_ioCharsetDecoder : MainBase
- (instancetype)initWith_charset:(MainKtor_ioCharset *)_charset __attribute__((swift_name("init(_charset:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("Ktor_ioCharsetEncoder")))
@interface MainKtor_ioCharsetEncoder : MainBase
- (instancetype)initWith_charset:(MainKtor_ioCharset *)_charset __attribute__((swift_name("init(_charset:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreEncoder")))
@protocol MainKotlinx_serialization_coreEncoder
@required
- (id<MainKotlinx_serialization_coreCompositeEncoder>)beginCollectionDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor collectionSize:(int32_t)collectionSize __attribute__((swift_name("beginCollection(descriptor:collectionSize:)")));
- (id<MainKotlinx_serialization_coreCompositeEncoder>)beginStructureDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (void)encodeBooleanValue:(BOOL)value __attribute__((swift_name("encodeBoolean(value:)")));
- (void)encodeByteValue:(int8_t)value __attribute__((swift_name("encodeByte(value:)")));
- (void)encodeCharValue:(unichar)value __attribute__((swift_name("encodeChar(value:)")));
- (void)encodeDoubleValue:(double)value __attribute__((swift_name("encodeDouble(value:)")));
- (void)encodeEnumEnumDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)enumDescriptor index:(int32_t)index __attribute__((swift_name("encodeEnum(enumDescriptor:index:)")));
- (void)encodeFloatValue:(float)value __attribute__((swift_name("encodeFloat(value:)")));
- (id<MainKotlinx_serialization_coreEncoder>)encodeInlineInlineDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("encodeInline(inlineDescriptor:)")));
- (void)encodeIntValue:(int32_t)value __attribute__((swift_name("encodeInt(value:)")));
- (void)encodeLongValue:(int64_t)value __attribute__((swift_name("encodeLong(value:)")));
- (void)encodeNotNullMark __attribute__((swift_name("encodeNotNullMark()")));
- (void)encodeNull __attribute__((swift_name("encodeNull()")));
- (void)encodeNullableSerializableValueSerializer:(id<MainKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableValue(serializer:value:)")));
- (void)encodeSerializableValueSerializer:(id<MainKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableValue(serializer:value:)")));
- (void)encodeShortValue:(int16_t)value __attribute__((swift_name("encodeShort(value:)")));
- (void)encodeStringValue:(NSString *)value __attribute__((swift_name("encodeString(value:)")));
@property (readonly) MainKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialDescriptor")))
@protocol MainKotlinx_serialization_coreSerialDescriptor
@required
- (NSArray<id<MainKotlinAnnotation>> *)getElementAnnotationsIndex:(int32_t)index __attribute__((swift_name("getElementAnnotations(index:)")));
- (id<MainKotlinx_serialization_coreSerialDescriptor>)getElementDescriptorIndex:(int32_t)index __attribute__((swift_name("getElementDescriptor(index:)")));
- (int32_t)getElementIndexName:(NSString *)name __attribute__((swift_name("getElementIndex(name:)")));
- (NSString *)getElementNameIndex:(int32_t)index __attribute__((swift_name("getElementName(index:)")));
- (BOOL)isElementOptionalIndex:(int32_t)index __attribute__((swift_name("isElementOptional(index:)")));
@property (readonly) NSArray<id<MainKotlinAnnotation>> *annotations __attribute__((swift_name("annotations")));
@property (readonly) int32_t elementsCount __attribute__((swift_name("elementsCount")));
@property (readonly) BOOL isInline __attribute__((swift_name("isInline")));
@property (readonly) BOOL isNullable __attribute__((swift_name("isNullable")));
@property (readonly) MainKotlinx_serialization_coreSerialKind *kind __attribute__((swift_name("kind")));
@property (readonly) NSString *serialName __attribute__((swift_name("serialName")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreDecoder")))
@protocol MainKotlinx_serialization_coreDecoder
@required
- (id<MainKotlinx_serialization_coreCompositeDecoder>)beginStructureDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("beginStructure(descriptor:)")));
- (BOOL)decodeBoolean __attribute__((swift_name("decodeBoolean()")));
- (int8_t)decodeByte __attribute__((swift_name("decodeByte()")));
- (unichar)decodeChar __attribute__((swift_name("decodeChar()")));
- (double)decodeDouble __attribute__((swift_name("decodeDouble()")));
- (int32_t)decodeEnumEnumDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)enumDescriptor __attribute__((swift_name("decodeEnum(enumDescriptor:)")));
- (float)decodeFloat __attribute__((swift_name("decodeFloat()")));
- (id<MainKotlinx_serialization_coreDecoder>)decodeInlineInlineDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)inlineDescriptor __attribute__((swift_name("decodeInline(inlineDescriptor:)")));
- (int32_t)decodeInt __attribute__((swift_name("decodeInt()")));
- (int64_t)decodeLong __attribute__((swift_name("decodeLong()")));
- (BOOL)decodeNotNullMark __attribute__((swift_name("decodeNotNullMark()")));
- (MainKotlinNothing * _Nullable)decodeNull __attribute__((swift_name("decodeNull()")));
- (id _Nullable)decodeNullableSerializableValueDeserializer:(id<MainKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeNullableSerializableValue(deserializer:)")));
- (id _Nullable)decodeSerializableValueDeserializer:(id<MainKotlinx_serialization_coreDeserializationStrategy>)deserializer __attribute__((swift_name("decodeSerializableValue(deserializer:)")));
- (int16_t)decodeShort __attribute__((swift_name("decodeShort()")));
- (NSString *)decodeString __attribute__((swift_name("decodeString()")));
@property (readonly) MainKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngine")))
@protocol MainKtor_client_coreHttpClientEngine <MainKotlinx_coroutines_coreCoroutineScope, MainKtor_ioCloseable>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeData:(MainKtor_client_coreHttpRequestData *)data completionHandler:(void (^)(MainKtor_client_coreHttpResponseData * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(data:completionHandler:)")));
- (void)installClient:(MainKtor_client_coreHttpClient *)client __attribute__((swift_name("install(client:)")));
@property (readonly) MainKtor_client_coreHttpClientEngineConfig *config __attribute__((swift_name("config")));
@property (readonly) MainKotlinx_coroutines_coreCoroutineDispatcher *dispatcher __attribute__((swift_name("dispatcher")));
@property (readonly) NSSet<id<MainKtor_client_coreHttpClientEngineCapability>> *supportedCapabilities __attribute__((swift_name("supportedCapabilities")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineConfig")))
@interface MainKtor_client_coreHttpClientEngineConfig : MainBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property BOOL pipelining __attribute__((swift_name("pipelining")));
@property MainKtor_client_coreProxyConfig * _Nullable proxy __attribute__((swift_name("proxy")));
@property (readonly) MainKotlinNothing *response __attribute__((swift_name("response"))) __attribute__((unavailable("Response config is deprecated. See [HttpPlainText] feature for charset configuration")));
@property int32_t threadsCount __attribute__((swift_name("threadsCount")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClientConfig")))
@interface MainKtor_client_coreHttpClientConfig<T> : MainBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (MainKtor_client_coreHttpClientConfig<T> *)clone __attribute__((swift_name("clone()")));
- (void)engineBlock:(void (^)(T))block __attribute__((swift_name("engine(block:)")));
- (void)installClient:(MainKtor_client_coreHttpClient *)client __attribute__((swift_name("install(client:)")));
- (void)installFeature:(id<MainKtor_client_coreHttpClientFeature>)feature configure:(void (^)(id))configure __attribute__((swift_name("install(feature:configure:)")));
- (void)installKey:(NSString *)key block:(void (^)(MainKtor_client_coreHttpClient *))block __attribute__((swift_name("install(key:block:)")));
- (void)plusAssignOther:(MainKtor_client_coreHttpClientConfig<T> *)other __attribute__((swift_name("plusAssign(other:)")));
@property BOOL developmentMode __attribute__((swift_name("developmentMode")));
@property BOOL expectSuccess __attribute__((swift_name("expectSuccess")));
@property BOOL followRedirects __attribute__((swift_name("followRedirects")));
@property BOOL useDefaultTransformers __attribute__((swift_name("useDefaultTransformers")));
@end;

__attribute__((swift_name("KotlinRuntimeException")))
@interface MainKotlinRuntimeException : MainKotlinException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinIllegalStateException")))
@interface MainKotlinIllegalStateException : MainKotlinRuntimeException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("KotlinCancellationException")))
@interface MainKotlinCancellationException : MainKotlinIllegalStateException
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (instancetype)initWithMessage:(NSString * _Nullable)message __attribute__((swift_name("init(message:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMessage:(NSString * _Nullable)message cause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(message:cause:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCause:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("init(cause:)"))) __attribute__((objc_designated_initializer));
@end;

__attribute__((swift_name("Ktor_httpHttpMessageBuilder")))
@protocol MainKtor_httpHttpMessageBuilder
@required
@property (readonly) MainKtor_httpHeadersBuilder *headers __attribute__((swift_name("headers")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestBuilder")))
@interface MainKtor_client_coreHttpRequestBuilder : MainBase <MainKtor_httpHttpMessageBuilder>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
@property (class, readonly, getter=companion) MainKtor_client_coreHttpRequestBuilderCompanion *companion __attribute__((swift_name("companion")));
- (MainKtor_client_coreHttpRequestData *)build __attribute__((swift_name("build()")));
- (id _Nullable)getCapabilityOrNullKey:(id<MainKtor_client_coreHttpClientEngineCapability>)key __attribute__((swift_name("getCapabilityOrNull(key:)")));
- (void)setAttributesBlock:(void (^)(id<MainKtor_utilsAttributes>))block __attribute__((swift_name("setAttributes(block:)")));
- (void)setCapabilityKey:(id<MainKtor_client_coreHttpClientEngineCapability>)key capability:(id)capability __attribute__((swift_name("setCapability(key:capability:)")));
- (MainKtor_client_coreHttpRequestBuilder *)takeFromBuilder:(MainKtor_client_coreHttpRequestBuilder *)builder __attribute__((swift_name("takeFrom(builder:)")));
- (MainKtor_client_coreHttpRequestBuilder *)takeFromWithExecutionContextBuilder:(MainKtor_client_coreHttpRequestBuilder *)builder __attribute__((swift_name("takeFromWithExecutionContext(builder:)")));
- (void)urlBlock:(void (^)(MainKtor_httpURLBuilder *, MainKtor_httpURLBuilder *))block __attribute__((swift_name("url(block:)")));
@property (readonly) id<MainKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property id body __attribute__((swift_name("body")));
@property (readonly) id<MainKotlinx_coroutines_coreJob> executionContext __attribute__((swift_name("executionContext")));
@property (readonly) MainKtor_httpHeadersBuilder *headers __attribute__((swift_name("headers")));
@property MainKtor_httpHttpMethod *method __attribute__((swift_name("method")));
@property (readonly) MainKtor_httpURLBuilder *url __attribute__((swift_name("url")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientCall")))
@interface MainKtor_client_coreHttpClientCall : MainBase <MainKotlinx_coroutines_coreCoroutineScope>
@property (class, readonly, getter=companion) MainKtor_client_coreHttpClientCallCompanion *companion __attribute__((swift_name("companion")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)getResponseContentWithCompletionHandler:(void (^)(id<MainKtor_ioByteReadChannel> _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("getResponseContent(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)receiveInfo:(MainKtor_client_coreTypeInfo *)info completionHandler:(void (^)(id _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("receive(info:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)receiveInfo:(id<MainKtor_utilsTypeInfo>)info completionHandler_:(void (^)(id _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("receive(info:completionHandler_:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL allowDoubleReceive __attribute__((swift_name("allowDoubleReceive")));
@property (readonly) id<MainKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) MainKtor_client_coreHttpClient * _Nullable client __attribute__((swift_name("client")));
@property (readonly) id<MainKotlinCoroutineContext> coroutineContext __attribute__((swift_name("coroutineContext")));
@property (readonly) id<MainKtor_client_coreHttpRequest> request __attribute__((swift_name("request")));
@property (readonly) MainKtor_client_coreHttpResponse *response __attribute__((swift_name("response")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientEngineCapability")))
@protocol MainKtor_client_coreHttpClientEngineCapability
@required
@end;

__attribute__((swift_name("Ktor_utilsAttributes")))
@protocol MainKtor_utilsAttributes
@required
- (id)computeIfAbsentKey:(MainKtor_utilsAttributeKey<id> *)key block:(id (^)(void))block __attribute__((swift_name("computeIfAbsent(key:block:)")));
- (BOOL)containsKey:(MainKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("contains(key:)")));
- (id)getKey_:(MainKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("get(key_:)")));
- (id _Nullable)getOrNullKey:(MainKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("getOrNull(key:)")));
- (void)putKey:(MainKtor_utilsAttributeKey<id> *)key value:(id)value __attribute__((swift_name("put(key:value:)")));
- (void)removeKey:(MainKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("remove(key:)")));
- (id)takeKey:(MainKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("take(key:)")));
- (id _Nullable)takeOrNullKey:(MainKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("takeOrNull(key:)")));
@property (readonly) NSArray<MainKtor_utilsAttributeKey<id> *> *allKeys __attribute__((swift_name("allKeys")));
@end;

__attribute__((swift_name("Ktor_utilsPipeline")))
@interface MainKtor_utilsPipeline<TSubject, TContext> : MainBase
- (instancetype)initWithPhase:(MainKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<MainKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhases:(MainKotlinArray<MainKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer));
- (void)addPhasePhase:(MainKtor_utilsPipelinePhase *)phase __attribute__((swift_name("addPhase(phase:)")));
- (void)afterIntercepted __attribute__((swift_name("afterIntercepted()")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)executeContext:(TContext)context subject:(TSubject)subject completionHandler:(void (^)(TSubject _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("execute(context:subject:completionHandler:)")));
- (void)insertPhaseAfterReference:(MainKtor_utilsPipelinePhase *)reference phase:(MainKtor_utilsPipelinePhase *)phase __attribute__((swift_name("insertPhaseAfter(reference:phase:)")));
- (void)insertPhaseBeforeReference:(MainKtor_utilsPipelinePhase *)reference phase:(MainKtor_utilsPipelinePhase *)phase __attribute__((swift_name("insertPhaseBefore(reference:phase:)")));
- (void)interceptPhase:(MainKtor_utilsPipelinePhase *)phase block:(id<MainKotlinSuspendFunction2>)block __attribute__((swift_name("intercept(phase:block:)")));
- (void)mergeFrom:(MainKtor_utilsPipeline<TSubject, TContext> *)from __attribute__((swift_name("merge(from:)")));
@property (readonly) id<MainKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@property (readonly) BOOL isEmpty __attribute__((swift_name("isEmpty")));
@property (readonly) NSArray<MainKtor_utilsPipelinePhase *> *items __attribute__((swift_name("items")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpReceivePipeline")))
@interface MainKtor_client_coreHttpReceivePipeline : MainKtor_utilsPipeline<MainKtor_client_coreHttpResponse *, MainKtor_client_coreHttpClientCall *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(MainKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<MainKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(MainKotlinArray<MainKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) MainKtor_client_coreHttpReceivePipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestPipeline")))
@interface MainKtor_client_coreHttpRequestPipeline : MainKtor_utilsPipeline<id, MainKtor_client_coreHttpRequestBuilder *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(MainKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<MainKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(MainKotlinArray<MainKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) MainKtor_client_coreHttpRequestPipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponsePipeline")))
@interface MainKtor_client_coreHttpResponsePipeline : MainKtor_utilsPipeline<MainKtor_client_coreHttpResponseContainer *, MainKtor_client_coreHttpClientCall *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(MainKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<MainKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(MainKotlinArray<MainKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) MainKtor_client_coreHttpResponsePipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpSendPipeline")))
@interface MainKtor_client_coreHttpSendPipeline : MainKtor_utilsPipeline<id, MainKtor_client_coreHttpRequestBuilder *>
- (instancetype)initWithDevelopmentMode:(BOOL)developmentMode __attribute__((swift_name("init(developmentMode:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithPhase:(MainKtor_utilsPipelinePhase *)phase interceptors:(NSArray<id<MainKotlinSuspendFunction2>> *)interceptors __attribute__((swift_name("init(phase:interceptors:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (instancetype)initWithPhases:(MainKotlinArray<MainKtor_utilsPipelinePhase *> *)phases __attribute__((swift_name("init(phases:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) MainKtor_client_coreHttpSendPipelinePhases *companion __attribute__((swift_name("companion")));
@property (readonly) BOOL developmentMode __attribute__((swift_name("developmentMode")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreCompositeEncoder")))
@protocol MainKotlinx_serialization_coreCompositeEncoder
@required
- (void)encodeBooleanElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(BOOL)value __attribute__((swift_name("encodeBooleanElement(descriptor:index:value:)")));
- (void)encodeByteElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int8_t)value __attribute__((swift_name("encodeByteElement(descriptor:index:value:)")));
- (void)encodeCharElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(unichar)value __attribute__((swift_name("encodeCharElement(descriptor:index:value:)")));
- (void)encodeDoubleElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(double)value __attribute__((swift_name("encodeDoubleElement(descriptor:index:value:)")));
- (void)encodeFloatElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(float)value __attribute__((swift_name("encodeFloatElement(descriptor:index:value:)")));
- (id<MainKotlinx_serialization_coreEncoder>)encodeInlineElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("encodeInlineElement(descriptor:index:)")));
- (void)encodeIntElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int32_t)value __attribute__((swift_name("encodeIntElement(descriptor:index:value:)")));
- (void)encodeLongElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int64_t)value __attribute__((swift_name("encodeLongElement(descriptor:index:value:)")));
- (void)encodeNullableSerializableElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<MainKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeNullableSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeSerializableElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index serializer:(id<MainKotlinx_serialization_coreSerializationStrategy>)serializer value:(id _Nullable)value __attribute__((swift_name("encodeSerializableElement(descriptor:index:serializer:value:)")));
- (void)encodeShortElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(int16_t)value __attribute__((swift_name("encodeShortElement(descriptor:index:value:)")));
- (void)encodeStringElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index value:(NSString *)value __attribute__((swift_name("encodeStringElement(descriptor:index:value:)")));
- (void)endStructureDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
- (BOOL)shouldEncodeElementDefaultDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("shouldEncodeElementDefault(descriptor:index:)")));
@property (readonly) MainKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModule")))
@interface MainKotlinx_serialization_coreSerializersModule : MainBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)dumpToCollector:(id<MainKotlinx_serialization_coreSerializersModuleCollector>)collector __attribute__((swift_name("dumpTo(collector:)")));
- (id<MainKotlinx_serialization_coreKSerializer> _Nullable)getContextualKClass:(id<MainKotlinKClass>)kClass typeArgumentsSerializers:(NSArray<id<MainKotlinx_serialization_coreKSerializer>> *)typeArgumentsSerializers __attribute__((swift_name("getContextual(kClass:typeArgumentsSerializers:)")));
- (id<MainKotlinx_serialization_coreSerializationStrategy> _Nullable)getPolymorphicBaseClass:(id<MainKotlinKClass>)baseClass value:(id)value __attribute__((swift_name("getPolymorphic(baseClass:value:)")));
- (id<MainKotlinx_serialization_coreDeserializationStrategy> _Nullable)getPolymorphicBaseClass:(id<MainKotlinKClass>)baseClass serializedClassName:(NSString * _Nullable)serializedClassName __attribute__((swift_name("getPolymorphic(baseClass:serializedClassName:)")));
@end;

__attribute__((swift_name("KotlinAnnotation")))
@protocol MainKotlinAnnotation
@required
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerialKind")))
@interface MainKotlinx_serialization_coreSerialKind : MainBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreCompositeDecoder")))
@protocol MainKotlinx_serialization_coreCompositeDecoder
@required
- (BOOL)decodeBooleanElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeBooleanElement(descriptor:index:)")));
- (int8_t)decodeByteElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeByteElement(descriptor:index:)")));
- (unichar)decodeCharElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeCharElement(descriptor:index:)")));
- (int32_t)decodeCollectionSizeDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeCollectionSize(descriptor:)")));
- (double)decodeDoubleElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeDoubleElement(descriptor:index:)")));
- (int32_t)decodeElementIndexDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("decodeElementIndex(descriptor:)")));
- (float)decodeFloatElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeFloatElement(descriptor:index:)")));
- (id<MainKotlinx_serialization_coreDecoder>)decodeInlineElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeInlineElement(descriptor:index:)")));
- (int32_t)decodeIntElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeIntElement(descriptor:index:)")));
- (int64_t)decodeLongElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeLongElement(descriptor:index:)")));
- (id _Nullable)decodeNullableSerializableElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<MainKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeNullableSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (BOOL)decodeSequentially __attribute__((swift_name("decodeSequentially()")));
- (id _Nullable)decodeSerializableElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index deserializer:(id<MainKotlinx_serialization_coreDeserializationStrategy>)deserializer previousValue:(id _Nullable)previousValue __attribute__((swift_name("decodeSerializableElement(descriptor:index:deserializer:previousValue:)")));
- (int16_t)decodeShortElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeShortElement(descriptor:index:)")));
- (NSString *)decodeStringElementDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor index:(int32_t)index __attribute__((swift_name("decodeStringElement(descriptor:index:)")));
- (void)endStructureDescriptor:(id<MainKotlinx_serialization_coreSerialDescriptor>)descriptor __attribute__((swift_name("endStructure(descriptor:)")));
@property (readonly) MainKotlinx_serialization_coreSerializersModule *serializersModule __attribute__((swift_name("serializersModule")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinNothing")))
@interface MainKotlinNothing : MainBase
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestData")))
@interface MainKtor_client_coreHttpRequestData : MainBase
- (instancetype)initWithUrl:(MainKtor_httpUrl *)url method:(MainKtor_httpHttpMethod *)method headers:(id<MainKtor_httpHeaders>)headers body:(MainKtor_httpOutgoingContent *)body executionContext:(id<MainKotlinx_coroutines_coreJob>)executionContext attributes:(id<MainKtor_utilsAttributes>)attributes __attribute__((swift_name("init(url:method:headers:body:executionContext:attributes:)"))) __attribute__((objc_designated_initializer));
- (id _Nullable)getCapabilityOrNullKey:(id<MainKtor_client_coreHttpClientEngineCapability>)key __attribute__((swift_name("getCapabilityOrNull(key:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<MainKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) MainKtor_httpOutgoingContent *body __attribute__((swift_name("body")));
@property (readonly) id<MainKotlinx_coroutines_coreJob> executionContext __attribute__((swift_name("executionContext")));
@property (readonly) id<MainKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@property (readonly) MainKtor_httpHttpMethod *method __attribute__((swift_name("method")));
@property (readonly) MainKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponseData")))
@interface MainKtor_client_coreHttpResponseData : MainBase
- (instancetype)initWithStatusCode:(MainKtor_httpHttpStatusCode *)statusCode requestTime:(MainKtor_utilsGMTDate *)requestTime headers:(id<MainKtor_httpHeaders>)headers version:(MainKtor_httpHttpProtocolVersion *)version body:(id)body callContext:(id<MainKotlinCoroutineContext>)callContext __attribute__((swift_name("init(statusCode:requestTime:headers:version:body:callContext:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id body __attribute__((swift_name("body")));
@property (readonly) id<MainKotlinCoroutineContext> callContext __attribute__((swift_name("callContext")));
@property (readonly) id<MainKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@property (readonly) MainKtor_utilsGMTDate *requestTime __attribute__((swift_name("requestTime")));
@property (readonly) MainKtor_utilsGMTDate *responseTime __attribute__((swift_name("responseTime")));
@property (readonly) MainKtor_httpHttpStatusCode *statusCode __attribute__((swift_name("statusCode")));
@property (readonly) MainKtor_httpHttpProtocolVersion *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreProxyConfig")))
@interface MainKtor_client_coreProxyConfig : MainBase
- (instancetype)initWithUrl:(MainKtor_httpUrl *)url __attribute__((swift_name("init(url:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) MainKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpClientFeature")))
@protocol MainKtor_client_coreHttpClientFeature
@required
- (void)installFeature:(id)feature scope:(MainKtor_client_coreHttpClient *)scope __attribute__((swift_name("install(feature:scope:)")));
- (id)prepareBlock:(void (^)(id))block __attribute__((swift_name("prepare(block:)")));
@property (readonly) MainKtor_utilsAttributeKey<id> *key __attribute__((swift_name("key")));
@end;

__attribute__((swift_name("Ktor_utilsStringValuesBuilder")))
@interface MainKtor_utilsStringValuesBuilder : MainBase
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer));
- (void)appendName:(NSString *)name value:(NSString *)value __attribute__((swift_name("append(name:value:)")));
- (void)appendAllStringValues:(id<MainKtor_utilsStringValues>)stringValues __attribute__((swift_name("appendAll(stringValues:)")));
- (void)appendAllName:(NSString *)name values:(id)values __attribute__((swift_name("appendAll(name:values:)")));
- (void)appendMissingStringValues:(id<MainKtor_utilsStringValues>)stringValues __attribute__((swift_name("appendMissing(stringValues:)")));
- (void)appendMissingName:(NSString *)name values:(id)values __attribute__((swift_name("appendMissing(name:values:)")));
- (id<MainKtor_utilsStringValues>)build __attribute__((swift_name("build()")));
- (void)clear __attribute__((swift_name("clear()")));
- (BOOL)containsName:(NSString *)name __attribute__((swift_name("contains(name:)")));
- (BOOL)containsName:(NSString *)name value:(NSString *)value __attribute__((swift_name("contains(name:value:)")));
- (NSSet<id<MainKotlinMapEntry>> *)entries __attribute__((swift_name("entries()")));
- (NSString * _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSArray<NSString *> * _Nullable)getAllName:(NSString *)name __attribute__((swift_name("getAll(name:)")));
- (BOOL)isEmpty __attribute__((swift_name("isEmpty()")));
- (NSSet<NSString *> *)names __attribute__((swift_name("names()")));
- (void)removeName:(NSString *)name __attribute__((swift_name("remove(name:)")));
- (BOOL)removeName:(NSString *)name value:(NSString *)value __attribute__((swift_name("remove(name:value:)")));
- (void)removeKeysWithNoEntries __attribute__((swift_name("removeKeysWithNoEntries()")));
- (void)setName:(NSString *)name value:(NSString *)value __attribute__((swift_name("set(name:value:)")));
- (void)validateNameName:(NSString *)name __attribute__((swift_name("validateName(name:)")));
- (void)validateValueValue:(NSString *)value __attribute__((swift_name("validateValue(value:)")));
@property BOOL built __attribute__((swift_name("built")));
@property (readonly) BOOL caseInsensitiveName __attribute__((swift_name("caseInsensitiveName")));
@property (readonly) MainMutableDictionary<NSString *, NSMutableArray<NSString *> *> *values __attribute__((swift_name("values")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeadersBuilder")))
@interface MainKtor_httpHeadersBuilder : MainKtor_utilsStringValuesBuilder
- (instancetype)initWithSize:(int32_t)size __attribute__((swift_name("init(size:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (id<MainKtor_httpHeaders>)build __attribute__((swift_name("build()")));
- (void)validateNameName:(NSString *)name __attribute__((swift_name("validateName(name:)")));
- (void)validateValueValue:(NSString *)value __attribute__((swift_name("validateValue(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestBuilder.Companion")))
@interface MainKtor_client_coreHttpRequestBuilderCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_client_coreHttpRequestBuilderCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLBuilder")))
@interface MainKtor_httpURLBuilder : MainBase
- (instancetype)initWithProtocol:(MainKtor_httpURLProtocol *)protocol host:(NSString *)host port:(int32_t)port user:(NSString * _Nullable)user password:(NSString * _Nullable)password encodedPath:(NSString *)encodedPath parameters:(MainKtor_httpParametersBuilder *)parameters fragment:(NSString *)fragment trailingQuery:(BOOL)trailingQuery __attribute__((swift_name("init(protocol:host:port:user:password:encodedPath:parameters:fragment:trailingQuery:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainKtor_httpURLBuilderCompanion *companion __attribute__((swift_name("companion")));
- (MainKtor_httpUrl *)build __attribute__((swift_name("build()")));
- (NSString *)buildString __attribute__((swift_name("buildString()")));
- (MainKtor_httpURLBuilder *)pathComponents:(MainKotlinArray<NSString *> *)components __attribute__((swift_name("path(components:)")));
- (MainKtor_httpURLBuilder *)pathComponents_:(NSArray<NSString *> *)components __attribute__((swift_name("path(components_:)")));
@property NSString *encodedPath __attribute__((swift_name("encodedPath")));
@property NSString *fragment __attribute__((swift_name("fragment")));
@property NSString *host __attribute__((swift_name("host")));
@property (readonly) MainKtor_httpParametersBuilder *parameters __attribute__((swift_name("parameters")));
@property NSString * _Nullable password __attribute__((swift_name("password")));
@property int32_t port __attribute__((swift_name("port")));
@property MainKtor_httpURLProtocol *protocol __attribute__((swift_name("protocol")));
@property BOOL trailingQuery __attribute__((swift_name("trailingQuery")));
@property NSString * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreJob")))
@protocol MainKotlinx_coroutines_coreJob <MainKotlinCoroutineContextElement>
@required
- (id<MainKotlinx_coroutines_coreChildHandle>)attachChildChild:(id<MainKotlinx_coroutines_coreChildJob>)child __attribute__((swift_name("attachChild(child:)")));
- (void)cancelCause:(MainKotlinCancellationException * _Nullable)cause __attribute__((swift_name("cancel(cause:)")));
- (MainKotlinCancellationException *)getCancellationException __attribute__((swift_name("getCancellationException()")));
- (id<MainKotlinx_coroutines_coreDisposableHandle>)invokeOnCompletionOnCancelling:(BOOL)onCancelling invokeImmediately:(BOOL)invokeImmediately handler:(void (^)(MainKotlinThrowable * _Nullable))handler __attribute__((swift_name("invokeOnCompletion(onCancelling:invokeImmediately:handler:)")));
- (id<MainKotlinx_coroutines_coreDisposableHandle>)invokeOnCompletionHandler:(void (^)(MainKotlinThrowable * _Nullable))handler __attribute__((swift_name("invokeOnCompletion(handler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)joinWithCompletionHandler:(void (^)(MainKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("join(completionHandler:)")));
- (id<MainKotlinx_coroutines_coreJob>)plusOther_:(id<MainKotlinx_coroutines_coreJob>)other __attribute__((swift_name("plus(other_:)"))) __attribute__((unavailable("Operator '+' on two Job objects is meaningless. Job is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The job to the right of `+` just replaces the job the left of `+`.")));
- (BOOL)start __attribute__((swift_name("start_()")));
@property (readonly) id<MainKotlinSequence> children __attribute__((swift_name("children")));
@property (readonly) BOOL isActive __attribute__((swift_name("isActive")));
@property (readonly) BOOL isCancelled __attribute__((swift_name("isCancelled")));
@property (readonly) BOOL isCompleted __attribute__((swift_name("isCompleted")));
@property (readonly) id<MainKotlinx_coroutines_coreSelectClause0> onJoin __attribute__((swift_name("onJoin")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpMethod")))
@interface MainKtor_httpHttpMethod : MainBase
- (instancetype)initWithValue:(NSString *)value __attribute__((swift_name("init(value:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainKtor_httpHttpMethodCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (MainKtor_httpHttpMethod *)doCopyValue:(NSString *)value __attribute__((swift_name("doCopy(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpClientCall.Companion")))
@interface MainKtor_client_coreHttpClientCallCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_client_coreHttpClientCallCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) MainKtor_utilsAttributeKey<id> *CustomResponse __attribute__((swift_name("CustomResponse"))) __attribute__((deprecated("This is going to be removed. Please file a ticket with clarification why and what for do you need it.")));
@end;

__attribute__((swift_name("Ktor_ioByteReadChannel")))
@protocol MainKtor_ioByteReadChannel
@required
- (BOOL)cancelCause_:(MainKotlinThrowable * _Nullable)cause __attribute__((swift_name("cancel(cause_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)discardMax:(int64_t)max completionHandler:(void (^)(MainLong * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("discard(max:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)peekToDestination:(MainKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max completionHandler:(void (^)(MainLong * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(MainKtor_ioIoBuffer *)dst completionHandler:(void (^)(MainInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(MainKotlinByteArray *)dst offset:(int32_t)offset length:(int32_t)length completionHandler:(void (^)(MainInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:offset:length:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(void *)dst offset:(int32_t)offset length:(int32_t)length completionHandler_:(void (^)(MainInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:offset:length:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readAvailableDst:(void *)dst offset:(int64_t)offset length:(int64_t)length completionHandler__:(void (^)(MainInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readAvailable(dst:offset:length:completionHandler__:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readBooleanWithCompletionHandler:(void (^)(MainBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readBoolean(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readByteWithCompletionHandler:(void (^)(MainByte * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readByte(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readDoubleWithCompletionHandler:(void (^)(MainDouble * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readDouble(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFloatWithCompletionHandler:(void (^)(MainFloat * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFloat(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(MainKtor_ioIoBuffer *)dst n:(int32_t)n completionHandler:(void (^)(MainKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:n:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(MainKotlinByteArray *)dst offset:(int32_t)offset length:(int32_t)length completionHandler:(void (^)(MainKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:offset:length:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(void *)dst offset:(int32_t)offset length:(int32_t)length completionHandler_:(void (^)(MainKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:offset:length:completionHandler_:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readFullyDst:(void *)dst offset:(int64_t)offset length:(int64_t)length completionHandler__:(void (^)(MainKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readFully(dst:offset:length:completionHandler__:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readIntWithCompletionHandler:(void (^)(MainInt * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readInt(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readLongWithCompletionHandler:(void (^)(MainLong * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readLong(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readPacketSize:(int32_t)size headerSizeHint:(int32_t)headerSizeHint completionHandler:(void (^)(MainKtor_ioByteReadPacket * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readPacket(size:headerSizeHint:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readRemainingLimit:(int64_t)limit headerSizeHint:(int32_t)headerSizeHint completionHandler:(void (^)(MainKtor_ioByteReadPacket * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readRemaining(limit:headerSizeHint:completionHandler:)")));
- (void)readSessionConsumer:(void (^)(id<MainKtor_ioReadSession>))consumer __attribute__((swift_name("readSession(consumer:)"))) __attribute__((deprecated("Use read { } instead.")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readShortWithCompletionHandler:(void (^)(MainShort * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readShort(completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readSuspendableSessionConsumer:(id<MainKotlinSuspendFunction1>)consumer completionHandler:(void (^)(MainKotlinUnit * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readSuspendableSession(consumer:completionHandler:)"))) __attribute__((deprecated("Use read { } instead.")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readUTF8LineLimit:(int32_t)limit completionHandler:(void (^)(NSString * _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("readUTF8Line(limit:completionHandler:)")));

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)readUTF8LineToOut:(id<MainKotlinAppendable>)out limit:(int32_t)limit completionHandler:(void (^)(MainBoolean * _Nullable, NSError * _Nullable))completionHandler __attribute__((swift_name("readUTF8LineTo(out:limit:completionHandler:)")));
@property (readonly) int32_t availableForRead __attribute__((swift_name("availableForRead")));
@property (readonly) MainKotlinThrowable * _Nullable closedCause __attribute__((swift_name("closedCause")));
@property (readonly) BOOL isClosedForRead __attribute__((swift_name("isClosedForRead")));
@property (readonly) BOOL isClosedForWrite __attribute__((swift_name("isClosedForWrite")));
@property MainKtor_ioByteOrder *readByteOrder __attribute__((swift_name("readByteOrder"))) __attribute__((unavailable("Setting byte order is no longer supported. Read/write in big endian and use reverseByteOrder() extensions.")));
@property (readonly) int64_t totalBytesRead __attribute__((swift_name("totalBytesRead")));
@end;

__attribute__((swift_name("Ktor_utilsTypeInfo")))
@protocol MainKtor_utilsTypeInfo
@required
@property (readonly) id<MainKotlinKType> _Nullable kotlinType __attribute__((swift_name("kotlinType")));
@property (readonly) id<MainKotlinKType> reifiedType __attribute__((swift_name("reifiedType")));
@property (readonly) id<MainKotlinKClass> type __attribute__((swift_name("type")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreTypeInfo")))
@interface MainKtor_client_coreTypeInfo : MainBase <MainKtor_utilsTypeInfo>
- (instancetype)initWithType:(id<MainKotlinKClass>)type reifiedType:(id<MainKotlinKType>)reifiedType kotlinType:(id<MainKotlinKType> _Nullable)kotlinType __attribute__((swift_name("init(type:reifiedType:kotlinType:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("This was moved to another package.")));
- (id<MainKotlinKClass>)component1 __attribute__((swift_name("component1()")));
- (id<MainKotlinKType>)component2 __attribute__((swift_name("component2()")));
- (id<MainKotlinKType> _Nullable)component3 __attribute__((swift_name("component3()")));
- (MainKtor_client_coreTypeInfo *)doCopyType:(id<MainKotlinKClass>)type reifiedType:(id<MainKotlinKType>)reifiedType kotlinType:(id<MainKotlinKType> _Nullable)kotlinType __attribute__((swift_name("doCopy(type:reifiedType:kotlinType:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<MainKotlinKType> _Nullable kotlinType __attribute__((swift_name("kotlinType")));
@property (readonly) id<MainKotlinKType> reifiedType __attribute__((swift_name("reifiedType")));
@property (readonly) id<MainKotlinKClass> type __attribute__((swift_name("type")));
@end;

__attribute__((swift_name("Ktor_httpHttpMessage")))
@protocol MainKtor_httpHttpMessage
@required
@property (readonly) id<MainKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpRequest")))
@protocol MainKtor_client_coreHttpRequest <MainKtor_httpHttpMessage, MainKotlinx_coroutines_coreCoroutineScope>
@required
@property (readonly) id<MainKtor_utilsAttributes> attributes __attribute__((swift_name("attributes")));
@property (readonly) MainKtor_client_coreHttpClientCall *call __attribute__((swift_name("call")));
@property (readonly) MainKtor_httpOutgoingContent *content __attribute__((swift_name("content")));
@property (readonly) MainKtor_httpHttpMethod *method __attribute__((swift_name("method")));
@property (readonly) MainKtor_httpUrl *url __attribute__((swift_name("url")));
@end;

__attribute__((swift_name("Ktor_client_coreHttpResponse")))
@interface MainKtor_client_coreHttpResponse : MainBase <MainKtor_httpHttpMessage, MainKotlinx_coroutines_coreCoroutineScope>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) MainKtor_client_coreHttpClientCall *call __attribute__((swift_name("call")));
@property (readonly) id<MainKtor_ioByteReadChannel> content __attribute__((swift_name("content")));
@property (readonly) MainKtor_utilsGMTDate *requestTime __attribute__((swift_name("requestTime")));
@property (readonly) MainKtor_utilsGMTDate *responseTime __attribute__((swift_name("responseTime")));
@property (readonly) MainKtor_httpHttpStatusCode *status __attribute__((swift_name("status")));
@property (readonly) MainKtor_httpHttpProtocolVersion *version __attribute__((swift_name("version")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsAttributeKey")))
@interface MainKtor_utilsAttributeKey<T> : MainBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsPipelinePhase")))
@interface MainKtor_utilsPipelinePhase : MainBase
- (instancetype)initWithName:(NSString *)name __attribute__((swift_name("init(name:)"))) __attribute__((objc_designated_initializer));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("KotlinFunction")))
@protocol MainKotlinFunction
@required
@end;

__attribute__((swift_name("KotlinSuspendFunction2")))
@protocol MainKotlinSuspendFunction2 <MainKotlinFunction>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeP1:(id _Nullable)p1 p2:(id _Nullable)p2 completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(p1:p2:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpReceivePipeline.Phases")))
@interface MainKtor_client_coreHttpReceivePipelinePhases : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_client_coreHttpReceivePipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) MainKtor_utilsPipelinePhase *After __attribute__((swift_name("After")));
@property (readonly) MainKtor_utilsPipelinePhase *Before __attribute__((swift_name("Before")));
@property (readonly) MainKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpRequestPipeline.Phases")))
@interface MainKtor_client_coreHttpRequestPipelinePhases : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_client_coreHttpRequestPipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) MainKtor_utilsPipelinePhase *Before __attribute__((swift_name("Before")));
@property (readonly) MainKtor_utilsPipelinePhase *Render __attribute__((swift_name("Render")));
@property (readonly) MainKtor_utilsPipelinePhase *Send __attribute__((swift_name("Send")));
@property (readonly) MainKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@property (readonly) MainKtor_utilsPipelinePhase *Transform __attribute__((swift_name("Transform")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponsePipeline.Phases")))
@interface MainKtor_client_coreHttpResponsePipelinePhases : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_client_coreHttpResponsePipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) MainKtor_utilsPipelinePhase *After __attribute__((swift_name("After")));
@property (readonly) MainKtor_utilsPipelinePhase *Parse __attribute__((swift_name("Parse")));
@property (readonly) MainKtor_utilsPipelinePhase *Receive __attribute__((swift_name("Receive")));
@property (readonly) MainKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@property (readonly) MainKtor_utilsPipelinePhase *Transform __attribute__((swift_name("Transform")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpResponseContainer")))
@interface MainKtor_client_coreHttpResponseContainer : MainBase
- (instancetype)initWithExpectedType:(id<MainKtor_utilsTypeInfo>)expectedType response:(id)response __attribute__((swift_name("init(expectedType:response:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithExpectedType:(MainKtor_client_coreTypeInfo *)expectedType response_:(id)response __attribute__((swift_name("init(expectedType:response_:)"))) __attribute__((objc_designated_initializer));
- (MainKtor_client_coreTypeInfo *)component1 __attribute__((swift_name("component1()")));
- (id)component2 __attribute__((swift_name("component2()")));
- (MainKtor_client_coreHttpResponseContainer *)doCopyExpectedType:(MainKtor_client_coreTypeInfo *)expectedType response:(id)response __attribute__((swift_name("doCopy(expectedType:response:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) MainKtor_client_coreTypeInfo *expectedType __attribute__((swift_name("expectedType")));
@property (readonly) id response __attribute__((swift_name("response")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_client_coreHttpSendPipeline.Phases")))
@interface MainKtor_client_coreHttpSendPipelinePhases : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)phases __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_client_coreHttpSendPipelinePhases *shared __attribute__((swift_name("shared")));
@property (readonly) MainKtor_utilsPipelinePhase *Before __attribute__((swift_name("Before")));
@property (readonly) MainKtor_utilsPipelinePhase *Engine __attribute__((swift_name("Engine")));
@property (readonly) MainKtor_utilsPipelinePhase *Monitoring __attribute__((swift_name("Monitoring")));
@property (readonly) MainKtor_utilsPipelinePhase *Receive __attribute__((swift_name("Receive")));
@property (readonly) MainKtor_utilsPipelinePhase *State __attribute__((swift_name("State")));
@end;

__attribute__((swift_name("Kotlinx_serialization_coreSerializersModuleCollector")))
@protocol MainKotlinx_serialization_coreSerializersModuleCollector
@required
- (void)contextualKClass:(id<MainKotlinKClass>)kClass provider:(id<MainKotlinx_serialization_coreKSerializer> (^)(NSArray<id<MainKotlinx_serialization_coreKSerializer>> *))provider __attribute__((swift_name("contextual(kClass:provider:)")));
- (void)contextualKClass:(id<MainKotlinKClass>)kClass serializer:(id<MainKotlinx_serialization_coreKSerializer>)serializer __attribute__((swift_name("contextual(kClass:serializer:)")));
- (void)polymorphicBaseClass:(id<MainKotlinKClass>)baseClass actualClass:(id<MainKotlinKClass>)actualClass actualSerializer:(id<MainKotlinx_serialization_coreKSerializer>)actualSerializer __attribute__((swift_name("polymorphic(baseClass:actualClass:actualSerializer:)")));
- (void)polymorphicDefaultBaseClass:(id<MainKotlinKClass>)baseClass defaultSerializerProvider:(id<MainKotlinx_serialization_coreDeserializationStrategy> _Nullable (^)(NSString * _Nullable))defaultSerializerProvider __attribute__((swift_name("polymorphicDefault(baseClass:defaultSerializerProvider:)")));
@end;

__attribute__((swift_name("KotlinKDeclarationContainer")))
@protocol MainKotlinKDeclarationContainer
@required
@end;

__attribute__((swift_name("KotlinKAnnotatedElement")))
@protocol MainKotlinKAnnotatedElement
@required
@end;

__attribute__((swift_name("KotlinKClassifier")))
@protocol MainKotlinKClassifier
@required
@end;

__attribute__((swift_name("KotlinKClass")))
@protocol MainKotlinKClass <MainKotlinKDeclarationContainer, MainKotlinKAnnotatedElement, MainKotlinKClassifier>
@required
- (BOOL)isInstanceValue:(id _Nullable)value __attribute__((swift_name("isInstance(value:)")));
@property (readonly) NSString * _Nullable qualifiedName __attribute__((swift_name("qualifiedName")));
@property (readonly) NSString * _Nullable simpleName __attribute__((swift_name("simpleName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpUrl")))
@interface MainKtor_httpUrl : MainBase
- (instancetype)initWithProtocol:(MainKtor_httpURLProtocol *)protocol host:(NSString *)host specifiedPort:(int32_t)specifiedPort encodedPath:(NSString *)encodedPath parameters:(id<MainKtor_httpParameters>)parameters fragment:(NSString *)fragment user:(NSString * _Nullable)user password:(NSString * _Nullable)password trailingQuery:(BOOL)trailingQuery __attribute__((swift_name("init(protocol:host:specifiedPort:encodedPath:parameters:fragment:user:password:trailingQuery:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainKtor_httpUrlCompanion *companion __attribute__((swift_name("companion")));
- (MainKtor_httpURLProtocol *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (NSString *)component4 __attribute__((swift_name("component4()")));
- (id<MainKtor_httpParameters>)component5 __attribute__((swift_name("component5()")));
- (NSString *)component6 __attribute__((swift_name("component6()")));
- (NSString * _Nullable)component7 __attribute__((swift_name("component7()")));
- (NSString * _Nullable)component8 __attribute__((swift_name("component8()")));
- (BOOL)component9 __attribute__((swift_name("component9()")));
- (MainKtor_httpUrl *)doCopyProtocol:(MainKtor_httpURLProtocol *)protocol host:(NSString *)host specifiedPort:(int32_t)specifiedPort encodedPath:(NSString *)encodedPath parameters:(id<MainKtor_httpParameters>)parameters fragment:(NSString *)fragment user:(NSString * _Nullable)user password:(NSString * _Nullable)password trailingQuery:(BOOL)trailingQuery __attribute__((swift_name("doCopy(protocol:host:specifiedPort:encodedPath:parameters:fragment:user:password:trailingQuery:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *encodedPath __attribute__((swift_name("encodedPath")));
@property (readonly) NSString *fragment __attribute__((swift_name("fragment")));
@property (readonly) NSString *host __attribute__((swift_name("host")));
@property (readonly) id<MainKtor_httpParameters> parameters __attribute__((swift_name("parameters")));
@property (readonly) NSString * _Nullable password __attribute__((swift_name("password")));
@property (readonly) int32_t port __attribute__((swift_name("port")));
@property (readonly) MainKtor_httpURLProtocol *protocol __attribute__((swift_name("protocol")));
@property (readonly) int32_t specifiedPort __attribute__((swift_name("specifiedPort")));
@property (readonly) BOOL trailingQuery __attribute__((swift_name("trailingQuery")));
@property (readonly) NSString * _Nullable user __attribute__((swift_name("user")));
@end;

__attribute__((swift_name("Ktor_utilsStringValues")))
@protocol MainKtor_utilsStringValues
@required
- (BOOL)containsName:(NSString *)name __attribute__((swift_name("contains(name:)")));
- (BOOL)containsName:(NSString *)name value:(NSString *)value __attribute__((swift_name("contains(name:value:)")));
- (NSSet<id<MainKotlinMapEntry>> *)entries __attribute__((swift_name("entries()")));
- (void)forEachBody:(void (^)(NSString *, NSArray<NSString *> *))body __attribute__((swift_name("forEach(body:)")));
- (NSString * _Nullable)getName:(NSString *)name __attribute__((swift_name("get(name:)")));
- (NSArray<NSString *> * _Nullable)getAllName:(NSString *)name __attribute__((swift_name("getAll(name:)")));
- (BOOL)isEmpty_ __attribute__((swift_name("isEmpty()")));
- (NSSet<NSString *> *)names __attribute__((swift_name("names()")));
@property (readonly) BOOL caseInsensitiveName __attribute__((swift_name("caseInsensitiveName")));
@end;

__attribute__((swift_name("Ktor_httpHeaders")))
@protocol MainKtor_httpHeaders <MainKtor_utilsStringValues>
@required
@end;

__attribute__((swift_name("Ktor_httpOutgoingContent")))
@interface MainKtor_httpOutgoingContent : MainBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id _Nullable)getPropertyKey:(MainKtor_utilsAttributeKey<id> *)key __attribute__((swift_name("getProperty(key:)")));
- (void)setPropertyKey:(MainKtor_utilsAttributeKey<id> *)key value:(id _Nullable)value __attribute__((swift_name("setProperty(key:value:)")));
@property (readonly) MainLong * _Nullable contentLength __attribute__((swift_name("contentLength")));
@property (readonly) MainKtor_httpContentType * _Nullable contentType __attribute__((swift_name("contentType")));
@property (readonly) id<MainKtor_httpHeaders> headers __attribute__((swift_name("headers")));
@property (readonly) MainKtor_httpHttpStatusCode * _Nullable status __attribute__((swift_name("status")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpStatusCode")))
@interface MainKtor_httpHttpStatusCode : MainBase
- (instancetype)initWithValue:(int32_t)value description:(NSString *)description __attribute__((swift_name("init(value:description:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainKtor_httpHttpStatusCodeCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (MainKtor_httpHttpStatusCode *)doCopyValue:(int32_t)value description:(NSString *)description __attribute__((swift_name("doCopy(value:description:)")));
- (MainKtor_httpHttpStatusCode *)descriptionValue:(NSString *)value __attribute__((swift_name("description(value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *description_ __attribute__((swift_name("description_")));
@property (readonly) int32_t value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsGMTDate")))
@interface MainKtor_utilsGMTDate : MainBase <MainKotlinComparable>
@property (class, readonly, getter=companion) MainKtor_utilsGMTDateCompanion *companion __attribute__((swift_name("companion")));
- (int32_t)compareToOther:(MainKtor_utilsGMTDate *)other __attribute__((swift_name("compareTo(other:)")));
- (int32_t)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (MainKtor_utilsWeekDay *)component4 __attribute__((swift_name("component4()")));
- (int32_t)component5 __attribute__((swift_name("component5()")));
- (int32_t)component6 __attribute__((swift_name("component6()")));
- (MainKtor_utilsMonth *)component7 __attribute__((swift_name("component7()")));
- (int32_t)component8 __attribute__((swift_name("component8()")));
- (int64_t)component9 __attribute__((swift_name("component9()")));
- (MainKtor_utilsGMTDate *)doCopySeconds:(int32_t)seconds minutes:(int32_t)minutes hours:(int32_t)hours dayOfWeek:(MainKtor_utilsWeekDay *)dayOfWeek dayOfMonth:(int32_t)dayOfMonth dayOfYear:(int32_t)dayOfYear month:(MainKtor_utilsMonth *)month year:(int32_t)year timestamp:(int64_t)timestamp __attribute__((swift_name("doCopy(seconds:minutes:hours:dayOfWeek:dayOfMonth:dayOfYear:month:year:timestamp:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t dayOfMonth __attribute__((swift_name("dayOfMonth")));
@property (readonly) MainKtor_utilsWeekDay *dayOfWeek __attribute__((swift_name("dayOfWeek")));
@property (readonly) int32_t dayOfYear __attribute__((swift_name("dayOfYear")));
@property (readonly) int32_t hours __attribute__((swift_name("hours")));
@property (readonly) int32_t minutes __attribute__((swift_name("minutes")));
@property (readonly) MainKtor_utilsMonth *month __attribute__((swift_name("month")));
@property (readonly) int32_t seconds __attribute__((swift_name("seconds")));
@property (readonly) int64_t timestamp __attribute__((swift_name("timestamp")));
@property (readonly) int32_t year __attribute__((swift_name("year")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpProtocolVersion")))
@interface MainKtor_httpHttpProtocolVersion : MainBase
- (instancetype)initWithName:(NSString *)name major:(int32_t)major minor:(int32_t)minor __attribute__((swift_name("init(name:major:minor:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainKtor_httpHttpProtocolVersionCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (int32_t)component3 __attribute__((swift_name("component3()")));
- (MainKtor_httpHttpProtocolVersion *)doCopyName:(NSString *)name major:(int32_t)major minor:(int32_t)minor __attribute__((swift_name("doCopy(name:major:minor:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t major __attribute__((swift_name("major")));
@property (readonly) int32_t minor __attribute__((swift_name("minor")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((swift_name("KotlinMapEntry")))
@protocol MainKotlinMapEntry
@required
@property (readonly) id _Nullable key __attribute__((swift_name("key")));
@property (readonly) id _Nullable value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLProtocol")))
@interface MainKtor_httpURLProtocol : MainBase
- (instancetype)initWithName:(NSString *)name defaultPort:(int32_t)defaultPort __attribute__((swift_name("init(name:defaultPort:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainKtor_httpURLProtocolCompanion *companion __attribute__((swift_name("companion")));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (int32_t)component2 __attribute__((swift_name("component2()")));
- (MainKtor_httpURLProtocol *)doCopyName:(NSString *)name defaultPort:(int32_t)defaultPort __attribute__((swift_name("doCopy(name:defaultPort:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) int32_t defaultPort __attribute__((swift_name("defaultPort")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpParametersBuilder")))
@interface MainKtor_httpParametersBuilder : MainKtor_utilsStringValuesBuilder
- (instancetype)initWithSize:(int32_t)size urlEncodingOption:(MainKtor_httpUrlEncodingOption *)urlEncodingOption __attribute__((swift_name("init(size:urlEncodingOption:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithCaseInsensitiveName:(BOOL)caseInsensitiveName size:(int32_t)size __attribute__((swift_name("init(caseInsensitiveName:size:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
- (id<MainKtor_httpParameters>)build __attribute__((swift_name("build()")));
@property MainKtor_httpUrlEncodingOption *urlEncodingOption __attribute__((swift_name("urlEncodingOption")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLBuilder.Companion")))
@interface MainKtor_httpURLBuilderCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_httpURLBuilderCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreDisposableHandle")))
@protocol MainKotlinx_coroutines_coreDisposableHandle
@required
- (void)dispose __attribute__((swift_name("dispose()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreChildHandle")))
@protocol MainKotlinx_coroutines_coreChildHandle <MainKotlinx_coroutines_coreDisposableHandle>
@required
- (BOOL)childCancelledCause:(MainKotlinThrowable *)cause __attribute__((swift_name("childCancelled(cause:)")));
@property (readonly) id<MainKotlinx_coroutines_coreJob> _Nullable parent __attribute__((swift_name("parent")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreChildJob")))
@protocol MainKotlinx_coroutines_coreChildJob <MainKotlinx_coroutines_coreJob>
@required
- (void)parentCancelledParentJob:(id<MainKotlinx_coroutines_coreParentJob>)parentJob __attribute__((swift_name("parentCancelled(parentJob:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinUnit")))
@interface MainKotlinUnit : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)unit __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKotlinUnit *shared __attribute__((swift_name("shared")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("KotlinSequence")))
@protocol MainKotlinSequence
@required
- (id<MainKotlinIterator>)iterator __attribute__((swift_name("iterator()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreSelectClause0")))
@protocol MainKotlinx_coroutines_coreSelectClause0
@required
- (void)registerSelectClause0Select:(id<MainKotlinx_coroutines_coreSelectInstance>)select block:(id<MainKotlinSuspendFunction0>)block __attribute__((swift_name("registerSelectClause0(select:block:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpMethod.Companion")))
@interface MainKtor_httpHttpMethodCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_httpHttpMethodCompanion *shared __attribute__((swift_name("shared")));
- (MainKtor_httpHttpMethod *)parseMethod:(NSString *)method __attribute__((swift_name("parse(method:)")));
@property (readonly) NSArray<MainKtor_httpHttpMethod *> *DefaultMethods __attribute__((swift_name("DefaultMethods")));
@property (readonly) MainKtor_httpHttpMethod *Delete __attribute__((swift_name("Delete")));
@property (readonly) MainKtor_httpHttpMethod *Get __attribute__((swift_name("Get")));
@property (readonly) MainKtor_httpHttpMethod *Head __attribute__((swift_name("Head")));
@property (readonly) MainKtor_httpHttpMethod *Options __attribute__((swift_name("Options")));
@property (readonly) MainKtor_httpHttpMethod *Patch __attribute__((swift_name("Patch")));
@property (readonly) MainKtor_httpHttpMethod *Post __attribute__((swift_name("Post")));
@property (readonly) MainKtor_httpHttpMethod *Put __attribute__((swift_name("Put")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioMemory")))
@interface MainKtor_ioMemory : MainBase
- (instancetype)initWithPointer:(void *)pointer size:(int64_t)size __attribute__((swift_name("init(pointer:size:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainKtor_ioMemoryCompanion *companion __attribute__((swift_name("companion")));
- (void)doCopyToDestination:(MainKtor_ioMemory *)destination offset:(int32_t)offset length:(int32_t)length destinationOffset:(int32_t)destinationOffset __attribute__((swift_name("doCopyTo(destination:offset:length:destinationOffset:)")));
- (void)doCopyToDestination:(MainKtor_ioMemory *)destination offset:(int64_t)offset length:(int64_t)length destinationOffset_:(int64_t)destinationOffset __attribute__((swift_name("doCopyTo(destination:offset:length:destinationOffset_:)")));
- (int8_t)loadAtIndex:(int32_t)index __attribute__((swift_name("loadAt(index:)")));
- (int8_t)loadAtIndex_:(int64_t)index __attribute__((swift_name("loadAt(index_:)")));
- (MainKtor_ioMemory *)sliceOffset:(int32_t)offset length:(int32_t)length __attribute__((swift_name("slice(offset:length:)")));
- (MainKtor_ioMemory *)sliceOffset:(int64_t)offset length_:(int64_t)length __attribute__((swift_name("slice(offset:length_:)")));
- (void)storeAtIndex:(int32_t)index value:(int8_t)value __attribute__((swift_name("storeAt(index:value:)")));
- (void)storeAtIndex:(int64_t)index value_:(int8_t)value __attribute__((swift_name("storeAt(index:value_:)")));
@property (readonly) void *pointer __attribute__((swift_name("pointer")));
@property (readonly) int64_t size __attribute__((swift_name("size")));
@property (readonly) int32_t size32 __attribute__((swift_name("size32")));
@end;

__attribute__((swift_name("Ktor_ioBuffer")))
@interface MainKtor_ioBuffer : MainBase
- (instancetype)initWithMemory:(MainKtor_ioMemory *)memory __attribute__((swift_name("init(memory:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainKtor_ioBufferCompanion *companion __attribute__((swift_name("companion")));
- (void)commitWrittenCount:(int32_t)count __attribute__((swift_name("commitWritten(count:)")));
- (int32_t)discardCount:(int32_t)count __attribute__((swift_name("discard(count:)"))) __attribute__((unavailable("Use discardExact instead.")));
- (int64_t)discardCount_:(int64_t)count __attribute__((swift_name("discard(count_:)"))) __attribute__((unavailable("Use discardExact instead.")));
- (void)discardExactCount:(int32_t)count __attribute__((swift_name("discardExact(count:)")));
- (MainKtor_ioBuffer *)duplicate __attribute__((swift_name("duplicate()")));
- (void)duplicateToCopy:(MainKtor_ioBuffer *)copy __attribute__((swift_name("duplicateTo(copy:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (void)reserveEndGapEndGap:(int32_t)endGap __attribute__((swift_name("reserveEndGap(endGap:)")));
- (void)reserveStartGapStartGap:(int32_t)startGap __attribute__((swift_name("reserveStartGap(startGap:)")));
- (void)reset __attribute__((swift_name("reset()")));
- (void)resetForRead __attribute__((swift_name("resetForRead()")));
- (void)resetForWrite __attribute__((swift_name("resetForWrite()")));
- (void)resetForWriteLimit:(int32_t)limit __attribute__((swift_name("resetForWrite(limit:)")));
- (void)rewindCount:(int32_t)count __attribute__((swift_name("rewind(count:)")));
- (NSString *)description __attribute__((swift_name("description()")));
- (int32_t)tryPeekByte __attribute__((swift_name("tryPeekByte()")));
- (int32_t)tryReadByte __attribute__((swift_name("tryReadByte()")));
- (void)writeByteValue:(int8_t)value __attribute__((swift_name("writeByte(value:)")));
@property id _Nullable attachment __attribute__((swift_name("attachment"))) __attribute__((deprecated("Will be removed. Inherit Buffer and add required fields instead.")));
@property (readonly) int32_t capacity __attribute__((swift_name("capacity")));
@property (readonly) int32_t endGap __attribute__((swift_name("endGap")));
@property (readonly) int32_t limit __attribute__((swift_name("limit")));
@property (readonly) MainKtor_ioMemory *memory __attribute__((swift_name("memory")));
@property (readonly) int32_t readPosition __attribute__((swift_name("readPosition")));
@property (readonly) int32_t readRemaining __attribute__((swift_name("readRemaining")));
@property (readonly) int32_t startGap __attribute__((swift_name("startGap")));
@property (readonly) int32_t writePosition __attribute__((swift_name("writePosition")));
@property (readonly) int32_t writeRemaining __attribute__((swift_name("writeRemaining")));
@end;

__attribute__((swift_name("Ktor_ioChunkBuffer")))
@interface MainKtor_ioChunkBuffer : MainKtor_ioBuffer
- (instancetype)initWithMemory:(MainKtor_ioMemory *)memory origin:(MainKtor_ioChunkBuffer * _Nullable)origin parentPool:(id<MainKtor_ioObjectPool> _Nullable)parentPool __attribute__((swift_name("init(memory:origin:parentPool:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithMemory:(MainKtor_ioMemory *)memory __attribute__((swift_name("init(memory:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) MainKtor_ioChunkBufferCompanion *companion __attribute__((swift_name("companion")));
- (MainKtor_ioChunkBuffer * _Nullable)cleanNext __attribute__((swift_name("cleanNext()")));
- (MainKtor_ioChunkBuffer *)duplicate __attribute__((swift_name("duplicate()")));
- (void)releasePool:(id<MainKtor_ioObjectPool>)pool __attribute__((swift_name("release(pool:)")));
- (void)reset __attribute__((swift_name("reset()")));
@property (getter=next_) MainKtor_ioChunkBuffer * _Nullable next __attribute__((swift_name("next")));
@property (readonly) MainKtor_ioChunkBuffer * _Nullable origin __attribute__((swift_name("origin")));
@property (readonly) int32_t referenceCount __attribute__((swift_name("referenceCount")));
@end;

__attribute__((swift_name("Ktor_ioInput")))
@protocol MainKtor_ioInput <MainKtor_ioCloseable>
@required
- (int64_t)discardN:(int64_t)n __attribute__((swift_name("discard(n:)")));
- (int64_t)peekToDestination:(MainKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (int32_t)tryPeek __attribute__((swift_name("tryPeek()")));
@property MainKtor_ioByteOrder *byteOrder __attribute__((swift_name("byteOrder"))) __attribute__((unavailable("Not supported anymore. All operations are big endian by default. Use readXXXLittleEndian or readXXX then X.reverseByteOrder() instead.")));
@property (readonly) BOOL endOfInput __attribute__((swift_name("endOfInput")));
@end;

__attribute__((swift_name("KotlinAppendable")))
@protocol MainKotlinAppendable
@required
- (id<MainKotlinAppendable>)appendValue:(unichar)value __attribute__((swift_name("append(value:)")));
- (id<MainKotlinAppendable>)appendValue_:(id _Nullable)value __attribute__((swift_name("append(value_:)")));
- (id<MainKotlinAppendable>)appendValue:(id _Nullable)value startIndex:(int32_t)startIndex endIndex:(int32_t)endIndex __attribute__((swift_name("append(value:startIndex:endIndex:)")));
@end;

__attribute__((swift_name("Ktor_ioOutput")))
@protocol MainKtor_ioOutput <MainKotlinAppendable, MainKtor_ioCloseable>
@required
- (id<MainKotlinAppendable>)appendCsq:(MainKotlinCharArray *)csq start:(int32_t)start end:(int32_t)end __attribute__((swift_name("append(csq:start:end:)")));
- (void)flush __attribute__((swift_name("flush()")));
- (void)writeByteV:(int8_t)v __attribute__((swift_name("writeByte(v:)")));
@property MainKtor_ioByteOrder *byteOrder __attribute__((swift_name("byteOrder"))) __attribute__((deprecated("Write with writeXXXLittleEndian or do X.reverseByteOrder() and then writeXXX instead.")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioIoBuffer")))
@interface MainKtor_ioIoBuffer : MainKtor_ioChunkBuffer <MainKtor_ioInput, MainKtor_ioOutput>
- (instancetype)initWithMemory:(MainKtor_ioMemory *)memory origin:(MainKtor_ioChunkBuffer * _Nullable)origin __attribute__((swift_name("init(memory:origin:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Use Buffer instead.")));
- (instancetype)initWithContent:(void *)content contentCapacity:(int32_t)contentCapacity __attribute__((swift_name("init(content:contentCapacity:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Use Buffer instead.")));
- (instancetype)initWithMemory:(MainKtor_ioMemory *)memory origin:(MainKtor_ioChunkBuffer * _Nullable)origin parentPool:(id<MainKtor_ioObjectPool> _Nullable)parentPool __attribute__((swift_name("init(memory:origin:parentPool:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) MainKtor_ioIoBufferCompanion *companion __attribute__((swift_name("companion")));
- (id<MainKotlinAppendable>)appendValue:(unichar)c __attribute__((swift_name("append(value:)")));
- (id<MainKotlinAppendable>)appendCsq:(MainKotlinCharArray *)csq start:(int32_t)start end:(int32_t)end __attribute__((swift_name("append(csq:start:end:)")));
- (id<MainKotlinAppendable>)appendValue_:(id _Nullable)csq __attribute__((swift_name("append(value_:)")));
- (id<MainKotlinAppendable>)appendValue:(id _Nullable)csq startIndex:(int32_t)start endIndex:(int32_t)end __attribute__((swift_name("append(value:startIndex:endIndex:)")));
- (int32_t)appendCharsCsq:(MainKotlinCharArray *)csq start:(int32_t)start end:(int32_t)end __attribute__((swift_name("appendChars(csq:start:end:)")));
- (int32_t)appendCharsCsq:(id)csq start:(int32_t)start end_:(int32_t)end __attribute__((swift_name("appendChars(csq:start:end_:)")));
- (void)close __attribute__((swift_name("close()")));
- (MainKtor_ioIoBuffer *)duplicate __attribute__((swift_name("duplicate()")));
- (void)flush __attribute__((swift_name("flush()")));
- (MainKtor_ioIoBuffer *)makeView __attribute__((swift_name("makeView()")));
- (int64_t)peekToDestination:(MainKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:)")));
- (int32_t)readDirectBlock:(MainInt *(^)(id))block __attribute__((swift_name("readDirect(block:)")));
- (void)releasePool_:(id<MainKtor_ioObjectPool>)pool __attribute__((swift_name("release(pool_:)")));
- (NSString *)description __attribute__((swift_name("description()")));
- (int32_t)tryPeek __attribute__((swift_name("tryPeek()")));
- (int32_t)writeDirectBlock:(MainInt *(^)(id))block __attribute__((swift_name("writeDirect(block:)")));
@property MainKtor_ioByteOrder *byteOrder __attribute__((swift_name("byteOrder"))) __attribute__((unavailable("Not supported anymore. All operations are big endian by default.")));
@property (readonly) BOOL endOfInput __attribute__((swift_name("endOfInput")));
@end;

__attribute__((swift_name("Ktor_ioAbstractInput")))
@interface MainKtor_ioAbstractInput : MainBase <MainKtor_ioInput>
- (instancetype)initWithHead:(MainKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<MainKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("AbstractInput is deprecated and will be merged with Input in 2.0.0")));
@property (class, readonly, getter=companion) MainKtor_ioAbstractInputCompanion *companion __attribute__((swift_name("companion")));
- (BOOL)canRead __attribute__((swift_name("canRead()")));
- (void)close __attribute__((swift_name("close()")));
- (void)closeSource __attribute__((swift_name("closeSource()")));
- (int32_t)discardN_:(int32_t)n __attribute__((swift_name("discard(n_:)")));
- (int64_t)discardN:(int64_t)n __attribute__((swift_name("discard(n:)")));
- (void)discardExactN:(int32_t)n __attribute__((swift_name("discardExact(n:)")));
- (MainKtor_ioChunkBuffer * _Nullable)ensureNextHeadCurrent:(MainKtor_ioChunkBuffer *)current __attribute__((swift_name("ensureNextHead(current:)")));
- (MainKtor_ioChunkBuffer * _Nullable)fill __attribute__((swift_name("fill()")));
- (int32_t)fillDestination:(MainKtor_ioMemory *)destination offset:(int32_t)offset length:(int32_t)length __attribute__((swift_name("fill(destination:offset:length:)")));
- (void)fixGapAfterReadCurrent:(MainKtor_ioChunkBuffer *)current __attribute__((swift_name("fixGapAfterRead(current:)")));
- (BOOL)hasBytesN:(int32_t)n __attribute__((swift_name("hasBytes(n:)")));
- (void)markNoMoreChunksAvailable __attribute__((swift_name("markNoMoreChunksAvailable()")));
- (int64_t)peekToDestination:(MainKtor_ioMemory *)destination destinationOffset:(int64_t)destinationOffset offset:(int64_t)offset min:(int64_t)min max:(int64_t)max __attribute__((swift_name("peekTo(destination:destinationOffset:offset:min:max:)")));
- (MainKtor_ioChunkBuffer * _Nullable)prepareReadHeadMinSize:(int32_t)minSize __attribute__((swift_name("prepareReadHead(minSize:)")));
- (int8_t)readByte __attribute__((swift_name("readByte()")));
- (NSString *)readTextMin:(int32_t)min max:(int32_t)max __attribute__((swift_name("readText(min:max:)")));
- (int32_t)readTextOut:(id<MainKotlinAppendable>)out min:(int32_t)min max:(int32_t)max __attribute__((swift_name("readText(out:min:max:)")));
- (NSString *)readTextExactExactCharacters:(int32_t)exactCharacters __attribute__((swift_name("readTextExact(exactCharacters:)")));
- (void)readTextExactOut:(id<MainKotlinAppendable>)out exactCharacters:(int32_t)exactCharacters __attribute__((swift_name("readTextExact(out:exactCharacters:)")));
- (void)release_ __attribute__((swift_name("release()")));
- (int32_t)tryPeek __attribute__((swift_name("tryPeek()")));
- (void)updateHeadRemainingRemaining:(int32_t)remaining __attribute__((swift_name("updateHeadRemaining(remaining:)"))) __attribute__((unavailable("Not supported anymore.")));
@property MainKtor_ioByteOrder *byteOrder __attribute__((swift_name("byteOrder"))) __attribute__((unavailable("Not supported anymore. All operations are big endian by default.")));
@property (readonly) BOOL endOfInput __attribute__((swift_name("endOfInput")));
@property (readonly) id<MainKtor_ioObjectPool> pool __attribute__((swift_name("pool")));
@property (readonly) int64_t remaining __attribute__((swift_name("remaining")));
@end;

__attribute__((swift_name("Ktor_ioByteReadPacketBase")))
@interface MainKtor_ioByteReadPacketBase : MainKtor_ioAbstractInput
- (instancetype)initWithHead:(MainKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<MainKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((deprecated("Will be removed in the future releases. Use Input or AbstractInput instead.")));
@property (class, readonly, getter=companion) MainKtor_ioByteReadPacketBaseCompanion *companion __attribute__((swift_name("companion")));
@end;

__attribute__((swift_name("Ktor_ioByteReadPacketPlatformBase")))
@interface MainKtor_ioByteReadPacketPlatformBase : MainKtor_ioByteReadPacketBase
- (instancetype)initWithHead:(MainKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<MainKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable("Will be removed in future releases.")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteReadPacket")))
@interface MainKtor_ioByteReadPacket : MainKtor_ioByteReadPacketPlatformBase <MainKtor_ioInput>
- (instancetype)initWithHead:(MainKtor_ioChunkBuffer *)head pool:(id<MainKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:pool:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithHead:(MainKtor_ioChunkBuffer *)head remaining:(int64_t)remaining pool:(id<MainKtor_ioObjectPool>)pool __attribute__((swift_name("init(head:remaining:pool:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) MainKtor_ioByteReadPacketCompanion *companion __attribute__((swift_name("companion")));
- (void)closeSource __attribute__((swift_name("closeSource()")));
- (MainKtor_ioByteReadPacket *)doCopy __attribute__((swift_name("doCopy()")));
- (MainKtor_ioChunkBuffer * _Nullable)fill __attribute__((swift_name("fill()")));
- (int32_t)fillDestination:(MainKtor_ioMemory *)destination offset:(int32_t)offset length:(int32_t)length __attribute__((swift_name("fill(destination:offset:length:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@end;

__attribute__((swift_name("Ktor_ioReadSession")))
@protocol MainKtor_ioReadSession
@required
- (int32_t)discardN_:(int32_t)n __attribute__((swift_name("discard(n_:)")));
- (MainKtor_ioIoBuffer * _Nullable)requestAtLeast:(int32_t)atLeast __attribute__((swift_name("request(atLeast:)")));
@property (readonly) int32_t availableForRead __attribute__((swift_name("availableForRead")));
@end;

__attribute__((swift_name("KotlinSuspendFunction1")))
@protocol MainKotlinSuspendFunction1 <MainKotlinFunction>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeP1:(id _Nullable)p1 completionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(p1:completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteOrder")))
@interface MainKtor_ioByteOrder : MainKotlinEnum<MainKtor_ioByteOrder *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) MainKtor_ioByteOrderCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) MainKtor_ioByteOrder *bigEndian __attribute__((swift_name("bigEndian")));
@property (class, readonly) MainKtor_ioByteOrder *littleEndian __attribute__((swift_name("littleEndian")));
+ (MainKotlinArray<MainKtor_ioByteOrder *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((swift_name("KotlinKType")))
@protocol MainKotlinKType
@required
@property (readonly) NSArray<MainKotlinKTypeProjection *> *arguments __attribute__((swift_name("arguments")));
@property (readonly) id<MainKotlinKClassifier> _Nullable classifier __attribute__((swift_name("classifier")));
@property (readonly) BOOL isMarkedNullable __attribute__((swift_name("isMarkedNullable")));
@end;

__attribute__((swift_name("Ktor_httpParameters")))
@protocol MainKtor_httpParameters <MainKtor_utilsStringValues>
@required
@property (readonly) MainKtor_httpUrlEncodingOption *urlEncodingOption __attribute__((swift_name("urlEncodingOption")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpUrl.Companion")))
@interface MainKtor_httpUrlCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_httpUrlCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((swift_name("Ktor_httpHeaderValueWithParameters")))
@interface MainKtor_httpHeaderValueWithParameters : MainBase
- (instancetype)initWithContent:(NSString *)content parameters:(NSArray<MainKtor_httpHeaderValueParam *> *)parameters __attribute__((swift_name("init(content:parameters:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainKtor_httpHeaderValueWithParametersCompanion *companion __attribute__((swift_name("companion")));
- (NSString * _Nullable)parameterName:(NSString *)name __attribute__((swift_name("parameter(name:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *content __attribute__((swift_name("content")));
@property (readonly) NSArray<MainKtor_httpHeaderValueParam *> *parameters __attribute__((swift_name("parameters")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpContentType")))
@interface MainKtor_httpContentType : MainKtor_httpHeaderValueWithParameters
- (instancetype)initWithContentType:(NSString *)contentType contentSubtype:(NSString *)contentSubtype parameters:(NSArray<MainKtor_httpHeaderValueParam *> *)parameters __attribute__((swift_name("init(contentType:contentSubtype:parameters:)"))) __attribute__((objc_designated_initializer));
- (instancetype)initWithContent:(NSString *)content parameters:(NSArray<MainKtor_httpHeaderValueParam *> *)parameters __attribute__((swift_name("init(content:parameters:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) MainKtor_httpContentTypeCompanion *companion __attribute__((swift_name("companion")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (BOOL)matchPattern:(MainKtor_httpContentType *)pattern __attribute__((swift_name("match(pattern:)")));
- (BOOL)matchPattern_:(NSString *)pattern __attribute__((swift_name("match(pattern_:)")));
- (MainKtor_httpContentType *)withParameterName:(NSString *)name value:(NSString *)value __attribute__((swift_name("withParameter(name:value:)")));
- (MainKtor_httpContentType *)withoutParameters __attribute__((swift_name("withoutParameters()")));
@property (readonly) NSString *contentSubtype __attribute__((swift_name("contentSubtype")));
@property (readonly) NSString *contentType __attribute__((swift_name("contentType")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpStatusCode.Companion")))
@interface MainKtor_httpHttpStatusCodeCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_httpHttpStatusCodeCompanion *shared __attribute__((swift_name("shared")));
- (MainKtor_httpHttpStatusCode *)fromValueValue:(int32_t)value __attribute__((swift_name("fromValue(value:)")));
@property (readonly) MainKtor_httpHttpStatusCode *Accepted __attribute__((swift_name("Accepted")));
@property (readonly) MainKtor_httpHttpStatusCode *BadGateway __attribute__((swift_name("BadGateway")));
@property (readonly) MainKtor_httpHttpStatusCode *BadRequest __attribute__((swift_name("BadRequest")));
@property (readonly) MainKtor_httpHttpStatusCode *Conflict __attribute__((swift_name("Conflict")));
@property (readonly) MainKtor_httpHttpStatusCode *Continue __attribute__((swift_name("Continue")));
@property (readonly) MainKtor_httpHttpStatusCode *Created __attribute__((swift_name("Created")));
@property (readonly) MainKtor_httpHttpStatusCode *ExpectationFailed __attribute__((swift_name("ExpectationFailed")));
@property (readonly) MainKtor_httpHttpStatusCode *FailedDependency __attribute__((swift_name("FailedDependency")));
@property (readonly) MainKtor_httpHttpStatusCode *Forbidden __attribute__((swift_name("Forbidden")));
@property (readonly) MainKtor_httpHttpStatusCode *Found __attribute__((swift_name("Found")));
@property (readonly) MainKtor_httpHttpStatusCode *GatewayTimeout __attribute__((swift_name("GatewayTimeout")));
@property (readonly) MainKtor_httpHttpStatusCode *Gone __attribute__((swift_name("Gone")));
@property (readonly) MainKtor_httpHttpStatusCode *InsufficientStorage __attribute__((swift_name("InsufficientStorage")));
@property (readonly) MainKtor_httpHttpStatusCode *InternalServerError __attribute__((swift_name("InternalServerError")));
@property (readonly) MainKtor_httpHttpStatusCode *LengthRequired __attribute__((swift_name("LengthRequired")));
@property (readonly) MainKtor_httpHttpStatusCode *Locked __attribute__((swift_name("Locked")));
@property (readonly) MainKtor_httpHttpStatusCode *MethodNotAllowed __attribute__((swift_name("MethodNotAllowed")));
@property (readonly) MainKtor_httpHttpStatusCode *MovedPermanently __attribute__((swift_name("MovedPermanently")));
@property (readonly) MainKtor_httpHttpStatusCode *MultiStatus __attribute__((swift_name("MultiStatus")));
@property (readonly) MainKtor_httpHttpStatusCode *MultipleChoices __attribute__((swift_name("MultipleChoices")));
@property (readonly) MainKtor_httpHttpStatusCode *NoContent __attribute__((swift_name("NoContent")));
@property (readonly) MainKtor_httpHttpStatusCode *NonAuthoritativeInformation __attribute__((swift_name("NonAuthoritativeInformation")));
@property (readonly) MainKtor_httpHttpStatusCode *NotAcceptable __attribute__((swift_name("NotAcceptable")));
@property (readonly) MainKtor_httpHttpStatusCode *NotFound __attribute__((swift_name("NotFound")));
@property (readonly) MainKtor_httpHttpStatusCode *NotImplemented __attribute__((swift_name("NotImplemented")));
@property (readonly) MainKtor_httpHttpStatusCode *NotModified __attribute__((swift_name("NotModified")));
@property (readonly) MainKtor_httpHttpStatusCode *OK __attribute__((swift_name("OK")));
@property (readonly) MainKtor_httpHttpStatusCode *PartialContent __attribute__((swift_name("PartialContent")));
@property (readonly) MainKtor_httpHttpStatusCode *PayloadTooLarge __attribute__((swift_name("PayloadTooLarge")));
@property (readonly) MainKtor_httpHttpStatusCode *PaymentRequired __attribute__((swift_name("PaymentRequired")));
@property (readonly) MainKtor_httpHttpStatusCode *PermanentRedirect __attribute__((swift_name("PermanentRedirect")));
@property (readonly) MainKtor_httpHttpStatusCode *PreconditionFailed __attribute__((swift_name("PreconditionFailed")));
@property (readonly) MainKtor_httpHttpStatusCode *Processing __attribute__((swift_name("Processing")));
@property (readonly) MainKtor_httpHttpStatusCode *ProxyAuthenticationRequired __attribute__((swift_name("ProxyAuthenticationRequired")));
@property (readonly) MainKtor_httpHttpStatusCode *RequestHeaderFieldTooLarge __attribute__((swift_name("RequestHeaderFieldTooLarge")));
@property (readonly) MainKtor_httpHttpStatusCode *RequestTimeout __attribute__((swift_name("RequestTimeout")));
@property (readonly) MainKtor_httpHttpStatusCode *RequestURITooLong __attribute__((swift_name("RequestURITooLong")));
@property (readonly) MainKtor_httpHttpStatusCode *RequestedRangeNotSatisfiable __attribute__((swift_name("RequestedRangeNotSatisfiable")));
@property (readonly) MainKtor_httpHttpStatusCode *ResetContent __attribute__((swift_name("ResetContent")));
@property (readonly) MainKtor_httpHttpStatusCode *SeeOther __attribute__((swift_name("SeeOther")));
@property (readonly) MainKtor_httpHttpStatusCode *ServiceUnavailable __attribute__((swift_name("ServiceUnavailable")));
@property (readonly) MainKtor_httpHttpStatusCode *SwitchProxy __attribute__((swift_name("SwitchProxy")));
@property (readonly) MainKtor_httpHttpStatusCode *SwitchingProtocols __attribute__((swift_name("SwitchingProtocols")));
@property (readonly) MainKtor_httpHttpStatusCode *TemporaryRedirect __attribute__((swift_name("TemporaryRedirect")));
@property (readonly) MainKtor_httpHttpStatusCode *TooManyRequests __attribute__((swift_name("TooManyRequests")));
@property (readonly) MainKtor_httpHttpStatusCode *Unauthorized __attribute__((swift_name("Unauthorized")));
@property (readonly) MainKtor_httpHttpStatusCode *UnprocessableEntity __attribute__((swift_name("UnprocessableEntity")));
@property (readonly) MainKtor_httpHttpStatusCode *UnsupportedMediaType __attribute__((swift_name("UnsupportedMediaType")));
@property (readonly) MainKtor_httpHttpStatusCode *UpgradeRequired __attribute__((swift_name("UpgradeRequired")));
@property (readonly) MainKtor_httpHttpStatusCode *UseProxy __attribute__((swift_name("UseProxy")));
@property (readonly) MainKtor_httpHttpStatusCode *VariantAlsoNegotiates __attribute__((swift_name("VariantAlsoNegotiates")));
@property (readonly) MainKtor_httpHttpStatusCode *VersionNotSupported __attribute__((swift_name("VersionNotSupported")));
@property (readonly) NSArray<MainKtor_httpHttpStatusCode *> *allStatusCodes __attribute__((swift_name("allStatusCodes")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsGMTDate.Companion")))
@interface MainKtor_utilsGMTDateCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_utilsGMTDateCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) MainKtor_utilsGMTDate *START __attribute__((swift_name("START")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsWeekDay")))
@interface MainKtor_utilsWeekDay : MainKotlinEnum<MainKtor_utilsWeekDay *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) MainKtor_utilsWeekDayCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) MainKtor_utilsWeekDay *monday __attribute__((swift_name("monday")));
@property (class, readonly) MainKtor_utilsWeekDay *tuesday __attribute__((swift_name("tuesday")));
@property (class, readonly) MainKtor_utilsWeekDay *wednesday __attribute__((swift_name("wednesday")));
@property (class, readonly) MainKtor_utilsWeekDay *thursday __attribute__((swift_name("thursday")));
@property (class, readonly) MainKtor_utilsWeekDay *friday __attribute__((swift_name("friday")));
@property (class, readonly) MainKtor_utilsWeekDay *saturday __attribute__((swift_name("saturday")));
@property (class, readonly) MainKtor_utilsWeekDay *sunday __attribute__((swift_name("sunday")));
+ (MainKotlinArray<MainKtor_utilsWeekDay *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsMonth")))
@interface MainKtor_utilsMonth : MainKotlinEnum<MainKtor_utilsMonth *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly, getter=companion) MainKtor_utilsMonthCompanion *companion __attribute__((swift_name("companion")));
@property (class, readonly) MainKtor_utilsMonth *january __attribute__((swift_name("january")));
@property (class, readonly) MainKtor_utilsMonth *february __attribute__((swift_name("february")));
@property (class, readonly) MainKtor_utilsMonth *march __attribute__((swift_name("march")));
@property (class, readonly) MainKtor_utilsMonth *april __attribute__((swift_name("april")));
@property (class, readonly) MainKtor_utilsMonth *may __attribute__((swift_name("may")));
@property (class, readonly) MainKtor_utilsMonth *june __attribute__((swift_name("june")));
@property (class, readonly) MainKtor_utilsMonth *july __attribute__((swift_name("july")));
@property (class, readonly) MainKtor_utilsMonth *august __attribute__((swift_name("august")));
@property (class, readonly) MainKtor_utilsMonth *september __attribute__((swift_name("september")));
@property (class, readonly) MainKtor_utilsMonth *october __attribute__((swift_name("october")));
@property (class, readonly) MainKtor_utilsMonth *november __attribute__((swift_name("november")));
@property (class, readonly) MainKtor_utilsMonth *december __attribute__((swift_name("december")));
+ (MainKotlinArray<MainKtor_utilsMonth *> *)values __attribute__((swift_name("values()")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHttpProtocolVersion.Companion")))
@interface MainKtor_httpHttpProtocolVersionCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_httpHttpProtocolVersionCompanion *shared __attribute__((swift_name("shared")));
- (MainKtor_httpHttpProtocolVersion *)fromValueName:(NSString *)name major:(int32_t)major minor:(int32_t)minor __attribute__((swift_name("fromValue(name:major:minor:)")));
- (MainKtor_httpHttpProtocolVersion *)parseValue:(id)value __attribute__((swift_name("parse(value:)")));
@property (readonly) MainKtor_httpHttpProtocolVersion *HTTP_1_0 __attribute__((swift_name("HTTP_1_0")));
@property (readonly) MainKtor_httpHttpProtocolVersion *HTTP_1_1 __attribute__((swift_name("HTTP_1_1")));
@property (readonly) MainKtor_httpHttpProtocolVersion *HTTP_2_0 __attribute__((swift_name("HTTP_2_0")));
@property (readonly) MainKtor_httpHttpProtocolVersion *QUIC __attribute__((swift_name("QUIC")));
@property (readonly) MainKtor_httpHttpProtocolVersion *SPDY_3 __attribute__((swift_name("SPDY_3")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpURLProtocol.Companion")))
@interface MainKtor_httpURLProtocolCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_httpURLProtocolCompanion *shared __attribute__((swift_name("shared")));
- (MainKtor_httpURLProtocol *)createOrDefaultName:(NSString *)name __attribute__((swift_name("createOrDefault(name:)")));
@property (readonly) MainKtor_httpURLProtocol *HTTP __attribute__((swift_name("HTTP")));
@property (readonly) MainKtor_httpURLProtocol *HTTPS __attribute__((swift_name("HTTPS")));
@property (readonly) MainKtor_httpURLProtocol *SOCKS __attribute__((swift_name("SOCKS")));
@property (readonly) MainKtor_httpURLProtocol *WS __attribute__((swift_name("WS")));
@property (readonly) MainKtor_httpURLProtocol *WSS __attribute__((swift_name("WSS")));
@property (readonly) NSDictionary<NSString *, MainKtor_httpURLProtocol *> *byName __attribute__((swift_name("byName")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpUrlEncodingOption")))
@interface MainKtor_httpUrlEncodingOption : MainKotlinEnum<MainKtor_httpUrlEncodingOption *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) MainKtor_httpUrlEncodingOption *default_ __attribute__((swift_name("default_")));
@property (class, readonly) MainKtor_httpUrlEncodingOption *keyOnly __attribute__((swift_name("keyOnly")));
@property (class, readonly) MainKtor_httpUrlEncodingOption *valueOnly __attribute__((swift_name("valueOnly")));
@property (class, readonly) MainKtor_httpUrlEncodingOption *noEncoding __attribute__((swift_name("noEncoding")));
+ (MainKotlinArray<MainKtor_httpUrlEncodingOption *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreParentJob")))
@protocol MainKotlinx_coroutines_coreParentJob <MainKotlinx_coroutines_coreJob>
@required
- (MainKotlinCancellationException *)getChildJobCancellationCause __attribute__((swift_name("getChildJobCancellationCause()")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreSelectInstance")))
@protocol MainKotlinx_coroutines_coreSelectInstance
@required
- (void)disposeOnSelectHandle:(id<MainKotlinx_coroutines_coreDisposableHandle>)handle __attribute__((swift_name("disposeOnSelect(handle:)")));
- (id _Nullable)performAtomicTrySelectDesc:(MainKotlinx_coroutines_coreAtomicDesc *)desc __attribute__((swift_name("performAtomicTrySelect(desc:)")));
- (void)resumeSelectWithExceptionException:(MainKotlinThrowable *)exception __attribute__((swift_name("resumeSelectWithException(exception:)")));
- (BOOL)trySelect __attribute__((swift_name("trySelect()")));
- (id _Nullable)trySelectOtherOtherOp:(MainKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp * _Nullable)otherOp __attribute__((swift_name("trySelectOther(otherOp:)")));
@property (readonly) id<MainKotlinContinuation> completion __attribute__((swift_name("completion")));
@property (readonly) BOOL isSelected __attribute__((swift_name("isSelected")));
@end;

__attribute__((swift_name("KotlinSuspendFunction0")))
@protocol MainKotlinSuspendFunction0 <MainKotlinFunction>
@required

/**
 @note This method converts instances of CancellationException to errors.
 Other uncaught Kotlin exceptions are fatal.
*/
- (void)invokeWithCompletionHandler:(void (^)(id _Nullable_result, NSError * _Nullable))completionHandler __attribute__((swift_name("invoke(completionHandler:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioMemory.Companion")))
@interface MainKtor_ioMemoryCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_ioMemoryCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) MainKtor_ioMemory *Empty __attribute__((swift_name("Empty")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioBuffer.Companion")))
@interface MainKtor_ioBufferCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_ioBufferCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) MainKtor_ioBuffer *Empty __attribute__((swift_name("Empty")));
@property (readonly) int32_t ReservedSize __attribute__((swift_name("ReservedSize")));
@end;

__attribute__((swift_name("Ktor_ioObjectPool")))
@protocol MainKtor_ioObjectPool <MainKtor_ioCloseable>
@required
- (id)borrow __attribute__((swift_name("borrow()")));
- (void)dispose __attribute__((swift_name("dispose()")));
- (void)recycleInstance:(id)instance __attribute__((swift_name("recycle(instance:)")));
@property (readonly) int32_t capacity __attribute__((swift_name("capacity")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioChunkBuffer.Companion")))
@interface MainKtor_ioChunkBufferCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_ioChunkBufferCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) MainKtor_ioChunkBuffer *Empty __attribute__((swift_name("Empty")));
@property (readonly) id<MainKtor_ioObjectPool> EmptyPool __attribute__((swift_name("EmptyPool")));
@property (readonly) id<MainKtor_ioObjectPool> Pool __attribute__((swift_name("Pool")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinCharArray")))
@interface MainKotlinCharArray : MainBase
+ (instancetype)arrayWithSize:(int32_t)size __attribute__((swift_name("init(size:)")));
+ (instancetype)arrayWithSize:(int32_t)size init:(id (^)(MainInt *))init __attribute__((swift_name("init(size:init:)")));
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (unichar)getIndex:(int32_t)index __attribute__((swift_name("get(index:)")));
- (MainKotlinCharIterator *)iterator __attribute__((swift_name("iterator()")));
- (void)setIndex:(int32_t)index value:(unichar)value __attribute__((swift_name("set(index:value:)")));
@property (readonly) int32_t size __attribute__((swift_name("size")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioIoBuffer.Companion")))
@interface MainKtor_ioIoBufferCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_ioIoBufferCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) MainKtor_ioIoBuffer *Empty __attribute__((swift_name("Empty")));
@property (readonly) id<MainKtor_ioObjectPool> EmptyPool __attribute__((swift_name("EmptyPool")));
@property (readonly) id<MainKtor_ioObjectPool> NoPool __attribute__((swift_name("NoPool")));
@property (readonly) id<MainKtor_ioObjectPool> Pool __attribute__((swift_name("Pool")));
@property (readonly) int32_t ReservedSize __attribute__((swift_name("ReservedSize")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioAbstractInput.Companion")))
@interface MainKtor_ioAbstractInputCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_ioAbstractInputCompanion *shared __attribute__((swift_name("shared")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteReadPacketBase.Companion")))
@interface MainKtor_ioByteReadPacketBaseCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_ioByteReadPacketBaseCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) MainKtor_ioByteReadPacket *Empty __attribute__((swift_name("Empty"))) __attribute__((unavailable("Use ByteReadPacket.Empty instead")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteReadPacket.Companion")))
@interface MainKtor_ioByteReadPacketCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_ioByteReadPacketCompanion *shared __attribute__((swift_name("shared")));
@property (readonly) MainKtor_ioByteReadPacket *Empty __attribute__((swift_name("Empty")));
@property (readonly) int32_t ReservedSize __attribute__((swift_name("ReservedSize")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_ioByteOrder.Companion")))
@interface MainKtor_ioByteOrderCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_ioByteOrderCompanion *shared __attribute__((swift_name("shared")));
- (MainKtor_ioByteOrder *)nativeOrder __attribute__((swift_name("nativeOrder()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKTypeProjection")))
@interface MainKotlinKTypeProjection : MainBase
- (instancetype)initWithVariance:(MainKotlinKVariance * _Nullable)variance type:(id<MainKotlinKType> _Nullable)type __attribute__((swift_name("init(variance:type:)"))) __attribute__((objc_designated_initializer));
@property (class, readonly, getter=companion) MainKotlinKTypeProjectionCompanion *companion __attribute__((swift_name("companion")));
- (MainKotlinKVariance * _Nullable)component1 __attribute__((swift_name("component1()")));
- (id<MainKotlinKType> _Nullable)component2 __attribute__((swift_name("component2()")));
- (MainKotlinKTypeProjection *)doCopyVariance:(MainKotlinKVariance * _Nullable)variance type:(id<MainKotlinKType> _Nullable)type __attribute__((swift_name("doCopy(variance:type:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) id<MainKotlinKType> _Nullable type __attribute__((swift_name("type")));
@property (readonly) MainKotlinKVariance * _Nullable variance __attribute__((swift_name("variance")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeaderValueParam")))
@interface MainKtor_httpHeaderValueParam : MainBase
- (instancetype)initWithName:(NSString *)name value:(NSString *)value __attribute__((swift_name("init(name:value:)"))) __attribute__((objc_designated_initializer));
- (NSString *)component1 __attribute__((swift_name("component1()")));
- (NSString *)component2 __attribute__((swift_name("component2()")));
- (MainKtor_httpHeaderValueParam *)doCopyName:(NSString *)name value:(NSString *)value __attribute__((swift_name("doCopy(name:value:)")));
- (BOOL)isEqual:(id _Nullable)other __attribute__((swift_name("isEqual(_:)")));
- (NSUInteger)hash __attribute__((swift_name("hash()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) NSString *name __attribute__((swift_name("name")));
@property (readonly) NSString *value __attribute__((swift_name("value")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpHeaderValueWithParameters.Companion")))
@interface MainKtor_httpHeaderValueWithParametersCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_httpHeaderValueWithParametersCompanion *shared __attribute__((swift_name("shared")));
- (id _Nullable)parseValue:(NSString *)value init:(id _Nullable (^)(NSString *, NSArray<MainKtor_httpHeaderValueParam *> *))init __attribute__((swift_name("parse(value:init:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_httpContentType.Companion")))
@interface MainKtor_httpContentTypeCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_httpContentTypeCompanion *shared __attribute__((swift_name("shared")));
- (MainKtor_httpContentType *)parseValue:(NSString *)value __attribute__((swift_name("parse(value:)")));
@property (readonly) MainKtor_httpContentType *Any __attribute__((swift_name("Any")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsWeekDay.Companion")))
@interface MainKtor_utilsWeekDayCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_utilsWeekDayCompanion *shared __attribute__((swift_name("shared")));
- (MainKtor_utilsWeekDay *)fromOrdinal:(int32_t)ordinal __attribute__((swift_name("from(ordinal:)")));
- (MainKtor_utilsWeekDay *)fromValue:(NSString *)value __attribute__((swift_name("from(value:)")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Ktor_utilsMonth.Companion")))
@interface MainKtor_utilsMonthCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKtor_utilsMonthCompanion *shared __attribute__((swift_name("shared")));
- (MainKtor_utilsMonth *)fromOrdinal:(int32_t)ordinal __attribute__((swift_name("from(ordinal:)")));
- (MainKtor_utilsMonth *)fromValue:(NSString *)value __attribute__((swift_name("from(value:)")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreAtomicDesc")))
@interface MainKotlinx_coroutines_coreAtomicDesc : MainBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeOp:(MainKotlinx_coroutines_coreAtomicOp<id> *)op failure:(id _Nullable)failure __attribute__((swift_name("complete(op:failure:)")));
- (id _Nullable)prepareOp:(MainKotlinx_coroutines_coreAtomicOp<id> *)op __attribute__((swift_name("prepare(op:)")));
@property MainKotlinx_coroutines_coreAtomicOp<id> *atomicOp __attribute__((swift_name("atomicOp")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreOpDescriptor")))
@interface MainKotlinx_coroutines_coreOpDescriptor : MainBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (BOOL)isEarlierThanThat:(MainKotlinx_coroutines_coreOpDescriptor *)that __attribute__((swift_name("isEarlierThan(that:)")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) MainKotlinx_coroutines_coreAtomicOp<id> * _Nullable atomicOp __attribute__((swift_name("atomicOp")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNode.PrepareOp")))
@interface MainKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp : MainKotlinx_coroutines_coreOpDescriptor
- (instancetype)initWithAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)next desc:(MainKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc *)desc __attribute__((swift_name("init(affected:next:desc:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)finishPrepare __attribute__((swift_name("finishPrepare()")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNode *affected __attribute__((swift_name("affected")));
@property (readonly) MainKotlinx_coroutines_coreAtomicOp<id> *atomicOp __attribute__((swift_name("atomicOp")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc *desc __attribute__((swift_name("desc")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNode *next __attribute__((swift_name("next")));
@end;

__attribute__((swift_name("KotlinCharIterator")))
@interface MainKotlinCharIterator : MainBase <MainKotlinIterator>
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (id)next __attribute__((swift_name("next()")));
- (unichar)nextChar __attribute__((swift_name("nextChar()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKVariance")))
@interface MainKotlinKVariance : MainKotlinEnum<MainKotlinKVariance *>
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
- (instancetype)initWithName:(NSString *)name ordinal:(int32_t)ordinal __attribute__((swift_name("init(name:ordinal:)"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
@property (class, readonly) MainKotlinKVariance *invariant __attribute__((swift_name("invariant")));
@property (class, readonly) MainKotlinKVariance *in __attribute__((swift_name("in")));
@property (class, readonly) MainKotlinKVariance *out __attribute__((swift_name("out")));
+ (MainKotlinArray<MainKotlinKVariance *> *)values __attribute__((swift_name("values()")));
@end;

__attribute__((objc_subclassing_restricted))
__attribute__((swift_name("KotlinKTypeProjection.Companion")))
@interface MainKotlinKTypeProjectionCompanion : MainBase
+ (instancetype)alloc __attribute__((unavailable));
+ (instancetype)allocWithZone:(struct _NSZone *)zone __attribute__((unavailable));
+ (instancetype)companion __attribute__((swift_name("init()")));
@property (class, readonly, getter=shared) MainKotlinKTypeProjectionCompanion *shared __attribute__((swift_name("shared")));
- (MainKotlinKTypeProjection *)contravariantType:(id<MainKotlinKType>)type __attribute__((swift_name("contravariant(type:)")));
- (MainKotlinKTypeProjection *)covariantType:(id<MainKotlinKType>)type __attribute__((swift_name("covariant(type:)")));
- (MainKotlinKTypeProjection *)invariantType:(id<MainKotlinKType>)type __attribute__((swift_name("invariant(type:)")));
@property (readonly) MainKotlinKTypeProjection *STAR __attribute__((swift_name("STAR")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreAtomicOp")))
@interface MainKotlinx_coroutines_coreAtomicOp<__contravariant T> : MainKotlinx_coroutines_coreOpDescriptor
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeAffected:(T _Nullable)affected failure:(id _Nullable)failure __attribute__((swift_name("complete(affected:failure:)")));
- (id _Nullable)decideDecision:(id _Nullable)decision __attribute__((swift_name("decide(decision:)")));
- (id _Nullable)performAffected:(id _Nullable)affected __attribute__((swift_name("perform(affected:)")));
- (id _Nullable)prepareAffected:(T _Nullable)affected __attribute__((swift_name("prepare(affected:)")));
@property (readonly) MainKotlinx_coroutines_coreAtomicOp<id> *atomicOp __attribute__((swift_name("atomicOp")));
@property (readonly) id _Nullable consensus __attribute__((swift_name("consensus")));
@property (readonly) BOOL isDecided __attribute__((swift_name("isDecided")));
@property (readonly) int64_t opSequence __attribute__((swift_name("opSequence")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNode")))
@interface MainKotlinx_coroutines_coreLockFreeLinkedListNode : MainBase
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)addLastNode:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)node __attribute__((swift_name("addLast(node:)")));
- (BOOL)addLastIfNode:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)node condition:(MainBoolean *(^)(void))condition __attribute__((swift_name("addLastIf(node:condition:)")));
- (BOOL)addLastIfPrevNode:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)node predicate:(MainBoolean *(^)(MainKotlinx_coroutines_coreLockFreeLinkedListNode *))predicate __attribute__((swift_name("addLastIfPrev(node:predicate:)")));
- (BOOL)addLastIfPrevAndIfNode:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)node predicate:(MainBoolean *(^)(MainKotlinx_coroutines_coreLockFreeLinkedListNode *))predicate condition:(MainBoolean *(^)(void))condition __attribute__((swift_name("addLastIfPrevAndIf(node:predicate:condition:)")));
- (BOOL)addOneIfEmptyNode:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)node __attribute__((swift_name("addOneIfEmpty(node:)")));
- (MainKotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc<MainKotlinx_coroutines_coreLockFreeLinkedListNode *> *)describeAddLastNode:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)node __attribute__((swift_name("describeAddLast(node:)")));
- (MainKotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc<MainKotlinx_coroutines_coreLockFreeLinkedListNode *> *)describeRemoveFirst __attribute__((swift_name("describeRemoveFirst()")));
- (void)helpRemove __attribute__((swift_name("helpRemove()")));
- (MainKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)nextIfRemoved __attribute__((swift_name("nextIfRemoved()")));
- (BOOL)remove __attribute__((swift_name("remove()")));
- (id _Nullable)removeFirstIfIsInstanceOfOrPeekIfPredicate:(MainBoolean *(^)(id _Nullable))predicate __attribute__((swift_name("removeFirstIfIsInstanceOfOrPeekIf(predicate:)")));
- (MainKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)removeFirstOrNull __attribute__((swift_name("removeFirstOrNull()")));
- (NSString *)description __attribute__((swift_name("description()")));
@property (readonly) BOOL isRemoved __attribute__((swift_name("isRemoved")));
@property (readonly, getter=next_) id _Nullable next __attribute__((swift_name("next")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNode *nextNode __attribute__((swift_name("nextNode")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNode *prevNode __attribute__((swift_name("prevNode")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNode.AbstractAtomicDesc")))
@interface MainKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc : MainKotlinx_coroutines_coreAtomicDesc
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer));
+ (instancetype)new __attribute__((availability(swift, unavailable, message="use object initializers instead")));
- (void)completeOp:(MainKotlinx_coroutines_coreAtomicOp<id> *)op failure:(id _Nullable)failure __attribute__((swift_name("complete(op:failure:)")));
- (id _Nullable)failureAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)affected __attribute__((swift_name("failure(affected:)")));
- (void)finishOnSuccessAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(MainKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (id _Nullable)onPreparePrepareOp:(MainKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("onPrepare(prepareOp:)")));
- (void)onRemovedAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)affected __attribute__((swift_name("onRemoved(affected:)")));
- (id _Nullable)prepareOp:(MainKotlinx_coroutines_coreAtomicOp<id> *)op __attribute__((swift_name("prepare(op:)")));
- (BOOL)retryAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
- (MainKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)takeAffectedNodeOp:(MainKotlinx_coroutines_coreOpDescriptor *)op __attribute__((swift_name("takeAffectedNode(op:)")));
- (id)updatedNextAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("updatedNext(affected:next:)")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable affectedNode __attribute__((swift_name("affectedNode")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable originalNext __attribute__((swift_name("originalNext")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc")))
@interface MainKotlinx_coroutines_coreLockFreeLinkedListNodeAddLastDesc<T> : MainKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc
- (instancetype)initWithQueue:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)queue node:(T)node __attribute__((swift_name("init(queue:node:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (void)finishOnSuccessAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(MainKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (BOOL)retryAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
- (MainKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)takeAffectedNodeOp:(MainKotlinx_coroutines_coreOpDescriptor *)op __attribute__((swift_name("takeAffectedNode(op:)")));
- (id)updatedNextAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("updatedNext(affected:next:)")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable affectedNode __attribute__((swift_name("affectedNode")));
@property (readonly) T node __attribute__((swift_name("node")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNode *originalNext __attribute__((swift_name("originalNext")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNode *queue __attribute__((swift_name("queue")));
@end;

__attribute__((swift_name("Kotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc")))
@interface MainKotlinx_coroutines_coreLockFreeLinkedListNodeRemoveFirstDesc<T> : MainKotlinx_coroutines_coreLockFreeLinkedListNodeAbstractAtomicDesc
- (instancetype)initWithQueue:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)queue __attribute__((swift_name("init(queue:)"))) __attribute__((objc_designated_initializer));
- (instancetype)init __attribute__((swift_name("init()"))) __attribute__((objc_designated_initializer)) __attribute__((unavailable));
+ (instancetype)new __attribute__((unavailable));
- (id _Nullable)failureAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)affected __attribute__((swift_name("failure(affected:)")));
- (void)finishOnSuccessAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("finishOnSuccess(affected:next:)")));
- (void)finishPreparePrepareOp:(MainKotlinx_coroutines_coreLockFreeLinkedListNodePrepareOp *)prepareOp __attribute__((swift_name("finishPrepare(prepareOp:)")));
- (BOOL)retryAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(id)next __attribute__((swift_name("retry(affected:next:)")));
- (MainKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable)takeAffectedNodeOp:(MainKotlinx_coroutines_coreOpDescriptor *)op __attribute__((swift_name("takeAffectedNode(op:)")));
- (id)updatedNextAffected:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)affected next:(MainKotlinx_coroutines_coreLockFreeLinkedListNode *)next __attribute__((swift_name("updatedNext(affected:next:)")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable affectedNode __attribute__((swift_name("affectedNode")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNode * _Nullable originalNext __attribute__((swift_name("originalNext")));
@property (readonly) MainKotlinx_coroutines_coreLockFreeLinkedListNode *queue __attribute__((swift_name("queue")));
@property (readonly) T _Nullable result __attribute__((swift_name("result")));
@end;

#pragma pop_macro("_Nullable_result")
#pragma clang diagnostic pop
NS_ASSUME_NONNULL_END
