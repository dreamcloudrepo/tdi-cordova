#ifndef _DEP_DATATYPES_H
#define _DEP_DATATYPES_H

#include <stdint.h>
#include <stdlib.h>
#include "deperrorcodes.h"

/**
* @brief Define the bytes array
* @see  kDEP_MaxBuffer
*/

/**
 * @brief Type definition for byte type
 */
typedef unsigned char DEPByte;

/**
 * @brief Type definition which represents a pointer to a variable of type unsigned char *.
 */
typedef DEPByte *  DEPBytePtr;

typedef struct DEP_BYTE_ARRAY {
    size_t length;          /**< length of bytes, range (0, kEP_MaxBuffer] */
    unsigned char * pData;  /**< pointer to the bytes.*/
} DEPByteArray;


typedef struct  DEP_SESSION {
    uint32_t sequenceNumber;
    uint32_t version;
    DEPByteArray * sequence;  // 4 bytes
    DEPByteArray * dataKey;   // 16 bytes
    DEPByteArray * macKey;    // 16 bytes
    DEPByteArray * obfuscatedKey; // 16 bytes
    DEPByteArray * sessionIdentifier;  //4 bytes
} DEPSession;


typedef struct DEP_RESPONSE {
    int statusCode;
    DEPByteArray * content;
    DEPByteArray * contentType;
    DEPErrorCode errorCode;
}DEPResponse;


typedef struct DEP_REQUEST {
    DEPByteArray * encodeData;
    DEPErrorCode errorCode;
}DEPRequest;



typedef struct DEP_PUBLIC_KEY {
    DEPByteArray * modulus;
    DEPByteArray * exponent;
}DEPPublicKey;




#endif
