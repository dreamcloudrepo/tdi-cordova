/*
*  Copyright (c) 2017 GEMALTO
*  This computer program includes confidential and proprietary information of
*  Gemalto and is a trade secret of Gemalto. All use, disclosure, and/or
*  reproduction is prohibited unless authorized in writing by Gemalto.
*  All Rights Reserved.
*/

#ifndef _DEP_BYTEARRAY_
#define _DEP_BYTEARRAY_

#include "dep/deperrorcodes.h"
#include "dep/depdatatypes.h"
#include <stdbool.h>

#if defined __cplusplus
extern "C" {
#endif

    /**
     *  The method to allocate memory for given buffer pointer
     *
     *  @param[out] array       The given pointer to hold the byte array buffer
     *  @param[in] length   The length of data
     *
     *  @return kDEPErrorCode_Ok for success, otherwise indicate error type.
     */
    DEPErrorCode DEPByteArray_secureMalloc(DEPByteArray ** array, size_t length);

     

    /**
     *  The method to free memory for given buffer pointer
     *
     *  @param[in,out] array       The given pointer to hold the byte array buffer
     *

     */
    void DEPByteArray_secureFree(DEPByteArray ** array);


    /**
     *  The method to obfuscate given buffer
     *
     *  @param[in,out] array    The given pointer to hold the byte array buffer
     *
     *  @return kDEPErrorCode_Ok for success
     *          kDEPErrorCode_InvalidParameter, if array is empty or invalid pointer 
     */
    DEPErrorCode DEPByteArray_obfuscate(DEPByteArray * array);



    /**
     *  The method to perform XOR operation for array1 and array2
     *
     *  @param[in] array1   The given pointer to buffer 1
     *  @param[in] array2    The given pointer to buffer 2
     *  @param[out] result    The given pointer to get output buffer
     *
     *  @return kDEPErrorCode_Ok for success, otherwise indicate error type.
     */
    DEPErrorCode DEPByteArray_xor(const DEPByteArray * const array1,
                                  const DEPByteArray * const array2,
                                  DEPByteArray ** result);
    


    /**
     *  The method to check buffer is all zero(0) or '\0'
     *
     *  @param[in] array    The given pointer to buffer
     *
     *  @return kDEPErrorCode_True, the buffer is all zeros or '\0'
     *          kDEPErrorCode_False, the buffer is not all zeros or '\0'
     *          Otherwise, indicate error type.
     */
    DEPErrorCode DEPByteArray_isEmptyOrZeros(const DEPByteArray * const array);


    
    /**
     *  The method to make a copy of buffer
     *
     *  @param[in] src  The given pointer to source buffer
     *  @param[out] dest The given pointer to output buffer
     *
     *  @return kDEPErrorCode_Ok for success, otherwise indicate error type.
     */
    DEPErrorCode DEPByteArray_copy(const DEPByteArray * const src, DEPByteArray ** dest);
    


    /**
     *  The method to combine bytes1 and bytes2 and output to out_bytes
     *
     *  @param[in] bytes1       The given pointer to buffer1
     *  @param[in] bytes2       The given pointer to buffer2
     *  @param[out] out_bytes    The given pointer to output buffer
     *
     *  @return kDEPErrorCode_Ok for success, otherwise indicate error type.
     */
    DEPErrorCode DEPByteArray_combine(const DEPByteArray * const bytes1,
                                      const DEPByteArray * const bytes2,
                                      DEPByteArray ** out_bytes);
    
   /**
     *  The method to check if bytes1 and bytes2 is equal 
     *
     *  @param[in] bytes1       The given pointer to buffer1
     *  @param[in] bytes2       The given pointer to buffer2
     *
     *  @return true for equal, otherwise not equal.
     */
    bool DEPByteArray_isEqual(const DEPByteArray * const bytes1, const DEPByteArray * const bytes2);


	/**
     *  The method to convert char to DEPByteArray
     *
     *  @param[in] src       The given pointer to src 
     *  @param[in] src_length       The length of src data
	 *  @param[out] out        The given pointer to output buffer
     *
     *  @return kDEPErrorCode_Ok for success, otherwise indicate error type.
     */
    DEPErrorCode DEPByteArray_convertFromCString(const char * src,
                                                 size_t src_length, 
                                                 DEPByteArray **out);
    
    /**
     *  The method to print buffer in HEX when DEP_DEBUG enabled. 
     *
     *  @param[in] array        The given pointer to buffer
     */
    void DEPByteArray_printHex(const DEPByteArray* const array); /**< DEBUG USAGE */



    /**
     *  The method to print buffer in char when DEP_DEBUG enabled. 
     *
     *  @param[in] array        The given pointer to buffer
     */
    void DEPByteArray_printChar(const DEPByteArray* const array); /**< DEBUG USAGE */
	
    /**
     *  The method to print buffer in cstring when DEP_DEBUG enabled. 
     *
     *  @param[in] array        The given pointer to buffer
     */
    void DEPByteArray_printCString(const DEPByteArray* const array); /**< DEBUG USAGE */


#if defined __cplusplus
}
#endif
#endif 