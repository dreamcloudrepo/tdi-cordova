/*
 *  Copyright (c) 2017 GEMALTO
 *  This computer program includes confidential and proprietary information of
 *  Gemalto and is a trade secret of Gemalto. All use, disclosure, and/or
 *  reproduction is prohibited unless authorized in writing by Gemalto.
 *  All Rights Reserved.
 */


 /**
  *  Data Exchange Protocol (v1)
  *  PUBLIC APIs for DEP
  */

#ifndef DEP_h
#define DEP_h

#include "depdatatypes.h"
#include "deperrorcodes.h"

#if defined __cplusplus
extern "C" {
#endif
    

    
    /**
     *  Method to allocate memory and initialize the session
     *
     *  @param[in,out] session The output of the DEPSession
     *
     *  @return kDEPErrorCode_Ok for success, otherwise indicate error type.
     */
    DEPErrorCode DEPInitSession(DEPSession ** session);


    /**
     *  The method to deallocate memory of session
     *
     *  @param[in,out] session The session to destroy
     *s
     */
    void DEPDestroySession(DEPSession ** session);



    /**
     *  The method to destroy the DEPRequest
     *
     *  @param[in,out] request The request to destroy
     *
     */
    void DEPDestroyReqeust(DEPRequest ** request);

    
    /**
     *  The method to destroy the response
     *
     *  @param[in,out] response The given response to destroy
     *
  s   */
    void DEPDestroyResponse(DEPResponse ** response);


    
    /**
     *  The method to copy the Dep session related attribute into a new session
     *
     *  @param[in] src  The source session
     *  @param[out] dest The destination session
     *
     *  @return  kDEPErrorCode_Ok for success, otherwise indicate error type.
     */
    DEPErrorCode DEPCopySession(const DEPSession * const src , DEPSession ** dest);


    
    /**
     *   The method to form the DEP request
     *
     *  @param[in] session     The given session to read
     *  @param[in] publicKey   The server public key
     *  @param[in] content     The data to be encoded using DEP protocol
     *  @param[in] contentType The type of the data to be encoded
     *  @param[out] request     Output data for request
     *
     *  @return kDEPErrorCode_Ok for success, otherwise indicate error type.
     */
    DEPErrorCode DEPEncodeRequest(DEPSession * session,
                                  const DEPPublicKey * const publicKey,
                                  const DEPByteArray * const content,
                                  const DEPByteArray * const contentType,
                                  DEPRequest ** request);


    /**
     *  The method to decode the DEP response.
     *
     *  @param[in] session  The given session
     *  @param[in] indata   The given data in byte
     *  @param[out] response The output decoded response
     *
     *  @return kDEPErrorCode_Ok for success, otherwise indicate error type.
     */
    DEPErrorCode DEPDecodeResponse(DEPSession * session,
                                   const DEPByteArray * const indata, 
                                   DEPResponse ** response);


    
    /**
     *  The method that return the error code from given request
     *
     *  @param[in] request The given request
     *
     *  @return The error code of request.
     */
    DEPErrorCode DEPGetRequestErrorCode(const DEPRequest * const request);


    
    /**
     *  Method that return the error code from given response
     *
     *  @param[in] response The given response to read
     *
     *  @return The error code of response
     */
    DEPErrorCode DEPGetResponseErrorCode(const DEPResponse * const response);


    /**
     *  Method that return the status code from given response
     *
     *  @param[in] response The given response
     *
     *  @return The status code of response
     */
    int DEPGetResponseStatusCode(const DEPResponse * const response);


    
    /**
     *  The method that return the encode data from given request
     *
     *  @param[in] request The given request
     *  @param[out] outdata The output data
     *
     *  @return kDEPErrorCode_Ok for success, otherwise indicate error type.
     */
    DEPErrorCode DEPGetEncodeDataFromRequest(const DEPRequest * const request, DEPByteArray ** outdata);

    
    /**
     *  The method that return the content type from given response
     *
     *  @param[in] response The given response
     *  @param[out] outdata  The output data
     *
     *  @return kDEPErrorCode_Ok for success, otherwise indicate error type.
     */
    DEPErrorCode DEPGetContentTypeFromResponse(const DEPResponse * const response, DEPByteArray ** outdata);

    
    
    /**
     *  Method that return the content  from given response
     *
     *  @param[in] response The given response
     *  @param[out] outdata  The output data
     *
     *  @return kDEPErrorCode_Ok for success, otherwise indicate error type.
     */
    DEPErrorCode DEPGetContentFromResponse(const DEPResponse * const response, DEPByteArray ** outdata);



    /**
     *  The method to get DEP SDK Version
     *
     *  @return DEP SDK version in c-string.
     */
    char* DEPGetSDKVersion(void);

#if defined __cplusplus
};
#endif
#endif
