#pragma once
#ifndef _IDVNFCCaptureResult_h_
#define _IDVNFCCaptureResult_h_

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


/**
 * Structure containing the MRZ.
 */
__attribute__ ((visibility ("default"))) @interface IDVNFCMRZ: NSObject { //DG1
}
/**
*/
@property (nonatomic, strong) NSString *documentType;
/**
*/
@property (nonatomic, strong) NSString *organization;
/**
*/
@property (nonatomic, strong) NSString *documentNumber;
/**
*/
@property (nonatomic, strong) NSString *optionalData;
/**
*/
@property (nonatomic, strong) NSString *dateOfBirth;
/**
*/
@property (nonatomic, strong) NSString *gender;
/**
*/
@property (nonatomic, strong) NSString *dateOfExpiry;
/**
*/
@property (nonatomic, strong) NSString *nationality;
/**
*/
@property (nonatomic, strong) NSString *nameOfDocumentHolder;
/**
*/
@property (nonatomic, strong) NSString *checkDigitDocNumber;
/**
*/
@property (nonatomic, strong) NSString *checkDigitDateOfBirth;
/**
*/
@property (nonatomic, strong) NSString *checkDigitExpiryDate;
/**
*/
@property (nonatomic, strong) NSString *checkDigitComposite;
/**
*/
@property (nonatomic, strong) NSString *checkDigitOptionalData;

@end


/**
 * Structure containing the Personal details.
 */
__attribute__ ((visibility ("default"))) @interface IDVNFCPersonalDetails: NSObject { //DG11
}
/**
*/
@property (nonatomic, strong) UIImage *citizenshipImage;
/**
*/
@property (nonatomic, strong) NSString *fullName;
/**
*/
@property (nonatomic, strong) NSString *otherName;
/**
*/
@property (nonatomic, strong) NSString *personalNumber;
/**
*/
@property (nonatomic, strong) NSString *dateOfBirth;
/**
*/
@property (nonatomic, strong) NSString *placeOfBirth;
/**
*/
@property (nonatomic, strong) NSString *permanentAddress;
/**
*/
@property (nonatomic, strong) NSString *telephone;
/**
*/
@property (nonatomic, strong) NSString *profession;
/**
*/
@property (nonatomic, strong) NSString *title;
/**
*/
@property (nonatomic, strong) NSString *personalSummary;
/**
*/
@property (nonatomic, strong) NSString *tdNumbers;
/**
*/
@property (nonatomic, strong) NSString *custodyInformation;

@end


/**
 * Structure containing the Document details.
 */
__attribute__ ((visibility ("default"))) @interface IDVNFCDocumentDetails: NSObject { //DG12
}
/**
*/
@property (nonatomic, strong) UIImage *frontImgMRTD;
/**
*/
@property (nonatomic, strong) UIImage *rearImgMRTD;
/**
*/
@property (nonatomic, strong) NSString *issuingAuthority;
/**
*/
@property (nonatomic, strong) NSString *dateOfIssue;
/**
*/
@property (nonatomic, strong) NSString *otherPersonDetails;
/**
*/
@property (nonatomic, strong) NSString *observations;
/**
*/
@property (nonatomic, strong) NSString *taxExitRequirements;
/**
*/
@property (nonatomic, strong) NSString *personalizationTime;
/**
*/
@property (nonatomic, strong) NSString *personalizationDeviceSerialNumber;

@end

/**
 * Structure containing all the read data.
 */
__attribute__ ((visibility ("default"))) @interface IDVNFCRawData: NSObject {
}
/**
 * Dictionary with the raw data read from the NFC chip for each data group
*/
@property (nonatomic, strong) NSMutableDictionary<NSString*, NSData*> *dg;
/**
 * Dictionary with the error code from the read process for each data group
*/
@property (nonatomic, strong) NSMutableDictionary<NSString*, NSNumber*> *status;
/**
 * Raw data read from the NFC chip for data group COM
*/
@property (nonatomic, strong) NSData *com;
/**
 * Raw data read from the NFC chip for data group SOD
*/
@property (nonatomic, strong) NSData *sod;
/**
 * Signature to check the data integrity
*/
@property (nonatomic, strong) NSData *signature;

@end

/**
 * Structure containing the parsed data.
 */
__attribute__ ((visibility ("default"))) @interface IDVNFCParsed: NSObject {
}
/**
 * MRZ information
*/
@property IDVNFCMRZ* MRZ;                               //DG1

/**
 * Portrait face image
*/
@property (nonatomic, strong) UIImage *faceImage;       //DG2

/**
 * Signature image
*/
@property (nonatomic, strong) UIImage *signatureImage;  //DG7

/**
 * Personal details
*/
@property IDVNFCPersonalDetails* personalDetails;       //DG11

/**
 * Document details
*/
@property IDVNFCDocumentDetails* documentDetails;       //DG12

@end

/**
 * Class that returns the data read from the document.
 * It provides and error code and an array of read data groups in raw data. 
 * Also provides parsed data for MRZ, picture and signature image (if exists).
 */
__attribute__ ((visibility ("default"))) @interface IDVNFCCaptureResult : NSObject {
}

    /**
     * Error code while reading data.
     * Possible values are:
     *       0: No error found
     *      -1: Invalid document chip
     *       1: Some non critical error were found (each data group describes their own errors)
     *  0x6300: No valid keys
     *  0x6700: Invalid document type
     *  0x9789: Connection lost
     */
    @property (nonatomic, assign) NSInteger errorCode;

    /**
     * Convenience field to store parsed data.
     */
    @property IDVNFCParsed* parsedData;

    /**
    * Datagroup raw data ( <gdId, data>  /  <gdid, error>  / signature / com / sod )
    */
    @property (nonatomic, strong) IDVNFCRawData *rawData;

    /**
    * Access used to get the chip data.
    */
    @property (nonatomic, strong) NSString *access;

    /**
    * Protocol used to get the chip data.
    */
    @property (nonatomic, strong) NSString *protocol;

@end


#endif
