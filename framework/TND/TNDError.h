//
//  TNDError.h
//  TND
//
//

#ifndef TNDError_h
#define TNDError_h

extern NSString* const TNDErrorDomain;

typedef NS_ERROR_ENUM(TNDErrorDomain, TNDErrorCode) {
    /**
     The operation has been cancelled.
     */
    TNDErrorCodeGenericError = 901,
};

#endif /* TNDError_h */
