//
//  TNDSDK.h
//  TND
//
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define DISABLE_TAMPER() [TNDSDK refresh];

@interface TNDSDK : NSObject

+ (void)supportBitcode;

- (instancetype)init NS_UNAVAILABLE;

+ (instancetype)getInstance;

- (void)setAPIBaseUrl:(NSString*)urlString;

- (void)setWebsiteID:(NSString*)clientID;

- (void)setSessionID:(NSString*)sessionID;

- (void)setPlacementString:(NSString*)placementString;

- (void)setPlacementPage:(NSInteger)placementPage;

- (void)initialize:(void(^)(NSError* _Nullable error))completionBlock;

- (void)createPayloadWithCompletion:(void(^)(NSString* _Nullable result, NSError* _Nullable error))completionBlock;

@end

NS_ASSUME_NONNULL_END
