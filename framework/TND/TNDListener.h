//
//  TNDListener.h
//  TND
//
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS (NSInteger, TNDListenerTypes) {
    TND_NDS_LISTEN_FOCUS = (1 << 0),
    TND_NDS_LISTEN_TOUCH = (1 << 1),
    TND_NDS_LISTEN_KEYDOWN = (1 << 2),
    TND_NDS_LISTEN_DEVICE_MOTION = (1 << 3)
};

@interface TNDListener : NSObject

- (void)register:(UIView *)view;

- (void)register:(UIView *)view types:(TNDListenerTypes)types;

- (void)unregister;

@end

NS_ASSUME_NONNULL_END
