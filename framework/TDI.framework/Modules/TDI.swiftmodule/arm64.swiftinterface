// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3.2 (swiftlang-1200.0.45 clang-1200.0.32.28)
// swift-module-flags: -target arm64-apple-ios12.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name TDI
import AVFoundation
import Accelerate
import D1
import FaceLiveness
import Foundation
import IDV_DOC
import IDV_FACE
import IDV_MRZ
import IDV_NFC
import PDFKit
import PencilKit
import Security
import Swift
import SystemConfiguration
@_exported import TDI
import UIKit
import Veridium4FBiometrics
import Veridium4FUI
import VeridiumCore
import WebKit
import libkern
import libmedl
import main
extension String {
  public var localized: Swift.String {
    get
  }
}
public protocol ISignatureView : AnyObject {
  var delegate: TDI.SwiftSignatureViewDelegate? { get set }
  var scale: CoreGraphics.CGFloat { get set }
  var maximumStrokeWidth: CoreGraphics.CGFloat { get set }
  var minimumStrokeWidth: CoreGraphics.CGFloat { get set }
  var strokeColor: UIKit.UIColor { get set }
  var strokeAlpha: CoreGraphics.CGFloat { get set }
  var signature: UIKit.UIImage? { get set }
  var isEmpty: Swift.Bool { get }
  func clear(cache: Swift.Bool)
  func undo()
  func redo()
  func getCroppedSignature() -> UIKit.UIImage?
}
@objc @_inheritsConvenienceInitializers open class SwiftSignatureView : UIKit.UIView, TDI.ISignatureView {
  weak public var delegate: TDI.SwiftSignatureViewDelegate? {
    get
    set
  }
  @objc @IBInspectable public var scale: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var minimumStrokeWidth: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var maximumStrokeWidth: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var strokeColor: UIKit.UIColor {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var strokeAlpha: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var signature: UIKit.UIImage? {
    @objc get
    @objc set
  }
  open var isEmpty: Swift.Bool {
    get
  }
  public func clear(cache: Swift.Bool = false)
  public func undo()
  public func redo()
  public func getCroppedSignature() -> UIKit.UIImage?
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc override dynamic open func updateConstraintsIfNeeded()
  @objc deinit
}
@objc @_inheritsConvenienceInitializers open class LegacySwiftSignatureView : UIKit.UIView, UIKit.UIGestureRecognizerDelegate, TDI.ISignatureView {
  weak open var delegate: TDI.SwiftSignatureViewDelegate?
  open var scale: CoreGraphics.CGFloat
  open var maximumStrokeWidth: CoreGraphics.CGFloat {
    get
    set
  }
  open var minimumStrokeWidth: CoreGraphics.CGFloat {
    get
    set
  }
  open var strokeColor: UIKit.UIColor
  open var strokeAlpha: CoreGraphics.CGFloat {
    get
    set
  }
  public var signature: UIKit.UIImage? {
    get
    set
  }
  open var isEmpty: Swift.Bool {
    get
  }
  open func clear(cache: Swift.Bool = false)
  open func undo()
  open func redo()
  @objc deinit
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  public func getCroppedSignature() -> UIKit.UIImage?
  @objc public func gestureRecognizer(_ gestureRecognizer: UIKit.UIGestureRecognizer, shouldReceive touch: UIKit.UITouch) -> Swift.Bool
  @objc public func pan(_ pan: UIKit.UIPanGestureRecognizer)
  @objc override dynamic open func draw(_ rect: CoreGraphics.CGRect)
}
@objc @_inheritsConvenienceInitializers @available(iOS 13.0, *)
open class PencilKitSignatureView : UIKit.UIView, TDI.ISignatureView {
  weak open var delegate: TDI.SwiftSignatureViewDelegate?
  open var scale: CoreGraphics.CGFloat
  open var maximumStrokeWidth: CoreGraphics.CGFloat {
    get
    set
  }
  open var minimumStrokeWidth: CoreGraphics.CGFloat
  open var strokeColor: UIKit.UIColor {
    get
    set
  }
  open var strokeAlpha: CoreGraphics.CGFloat
  open var signature: UIKit.UIImage? {
    get
    set
  }
  open func getCroppedSignature() -> UIKit.UIImage?
  open var isEmpty: Swift.Bool {
    get
  }
  open func clear(cache: Swift.Bool)
  open func undo()
  open func redo()
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc override dynamic open func updateConstraintsIfNeeded()
  @objc deinit
}
@available(iOS 13.0, *)
extension PencilKitSignatureView : PencilKit.PKCanvasViewDelegate {
  @objc dynamic public func canvasViewDidEndUsingTool(_ canvasView: PencilKit.PKCanvasView)
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc public class TdiMobileSdk : ObjectiveC.NSObject, Foundation.URLSessionDelegate {
  public var captorProvider: main.CaptorProvider!
  public var hideNavagationbar: Swift.Bool
  public var token: Swift.String!
  public var delegate: TDI.TdiMobileSdkDelegate?
  @objc public static let shared: TDI.TdiMobileSdk
  @objc public var configuration: [Swift.String : Any]
  public func getCurrentSession() -> main.AbstractSessionImpl
  public func getVersion() -> Swift.String
  public func setViewController(myViewController: UIKit.UIViewController)
  public func setLicenses(licenses: [Swift.String : Swift.String])
  public func newSession(tdiServerURL: Swift.String, scenarioName: Swift.String, jwtToken: Swift.String, tenantId: Swift.String, correlationId: Swift.String?) -> main.Session?
  public func getProfile(tdiServerURL: Swift.String, jwtToken: Swift.String, tenantId: Swift.String)
  @objc deinit
}
@objc public protocol TdiMobileSdkDelegate {
  @objc func taskStateListener(status: main.Status, task: Foundation.NSMutableArray?, result: main.Result?, profileInfo: main.TdiProfileInfo?)
}
public struct TDIConfiguration {
  public static let blackAndWhite: IDV_DOC.PhotocopyMode
  public static let disable: IDV_DOC.PhotocopyMode
  public static let blurDisabled: IDV_DOC.BlurMode
  public static let blurRelaxed: IDV_DOC.BlurMode
  public static let blurStrict: IDV_DOC.BlurMode
  public static let blurTextBlock: IDV_DOC.BlurMode
  public static let glareDisabled: IDV_DOC.GlareMode
  public static let glareWhite: IDV_DOC.GlareMode
  public static let glareColor: IDV_DOC.GlareMode
  public static let darknessDisabled: IDV_DOC.DarknessMode
  public static let darknessRelaxed: IDV_DOC.DarknessMode
  public static let detectionImageProcessing: IDV_DOC.IDVDOCDetectionMode
  public static let detectionMachineLearning: IDV_DOC.IDVDOCDetectionMode
}
public protocol SwiftSignatureViewDelegate : AnyObject {
  func swiftSignatureViewDidDrawGesture(_ view: TDI.ISignatureView, _ tap: UIKit.UIGestureRecognizer)
  func swiftSignatureViewDidDraw(_ view: TDI.ISignatureView)
}
@objc final public class Chronometer : ObjectiveC.NSObject {
  final public var isPlaying: Swift.Bool
  final public var timerCurrentValue: Swift.Double
  final public var timerDidUpdate: ((Foundation.TimeInterval) -> ())?
  final public var timerDidComplete: (() -> ())?
  public init(withTimeInterval timeInterval: Foundation.TimeInterval = 0.0)
  final public func start(shouldFire fire: Swift.Bool = true)
  final public func pause()
  final public func stop()
  @objc deinit
  @objc override dynamic public init()
}
@objc @_inheritsConvenienceInitializers public class AudioVisualizationView : TDI.BaseNibView {
  public enum AudioVisualizationMode {
    case read
    case write
    public static func == (a: TDI.AudioVisualizationView.AudioVisualizationMode, b: TDI.AudioVisualizationView.AudioVisualizationMode) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
  @objc @IBInspectable public var meteringLevelBarWidth: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var meteringLevelBarInterItem: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var meteringLevelBarCornerRadius: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var meteringLevelBarSingleStick: Swift.Bool {
    @objc get
    @objc set
  }
  public var audioVisualizationMode: TDI.AudioVisualizationView.AudioVisualizationMode
  public var audioVisualizationTimeInterval: Swift.Double
  public var currentGradientPercentage: Swift.Float?
  public var meteringLevels: [Swift.Float]? {
    get
    set
  }
  @objc @IBInspectable public var gradientStartColor: UIKit.UIColor {
    @objc get
    @objc set
  }
  @objc @IBInspectable public var gradientEndColor: UIKit.UIColor {
    @objc get
    @objc set
  }
  @objc dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic public func draw(_ rect: CoreGraphics.CGRect)
  public func reset()
  public func add(meteringLevel: Swift.Float)
  public func scaleSoundDataToFitScreen() -> [Swift.Float]
  public func play(from url: Foundation.URL)
  public func play(for duration: Foundation.TimeInterval)
  public func pause()
  public func stop()
  @objc deinit
}
@_hasMissingDesignatedInitializers final public class AudioContext {
  final public let audioURL: Foundation.URL
  final public let totalSamples: Swift.Int
  final public let asset: AVFoundation.AVAsset
  final public let assetTrack: AVFoundation.AVAssetTrack
  public static func load(fromAudioURL audioURL: Foundation.URL, completionHandler: @escaping (TDI.AudioContext?) -> ())
  final public func render(targetSamples: Swift.Int = 100) -> [Swift.Float]
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers public class BaseNibView : UIKit.UIView {
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic public func awakeFromNib()
  @objc deinit
}
@_hasMissingDesignatedInitializers open class AsyncTimer {
  public var isPaused: Swift.Bool {
    get
  }
  public var isStopped: Swift.Bool {
    get
  }
  public var isRunning: Swift.Bool {
    get
  }
  public var value: Swift.Int? {
    get
  }
  convenience public init(queue: Dispatch.DispatchQueue = .main, interval: Dispatch.DispatchTimeInterval, repeats: Swift.Bool = false, block: @escaping () -> Swift.Void = {})
  convenience public init(queue: Dispatch.DispatchQueue = .main, interval: Dispatch.DispatchTimeInterval, times: Swift.Int, block: @escaping (Swift.Int) -> Swift.Void = { _ in }, completion: @escaping () -> Swift.Void = {})
  @objc deinit
  open func start()
  open func pause()
  open func resume()
  open func stop()
  open func restart()
}
extension TDI.AudioVisualizationView.AudioVisualizationMode : Swift.Equatable {}
extension TDI.AudioVisualizationView.AudioVisualizationMode : Swift.Hashable {}
