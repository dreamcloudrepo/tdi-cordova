//
//  DepClient.h
//  TDI
//
//  Created by Rajeshkumar on 20/4/20.
//  Copyright © 2020 Rajesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/uikit.h>

@protocol DepEncodeDecodeDelegate <NSObject>
@optional
-(void)depResult:(NSString *_Nullable)result;
@end

NS_ASSUME_NONNULL_BEGIN

@interface DepClient : NSObject

- (NSData*)encodeTdiRequest:(NSData *)object withContentType:(NSString *)contentType;
- (NSData*)decodeTdiResponse:(NSData *)responseData withContentType:(NSString *)contentType;
-(void)clearSession;
@end

NS_ASSUME_NONNULL_END
