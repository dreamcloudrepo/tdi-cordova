//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef struct MovableSide {
    CGPoint     side_point;
    CGPoint     *move_corner_1;
    CGPoint     *move_corner_2;
    CGPoint     *anchor_corner_1;
    CGPoint     *anchor_corner_2;
} MovableSide;

typedef struct line2d
{
    float a;
    float b;
    float c;
    float m;
    float n;
} line2d;

@interface CropView : UIView
{
    CGPoint         m_topLeftCorner;
    CGPoint         m_topRightCorner;
    CGPoint         m_bottomRightCorner;
    CGPoint         m_bottomLeftCorner;
    CGRect          m_frame;
    
    MovableSide     m_topSide;
    MovableSide     m_rightSide;
    MovableSide     m_bottomSide;
    MovableSide     m_leftSide;
    
    CGPoint         *m_editedCorner;
    MovableSide     *m_editedSide;
    
    CGSize          m_imgSize;
    float           m_imgScale;
    //UIImage         *m_image;
    
    UIScrollView    *m_scrollView;
    
    bool            m_userUpdate;
}

-(void) deinitiallize;
-(void)setScrollViewPointer:(UIScrollView*)p_scrollView;

//-(void)setImage:(UIImage*)p_image;
-(void)updateFrame:(CGSize)imgSize;
-(void)setImageSize:(CGSize)p_size;
-(void)setImageScale:(float)p_scale;
-(void)setDocumentCornersWithTopLeft:(CGPoint)p_topLeft
                            topRight:(CGPoint)p_topRight
                         bottomRight:(CGPoint)p_bottomRight
                       andBottomLeft:(CGPoint)p_bottomLeft;

-(CGPoint)getTopLeft;
-(CGPoint)getTopRight;
-(CGPoint)getBottomRight;
-(CGPoint)getBottomLeft;
-(bool)isUserUpdate;
-(NSArray*) getCropPoints;

@end
