//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef DrawOverlayView_h
#define DrawOverlayView_h
#import <UIKit/UIKit.h>

typedef NS_ENUM( NSInteger, DocumentPlaceholder ) {
    DocumentPlaceholder_TD1 = 0,
    DocumentPlaceholder_TD3 = 1,
    DocumentPlaceholder_TD4 = 2
};

@interface DrawOverlayView : UIView

@property (strong,nonatomic) NSArray *normalizedPoints;
@property (nonatomic) CGRect placeholderRect;

- (void)setCameraFrame:(CGRect)rect;
- (void)drawDocumentPlaceholder:(DocumentPlaceholder)placeholder;
- (void)drawLinePoints:(CGPoint)topLeft :(CGPoint)topRight :(CGPoint)bottomRight :(CGPoint)bottomLeft;
- (void)clear;

@end

#endif
