//
//  Copyright © 2021 Thales Group. All rights reserved.
//

#ifndef MRZStartCallback_h
#define MRZStartCallback_h

#import "IDVMRZCaptureResult.h"

/**
 SDK frame process delegate
*/
@protocol IDVMRZStartCallback <NSObject>

/**
 Callback for each frame processed without a valid detected MRZ
 @param partial IDVMRZCaptureResult reference with partial results.
*/
- (void) onProcessedFrame:(IDVMRZCaptureResult *)partial;

/**
 Callback called once when a valid detected MRZ
 @param result IDVMRZCaptureResult reference with results.
*/
- (void) onSuccess:(IDVMRZCaptureResult *)result;

@end

#endif
