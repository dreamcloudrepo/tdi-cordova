//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef IDVMRZConfiguration_h
#define IDVMRZConfiguration_h


/**
 Auto focus modes. These values are used on the autoFocusMode field from IDVMRZConfiguration
*/
typedef NS_ENUM(NSInteger, IDVMRZAutoFocusMode) {
    /**
     Lets the device to set the focus mode.
    */
    Auto = 0,
    /**
     Lets the user to set the focus mode based on touches.
    */
    Manual = 1
};

/**
 Camera orientation options. These values are used on the cameraOrientation field from IDVMRZConfiguration
*/
typedef NS_ENUM(NSInteger, IDVMRZCameraOrientation) {
    /**
     Inteface sets the camera to the current app orientation.
    */
    Interface = 0,
    /**
     Device set the camera on the device actual orientation.
    */
    Device = 1
};

/**
 Object containing configuration info to initialize the SDK.
*/
__attribute__ ((visibility ("default"))) @interface IDVMRZConfiguration : NSObject {
    NSString *license;
}

@property (nonatomic, readwrite) NSString *license;

/**
 Mode to apply on the auto focus. IDVMRZAutoFocusMode
*/
@property (nonatomic, readwrite) IDVMRZAutoFocusMode autoFocusMode;

/**
 Camera orientation behaviour. IDVMRZCameraOrientation
*/
@property (nonatomic, readwrite) IDVMRZCameraOrientation cameraOrientation;

@end

#endif
