//
//  Copyright © 2019 Thales Group. All rights reserved.
//

#ifndef MRZCaptureCallback_h
#define MRZCaptureCallback_h

#import "IDVMRZCaptureResult.h"
#import "IDVMRZStartCallback.h"

/**
 SDK frame process delegate
 IDVMRZCaptureCallback is deprecated, use IDVMRZStartCallback instead
*/
__deprecated_msg("Use IDVMRZStartCallback instead.")
@protocol IDVMRZCaptureCallback <IDVMRZStartCallback>

/**
 Callback for each frame processed without a valid detected MRZ
 @param partial IDVMRZCaptureResult reference with partial results.
*/
- (void) onProcessedFrame:(IDVMRZCaptureResult *)partial __deprecated;

/**
 Callback called once when a valid detected MRZ
 @param result IDVMRZCaptureResult reference with results.
*/
- (void) onSuccess:(IDVMRZCaptureResult *)result __deprecated;

@end

#endif
