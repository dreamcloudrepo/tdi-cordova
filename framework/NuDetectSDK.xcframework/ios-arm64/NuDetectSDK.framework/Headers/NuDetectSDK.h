/*
 * © 2020 Mastercard. Proprietary. All Rights Reserved.
 *
 * These materials are licensed and not sold.
 * Among other things, the terms of your  license agreement (“License”) restrict
 * you in your use of these materials for the purposes set forth in  your License.
 *
 * Without limiting the terms of that license, you may not reverse engineer,
 * decompile, or disassemble, translate or otherwise attempt to extract the
 * source code from the object code files included with these materials.
 *
 * THESE MATERIALS ARE PROVIDED "AS IS" AND "AS AVAILABLE" AND MASTERCARD  AND ITS
 * AFFILIATES EXPRESSLY DISCLAIM ANY AND ALL REPRESENTATIONS AND WARRANTIES,
 * WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION IMPLIED WARRANTIES OF
 * TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 *
 * MASTERCARD DOES NOT GUARANTEE OR PROMISE ANY SPECIFIC RESULTS FROM USE OF THESE
 * MATERIALS, NOR DO THEY REPRESENT OR WARRANT THAT THEY ARE ACCURATE, COMPLETE,
 * RELIABLE, CURRENT, ERROR-FREE, FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.
 *
 * IN NO EVENT WILL MASTERCARD , OR ITS DIRECTORS, PARTNERS, OFFICERS, EMPLOYEES,
 * CONTRACTORS OR AGENTS, BE LIABLE TO YOU OR ANY THIRD PERSON FOR ANY INDIRECT,
 * CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING,
 * WITHOUT LIMITATION, FOR ANY LOST PROFITS OR LOST DATA, ARISING FROM YOUR USE OF
 * THESE MATERIALS, EVEN IF MASTERCARD IS AWARE OR HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 */

@import UIKit;

#import "NDViewController.h"
#import "NDSCaptchaWebView.h"
#import "MobileNuCaptcha.h"
#import "MobileNuCaptchaRequest.h"
#import "MobileNuCaptchaResponse.h"
#import "MobileNuCaptchaType.h"
#import "NuDetectListener.h"
#import "NDErrorStatusCode.h"

FOUNDATION_EXPORT double NuDetectSDKVersionNumber;
FOUNDATION_EXPORT const unsigned char NuDetectSDKVersionString[];

/**
 * A block that handle errors.
 * @param callback The NSError instance pointer.
 */
#define NDSCOMPLETION_CALLBACK_DEFINED
typedef void (^NdsCompletionCallback)(NSError * _Nullable error);
extern NSString * _Nonnull const NDErrorDomain;

/**
 * NuDetect Mobile SDK framework
 */
@interface NuDetectSDK: NSObject

/**
 * Returns the instance of the NuDetectSDK framework. It is a signleton object.
 * @return The instance of the NuDetectSDK framework
 */
+ (nonnull instancetype)getInstance;

/**
 * Create the encoded NuDetect API payload data for the AIM server without the error handling.
 * @return The encoded payload.
 */
- (nullable NSString *)createPayload
__attribute((deprecated("This method is being deprecated, please use createPayloadWithCompletion:")));

 /**
  * Create the encoded NuDetect API payload data for the AIM server with the error handling.
  * @param errorCallback The error callback block.
  * @return The encoded payload.
  */
- (nullable NSString *)createPayload:(nullable NdsCompletionCallback)errorCallback
__attribute((deprecated("This method is being deprecated, please use createPayloadWithCompletion:")));

/**
 * Create the encoded NuDetect API payload data for the AIM server with the error handling.
 * @param completion The completion callback block. This completion can never be Null.
 */
- (void)createPayloadWithCompletion:(void (^_Nonnull)(NSString * _Nullable result , NSError * _Nullable error))completion;

/**
 * Read the encoded NuDetect API payload data from the AIM server and will return an error if the payload passed in is not valid. 
 * @note If captcha data is included in the payload, failure during set up for the MobileNuCaptchaRequest and MobileNuCaptcha objects will not return an error. To check for successful set up of these objects please use `getCaptchaRequest` or `getMobileNuCaptcha` as required.
 * @param payload The encoded payload.
 * @param completion The completion callback block.
 */
- (void)readPayload:(nullable NSString *)payload completion:(nullable NdsCompletionCallback)completion;

/**
 * Read the encoded NuDetect API payload data from the AIM server.
 * @param payload The encoded payload.
 * :nodoc:
 */
- (void)readPayload:(nullable NSString *)payload
__attribute((deprecated("This method is being deprecated, please use readPayload:completion")));

/**
 * Get the session id.
 * @return The session id.
 */
- (nullable NSString *)getSessionID;

/**
 * Set the session id.
 * @param sessionID The session id.
 */
- (void)setSessionID:(nonnull NSString *)sessionID;

/**
 * Set the timeout for the NuDetect:initialize networking call
 * @param timeout The timeout in millisecond.
 */
- (void)setTimeout:(int)timeout;

/**
 * Set the force IP for the NuDetect:intialize networking call
 * @param forceIP The force IP.
 */
- (void)setForceIP:(nonnull NSString *)forceIP;

/**
 * Get a fully set up MobileNuCaptcha which handles display and playing of audio, video, and sprite captchas.
 * @note Sprite captchas will only be returned if they are enabled on the client AIM server.
 * @return The captcha player. Will return nil if a valid captcha request has not been received from the client AIM server.
 */
- (nullable MobileNuCaptcha *)getMobileNuCaptcha;

/**
 * Get the captcha request to use in the captcha player.
 * @note MobileNuCaptchaRequest is the legacy implementation of captcha functionality. Please see `getMobileNuCaptcha` and the `MobileNuCaptcha` class which is the new self contained captcha player.
 * @return The data for the captcha request
 */
- (nullable MobileNuCaptchaRequest *)getCaptchaRequest;

/**
 * Get the requested interdiction type for 3DS enabled sessions
 * @note Interdiction functionality beyond MobileNuCaptcha is only possible when using our 3DS enabled companion framework
 * @return The requested interdiction type
 */
- (nonnull NSString *)getInterdictionType;

/**
 * Check if 3DS enabled for this session.
 * @return True if 3DS enabled for this session, otherwise - false
 */
- (BOOL)is3DSEnabled;

/**
 * Set the captcha response based on captcha player data.
 * @note MobileNuCaptchaResponse is the legacy implementation of captcha functionality. Please see `getMobileNuCaptcha` and the `MobileNuCaptcha` class which is the new self contained captcha player.
 * @param captchaResponse The captcha response data
 */
- (void)setCaptchaResponse:(nonnull MobileNuCaptchaResponse *)captchaResponse;

/**
 * Set the NuDetect API Base URL.
 * @param url The URL.
 */
- (void)setAPIBaseUrl:(nonnull NSString *)url;

/**
 * Set the website Id.
 * @param websiteId The website Id.
 */
- (void)setWebsiteID:(nonnull NSString *)websiteId;

/**
 * Initialize the NuDetect SDK. This method should be invoked as soon as the user lands on a NuDetect enabled page.
 * @param completion The completion callback block.
 */
- (void)initialize:(nullable NdsCompletionCallback)completion;


/**
 * Set the placement string used to identify the flow associated with the user's action for use in calculating page based scoring. For example "Login" or "Purchase".
 * @note On each new page which must be monitored call setPlacementString and setPlacementPage to identify the current page of the appropriate flow. To update the NuDetect servers you must also call NuDetectSDK:intialize after setting up the new flow's placement string and page prior to making a score request.
 * @param placement The placement string.
 */
- (void)setPlacementString:(nonnull NSString *)placement;

/**
 * Set the placement page number within the current user flow for use in calculating page based scoring.
 * @note On each new page which must be monitored call setPlacementString and setPlacementPage to identify the current page of the appropriate flow. To update the NuDetect servers you must also call NuDetectSDK:intialize after setting up the new flow's placement string and page prior to making a score request.
 * @param placementPage The placement page.
 */
- (void)setPlacementPage:(NSInteger)placementPage;

/**
 *  Enable location processing without continously refreshing.
 *  @note This is equivalent to calling enableLocationProcessingWithRefreshInterval: with refreshInterval set to 0.
 */
- (void)enableLocationProcessing;

/**
 *  Enable location processing which updates at a specified interval.
 *  @param refreshInterval The frequency, in seconds, of refreshing location data. Can not be negative. To disable refreshInterval set it to 0.
 */
- (void)enableLocationProcessingWithRefreshInterval:(NSTimeInterval)refreshInterval;

/**
 *  Disable location processing.
 */
- (void)disableLocationProcessing;

@end
