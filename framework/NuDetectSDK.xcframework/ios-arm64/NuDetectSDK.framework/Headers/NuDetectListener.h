/*
 * © 2020 Mastercard. Proprietary. All Rights Reserved.
 *
 * These materials are licensed and not sold.
 * Among other things, the terms of your  license agreement (“License”) restrict
 * you in your use of these materials for the purposes set forth in  your License.
 *
 * Without limiting the terms of that license, you may not reverse engineer,
 * decompile, or disassemble, translate or otherwise attempt to extract the
 * source code from the object code files included with these materials.
 *
 * THESE MATERIALS ARE PROVIDED "AS IS" AND "AS AVAILABLE" AND MASTERCARD  AND ITS
 * AFFILIATES EXPRESSLY DISCLAIM ANY AND ALL REPRESENTATIONS AND WARRANTIES,
 * WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION IMPLIED WARRANTIES OF
 * TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 *
 * MASTERCARD DOES NOT GUARANTEE OR PROMISE ANY SPECIFIC RESULTS FROM USE OF THESE
 * MATERIALS, NOR DO THEY REPRESENT OR WARRANT THAT THEY ARE ACCURATE, COMPLETE,
 * RELIABLE, CURRENT, ERROR-FREE, FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.
 *
 * IN NO EVENT WILL MASTERCARD , OR ITS DIRECTORS, PARTNERS, OFFICERS, EMPLOYEES,
 * CONTRACTORS OR AGENTS, BE LIABLE TO YOU OR ANY THIRD PERSON FOR ANY INDIRECT,
 * CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING,
 * WITHOUT LIMITATION, FOR ANY LOST PROFITS OR LOST DATA, ARISING FROM YOUR USE OF
 * THESE MATERIALS, EVEN IF MASTERCARD IS AWARE OR HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 */

@import UIKit;

/**
 * The types of events for the NuDetectListener to observe and include in IPR data.
 */
typedef NS_OPTIONS (NSInteger, ListenerTypes) {
    /**
     * Form focus/blur events
     */
    NDS_LISTEN_FOCUS = (1 << 0),
    /**
     * Touch down events
     */
    NDS_LISTEN_TOUCH = (1 << 1),
    /**
     * Keydown (input text change) events
     */
    NDS_LISTEN_KEYDOWN = (1 << 2),
    /**
     * Accelerometer events
     */
    NDS_LISTEN_DEVICE_MOTION = (1 << 3)
};

/**
 * The NuDetectListener is the event listener for profiling user activity
 */
@interface NuDetectListener: NSObject

/**
 * Register the listener. This will initiate observation for all `ListenerTypes` that are possible for the device.
 * @param view The current view getting the events
 */
- (void)register:(nonnull UIView *)view;

/**
 * Register the listener. This will initiate observation only for `ListenerTypes` that are passed into the types parameter.
 * @param view The current view getting the events
 * @param types Types of events to listen
 */
- (void)register:(nonnull UIView *)view types:(ListenerTypes)types;

- (void)registerViewGroup:(nonnull UIView *)view
__attribute((deprecated("This method is being deprecated, please use register:types")));
/**
 * Unregister the listener.
 * @note Calling unregister clears all profiling data NuDetectSDK has collected but it does not stop NuDetectSDK from making further observations of a user's behaviour on views. To stop a view from being observed call NuDetectSDK:unregisterViewGroup and pass in the parent view that was previously registered. Calling unregister will also stop Device Motion observation as long as no other NuDetectListeners exist with the NDS_LISTEN_DEVICE_MOTION ListenerType.
 * @note Calling this function will clear the IPR data that was previously collected, when making a NuDetect score transaction, call NuDetectSDK:createPayload and complete your score request prior to calling unregister.
 */
- (void)unregister;

/**
 * Unregister the listener for the view group. This will stop observation for all `ListenerTypes` that were registered for this listener.
 * @note As Device Motion observation is not view dependent, device motion will continue until all NuDetectListeners which have the NDS_LISTEN_DEVICE_MOTION ListenerType set have either called NuDetectListner:unregister or are deallocated.
 * @param view The required group
 */
- (void)unregisterViewGroup:(nonnull UIView *)view;

@end
