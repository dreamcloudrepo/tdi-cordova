/*
 * © 2020 Mastercard. Proprietary. All Rights Reserved.
 *
 * These materials are licensed and not sold.
 * Among other things, the terms of your  license agreement (“License”) restrict
 * you in your use of these materials for the purposes set forth in  your License.
 *
 * Without limiting the terms of that license, you may not reverse engineer,
 * decompile, or disassemble, translate or otherwise attempt to extract the
 * source code from the object code files included with these materials.
 *
 * THESE MATERIALS ARE PROVIDED "AS IS" AND "AS AVAILABLE" AND MASTERCARD  AND ITS
 * AFFILIATES EXPRESSLY DISCLAIM ANY AND ALL REPRESENTATIONS AND WARRANTIES,
 * WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION IMPLIED WARRANTIES OF
 * TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 *
 * MASTERCARD DOES NOT GUARANTEE OR PROMISE ANY SPECIFIC RESULTS FROM USE OF THESE
 * MATERIALS, NOR DO THEY REPRESENT OR WARRANT THAT THEY ARE ACCURATE, COMPLETE,
 * RELIABLE, CURRENT, ERROR-FREE, FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.
 *
 * IN NO EVENT WILL MASTERCARD , OR ITS DIRECTORS, PARTNERS, OFFICERS, EMPLOYEES,
 * CONTRACTORS OR AGENTS, BE LIABLE TO YOU OR ANY THIRD PERSON FOR ANY INDIRECT,
 * CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING,
 * WITHOUT LIMITATION, FOR ANY LOST PROFITS OR LOST DATA, ARISING FROM YOUR USE OF
 * THESE MATERIALS, EVEN IF MASTERCARD IS AWARE OR HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 */

@import WebKit;

/**
 * CaptchaWebView is a WKWebView which contains a javascript based captcha player.
 * @note Instance methods are unavailable. To obtain an instance of CaptchaWebView first obtain an instance of MobileNuCaptcha by calling `NuDetectSDK:getMobileNuCaptcha` and then call `MobileNuCaptcha:getWebPlayer`.
 */
@interface CaptchaWebView: WKWebView

#ifndef ND_UNAVAILABLE
- (instancetype)init NS_UNAVAILABLE;
- (instancetype)new NS_UNAVAILABLE;
#endif

@end
