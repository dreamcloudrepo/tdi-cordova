/*
* © 2020 Mastercard. Proprietary. All Rights Reserved.
*
* These materials are licensed and not sold.
* Among other things, the terms of your  license agreement (“License”) restrict
* you in your use of these materials for the purposes set forth in  your License.
*
* Without limiting the terms of that license, you may not reverse engineer,
* decompile, or disassemble, translate or otherwise attempt to extract the
* source code from the object code files included with these materials.
*
* THESE MATERIALS ARE PROVIDED "AS IS" AND "AS AVAILABLE" AND MASTERCARD  AND ITS
* AFFILIATES EXPRESSLY DISCLAIM ANY AND ALL REPRESENTATIONS AND WARRANTIES,
* WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION IMPLIED WARRANTIES OF
* TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
*
* MASTERCARD DOES NOT GUARANTEE OR PROMISE ANY SPECIFIC RESULTS FROM USE OF THESE
* MATERIALS, NOR DO THEY REPRESENT OR WARRANT THAT THEY ARE ACCURATE, COMPLETE,
* RELIABLE, CURRENT, ERROR-FREE, FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.
*
* IN NO EVENT WILL MASTERCARD , OR ITS DIRECTORS, PARTNERS, OFFICERS, EMPLOYEES,
* CONTRACTORS OR AGENTS, BE LIABLE TO YOU OR ANY THIRD PERSON FOR ANY INDIRECT,
* CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING,
* WITHOUT LIMITATION, FOR ANY LOST PROFITS OR LOST DATA, ARISING FROM YOUR USE OF
* THESE MATERIALS, EVEN IF MASTERCARD IS AWARE OR HAS BEEN ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGES.
*/

@import Foundation;

#import "MobileNuCaptchaType.h"

/**
 * A Captcha Response object with self-contained required values to validate captcha.
 * @note This is the legacy implementation of captcha functionality. Please see `MobileNuCaptcha` which is the new self contained captcha player.
 */
@interface MobileNuCaptchaResponse: NSObject

/**
 * Constructor.
 * @return The new instance
 */
- (nonnull instancetype)init;

/**
 * Set the captcha token.
 * @param token The captcha token
 */
- (void)setToken:(nonnull NSString *)token;

/**
 * Set the challenge index.
 * @param index The challenge index
 */
- (void)setIndex:(int)index;

/**
 * Set the captcha type.
 * @param captchaType The captcha type
 */
- (void)setType:(MobileNuCaptchaType)captchaType;

/**
 * Set the captcha answer.
 * @param answer The captcha answer
 */
- (void)setAnswer:(nonnull NSString *)answer;

/**
 * Get the respresentation of the captch response in JSON format.
 * @return The respresentation of the captch response in JSON format
 * :nodoc:
 */
- (nonnull NSString *)toJSON;
@end
