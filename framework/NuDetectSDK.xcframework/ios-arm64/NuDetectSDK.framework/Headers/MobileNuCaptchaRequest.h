/*
 * © 2020 Mastercard. Proprietary. All Rights Reserved.
 *
 * These materials are licensed and not sold.
 * Among other things, the terms of your  license agreement (“License”) restrict
 * you in your use of these materials for the purposes set forth in  your License.
 *
 * Without limiting the terms of that license, you may not reverse engineer,
 * decompile, or disassemble, translate or otherwise attempt to extract the
 * source code from the object code files included with these materials.
 *
 * THESE MATERIALS ARE PROVIDED "AS IS" AND "AS AVAILABLE" AND MASTERCARD  AND ITS
 * AFFILIATES EXPRESSLY DISCLAIM ANY AND ALL REPRESENTATIONS AND WARRANTIES,
 * WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION IMPLIED WARRANTIES OF
 * TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 *
 * MASTERCARD DOES NOT GUARANTEE OR PROMISE ANY SPECIFIC RESULTS FROM USE OF THESE
 * MATERIALS, NOR DO THEY REPRESENT OR WARRANT THAT THEY ARE ACCURATE, COMPLETE,
 * RELIABLE, CURRENT, ERROR-FREE, FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.
 *
 * IN NO EVENT WILL MASTERCARD , OR ITS DIRECTORS, PARTNERS, OFFICERS, EMPLOYEES,
 * CONTRACTORS OR AGENTS, BE LIABLE TO YOU OR ANY THIRD PERSON FOR ANY INDIRECT,
 * CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING,
 * WITHOUT LIMITATION, FOR ANY LOST PROFITS OR LOST DATA, ARISING FROM YOUR USE OF
 * THESE MATERIALS, EVEN IF MASTERCARD IS AWARE OR HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 */

@import Foundation;

#import "MobileNuCaptchaType.h"

/**
 * Captcha Request object containing all required values to render captcha.
 * @note This is the legacy implementation of captcha functionality. Please see `MobileNuCaptcha` which is the new self contained captcha player.
 */
@interface MobileNuCaptchaRequest: NSObject

/**
 * Captcha request object factory.
 * @param token NuDetect API token
 * @param language The requested captcha language
 * @param requestId The request Id for the captcha
 * @param challengeBaseUrl The URL for captcha generator
 * @param captchaType The type of the requested captcha
 * @param captchaWidth The width of the captcha
 * @param captchaHeight The height of the captcha
 * @return The new instance
 */
+ (nonnull instancetype)createInstance:(nonnull NSString *)token
                              language:(nonnull NSString *)language
                             requestId:(nonnull NSString *)requestId
                      challengeBaseUrl:(nonnull NSString *)challengeBaseUrl
                                  type:(MobileNuCaptchaType)captchaType
                                 width:(int)captchaWidth
                                height:(int)captchaHeight;

/**
 * Get the NuDetect API token.
 * @return The NuDetect API token
 */
- (nonnull NSString *)getToken;

/**
 * Get the requested captcha language.
 * @return The requested captcha language
 */
- (nonnull NSString *)getLanguage;

/**
 * Get the URL for captcha generator.
 * @return The URL for captcha generator
 */
- (nonnull NSString *)getChallengeBaseUrl;

/**
 * Get the request Id for the captcha.
 * @return The request Id for the captcha
 */
- (nonnull NSString *)getRequestId;

/**
 * Get the type of the requested captcha.
 * @return The type of the requested captcha
 */
- (MobileNuCaptchaType)getType;

/**
 * Get the width of the captcha.
 * @return The width of the captcha
 */
- (int)getWidth;

/**
 * Get the height of the captcha.
 * @return The height of the captcha
 */
- (int)getHeight;
@end
