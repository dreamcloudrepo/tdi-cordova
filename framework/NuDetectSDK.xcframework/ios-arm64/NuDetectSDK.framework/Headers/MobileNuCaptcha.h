/*
 * © 2020 Mastercard. Proprietary. All Rights Reserved.
 *
 * These materials are licensed and not sold.
 * Among other things, the terms of your  license agreement (“License”) restrict
 * you in your use of these materials for the purposes set forth in  your License.
 *
 * Without limiting the terms of that license, you may not reverse engineer,
 * decompile, or disassemble, translate or otherwise attempt to extract the
 * source code from the object code files included with these materials.
 *
 * THESE MATERIALS ARE PROVIDED "AS IS" AND "AS AVAILABLE" AND MASTERCARD  AND ITS
 * AFFILIATES EXPRESSLY DISCLAIM ANY AND ALL REPRESENTATIONS AND WARRANTIES,
 * WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION IMPLIED WARRANTIES OF
 * TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
 *
 * MASTERCARD DOES NOT GUARANTEE OR PROMISE ANY SPECIFIC RESULTS FROM USE OF THESE
 * MATERIALS, NOR DO THEY REPRESENT OR WARRANT THAT THEY ARE ACCURATE, COMPLETE,
 * RELIABLE, CURRENT, ERROR-FREE, FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.
 *
 * IN NO EVENT WILL MASTERCARD , OR ITS DIRECTORS, PARTNERS, OFFICERS, EMPLOYEES,
 * CONTRACTORS OR AGENTS, BE LIABLE TO YOU OR ANY THIRD PERSON FOR ANY INDIRECT,
 * CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING,
 * WITHOUT LIMITATION, FOR ANY LOST PROFITS OR LOST DATA, ARISING FROM YOUR USE OF
 * THESE MATERIALS, EVEN IF MASTERCARD IS AWARE OR HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 */

@import Foundation;

@class CaptchaWebView;

/**
 * Fully functioning captcha player class. MobileNuCaptcha contains a WKWebview based UI to display a video captcha, a sprite captcha, or an audio captcha. MobileNuCaptcha also contains background handling for IPR and capturing the user's captcha answer.
 */
@interface MobileNuCaptcha: NSObject

/**
 * Instance method `init` is unavailable. To get an instance of MobileNuCaptcha use NuDetectSDK:getMobileNuCaptcha
 * @return does not return
 */
- (nonnull instancetype)init NS_UNAVAILABLE;

/**
 * Instance method `new` is unavailable. To get an instance of MobileNuCaptcha use NuDetectSDK:getMobileNuCaptcha
 * @return does not return
 */
- (nonnull instancetype)new NS_UNAVAILABLE;

/**
 * The CaptchaWebView containing the webPlayer.
 * @note The webPlayer returned is templated on the client server. Changes to it's layout and styling must be done on the client server.
 * @return An instance of CaptchaWebView (subclass of WKWebView) which is fully set up and self contained. Will be nil if the MobileNuCaptcha instance has not been set up with a valid payload.
 */
- (nullable CaptchaWebView *)getWebPlayer;

/**
 * This function will set the captcha answer string value.
 * @note It should only be used if the captcha player template on the client server has been modified to not use the standard textfield. For example if a native textfield is being used to capture the user's captcha answer instead of including the default html answer textfield in the template on the client server.
 * @param answer The string to set as the captcha response value
 */
- (void)setCaptchaAnswer:(nonnull NSString *)answer;
@end
